/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.price.nordpool;

import java.util.Date;

/**
 * 
 */
public final class PriceForecastData {
  private Date date;
  private double value;
  
  public PriceForecastData( Date date, double value ) {
    super();
    this.date = date;
    this.value = value;
  }
  public Date getDate() {
    return date;
  }
  public double getValue() {
    return value;
  }
  

}
