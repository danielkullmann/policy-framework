package risoe.syslab.price.nordpool;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.swing.text.Element;

/**
 * Parses the content of the web page on http://nordpool.com that contains the
 * current system prices for power.
 */
public final class NordPoolParser {

  public static final String QUERIES_URL = "http://www.nordpoolspot.com/api/marketdata/queries";

  public static final String MARKET_DATA_URL = "http://www.nordpoolspot.com/api/marketdata/page/";

  private PriceForecastData[][] data = null;

  private String unit = "EUR";

  static private Pattern datePattern = Pattern.compile("(\\d\\d)[-](\\d\\d)[-](\\d\\d\\d\\d)");
  static private Pattern hourPattern = Pattern.compile("(\\d\\d)[-](\\d\\d)");
  static private Pattern valuePattern = Pattern.compile("(\\d*)[,](\\d*)");

  public NordPoolParser() {

    try {
      String[] path = new String[] { "Elspot day-ahead", "Prices", "SYS", "Hourly" };

      JsonObject queriesResponse = getQueriesJson();
      JsonArray rows = getMarketData(queriesResponse, path);

      ArrayList<GregorianCalendar> dates = extractDates(rows);
      this.data = extractValues(rows, dates);

    } catch (MalformedURLException e) {
      throw new IllegalArgumentException();
    }
  }

  public PriceForecastData[] retrieveForecast(Date date) {
    ArrayList<PriceForecastData> resultList = new ArrayList<PriceForecastData>();
    for (int dateIndex = data.length - 1; dateIndex >= 0; dateIndex--) {
      for (int hourIndex = 0; hourIndex < data[dateIndex].length; hourIndex++) {
        PriceForecastData fcData = data[dateIndex][hourIndex];
        // Is actually a forecast data set..
        if (fcData == null) {
          System.err.println("PriceForecastServer: no data (" + dateIndex + "," + hourIndex + ")");
        } else if (fcData.getDate() == null) {
          System.err.println("PriceForecastServer: no date in " + fcData.getDate() + " " + fcData.getValue());
        } else if (!fcData.getDate().before(date)) {
          resultList.add(fcData);
        }
      }

    }

    // Convert list to array
    PriceForecastData[] result = new PriceForecastData[resultList.size()];
    resultList.toArray(result);
    return result;
  }

  /**
   * Simple getter for the {@link #data} field
   * 
   * @return the {@link #data} field as filled by {@link #parseData(Element)}
   */
  public PriceForecastData[][] getData() {
    return data;
  }

  /**
   * Get the monetary unit for the data returned by {@link #getData()}, as a
   * string (e.g. "EUR")
   * 
   * @return monetary unit of currency values in data
   */
  public String getUnit() {
    return unit;
  }

  public static void main(String args[]) {
    PrintStream out = System.out;
    NordPoolParser p = new NordPoolParser();
    for (int j = 0; j < 8; j++) {
      for (int i = 0; i < 24; i++) { // all prices in EUR/MWh
        out.println(p.getData()[j][i].getDate().toString() + ":" + p.getData()[j][i].getValue());
      }
      out.println();
    }
  }

  private PriceForecastData[][] extractValues(JsonArray rows, ArrayList<GregorianCalendar> dates) {
    PriceForecastData[][] data = new PriceForecastData[dates.size()][24];
    for (JsonValue rowValue : rows) {
      JsonObject row = (JsonObject) rowValue;
      String rowName = row.getString("Name").replace("&nbsp;", "");
      Matcher matcher = hourPattern.matcher(rowName);
      if (!matcher.matches())
        continue;
      JsonArray columns = row.getJsonArray("Columns");
      int columnNo = 0;
      for (JsonValue columnValue : columns) {
        JsonObject column = (JsonObject) columnValue;
        String value = column.getString("Value");
        Matcher valMat = valuePattern.matcher(value.trim());
        if (valMat.matches()) {
          double val = Double.valueOf(valMat.group(0).replace(",", "."));
          GregorianCalendar gc = null;
          int hour = Integer.parseInt(matcher.group(1), 10);
          gc = (GregorianCalendar) dates.get(columnNo).clone();
          gc.set(GregorianCalendar.HOUR_OF_DAY, hour);
          data[columnNo][hour] = new PriceForecastData(gc.getTime(), val);
        }
        columnNo += 1;
      }
    }
    return data;
  }

  private ArrayList<GregorianCalendar> extractDates(JsonArray rows) {
    JsonObject row0 = rows.getJsonObject(0);
    JsonArray columns0 = row0.getJsonArray("Columns");

    ArrayList<GregorianCalendar> dates = new ArrayList<GregorianCalendar>();

    for (JsonValue columnValue : columns0) {
      JsonObject column = (JsonObject) columnValue;
      Matcher dateMatch = datePattern.matcher(column.getString("Name"));
      if (dateMatch.matches()) {
        int day = Integer.valueOf(dateMatch.group(1));
        int month = Integer.valueOf(dateMatch.group(2));
        int year = Integer.valueOf(dateMatch.group(3));
        GregorianCalendar gc = new GregorianCalendar(year, month - 1, day);
        // gc.setTimeZone( TimeZone.getTimeZone( "GMT+1:00" ) );
        dates.add(gc);
      }
    }
    return dates;
  }

  private JsonValue find(JsonObject treeObject, String[] path, int pathIndex, JsonArray jsonArray) {
    String pathItem = path[pathIndex];
    for (JsonValue id : jsonArray) {
      JsonObject currentObject = treeObject.getJsonObject(id.toString());
      if (pathItem.equals(currentObject.getString("name"))) {
        int nextId = currentObject.getInt("id");
        if (pathIndex == path.length - 1) {
          return treeObject.getJsonObject("" + nextId);
        } else {
          return find(treeObject, path, pathIndex + 1, currentObject.getJsonArray("children"));
        }
      }
    }
    return null;
  }

  private JsonObject getJsonObject(URL url) {
    try {
      URLConnection connection = url.openConnection();
      InputStream is = connection.getInputStream();
      InputStreamReader isr = new InputStreamReader(is);

      JsonReader reader = Json.createReader(isr);
      return reader.readObject();
    } catch (IOException e) {
      throw new AssertionError(e);
    }
  }

  private JsonArray getMarketData(JsonObject queriesResponse, String[] path) throws MalformedURLException {
    JsonObject treeObject = queriesResponse.getJsonObject("tree");
    JsonArray topLevelList = queriesResponse.getJsonArray("topLevel");

    JsonObject hourlySysElspotPrices = (JsonObject) find(treeObject, path, 0, topLevelList);
    int id = hourlySysElspotPrices.getInt("id");

    URL dataUrl = new URL(MARKET_DATA_URL + id + "?currency=,,EUR,EUR");
    JsonObject dataResponse = getJsonObject(dataUrl);

    JsonObject data = dataResponse.getJsonObject("data");
    JsonArray rows = data.getJsonArray("Rows");
    return rows;
  }

  private JsonObject getQueriesJson() throws MalformedURLException {
    URL queriesUrl = new URL(QUERIES_URL);
    JsonObject queriesResponse = getJsonObject(queriesUrl);
    return queriesResponse;
  }

}
