package risoe.syslab.test.policy;

import java.util.Properties;

import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.control.policy.service.ServiceActivation;
import risoe.syslab.control.policy.service.ServiceNameImpl;
import junit.framework.TestCase;

/** Test class for {@link Service}.
 * 
 * @author daku
 */
public class ServiceTest extends TestCase {

  /** test parsing of {@link Service} class */
  public void testService() {
    ServiceNameImpl sd = new ServiceNameImpl("a", "b", "c");
    Properties props = new Properties();
    props.put("k1", "v1");
    props.put("k2", "v2");
    
    Service service = new Service( sd, props, new Properties() );
    
    String str = service.toFipaContent();
    Expression expr = new MessageParser().parse(str);
    Service result = Service.parse(expr );
    
    assertNotNull( result );
    assertEquals( service.getServiceName(), result.getServiceName() );
    assertEquals( service.getActivationProperties(), result.getActivationProperties() );
    assertEquals( service.getDescriptionProperties(), result.getDescriptionProperties() );
  }

  /** test parsing of {@link ServiceActivation} class */
  public void testServiceActivation() {
    ServiceNameImpl sd = new ServiceNameImpl("a", "b", "c");
    Properties props = new Properties();
    props.put("k1", "v1");
    props.put("k2", "v2");
    
    Properties props2 = new Properties();
    props2.put("k1", "vv1");
    props2.put("k2", "vv2");

    Service service = new Service( sd, props, new Properties() );
    ServiceActivation activation = new ServiceActivation( service, props2 );
    
    String str = activation.toFipaContent();
    Expression expr = new MessageParser().parse(str);
    ServiceActivation result = ServiceActivation.parse(expr );
    
    assertNotNull( result );
    assertEquals( activation.getService().getServiceName(), result.getService().getServiceName() );
    assertEquals( activation.getService().getActivationProperties(), result.getService().getActivationProperties() );
    assertEquals( activation.getService().getDescriptionProperties(), result.getService().getDescriptionProperties() );
    assertEquals( activation.getParameters(), result.getParameters() );
  }
}
