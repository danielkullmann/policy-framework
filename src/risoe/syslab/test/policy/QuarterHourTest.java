/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import risoe.syslab.demo.policy.evcharging1.QuarterHour;
import junit.framework.TestCase;

/** Test class for {@link QuarterHour}.
 * 
 * @author daku
 */
public class QuarterHourTest extends TestCase {

  /** tests parsing via {@link QuarterHour#tag()} */
  public void testTag() {
    QuarterHour qh1 = new QuarterHour( new Date() );
    QuarterHour qh2 = new QuarterHour( qh1.tag() );
    assertEquals( qh1, qh2 );
  }
  
  /** tests that {@link QuarterHour} does calculate the right time */
  public void testCalculate() {
    Calendar cal1 = new GregorianCalendar( 0, 0, 0 );
    Calendar cal2 = new GregorianCalendar( 0, 0, 0 );
    
    cal1.set( 2010, 01, 10, 12, 34, 26);
    Date input = cal1.getTime();
    Date output = new QuarterHour( input ).getDate();
    cal2.set( 2010, 01, 10, 12, 30, 00 );
    assertEquals( output, cal2.getTime() );

    cal1.set( 2010, 01, 10, 12, 45, 00 );
    input = cal1.getTime();
    output = new QuarterHour( input ).getDate();
    cal2.set( 2010, 01, 10, 12, 45, 00 );
    assertEquals( output, cal2.getTime() );
  
    cal1.set( 2010, 01, 10, 6, 11, 59 );
    input = cal1.getTime();
    output = new QuarterHour( input ).getDate();
    cal2.set( 2010, 01, 10, 6, 00, 00 );
    assertEquals( output, cal2.getTime() );
  }
  


}
