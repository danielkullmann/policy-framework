/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.Random;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.MessageSigner;
import risoe.syslab.control.policy.core.PolicyUtils;

/** Test class for message signing capabilities */
public class MessageSignerTest extends TestCase {

  /**
   * tests the MessageSigner class using the "not easy" methods that get
   * passed a keystore instance.
   */
  public void testFull() {
    Random rnd = new Random();
    MessageSigner o = new MessageSigner();
    String message = "(message-content :value " + Math.abs( rnd.nextInt() ) + ")";
    try {
      KeyStore keystore = KeyStore.getInstance( "jks" );
      keystore.load( new FileInputStream( "conf/keystore.jks" ), "ek4j.t5,rmn.20ef.rdjf".toCharArray() );
      byte[] signature = o.sign( message, keystore , "syslab-00", "pw-sys-00-2010" );

      KeyStore keystore2 = KeyStore.getInstance( "jks" );
      keystore2.load( new FileInputStream( "conf/keystore-pub.jks" ), "sa3l.7kf4dj.klj".toCharArray() );
      boolean right = o.verify( message, signature, keystore2, "syslab-00" );
      assertTrue( "signature can be verified", right );

      byte[] signature2 = signature.clone();
      // Change signature in small way
      int changeIndex = rnd.nextInt( signature2.length );
      signature2[ changeIndex] = (byte) (signature2[ changeIndex ] + 1 + rnd.nextInt( 255 ));

      try {
        boolean wrong1 = o.verify( message, signature2, keystore2, "syslab-00" );
        assertFalse( "changed signature can't be verified", wrong1 );
      } 
      catch (AssertionError e) {
        assertTrue( e.getCause() instanceof SignatureException );
        assertTrue( e.getCause().getMessage().equals( "invalid encoding for signature" ) );
      }
      
      int changeIndex2 = rnd.nextInt( message.length()-1 );
      char changedChar = message.charAt( changeIndex2 );
      changedChar = (char) (changedChar + 1 + rnd.nextInt( 255 ));
      String message2 = message.substring( 0, changeIndex2 ) +  changedChar +
        message.substring( changeIndex2+1 );

      boolean wrong2 = o.verify( message2, signature, keystore2, "syslab-00" );
      assertFalse( "changed message can't be verified", wrong2 );

    } catch ( KeyStoreException e ) {
      e.printStackTrace();
    } catch ( NoSuchAlgorithmException e ) {
      e.printStackTrace();
    } catch ( CertificateException e ) {
      e.printStackTrace();
    } catch ( FileNotFoundException e ) {
      e.printStackTrace();
    } catch ( IOException e ) {
      e.printStackTrace();
    }
  }

  /**
   * tests the MessageSigner class using the "easy" methods that use the common
   * keystore.
   */
  public void testFullEasy() {
    Random rnd = new Random();
    MessageSigner o = new MessageSigner();
    String message = "(message-content :value " + Math.abs( rnd.nextInt() ) + ")";
    byte[] signature = o.sign( message, "syslab-00" );

    boolean right = o.verify( message, signature, "syslab-00" );
    assertTrue( "signature can be verified", right );

    byte[] signature2 = signature.clone();
    // Change signature in small way
    int changeIndex = rnd.nextInt( signature2.length );
    signature2[ changeIndex] = (byte) (signature2[ changeIndex ] + 1 + rnd.nextInt( 255 ));
    
    try {
      boolean wrong1 = o.verify( message, signature2, "syslab-00" );
      assertFalse( "changed signature can't be verified", wrong1 );
    } 
    catch (AssertionError e) {
      assertTrue( ""+e.getCause(), e.getCause() instanceof SignatureException );
      assertTrue( e.getCause().getMessage(), e.getCause().getMessage().contains( "invalid" ) );
    }
    
    int changeIndex2 = rnd.nextInt( message.length()-1 );
    char changedChar = message.charAt( changeIndex2 );
    changedChar = (char) (changedChar + 1 + rnd.nextInt( 255 ));
    String message2 = message.substring( 0, changeIndex2 ) +  changedChar +
      message.substring( changeIndex2+1 );

    boolean wrong2 = o.verify( message2, signature, "syslab-00" );
    assertFalse( "changed message can't be verified", wrong2 );

  }
  
  
  public void testMany() {
    MessageSigner o = new MessageSigner();
    String msg = "The message";
    String name = "client-10";
    long t1 = System.currentTimeMillis();
    for ( int i = 0; i < 64; i++ ) {
      o.sign( msg, name );
    }
    System.err.println( "signing 1024 messages took: " + (System.currentTimeMillis()-t1) + " ms");
  }

  @Override
  protected void setUp() throws Exception {
    super.setUp();
    PolicyUtils.setup( "conf/policy-framework.properties" );
  }
  
}
