package risoe.syslab.test.policy;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;

/** Test class for {@link Thresholds} */
public class ThresholdsTest extends TestCase {

  /** tests {@link Thresholds#toFipaContent()} */
  public void testFipaContent() {
    Thresholds t1 = new Thresholds(49.983333333333334, 50.016666666666666);
    String c = t1.toFipaContent();
    Expression expression = new MessageParser().parse( c );
    Thresholds r1 = Thresholds.parse(expression );
    assertEquals( t1, r1 );
  }
  
}
