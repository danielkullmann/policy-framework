package risoe.syslab.test.policy;

import java.io.PrintStream;
import java.util.Date;
import java.util.UUID;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.AcceptPolicy;
import risoe.syslab.control.policy.message.GiveDeviceInfo;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.control.policy.message.ProposePolicy;
import risoe.syslab.control.policy.message.RequestNegotiation;
import risoe.syslab.control.policy.message.ServerRequestNegotiation;
import risoe.syslab.control.policy.rules.RuleBaseCreator;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.HCDeviceInfo;
import risoe.syslab.demo.policy.heatercontrol.HCPolicy;
import risoe.syslab.demo.policy.heatercontrol.control.DynamicPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.control.FreqHiLoThresholds;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;

public class MessageLengths {

  private static final PrintStream out = System.out;

  /**
   * @param args
   */
  public static void main( String[] args ) {
    UUID uuid = UUID.randomUUID();
    HttpCommunicationSystem hcs = new HttpCommunicationSystem( "" );
    AbstractAddress client = hcs.createAddress( "client-1" );
    AbstractAddress server = hcs.createAddress( "lp-012756" );

    ServiceList services = new ServiceList( null );
    services.addService( HCServices.dppService );
    services.addService( HCServices.fctService );
    DeviceInfo di = new HCDeviceInfo( 1024, services );
    HCPolicy policy = makePolicy(); 
    
    out( new RequestNegotiation( uuid, client, server, null ) );
    out( new ServerRequestNegotiation( uuid, client, server, null ) );
    out( new GiveDeviceInfo( uuid, client, server, di, null ) );
    out( new ProposePolicy( uuid, client, server, policy, null ) );
    out( new AcceptPolicy( uuid, client, server, policy, null ) );
  }
  

  private static void out( PolicyMessage msg ) {
    String msgStr = msg.toSignedFipaContent();
    out.println( msg.getClass().getSimpleName() + ": " + msgStr.length() );
  }

  private static HCPolicy makePolicy() {
    RuleBaseCreator rbc = new RuleBaseCreator( "data/rulebase/hc-rulebase.template");
    rbc.setActivationGroup("hc");
    
    double lf = 49.9;   
    double hf = 50.1;   

    double lp = 20;
    double hp = 40;
    
    Thresholds freq = null;
    Thresholds price = null;

    String[] setup1 = FreqHiLoThresholds.getSetup( lf, hf );
    rbc.addSetup( setup1[0], setup1[1] );
    String className;
    String activeTest;
    className = FreqHiLoThresholds.class.getName();
    activeTest = FreqHiLoThresholds.getActiveTestString();
    rbc.addRule( "data/rulebase/frequency-thresholds.template", className, activeTest );
    freq = new Thresholds( lf, hf );
    
    String[] setup2 = DynamicPowerPrice.getSetup( lp, hp );
    rbc.addSetup( setup2[0], setup2[1] );
    rbc.addRule( "data/rulebase/dynamic-power-price.template", DynamicPowerPrice.class.getName(), DynamicPowerPrice.getActiveTestString() );
    price = new Thresholds( lp, hp );
    
    String ruleBase = rbc.createRuleBase();
    
    return new HCPolicy(TimeFrame.createTimeFrame(new Date(), 0, 0, 10), ruleBase, freq, price, null );
  }

}
