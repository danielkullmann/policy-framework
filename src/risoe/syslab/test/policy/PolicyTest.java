/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.IdlePolicy;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.message.parser.MessageParser;

/**Test class for {@link Policy}.
 *  
 * @author daku
 */
public class PolicyTest extends TestCase {

  /** tests {@link IdlePolicy} */
  public void testIdlePolicy() {
    
    IdlePolicy ip1 = new IdlePolicy( 12 );
    IdlePolicy ip2 = IdlePolicy.parse( new MessageParser().parse( ip1.toFipaContent() ) );
    
    assertEquals( ip1, ip2 );
    
  }
  
  
}
