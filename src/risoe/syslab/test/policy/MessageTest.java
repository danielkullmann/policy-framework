/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.util.UUID;

import risoe.syslab.control.policy.core.PolicyUtils;

import junit.framework.TestCase;

/** Test class for Message related stuff
 * 
 * @author daku
 */
public class MessageTest extends TestCase {

  /** Tests {@link UUID} */
  public void testUUID() {
    UUID uuid1 = UUID.randomUUID();
    UUID uuid2 = UUID.fromString( uuid1.toString() );
    assertEquals( uuid1, uuid2 );
  }
  
  @Override
  protected void setUp() throws Exception {
    super.setUp();
    PolicyUtils.setup( "conf/policy-framework.properties" );
  }
  
}
