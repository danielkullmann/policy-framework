/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.util.Date;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.MessageParser;

/**
 * Test class for {@link TimeFrame}
 */
public class TimeFrameTest extends TestCase {

  
  /** tests parsing of {@link TimeFrame} */
  public void testParse() {
    String content = "(:from 1 :until 2)";
    TimeFrame tf = TimeFrame.parse( new MessageParser().parse( content ) );
    assertNotNull( tf );
    assertEquals( new Date( 1 ), tf.getFrom() );
    assertEquals( new Date( 2 ), tf.getUntil() );
  }
  
  
  /** tests parsing of {@link TimeFrame} */
  public void testParse2() {
    TimeFrame tf1 = TimeFrame.createTimeFrame( new Date(), 0, 1, 30 );
    TimeFrame tf2 = TimeFrame.parse( new MessageParser().parse( tf1.toFipaContent() ) );
    assertNotNull( tf2 );
    assertEquals( tf1, tf2 );
  }
  
  
  /** tests parsing of {@link TimeFrame} */
  public void testParseCurrent() {
    long from = new Date().getTime();
    long until = from + 12*60;
    String content = "(:from " + from + " :until " + until + ")";
    TimeFrame tf = TimeFrame.parse( new MessageParser().parse( content ) );
    assertNotNull( tf );
    assertEquals( new Date( from ), tf.getFrom() );
    assertEquals( new Date( until ), tf.getUntil() );
  }


  @Override
  protected void setUp() throws Exception {
    super.setUp();
    PolicyUtils.setup( "conf/policy-framework.properties" );
  }
  
}
