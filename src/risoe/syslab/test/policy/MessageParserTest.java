/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import junit.framework.TestCase;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.jade.JadeCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.AcceptPolicy;
import risoe.syslab.control.policy.message.AliveMessage;
import risoe.syslab.control.policy.message.GiveDeviceInfo;
import risoe.syslab.control.policy.message.ProposePolicy;
import risoe.syslab.control.policy.message.RegisterClient;
import risoe.syslab.control.policy.message.RejectPolicy;
import risoe.syslab.control.policy.message.RequestNegotiation;
import risoe.syslab.control.policy.message.ServerRequestNegotiation;
import risoe.syslab.control.policy.message.parser.DoubleConstant;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.IntegerConstant;
import risoe.syslab.control.policy.message.parser.Keyword;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.PlaceHolder;
import risoe.syslab.control.policy.message.parser.StringConstant;
import risoe.syslab.control.policy.message.parser.Symbol;
import risoe.syslab.demo.policy.dummy.DummyDeviceInfo;
import risoe.syslab.demo.policy.dummy.DummyPolicy;
import risoe.syslab.demo.policy.dummy.DummyServerBehaviour;

/** Test class for {@link MessageParser}.
 * 
 * @author daku
 */
public class MessageParserTest extends TestCase {

  private static DummyServerBehaviour serverImpl;
  
  /** {@link MessageParser} instance that is used in the tests */
  private MessageParser p = new MessageParser();
  
  static {
    serverImpl = new DummyServerBehaviour();
    PolicyUtils.setCommunicationSystem( new JadeCommunicationSystem( "localhost", "localhost" ) );
  }

  public void testRegisterClientMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    RegisterClient msg1 = new RegisterClient( UUID.randomUUID(), client, server, null );
    String content = msg1.toSignedFipaContent();
    RegisterClient msg2 = RegisterClient.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertNotNull( msg2 );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertEquals( msg1.getPolicy(), msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }
  
  /** tests parsing of {@link AcceptPolicy} message */
  public void testAcceptPolicyMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    TimeFrame timeFrame = TimeFrame.createTimeFrame( new Date(), 0, 12, 0 );
    AcceptPolicy msg1 = new AcceptPolicy( UUID.randomUUID(), client, server, new DummyPolicy( 24.0, timeFrame ), null );
    String content = msg1.toSignedFipaContent();
    AcceptPolicy msg2 = AcceptPolicy.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertNotNull( msg2 );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertEquals( msg1.getPolicy(), msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }
  

  /** tests parsing of {@link GiveDeviceInfo} message */
  public void testGiveDeviceInfoMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    GiveDeviceInfo msg1 = new GiveDeviceInfo( UUID.randomUUID(), client, server, new DummyDeviceInfo( "lala" ), null );
    String content = msg1.toSignedFipaContent();
    GiveDeviceInfo msg2 = GiveDeviceInfo.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertEquals( msg1.getDeviceInfo(), msg2.getDeviceInfo() );
    assertNull( msg1.getPolicy() );
    assertNull( msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }
  

  /** tests parsing of {@link ProposePolicy} message */
  public void testProposePolicyMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    TimeFrame timeFrame = TimeFrame.createTimeFrame( new Date(), 0, 12, 0 );
    ProposePolicy msg1 = new ProposePolicy( UUID.randomUUID(), client, server, new DummyPolicy( 36.0, timeFrame ), null );
    String content = msg1.toSignedFipaContent();
    ProposePolicy msg2 = ProposePolicy.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertNotNull( msg2 );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertEquals( msg1.getPolicy(), msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }
  

  /** tests parsing of {@link RejectPolicy} message */
  public void testRejectPolicyMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    TimeFrame timeFrame = TimeFrame.createTimeFrame( new Date(), 0, 12, 0 );
    RejectPolicy msg1 = new RejectPolicy( UUID.randomUUID(), client, server, new DummyPolicy( 36.0, timeFrame ), null );
    String content = msg1.toSignedFipaContent();
    RejectPolicy msg2 = RejectPolicy.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertNotNull( msg2 );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertEquals( msg1.getPolicy(), msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }
  

  /** tests parsing of {@link RequestNegotiation} message */
  public void testRequestNegotiationMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    RequestNegotiation msg1 = new RequestNegotiation( UUID.randomUUID(), client, server, null );
    String content = msg1.toSignedFipaContent();
    RequestNegotiation msg2 = RequestNegotiation.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertNotNull( msg2 );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertEquals( msg1.getPolicy(), msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }
  

  /** tests parsing of {@link ServerRequestNegotiation} message */
  public void testServerRequestNegotiationMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    ServerRequestNegotiation msg1 = new ServerRequestNegotiation( UUID.randomUUID(), client, server, null );
    String content = msg1.toSignedFipaContent();
    ServerRequestNegotiation msg2 = ServerRequestNegotiation.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertNotNull( msg2 );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertEquals( msg1.getPolicy(), msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }
  

  /** tests parsing of {@link AliveMessage} message */
  public void testAliveMessage() {
    AbstractAddress client = PolicyUtils.createAddress( "syslab-00", "b", "c" );
    AbstractAddress server = PolicyUtils.createAddress( "syslab-01", "e", "f" );
    AliveMessage msg1 = new AliveMessage( UUID.randomUUID(), client, server, null );
    String content = msg1.toSignedFipaContent();
    AliveMessage msg2 = AliveMessage.parse( client, server, (ExpressionList) p.parse( content ), serverImpl );
    assertNotNull( msg2 );
    assertEquals( msg1.getNegotiationId(), msg2.getNegotiationId() );
    assertNull( msg1.getPolicy() );
    assertNull( msg2.getPolicy() );
    assertEquals( msg1.getReceiverAddress(), msg2.getReceiverAddress() );
    assertEquals( msg1.getSenderAddress(), msg2.getSenderAddress() );
    assertEquals( msg1.getSignature(), msg2.getSignature() );
  }

  
  /** tests parsing of {@link Symbol} */
  public void testParseSymbol() {
    MessageParser.Pair<Expression,Integer> result = p.parseExpression( "abc", 0 );
    assertNotNull( result );
    assertEquals( "", new Symbol("abc"), result.getFirst() );
    assertEquals( "", 3, result.getSecond().intValue() );
  }
  
  
  /** tests parsing of {@link Keyword} */
  public void testParseKeyword() {
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( ":abc", 0 );
    assertNotNull( result );
    assertEquals( "", new Keyword("abc"), result.getFirst() );
    assertEquals( "", 4, result.getSecond().intValue() );
  }
  
  
  /** tests parsing of {@link PlaceHolder} */
  public void testParsePlaceHolder() {
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "?abc", 0 );
    assertNotNull( result );
    assertEquals( "", new PlaceHolder("abc"), result.getFirst() );
    assertEquals( "", 4, result.getSecond().intValue() );

    PlaceHolder e = new PlaceHolder( "def" );
    result = p.parseExpression( e.toString(), 0 );
    assertNotNull( result );
    assertEquals( "", e, result.getFirst() );
  }
  
  /** tests parsing of {@link String} */
  public void testParseString() {
      
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "\"abc\"", 0 );
    assertNotNull( result );
    assertEquals( "", new StringConstant("abc"), result.getFirst() );
    assertEquals( "", 5, result.getSecond().intValue() );
  }
  
  /** tests parsing failures */
  public void testParseFailures() {
    fails( ":", "Empty Keyword constant" );
    fails( "\"", "String constant ends at EOF" );
    fails( "\"a", "String constant ends at EOF" );
    fails( "\"\\", "String constant ends at EOF" );
    //fails( "(", "Can't parse list element" );
    fails( "(", "List finished by end of string" );
  }
   
  /** helper method for {@link #testParseFailures()}
   *  
   * @param str String to parse
   * @param messageStart start of expexted exception message
   */
  private void fails( String str, String messageStart ) {
    try {
      p.parseExpression( str, 0 );
      assertTrue( false );
    }
    catch ( IllegalArgumentException e ) {
      assertTrue( e.getMessage().startsWith( messageStart ) );
    }
  }
  

  /** tests parsing of String escapes */
  public void testParseStringEscapes() {
      
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "\"a\\\"c\\n\\b\\r\\t\\f\"", 0 );
    assertNotNull( result );
    assertEquals( "", new StringConstant("a\"c\n\b\r\t\f"), result.getFirst() );
    assertEquals( "", 16, result.getSecond().intValue() );
  }
  
  /** tests parsing of {@link Integer} */
  public void testParseInteger() {
      
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "25", 0 );
    assertNotNull( result );
    assertEquals( "", new IntegerConstant(25), result.getFirst() );
    assertEquals( "", 2, result.getSecond().intValue() );

    result = p.parseExpression( "-25", 0 );
    assertNotNull( result );
    assertEquals( "", new IntegerConstant(-25), result.getFirst() );
    assertEquals( "", 3, result.getSecond().intValue() );
 }
  
  /** tests parsing of {@link Double} */
  public void testParseDouble() {
    
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "25.26", 0 );
    assertNotNull( result );
    assertEquals( "", new DoubleConstant(25.26), result.getFirst() );
    assertEquals( "", 5, result.getSecond().intValue() );
  }
  
  /** tests parsing of double values in e notation (1.0006e10) */
  public void testDoubleParser() {
    p = new MessageParser();
    Double d = 3e-10;
    Expression e = p.parse( ""+d );
    assertNotNull( e );
    assertTrue( e instanceof DoubleConstant );
    assertEquals( d , ((DoubleConstant) e).getValue() );
    d = -42.0;
    e = p.parse( ""+d );
    assertNotNull( e );
    assertTrue( e instanceof DoubleConstant );
    assertEquals( d, ((DoubleConstant) e).getValue() );
  }
  
  /** tests parsing of empty list */
  public void testParseEmptyList() {
      
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "()", 0 );
    assertNotNull( result );
    assertEquals( "", null, result.getFirst() );
    assertEquals( "", 2, result.getSecond().intValue() );
  }
  
  /** tests parsing of single-element {@link ExpressionList} */
  public void testParseList() {
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "(25)", 0 );
    assertNotNull( result );
    assertEquals( "", new ExpressionList( new IntegerConstant( 25 ), null), result.getFirst() );
    assertEquals( "", 4, result.getSecond().intValue() );
  }
  
  /** tests parsing of two-element {@link ExpressionList} */
  public void testParseList2() {
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( "(25 26)", 0 );
    assertNotNull( result );
    assertEquals( "", new ExpressionList( new IntegerConstant( 25 ), new ExpressionList( new IntegerConstant( 26 ), null ) ), result.getFirst() );
    assertEquals( "", 7, result.getSecond().intValue() );
  }
  
  /** tests parsing of {@link ExpressionList} */
  public void testParseLongList() {
    MessageParser.Pair<Expression,Integer>result = p.parseExpression( " ( 25 symbol :key \"string\" )", 0 );
    assertNotNull( result );
    ExpressionList expected = 
        new ExpressionList( new IntegerConstant( 25 ), 
        new ExpressionList( new Symbol( "symbol" ), 
        new ExpressionList( new Keyword( "key" ), 
        new ExpressionList( new StringConstant( "string" ), null ) ) ) );
    assertEquals( "", expected, result.getFirst() );
    assertEquals( "", 28, result.getSecond().intValue() );
  }
  
  /** tests parsing of {@link ExpressionList} with different values*/
  public void testParseListStr() {
    String str = "(25 symbol :key \"string\")";
    Expression result = p.parse( str );
    assertNotNull( result );
    assertEquals( str, result.toString() );
  }

  
  /** tests {@link MessageParser#match(String, Expression)} */
  public void testMatch1() {
    String str = "(symbol :key \"string\" 1234)";
    Expression expr = p.parse( str );
    assertNotNull( expr );
    assertEquals( str, expr.toString() );
    
    String pattern = "(symbol :key \"string\" 1234)";
    HashMap<String, Expression> h = p.match( pattern, expr );
    assertNotNull( h );
    assertTrue( h.size() == 0 );

    pattern = "(symbol ?keyv \"string\" 1234)";
    h = p.match( pattern, expr );
    assertNotNull( h );
    assertTrue( h.size() == 1 );
    assertTrue( h.get("keyv").equals( new Keyword( "key" ) ) );

    pattern = "(symbol ?keyv ?strv ?numv)";
    h = p.match( pattern, expr );
    assertNotNull( h );
    assertTrue( h.size() == 3 );
    assertTrue( h.get("keyv").equals( new Keyword( "key" ) ) );
    assertTrue( h.get("strv").equals( new StringConstant( "string" ) ) );
    assertTrue( h.get("numv").equals( new IntegerConstant( 1234 ) ) );
  }


  @Override
  protected void setUp() throws Exception {
    super.setUp();
    PolicyUtils.setup( "conf/policy-framework.properties" );
  }
  
}
