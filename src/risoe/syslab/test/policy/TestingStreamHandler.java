/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.util.HashMap;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * This is a java.logging output handler that can be used for test classes.
 * It logs messages separately for every class in a StringBuilder, and 
 * the logged messages can be retrieved using {@link #getMessages(String)}
 */
public class TestingStreamHandler extends Handler {

  /** HashMap that stores the messages for each source class. */
  private static final HashMap<String, StringBuilder> MESSAGES = new HashMap<String, StringBuilder>();
  
  
  /**
   * Default constructor.
   */
  public TestingStreamHandler() {
    super();
  }

  
  
  /**
   * Returns the messages logged for the given source class.
   * 
   * @param sourceClassNameString the name of the class we are interested in
   * @return A String of messages, or the empty string; NEVER null
   */
  public static String getMessages( String sourceClassNameString ) {
    if ( MESSAGES.containsKey( sourceClassNameString ) ) {
      return MESSAGES.get( sourceClassNameString ).toString();
    }

    return "<none>";
  }
  
  @Override
  public void close() {
    // empty
  }

  @Override
  public void flush() {
    // empty
  }

  @Override
  public synchronized void publish( LogRecord record ) {
    String sourceClassName = record.getSourceClassName();
    if ( ! MESSAGES.containsKey( sourceClassName ) ) {
      MESSAGES.put( sourceClassName, new StringBuilder() );
    }
    MESSAGES.get( sourceClassName ).append( record.getMessage() );
  }

}
