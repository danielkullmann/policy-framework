package risoe.syslab.test.policy;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.impl.client.DefaultHttpClient;

import risoe.syslab.comm.lba.LbaSchemeSocketFactory;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;

public class SocketFactoryCheck {

  /**
   * @param args
   */
  public static void main( String[] args ) {
    try {
      PrintStream out = System.out;
      DefaultHttpClient client = new DefaultHttpClient();
      double latency = 1000;
      double upBandwidth = 0;
      double downBandwidth = 0;
      LbaSchemeSocketFactory socketFactory = new LbaSchemeSocketFactory();
      socketFactory.setParams( latency, upBandwidth, downBandwidth );
      Scheme scheme = new Scheme( "http", HttpCommunicationSystem.DEFAULT_PORT, socketFactory );
      client.getConnectionManager().getSchemeRegistry().register( scheme  );
      
      HttpResponse response = client.execute( new HttpHost( "localhost", 80 ), new HttpGet( "/" ) );
      byte[] buf = new byte[100];
      response.getEntity().getContent().read( buf );
      out.println( "content: " + new String( buf ) );
      
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    
  }
  
}
