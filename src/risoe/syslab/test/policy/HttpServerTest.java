package risoe.syslab.test.policy;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;


public class HttpServerTest {

  public static void main (String[] args) {

    try {
      DefaultHttpClient httpClient = new DefaultHttpClient();
      URL url = new URL( "http://localhost:80" );
      HttpPost request = new HttpPost( url.toURI() );
      List<NameValuePair> nvps = new ArrayList<NameValuePair>();
      nvps.add(new BasicNameValuePair( "from", "fv" ) );
      nvps.add(new BasicNameValuePair( "msg", "mv" ) );

      request.setEntity( new UrlEncodedFormEntity( nvps, HTTP.ASCII ) );

      HttpParams httpParams = new BasicHttpParams(); 
      httpParams.setParameter( "http.protocol.expect-continue", false );
      httpClient.setParams( httpParams );
      
      HttpResponse response = httpClient.execute( new HttpHost( url.getHost(), url.getPort() ), request );
      HttpEntity entity = response.getEntity();
      System.out.println( entity.getContentType().getValue() );
    } catch ( Exception e ) {
      e.printStackTrace();
    }
  }
  
}
