/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.net.InetAddress;
import java.net.UnknownHostException;

import junit.framework.TestCase;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.ClientBehaviour;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.demo.policy.dummy.DummyClientBehaviour;
import risoe.syslab.demo.policy.dummy.DummyServerBehaviour;
import risoe.syslab.demo.policy.dummy.DummyServerClientBehaviour;

/** Test behaviour of dummy example implementation */
public class DummyAppTest extends TestCase {

  private static String hostname;
  private static HttpCommunicationSystem commSystem;
  private static DummyServerBehaviour server;
  private AbstractAgent serverAgent;
  private AbstractAgent clientAgent;


  static {

    try {
      hostname = InetAddress.getLocalHost().getHostName();
    } catch ( UnknownHostException e ) {
      throw new AssertionError( "Can't determine local hostname" );
    }

    System.setProperty( "java.util.logging.config.file", "conf/logging-testing.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );
    server = new DummyServerBehaviour();
    HttpCommunicationSystem.SEND_IN_BACKGROUND = false;
    commSystem = new HttpCommunicationSystem( "syslab-00", hostname );
    PolicyUtils.setCommunicationSystem( commSystem );

  }



  public void test1() throws Exception {
    
    AbstractAddress serverAddress = PolicyUtils.createAddress( "syslab-00" );
    AbstractAddress clientAddress = PolicyUtils.createAddress( "syslab-01" );
    
    serverAgent = PolicyUtils.createServerAgent( serverAddress, server );
    ClientBehaviour clientBehaviour = new DummyClientBehaviour( clientAddress );
    clientAgent = PolicyUtils.createClientAgent( clientAddress, clientBehaviour, serverAddress );
    
    final String clientWanted = "CLIENT: policy activated 10.0";
    final String serverWanted = "SERVER: policy activated 10.0";

    String clientMessages = TestingStreamHandler.getMessages( DummyClientBehaviour.class.getName() );
    String serverMessages = TestingStreamHandler.getMessages( DummyServerClientBehaviour.class.getName() );

    int counter=0;
    while ( counter < 30 && ! ( clientMessages.contains( clientWanted ) && serverMessages.contains( serverWanted ) ) ) {
      Thread.sleep( 1*1000 );
      clientMessages = TestingStreamHandler.getMessages( DummyClientBehaviour.class.getName() );
      serverMessages = TestingStreamHandler.getMessages( DummyServerClientBehaviour.class.getName() );
      counter++;
    }
    
    assertTrue( clientMessages, clientMessages.contains( clientWanted ) );
    assertTrue( serverMessages, serverMessages.contains( serverWanted ) );

  }


  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
    if ( serverAgent != null ) serverAgent.stopAgent();
    if ( clientAgent != null ) clientAgent.stopAgent();
    if ( commSystem != null ) commSystem.shutdown();
  }
  
  
}
