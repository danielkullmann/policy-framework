/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.util.Random;

import junit.framework.TestCase;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.ClientBehaviour;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.demo.policy.dummy.DummyClientBehaviour;
import risoe.syslab.demo.policy.dummy.DummyServerBehaviour;
import risoe.syslab.demo.policy.dummy.DummyServerClientBehaviour;

/** Test behaviour of dummy example implementation */
public class AliveMessageTest extends TestCase {

  private static HttpCommunicationSystem commSystem = null;
  private AbstractAgent serverAgent = null;
  private AbstractAgent clientAgent = null;
  private DummyServerBehaviour serverImpl = null;

  @Override
  public void setUp() {

    System.setProperty( "java.util.logging.config.file", "conf/logging-testing.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );
    serverImpl = new DummyServerBehaviour();
    int port = new Random().nextInt( 10240 ) + 10240;
    commSystem = new HttpCommunicationSystem( port, "", null );
    PolicyUtils.setCommunicationSystem( commSystem );

  }



  public void test1() throws Exception {
    
    AbstractAddress serverAddress = PolicyUtils.createAddress( "syslab-00" );
    AbstractAddress clientAddress = PolicyUtils.createAddress( "syslab-01" );
    
    String serverMessages = TestingStreamHandler.getMessages( DummyServerClientBehaviour.class.getName() );

    serverAgent = PolicyUtils.createServerAgent( serverAddress, serverImpl );
    ClientBehaviour clientBehaviour = new DummyClientBehaviour( clientAddress );
    clientAgent = PolicyUtils.createClientAgent( clientAddress, clientBehaviour, serverAddress );
    
    int counter=0;
    while ( counter < 30 ) {
      Thread.sleep( 1*1000 );
      counter++;
    }

    assertFalse( serverMessages.toString(), serverMessages.contains( "AliveMessage" ) );
  }


  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
    if ( serverAgent != null ) serverAgent.stopAgent();
    if ( clientAgent != null ) clientAgent.stopAgent();
    if ( commSystem != null ) commSystem.shutdown();
  }
  
  
}
