/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.test.policy;

import java.util.Date;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.demo.policy.evcharging1.EVCharging1DeviceInfo;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Policy;

/** Test class for classes from the EVC1 example implementation */
public class EVC1Test extends TestCase {

  /** {@link MessageParser} to be used */
  private MessageParser p = new MessageParser();
  
  /** Tests parsing of {@link EVCharging1DeviceInfo} */
  public void testDeviceInfo() {
    int id = 28729389;
    double soc = 70;
    double tsoc = 100;
    double capacity = 9.0;
    Date nextUse = new Date();
    EVCharging1DeviceInfo devInfo1 = new EVCharging1DeviceInfo( id, soc, tsoc, capacity, nextUse  );
    EVCharging1DeviceInfo devInfo2 = EVCharging1DeviceInfo.parse( p.parse( devInfo1.toFipaContent() ) );
    assertEquals( devInfo1, devInfo2 );    
  }

  
  /** Tests parsing of {@link EVCharging1Policy} */
  public void testPolicy() {
  
    TimeFrame timeFrame = TimeFrame.createTimeFrame( new Date(), 0, 1, 0 );
    boolean[] charging = new boolean[] {true, true, false, true, false, false, true, false};
    EVCharging1Policy policy1 = new EVCharging1Policy( timeFrame, charging );
    String stringContent = policy1.toFipaContent();
    EVCharging1Policy policy2 = EVCharging1Policy.parse( p.parse( stringContent ) );
    
    assertEquals( policy1, policy2 );    
  }
  
  
  @Override
  protected void setUp() throws Exception {
    super.setUp();
    PolicyUtils.setup( "conf/policy-framework.properties" );
  }
  
}
