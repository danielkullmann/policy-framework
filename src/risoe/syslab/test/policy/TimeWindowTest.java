package risoe.syslab.test.policy;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.TimeWindow;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;

public class TimeWindowTest extends TestCase {

  private int frame = 3600;

  public void testFipaContent() {

    TimeWindow t1 = new TimeWindow(frame, 49, 31);
    String c = t1.toFipaContent();
    Expression expression = new MessageParser().parse(c);
    TimeWindow r1 = TimeWindow.parse(expression);
    assertEquals(t1, r1);
    
    TimeWindow tw = new TimeWindow( frame, 1800, 500 );
    assertEquals(tw, TimeWindow.parse(new MessageParser().parse(tw.toFipaContent())));
  }

  public void testCurrent() {
    Calendar cal = new GregorianCalendar();
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    Date s00_00 = cal.getTime();
    cal.set(Calendar.MINUTE, 15);
    cal.set(Calendar.SECOND, 0);
    Date s15_00 = cal.getTime();
    cal.set(Calendar.MINUTE, 30);
    cal.set(Calendar.SECOND, 0);
    Date s30_00 = cal.getTime();
    cal.set(Calendar.MINUTE, 45);
    cal.set(Calendar.SECOND, 0);
    Date s45_00 = cal.getTime();
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    Date s59_59 = cal.getTime();

    TimeWindow t1 = new TimeWindow(frame, 0, 1750);
    assertTrue(t1.isCurrent(s00_00));
    assertTrue(t1.isCurrent(s15_00));
    assertFalse(t1.isCurrent(s30_00));
    assertFalse(t1.isCurrent(s45_00));
    assertFalse(t1.isCurrent(s59_59));

    TimeWindow t2 = new TimeWindow(frame, 1400, 3500 - 1400);
    assertFalse(t2.isCurrent(s00_00));
    assertFalse(t2.isCurrent(s15_00));
    assertTrue(t2.isCurrent(s30_00));
    assertTrue(t2.isCurrent(s45_00));
    assertFalse(t2.isCurrent(s59_59));

    TimeWindow t3 = new TimeWindow(frame, 80, 10);
    assertFalse(t3.isCurrent(s00_00));
    assertFalse(t3.isCurrent(s15_00));
    assertFalse(t3.isCurrent(s30_00));
    assertFalse(t3.isCurrent(s45_00));
    assertFalse(t3.isCurrent(s59_59));

    TimeWindow t4 = new TimeWindow(frame, 0, 3600);
    assertTrue(t4.isCurrent(s00_00));
    assertTrue(t4.isCurrent(s15_00));
    assertTrue(t4.isCurrent(s30_00));
    assertTrue(t4.isCurrent(s45_00));
    assertTrue(t4.isCurrent(s59_59));
  }

  public void testRange() {
    new TimeWindow(frame, 0, 3599);
    new TimeWindow(frame, 1800, 0);
    try {
      new TimeWindow(frame, 3200, -2000);
      assertTrue("should have thrown an exception", false);
    } catch (IllegalArgumentException e) {
      assertTrue(true);
    }
    try {
      new TimeWindow(frame, -1, 2300);
      assertTrue("should have thrown an exception", false);
    } catch (IllegalArgumentException e) {
      assertTrue(true);
    }
    try {
      new TimeWindow(frame, 1000, -700);
      assertTrue("should have thrown an exception", false);
    } catch (IllegalArgumentException e) {
      assertTrue(true);
    }
    try {
      new TimeWindow(-1, 0, 1);
      assertTrue("should have thrown an exception", false);
    } catch (IllegalArgumentException e) {
      assertTrue(true);
    }
  }

  public void testOverlap() {
    int numClients = 10;
    int length = frame / numClients;
    TimeWindow[] windows = new TimeWindow[numClients];
    for (int i = 0; i < numClients; i++) {
      windows[i] = new TimeWindow(frame, i * length, length);
    }
    for (int j = 0; j < frame; j++) {
      int count = 0;
      for (int i = 0; i < numClients; i++) {
        if ( windows[i].isCurrent( j*1000 ) ) {
          count ++;
        }
      }
      assertEquals(1, count);
    }
  }

}
