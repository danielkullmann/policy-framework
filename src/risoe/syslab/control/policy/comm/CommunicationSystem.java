/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm;

import risoe.syslab.control.policy.core.PolicyPartner;
import risoe.syslab.control.policy.message.PolicyMessage;

/**
 * Main class for implementing different communication systems.
 * A communication system is used to send messages between the 
 * partners in a policy protocol communication.
 * Right now, we have JADE and HTTP communication systems.
 * 
 * The features that we need from a communication system are as follows:
 * <ul>
 * <li> Start up the communication system, which normally means to setup 
 *      some kind of server that can receive messages for {@link PolicyPartner}s
 * <li> Stop the communication system, which normally means to stop the server
 *      started during the setup
 * <li> Have some notion of address, so that the communication system knows where 
 *      a message has to be sent to. Since we started with JADE as communication system,
 *      an address has three parts right now: A host name, where the agent is located,
 *      a container name on this host, and the agent name inside the container.
 * <li> A means to send messages
 * <li> A means to receive messages
 * <li> Messages that are received are put into an inbox, and the {@link PolicyPartner}s
 *      query that inbox for messages that have been received.      
 * </ul>
 */
public interface CommunicationSystem {

  
  /**
   * Start this communication system up.
   */
  void startup();

  
  /**
   * Stop this communication system.
   */
  void shutdown();
  

  /**
   * Creates and runs an agent in this communciation system
   * 
   * @param address address of agent
   * @param behaviour behaviour this agent shall use
   * @return a new instance of AbstractAgent
   */
  AbstractAgent createandStartAgent( AbstractAddress address, PolicyPartner behaviour );
  
  
  /**
   * Create a local address for the given agent name. The container name
   * and host name for this agent have to be determined by the communication system.
   * 
   * @param agentName name part of agent address
   * @return an address for the agent on the local machine
   */
  AbstractAddress createAddress( String agentName );
  
  
  /**
   * Create a remote address with the three parts agent name, container name and host name.
   * 
   * @param agentName name of agent
   * @param containerName container the agent lives in
   * @param hostName hostname of agent
   * @return an address for the agent specified by the three parameters
   */
  AbstractAddress createAddress( String agentName, String containerName, String hostName );

  
  /** Creates an AbstractAddress from a full name of the address
   * (HTTP: URL, JADE: string version of address)
   * 
   * @param url
   * @return
   */
  AbstractAddress createAddressFromFullName( String fullName );
  

  /** register a {@link MessageListener}
   * 
   * @param listener listener to be registered
   */
  void addMessageListener( MessageListener listener );

  
  /** Notify all registered {@link MessageListener}s that a message has been sent
   * 
   * @param msg message that was sent
   */
  void notifyMessageSent(PolicyMessage msg);

  
  /** Notify all registered {@link MessageListener}s that a message has been received
   * 
   * @param msg message that was received
   */
  void notifyMessageReceived(PolicyMessage msg);


}
