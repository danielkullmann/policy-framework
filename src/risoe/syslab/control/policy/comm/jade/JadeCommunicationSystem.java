/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm.jade;

import jade.core.AID;
import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.logging.LogManager;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.MessageListener;
import risoe.syslab.control.policy.core.PolicyPartner;
import risoe.syslab.control.policy.message.PolicyMessage;

/**
 * Implementation of {@link CommunicationSystem} for the 
 * JADE Communication system. 
 * 
 * This class should be instantiated only in an application,
 * because it starts a JADE platform. It should theoretically be
 * possible to have more instances, but I don't know how that works..
 */
public final class JadeCommunicationSystem implements CommunicationSystem {

  /** TODO Why is this is needed? */
  public static boolean LocalExecution = false;

  /** JADE Runtime singleton */
  private Runtime runtime;
  
  /** Profile for the agent container */
  private Profile profile;
  
  /** container for the agents registered on this host */
  private AgentContainer container;

  /** list of listeners for message send and receive events */
  private ArrayList<MessageListener> messageListeners = new ArrayList<MessageListener>();

  /** list of registered agents */
  private ArrayList<WeakReference<AgentController>> agents = new ArrayList<WeakReference<AgentController>>();
;

  /**
   * Constructor; initializes the necessary root JADE classes
   * and startes the JADE platform.
   * @param containerName container name used for agents on this platform
   * @param hostName hostname for this JADE platform
   */
  public JadeCommunicationSystem( String containerName, String hostName ) {

    if ( System.getProperty( "log4j.configuration" ) == null ) {
      System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    }
    try {
      LogManager.getLogManager().readConfiguration();
    } catch ( SecurityException e ) {
      e.printStackTrace();
    } catch ( IOException e ) {
      e.printStackTrace();
    }
    runtime = Runtime.instance();
    profile = new ProfileImpl();
    profile.setParameter( Profile.DETECT_MAIN,"false");
    profile.setParameter( Profile.CONTAINER_NAME, containerName );
    profile.setParameter( Profile.PLATFORM_ID, hostName );
    try {
      container = runtime.createMainContainer( profile );
    } catch (Exception e) {
      System.err.println("main container could not be created...");
    }
    if ( container == null ) {
      container = runtime.createAgentContainer( profile );
    }
  }  
  
  
  @Override
  public AbstractAddress createAddress( String agentName ) {
    return new JadeAddress( createAgentId( agentName, 
        profile.getParameter( Profile.CONTAINER_NAME, null ), 
        profile.getParameter( Profile.PLATFORM_ID, null ) ) );
  }

  
  @Override
  public AbstractAddress createAddress( String agentName, String containerName, String hostName ) {
    return new JadeAddress( createAgentId( agentName,  containerName, hostName ) );
  }

  
  @Override
  public AbstractAddress createAddressFromFullName( String fullName ) {
//    String host = url.getHost();
//    String containerAndName = url.getFile();
//    int idx = containerAndName.indexOf( "/" );
//    String name = containerAndName.substring( 0, idx-1 );
//    String container = containerAndName.substring( idx+1 );
//    System.out.println( host + " : " + container + " : " + name );
//    return new JadeAddress( createAgentId( name, container, host ) );
    throw new IllegalArgumentException( fullName );
  }


  @Override
  public AbstractAgent createandStartAgent( AbstractAddress address, PolicyPartner partner ) {
    try {
      Agent agent = new Agent();
      agent.addBehaviour( new BehaviourWrapper( partner ) );
      //agent.setArguments( params );
      AgentController ac = container.acceptNewAgent( address.getName(), agent);
      AbstractAgent result = new JadeAgent( agent, ac, this, partner );
      partner.setAgent( result );
      ac.start();
      agents.add( new WeakReference<AgentController>(ac));
      return result;
    } catch ( StaleProxyException e ) {
      throw new AssertionError( e );
    }
  }

  
  @Override
  public void startup() {
    try {
      container.start();
    } catch ( ControllerException e ) {
      throw new AssertionError( e );
    }
  }

  
  /**
   * Create an AID from the three parts agent name, container name and host name.
   * 
   * package access!
   * 
   * @param name local name of agent
   * @param container name of container agent resides in
   * @param host name of host the agent container resides in
   * @return the created AID
   */
  static AID createAgentId( String name, String container, String host ) {
    AID result = null;
    if ( LocalExecution ) {
      result = new AID( name + "@" + container + ":1099/JADE", AID.ISGUID );
      result.addAddresses( "http://" + host + ":7778/acc" );
    } else {
      result = new AID( name + "@" + container, AID.ISGUID );
      result.addAddresses( "http://" + host + ":7778/acc" );
    }
    return result;
  }


  @Override
  public void shutdown() {
    // TODO This does not work yet..
    try {
      for (WeakReference<AgentController> acr : agents) {
        AgentController ac = acr.get();
        if ( ac != null ) ac.kill();
      }
      container.kill();
      Runtime.instance().shutDown();
    } catch ( StaleProxyException e ) {
      e.printStackTrace();
    }
  }

  @Override
  public void addMessageListener(MessageListener listener) {
    messageListeners.add( listener );
  }


  @Override
  public void notifyMessageReceived( PolicyMessage msg ) {
    for (MessageListener listener : messageListeners) {
      listener.messageReceived( msg );
    }
  }
  
  @Override
  public void notifyMessageSent( PolicyMessage msg ) {
    for (MessageListener listener : messageListeners) {
      listener.messageSent( msg );
    }
  }


}
