/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm.jade;

import java.net.MalformedURLException;
import java.net.URL;

import jade.core.AID;
import risoe.syslab.control.policy.comm.AbstractAddress;

/**
 * Implementation of {@link AbstractAddress} for the 
 * {@link JadeCommunicationSystem}.
 * 
 * The class simply encapsulates an AID object.
 */
public final class JadeAddress implements AbstractAddress {

  private AID address;


  /** Constructor for this class.
   * @param address agent id for this address
   */
  public JadeAddress( AID address ) {
    this.address = address;
  }


  public JadeAddress( String address ) {
    try {
      int idx = address.indexOf( "::" );
      String fullHost = address.substring( 0, idx );
      String host = new URL(fullHost).getHost();
      String containerAndName = address.substring( idx+2 );
      idx = containerAndName.indexOf( "@" );
      String container = containerAndName.substring( 0, idx );
      String name = containerAndName.substring( idx+1 );
      this.address = JadeCommunicationSystem.createAgentId( name, container, host );
    } catch ( MalformedURLException e ) {
      throw new IllegalArgumentException(e);
    }
  }

  @Override
  public Object getAddress() {
    return address;
  }


  @Override
  public String getName() {
    String name = address.getName();
    int atIndex = name.indexOf( '@' );
    if ( atIndex < 0 ) return name;
    return name.substring( 0, atIndex );
  }


  @Override
  public int compareTo( AbstractAddress o ) {
    return address.compareTo( o.getAddress() );
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((address == null) ? 0 : address.hashCode());
    return result;
  }


  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    JadeAddress other = (JadeAddress) obj;
    if ( address == null ) {
      if ( other.address != null ) {
        return false;
      }
    } else if ( !address.equals( other.address ) ) {
      return false;
    }
    return true;
  }


  @Override
  public String toString() {
    return address.getName() + "::" + address.getAddressesArray()[0];
  }

  
}
