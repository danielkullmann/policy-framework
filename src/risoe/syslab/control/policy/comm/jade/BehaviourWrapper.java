/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm.jade;

import jade.core.behaviours.Behaviour;
import risoe.syslab.control.policy.core.PolicyPartner;

/**
 * This wraps a {@link PolicyPartner} implementation
 * into a JADE {@link Behaviour}, so a JADE
 * {@link Agent} can use the {@link PolicyPartner}.
 */
@SuppressWarnings( "serial" )
public final class BehaviourWrapper extends Behaviour {

  /** {@link PolicyPartner} whose action() method is called */
  private PolicyPartner partner;

  /**
   * The constructor to construct an instance
   * @param partner partner instance to wrap
   */
  public BehaviourWrapper( PolicyPartner partner ) {
    this.partner = partner;
  }

  @Override
  public void action() {
    partner.action();
  }

  @Override
  public boolean done() {
    return partner.done();
  }

}
