/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm.jade;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.MessageTemplate.MatchExpression;
import jade.util.Logger;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.core.PolicyPartner;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.message.AcceptPolicy;
import risoe.syslab.control.policy.message.AliveMessage;
import risoe.syslab.control.policy.message.GiveDeviceInfo;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.control.policy.message.ProposePolicy;
import risoe.syslab.control.policy.message.RegisterClient;
import risoe.syslab.control.policy.message.RejectPolicy;
import risoe.syslab.control.policy.message.RequestNegotiation;
import risoe.syslab.control.policy.message.ServerRequestNegotiation;
import risoe.syslab.control.policy.message.UnregisterClient;

/**
 * Implementation of the {@link AbstractAgent} class for the 
 * {@link JadeCommunicationSystem}.
 * The sending and receiving of messages is handled by JADE
 * internally, so this class does not have to do much.
 * 
 * What it does, though, is filtering out all {@link ACLMessage}s
 * that can not be parsed into {@link PolicyMessage} objects;
 * those messages result in an warning message that is logged.
 */
public final class JadeAgent implements AbstractAgent {

  /** logger instance */
  private static final Logger logger = Logger.getMyLogger( JadeAgent.class.getName() );
  
  /** Agent instance that is encapsulated by this class */
  private Agent agent;

  /** Agent controller for controlling the agent, i.e. starting and stopping the agent */
  private AgentController ac;

  private CommunicationSystem communicationSystem;

  private PolicyPartner partner;

  /**
   * @param agent agent that should be encapsulated
   * @param ac agent controller for that agent
   * @param partner 
   */
  JadeAgent( Agent agent, AgentController ac, CommunicationSystem communicationSystem, PolicyPartner partner ) {
    this.agent = agent;
    this.ac = ac;
    this.communicationSystem = communicationSystem;
    this.partner = partner;
  }
  

  @SuppressWarnings( "serial" ) // for th anonymous inner classes
  @Override
  public PolicyMessage receiveMessage() {
    ACLMessage msg = agent.blockingReceive( 
        new MessageTemplate( 
          new MatchExpression() {
            @Override
            public boolean match( ACLMessage msg ) {
              // Message is a PolicyMessage, when the content can be parsed
              PolicyMessage parsed = null;
              try {
                parsed = PolicyUtils.parseMessage( 
                    new JadeAddress( msg.getSender() ), new JadeAddress( (AID) msg.getAllReceiver().next() ), msg.getContent(), partner );
                communicationSystem.notifyMessageReceived( parsed );
              } catch ( Exception e ) {
                // this happens when the message can't be parsed...
                if ( msg.getPerformative() != ACLMessage.FAILURE && ! msg.getContent().contains( "(MTS-error " ) ) {
                  logger.log( Level.SEVERE, "ERROR: parsing message failed: " + msg );
                }
              }
              return parsed != null;
            }
          } 
        ), 1000 // wait one second for a message..
      );
      
      // purge "MTS-Error" message from queue
      while (true) {
        ACLMessage purged = agent.receive( new MessageTemplate( new MatchExpression() {
          @Override
          public boolean match( ACLMessage msg ) {
            return msg.getPerformative() == ACLMessage.FAILURE && msg.getContent().contains( "(MTS-error " );
          }
        } ) );
        if ( purged == null ) break;

        // Determine recipient of failed message
        // Content example (with added line breaks):
        // ( (action ( agent-identifier :name lp-012756@lp-012756  
        //   :addresses (sequence http://lp-012756.risoe.dk:7778/acc )) 
        //   (ACLMessage) ) (MTS-error ( agent-identifier :name syslab-00@syslab-00  
        //   :addresses (sequence http://syslab-00:7778/acc )) (internal-error 
        //   "No valid address contained within the AID syslab-00@syslab-00")) )"
        Matcher match = Pattern.compile( 
            "\\( \\(action (.*) \\(ACLMessage\\).*", Pattern.DOTALL ).matcher( purged.getContent() );
        if ( match.matches() ) {
          // Address of client that could not be reached
          String address = match.group( 1 );
          logger.log( Level.SEVERE, "server: client " + address + " could not be reached" );
          // TODO remember failed address, so that server or client can remove it..
        } else {
          throw new AssertionError( "client address for failed send could not be determined" );
        }

        logger.log( Level.INFO, "ignoring message: " + purged );
      }
      
      // continuing with the normal task of the method...
      if ( msg != null ) {
        PolicyMessage pmsg = PolicyUtils.parseMessage( new JadeAddress( msg.getSender() ), new JadeAddress( (AID) msg.getAllReceiver().next() ), msg.getContent(), partner );
        if ( pmsg != null ) {
          pmsg.checkSignature();
        }
        String receiverClassName = pmsg == null ? "<null>" : pmsg.getClass().getName();
        logger.log( Level.FINE, ((AID) msg.getAllReceiver().next()).getLocalName() + " received " + receiverClassName );
        return pmsg;
      }
      return null;
  }


  @Override
  public void sendMessage( PolicyMessage msg ) {
    ACLMessage aclMsg = toACLMessage( msg );
    logger.log( Level.FINER, agent.getAID().getLocalName() + 
        " sends " + msg.getClass().getName() +
        " to " + msg.getReceiverAddress().getName() );
    logger.log( Level.FINEST, aclMsg.toString() );
    agent.send( aclMsg );
    communicationSystem.notifyMessageSent( msg );
  }

  
  /**
   * Creates an ACL Message from this message
   * @param message message to be converted into an {@link ACLMessage}
   * @return a new {@link ACLMessage} instance
   */
  public final ACLMessage toACLMessage( PolicyMessage message ) {
    ACLMessage msg = new ACLMessage( getMessageType( message ) );
    msg.addReceiver( (AID) ((JadeAddress)message.getReceiverAddress()).getAddress() );
    msg.setContent( message.toSignedFipaContent() );
    return msg;
  }


  /**
   * Returns ACLMessage type that should be used in {@link #toACLMessage()}.
   * @param message message whose type should be determined
   * @return one of the {@link ACLMessage} constants, like {@link ACLMessage#REQUEST}
   */
  protected int getMessageType(PolicyMessage message) {
    if ( message instanceof AcceptPolicy ) return ACLMessage.ACCEPT_PROPOSAL;
    if ( message instanceof AliveMessage ) return ACLMessage.INFORM;
    if ( message instanceof GiveDeviceInfo ) return ACLMessage.INFORM;
    if ( message instanceof ProposePolicy ) return ACLMessage.PROPOSE;
    if ( message instanceof RegisterClient ) return ACLMessage.REQUEST;
    if ( message instanceof RejectPolicy ) return ACLMessage.REJECT_PROPOSAL;
    if ( message instanceof RequestNegotiation ) return ACLMessage.REQUEST;
    if ( message instanceof ServerRequestNegotiation ) return ACLMessage.REQUEST;
    if ( message instanceof UnregisterClient ) return ACLMessage.INFORM;
    throw new AssertionError( "Unknown message type " + message.getClass().getName() );
  }


  @Override
  public void stopAgent() {
    try {
      ac.kill();
    } catch ( StaleProxyException e ) {
      e.printStackTrace();
    }
  }


  @Override
  public AbstractAddress getAddress() {
    return new JadeAddress( agent.getAID() );
  }


  @Override
  public void setPolicyPartner( PolicyPartner partner ) {
    agent.addBehaviour( new BehaviourWrapper( partner ) );
  }


  @Override
  public boolean areMessagesAvailable() {
    ACLMessage msg = agent.receive();
    if (msg != null) agent.putBack( msg );
    return msg != null;
  }


}
