/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm;

import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.comm.jade.JadeCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyPartner;
import risoe.syslab.control.policy.message.PolicyMessage;

/**
 * This class encapsulates the notions of sending and receiving messages.
 * 
 * @todo the name "agent" is not really that good; a better name
 *       would be something with communication
 * @todo How do we handle messages that could not be sent? 
 *       Right now, the system stops when sending fails 
 *       (at least for the {@link HttpCommunicationSystem}
 * There are a couple of options:
 * <ul>
 * <li> sendMessage() throws an exception
 * <li> sendMessage() returns a boolean flag
 * <li> sendMessage() accepts an additional parameter retrySendIfFail or so
 * <li> sendMessage() implicitly tries to resend failed messages
 * </ul> 
 * The third and fourth options seem to be the best ones, because they could also be 
 * implemented in the {@link JadeCommunicationSystem}.
 */
public interface AbstractAgent {

  /**
   * Simple setter for the {@link PolicyPartner}
   * that this agent uses. The AbstractAgent needs this
   * information because he has to call the {@link PolicyPartner#action()} 
   * method
   * 
   * @param partner the {@link PolicyPartner} for this agent
   */
  void setPolicyPartner( PolicyPartner partner );

  /** 
   * Simple getter for the {@link AbstractAddress} this agent 
   * listens on
   * 
   * @return address (a.k.a. URL, agent id) of this agent
   */
  AbstractAddress getAddress();

  
  /** 
   * Send a message with the communication system this agent belongs to.
   * @param message The message to be sent
   * @return whether the sending of the message succeeded (as far as the {@link CommunicationSystem} can know that)
   */
  void sendMessage( PolicyMessage message );
  

  /** Returns whether the agent in question has messages to process 
   * 
   * @return whether there are messages in the agent's inbox
   */
  boolean areMessagesAvailable();
  
  /**
   * Receive a message using the communication system this agent belongs to.
   * The method should wait up to one second for a message to arrive.
   * 
   * @return The received message, or NULL if there is none available
   */
  PolicyMessage receiveMessage();

  
  /**
   * Stop this agent. This means that the agent does not take part in the 
   * policy protocol communication anymore.
   */
  void stopAgent();

}
