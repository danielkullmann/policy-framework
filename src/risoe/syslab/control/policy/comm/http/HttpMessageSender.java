package risoe.syslab.control.policy.comm.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import risoe.syslab.comm.lba.LbaSchemeSocketFactory;
import risoe.syslab.control.policy.message.PolicyMessage;

/** This class implements the functionality of sending a message.
 * 
 * This is encapsulated inside a {@link Callable} so that the number of
 * simultaneously occurring http requests can be limited.
 * This was necessary because I got "java.net.SocketException: Too many open files"
 * exceptions. 
 * 
 * @author daku
 */
public class HttpMessageSender implements Runnable {

  private static final Logger logger = Logger.getLogger( HttpMessageSender.class.getName() );

  private static final int N_TRIES = 3;
  
  private HttpCommunicationSystem communicationSystem;
  private PolicyMessage message;
  private URL url;
  private String to;
  private String from;
  private String signedMsgString;

  private DefaultHttpClient httpClient = new DefaultHttpClient();
  
  public HttpMessageSender( HttpCommunicationSystem communicationSystem, PolicyMessage message ) {
    this.communicationSystem = communicationSystem;
    this.message = message;
    this.url = (URL) message.getReceiverAddress().getAddress();
    this.to = message.getReceiverAddress().toString();
    this.from = message.getSenderAddress().toString();
    this.signedMsgString = message.toSignedFipaContent();
    
  }

  @Override
  public void run() {
    for ( int ntry = 0; ntry < N_TRIES; ntry++ ) {
      try {
        send();
        // send() was successful, return
        return;
      } catch ( IOException e ) {
        if ( ntry == N_TRIES-1) {
          logger.log( Level.SEVERE, "failed to send message: " + from + " -> " + to, e );
        } else {
          logger.log( Level.SEVERE, "got exception, retrying: " + from + " -> " + to + " " + e.getMessage() );
        }
      } catch ( Exception e ) {
        throw new AssertionError( e );
      }
    }
  }
    
  public void send() throws Exception {
    // Send message using http client
    HttpEntity entity = null;
    HttpPost request = null;
    try {
      // TODO: Move the socket factory code to some other place
      //       so that it isn't called for every message sending
      LbaSchemeSocketFactory socketFactory = communicationSystem.getSocketFactory();
      if ( socketFactory != null ) {
        Scheme scheme = new Scheme( "http", url.getPort(), socketFactory );
        httpClient.getConnectionManager().getSchemeRegistry().register( scheme  );
      }
      
      request = new HttpPost( url.toString() );

      List<NameValuePair> nvps = new ArrayList<NameValuePair>();
      nvps.add(new BasicNameValuePair( "from", from ) );
      nvps.add(new BasicNameValuePair( "msg", signedMsgString ) );

      request.setEntity( new UrlEncodedFormEntity( nvps, HTTP.ASCII ) );

      HttpParams httpParams = new BasicHttpParams(); 
      httpParams.setParameter( "http.protocol.expect-continue", false );
      httpClient.setParams( httpParams );
      
      HttpResponse response = httpClient.execute( new HttpHost( url.getHost(), url.getPort() ), request );
      entity = response.getEntity();
      
      if ( response.getStatusLine().getStatusCode() != 200 ) {
        logger.log( Level.WARNING, "message could not be sent to " + url + "; rc: " + response.getStatusLine().getStatusCode() );
        BufferedReader br = new BufferedReader( new InputStreamReader( entity.getContent() ) );
        String rStr = "";
        String line = br.readLine();
        while( line != null ) {
          rStr = rStr + line;
          line = br.readLine();
        }
        logger.log( Level.FINER, "response from server: \n" + rStr );
      } else {
        communicationSystem.notifyMessageSent( message );
        logger.log( Level.FINER, from + 
            " sends " + message.getClass().getName() +
            " to " + to );
        logger.log( Level.FINEST, message.toFipaContent() );
      }
      EntityUtils.consume( entity );
    } catch ( Exception e ) {
      if (request != null) request.abort();
      throw e;
    } finally {
      try {
        if ( entity != null ) {
          entity.getContent().close();
        }
      } catch ( Exception e ) { /* ignore */ }
    }
  }

}
