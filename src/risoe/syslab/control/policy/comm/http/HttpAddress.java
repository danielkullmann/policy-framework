/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm.http;

import java.net.URL;

import risoe.syslab.control.policy.comm.AbstractAddress;

/**
 * Implementation of {@link AbstractAddress} for the 
 * {@link HttpCommunicationSystem}.
 * 
 * The class simply encapsulates an URL object.
 */
public final class HttpAddress implements AbstractAddress {

  private URL url;

  
  HttpAddress( URL url ) {
    this.url = url;
  }
  
  
  @Override
  public Object getAddress() {
    return url;
  }

  
  @Override
  public String getName() {
    String path = url.getPath();
    return path.substring( path.lastIndexOf( "/" ) + 1 );
  }

  
  public String getContext() {
    return url.getPath();
  }
  
  
  @Override
  public int compareTo( AbstractAddress o ) {
    return url.toString().compareTo( o.toString() );
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((url == null) ? 0 : url.hashCode());
    return result;
  }

  
  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    HttpAddress other = (HttpAddress) obj;
    if ( url == null ) {
      if ( other.url != null ) {
        return false;
      }
    } else if ( !url.equals( other.url ) ) {
      return false;
    }
    return true;
  }

  
  @Override
  public String toString() {
    return url.toString();
  }
  

}
