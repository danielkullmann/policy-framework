/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm.http;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.comm.lba.LbaSchemeSocketFactory;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.MessageListener;
import risoe.syslab.control.policy.core.PolicyPartner;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.message.PolicyMessage;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * Implementation of {@link CommunicationSystem} for the 
 * HTTP Communication system. 
 * 
 * This class should be instantiated only in an application,
 * because it starts a web server. It is theoretically possible
 * to have more instances, but then you have to specify a different
 * port.
 */
public final class HttpCommunicationSystem implements CommunicationSystem {

  /** logger for this class */
  private static Logger logger = Logger.getLogger( HttpCommunicationSystem.class.getName() );
  
  /** Default port the web server listens for incoming messages */
  public static final int DEFAULT_PORT = 41339;
  
  /** Number of simultaneous HTTP requests that are possible.
   * This is to avoid "java.net.SocketException: Too many open files"
   * exceptions.
   */
  public static final int SIMULTANEOUS_REQUESTS = 10;

  public static boolean SEND_IN_BACKGROUND = true;

  /** container name for this communication endpoint */
  private String containerName;

  /** container name for this communication endpoint */
  private String hostName;
  
  /** List of servers that have been started for this {@link CommunicationSystem}, one for each IP of the host name. */
  ArrayList<HttpServer> servers;
  
  /** list of contexts that have been created on the servers, one for each registered agent. */
  private Collection<String> contexts;

  /** used in {@link #setupServer(String[])} */
  private int numAddresses = 0;
  /** used in {@link #setupServer(String[])} */
  private IOException lastException = null;

  /** list of listeners for message send and receive events */
  private ArrayList<MessageListener> messageListeners = new ArrayList<MessageListener>();

  /** Can be set to test the communication system with certain latency, 
   * bandwidth and availability parameters. */
  private LbaSchemeSocketFactory socketFactory;

  /** port that this comm system listens on */
  private int port = DEFAULT_PORT;
  
  /** executor for sendiung messages.
   *  
   *  package access!
   */
  final ExecutorService messageSender = Executors.newFixedThreadPool( SIMULTANEOUS_REQUESTS );


  /**
   * Constructor for this class, giving a container name and a host name.
   * The web server listens on all IP addresses of the given hostname
   * @param containerName the container name may contain "/" characters,
   *        but any superfluous "/" at the start and end off the string are removed
   * @param hostName the hostname can specify a port using the normal "hostname:345"
   *        syntax. If none is given, the {@link #DEFAULT_PORT} ({@value #DEFAULT_PORT})
   *        is used.
   */
  public HttpCommunicationSystem( String containerName, String hostName ) {
    
    containerName = fixContainerName( containerName );
    hostName = fixHostName( hostName );
    
    this.containerName = containerName;
    this.hostName = hostName;
    this.contexts = new ArrayList<String>();
    this.servers = new ArrayList<HttpServer>();
    
    setupServer( null );
  }


  public HttpCommunicationSystem( String containerName ) {
    this( containerName, (String[]) null );
  }

  /**
   * @param containerName
   * @param additionalAddresses additional addresses to listen on
   */
  public HttpCommunicationSystem( String containerName, String[] additionalAddresses ) {
    containerName = fixContainerName( containerName );
    this.containerName = containerName;
    this.hostName = fixHostName("localhost");
//    try {
//      this.hostName = fixHostName( InetAddress.getLocalHost().getHostName() );
//    } catch ( UnknownHostException e ) {
//      throw new AssertionError( e );
//    }
    this.contexts = new ArrayList<String>();
    this.servers = new ArrayList<HttpServer>();
   
    setupServer( additionalAddresses );
   
  }


  /**
   * @param hostName name of the host; can also be a URL (to give port information)
   * @param containerName name of the container 
   * @param additionalAddresses additional address to listen on
   */
  public HttpCommunicationSystem( String hostName, String containerName, String[] additionalAddresses ) {
    containerName = fixContainerName( containerName );
    this.containerName = containerName;
    this.hostName = fixHostName( hostName );
    this.contexts = new ArrayList<String>();
    this.servers = new ArrayList<HttpServer>();
   
    setupServer( additionalAddresses );
   
  }

  /**
   * @param hostName name of the host; can also be a URL (to give port information)
   * @param containerName name of the container 
   * @param additionalAddresses additional address to listen on
   */
  public HttpCommunicationSystem( int port, String containerName, String[] additionalAddresses ) {
    containerName = fixContainerName( containerName );
    this.port = port;
    this.containerName = containerName;
    this.hostName = fixHostName("localhost");
//    try {
//      this.hostName = fixHostName( InetAddress.getLocalHost().getHostName() );
//    } catch ( UnknownHostException e ) {
//      throw new AssertionError( e );
//    }
    this.contexts = new ArrayList<String>();
    this.servers = new ArrayList<HttpServer>();
   
    setupServer( additionalAddresses );
   
  }

  /**
   * Method called by the constructores to setup the web servers.
   * @param additionalAddresses 
   */
  private final void setupServer(String[] additionalAddresses) {
    try {
      URL url = new URL( hostName );
      if ( port == DEFAULT_PORT && url.getPort() > 0 ) {
        this.port = url.getPort();
      }
      String host = url.getHost();
      
      // Fallback handler for all requests to the web server
      // that we do not recognize as agent URLS.
      DefaultHandler defaultHandler = new DefaultHandler();
      
      InetAddress[] addresses = InetAddress.getAllByName( host );
      
      // This special treatment for syslab-00 is necessary because
      // InetAddress#getAllByName(String) does not return all IP addresses.
      // I don't know why that does not work
      if ( host.equals( "syslab-00" ) ) {
        addresses = new InetAddress[3];
        addresses[0] = InetAddress.getByAddress( new byte[] {(byte)192,(byte)168,0,1} );
        addresses[1] = InetAddress.getByAddress( new byte[] {10,0,17,(byte) 186} );
        addresses[2] = InetAddress.getByAddress( new byte[] {(byte) 172,16,0,1} );
      }
      
      // Create a webserver for every IP address that tthe hostname resolves to
      numAddresses = 0;
      lastException = null;
      for ( int i = 0; i < addresses.length; i++ ) {
        setupServerAtAddress(port, defaultHandler, addresses[i] );
      }
      
      if ( additionalAddresses != null ) {
        for ( int j = 0; j < additionalAddresses.length; j++ ) {
          addresses = InetAddress.getAllByName( additionalAddresses[j] );
          for ( int i = 0; i < addresses.length; i++ ) {
            setupServerAtAddress(port, defaultHandler, addresses[i] );
          }
        }
      }
      
      // If no address could be bound..
      if ( numAddresses == 0 && lastException != null ) {
        throw lastException;
      }
      
    } catch ( MalformedURLException e ) {
      throw new AssertionError( e );
    } catch ( IOException e ) {
      throw new AssertionError( e );
    }
  }


  private void setupServerAtAddress(int port, DefaultHandler defaultHandler, InetAddress address ) throws IOException {
    logger.log( Level.FINE, "bind to: " + address.getHostAddress() + ":" + port );
    InetSocketAddress inetaddress = new InetSocketAddress( address.getHostAddress(), port );
    try {
      HttpServer server = HttpServer.create( inetaddress, -1 );
      server.createContext( "/", defaultHandler );
      servers.add( server );
      numAddresses += 1;
    } catch (BindException e) {
      logger.log( Level.SEVERE, "Could not bind to " + address.getHostAddress() + ":" + port );
      lastException = e;
    }
  }


  @Override
  public AbstractAddress createAddress( String agentName ) {
    return createAddress( agentName, containerName, hostName );
  }

  
  /** Creates an address from a URL.
   * 
   * The URL looks like this: http://lp-012756:41339/syslab-demo-server
   * 
   * @param url
   * @return
   */
  @Override
  public AbstractAddress createAddressFromFullName(String fullName) {
    
    URL url;
    try {
      url = new URL(fullName);
    } catch ( MalformedURLException e ) {
      throw new IllegalArgumentException(e);
    }
    
    String path = url.getPath();
    while ( path.endsWith( "/") ) path = path.substring(0, path.length()-1 );
    int idx = path.lastIndexOf("/");
    
    String containerName = "";
    if ( idx > 0 ) containerName = path.substring( 0, idx-1 );
    
    String agentName = path.substring( idx+1 );
    
    int port = url.getPort();
    if ( port < 0 ) port = DEFAULT_PORT;
    
    String hostName = url.getHost() + ":" + port ;
    
    return createAddress(agentName, containerName, hostName);
  }


  @Override
  public AbstractAddress createAddress( String agentName, String containerName, String hostName ) {
    try {
      
      containerName = fixContainerName( containerName );
      hostName = fixHostName( hostName );

      if ( containerName.equals( "" ) ) {
        return new HttpAddress( new URL( hostName + "/" + agentName ) );
      }
      
      return new HttpAddress( new URL( hostName + "/" + containerName + "/" + agentName ) );

    } catch ( MalformedURLException e ) {
      throw new AssertionError( e );
    }
  }
  

  @Override
  public AbstractAgent createandStartAgent( AbstractAddress address, PolicyPartner partner ) {
    HttpAgent agent = new HttpAgent( (URL) address.getAddress(), this );
    agent.setPolicyPartner( partner );
    partner.setAgent( agent );

    // Tell webserver to handle agent url
    String ctx = ((HttpAddress) address).getContext();
    AgentHandler agentHandler = new AgentHandler( agent );
    
    // Check for already existing context
    if ( contexts.contains( ctx ) ) {
      throw new IllegalArgumentException( "Client with this path exists already: " + ctx );      
    }
    
    for ( HttpServer server : servers ) {
      server.createContext( ctx, agentHandler );
    }

    contexts.add( ctx );
    logger.log(  Level.FINER, "handling context: " + address );
    
    return agent;
  }

  
  @Override
  public void shutdown() {
    
    for ( String ctx : contexts ) {
      for ( HttpServer server : servers ) {
        server.removeContext( ctx );
      }
    }
    for ( HttpServer server : servers ) {
      server.stop( 1 );
    }
    
    messageSender.shutdownNow();
  }
  

  @Override
  public void startup() {
    for ( HttpServer server : servers ) {
      server.start();
    }
  }


  /** 
   * Internal method that fixes the given hostname so that it is exactly
   * as we want it to have: http://hostname:41339.
   * 
   * @param hostName host name as it was given from outside
   * @return the fixed host name
   */
  private String fixHostName( String hostName ) {
    if ( hostName.matches("^:[0-9]+$") ) {
      hostName = "localhost" + hostName;
//      try {
//        hostName = InetAddress.getLocalHost().getHostName() + hostName;
//      } catch (UnknownHostException e) {
//        throw new AssertionError( e );
//      }
    }
    while ( hostName.endsWith( "/" ) ) hostName = hostName.substring( 0, hostName.length()-1 );
    if ( ! hostName.startsWith( "http://" ) && ! hostName.startsWith( "https://" ) ) {
      hostName = "http://" + hostName;
    }
    if ( ! hostName.substring( 6 ).contains( ":" ) ) {
      hostName = hostName + ":" + port;
    }
    return hostName;
  }


  /**
   * Internal mnethod that fixes the container name so tyhat it is exactly as we want it to be:
   * without leading and trailing slash "/" characters.
   * @param containerName container name as it was given from outside
   * @return the fixed container name
   */
  private String fixContainerName( String containerName ) {
    while ( containerName.startsWith( "/" ) ) containerName = containerName.substring( 1 );
    while ( containerName.endsWith( "/" ) ) containerName = containerName.substring( 0, containerName.length()-1 );
    return containerName;
  }

  
  /**
   * Class handling message sending requests to agent URLS
   */
  private class AgentHandler implements HttpHandler {

    /** agent that is handled by this handler */
    private HttpAgent agent;
    
    /** Constructor for this class
     * @param agent agent that gets the messages that this henalder receives
     * @param communicationSystem 
     */
    public AgentHandler( HttpAgent agent ) {
      this.agent = agent;
    }

    
    @Override
    public void handle( HttpExchange exchg ) throws IOException {

      InputStreamReader isr = new InputStreamReader( exchg.getRequestBody() );
      BufferedReader br = new BufferedReader( isr );
      try { 
        // Read request body into a string
        String body = "";
        String bodyLine = br.readLine();
        while ( bodyLine != null ) {
          body = body + bodyLine;
          bodyLine = br.readLine();
        }

        // Convert encoded request parameters into a HashMap
        HashMap<String,String> params = new HashMap<String, String>();
        StringTokenizer tk1 = new StringTokenizer( body, "&" );
        while ( tk1.hasMoreTokens() ) {
          String token1 = tk1.nextToken();
          int splitIndex = token1.indexOf( "=" );
          if ( splitIndex < 0 ) {
            params.put( token1, "" );
          } else {
            String key = token1.substring( 0, splitIndex );
            String value = token1.substring( splitIndex+1 );
            params.put( key, URLDecoder.decode( value, "UTF-8" ) );
          }
        }
        
        // Try to build a 
        String sender = params.get( "from" );
        String msg = params.get( "msg" );
        
        if ( sender != null && msg != null ) {
          PolicyMessage pmsg = PolicyUtils.parseMessage( new HttpAddress( new URL( sender ) ), agent.getAddress(), msg, agent.getPartner() );
          if ( pmsg == null ) {
            logger.log( Level.FINER, "==== message in request could not be parsed ==== " );
            logger.log( Level.FINER, agent.getAddress().getName() );
            logger.log( Level.FINER, exchg.getRequestURI().getQuery() );
            for ( String key : params.keySet() ) {
              logger.log( Level.FINER, key + ": " + params.get(key) );
            }
          } else {
            agent.putMessage( pmsg );
            notifyMessageReceived( pmsg );
          }
        } else {
          logger.log( Level.FINER, "==== unknown request received ==== " );
          logger.log( Level.FINER, agent.getAddress().getName() );
          logger.log( Level.FINER, exchg.getRequestURI().getQuery() );
          for ( String key : params.keySet() ) {
            logger.log( Level.FINER, key + ": " + params.get(key) );
          }
        }
        
        exchg.sendResponseHeaders( 200, -1 );
      } catch( Throwable t ) {
        t.printStackTrace();
      } finally {
        br.close();
        isr.close();
        exchg.getRequestBody().close();
        exchg.close();
      }
    }


  }

  
  /**
   * Class handling all requests that can't be mapped to an agent URL
   */
  private class DefaultHandler implements HttpHandler {

    @Override
    public void handle( HttpExchange exchg ) throws IOException {

      if ( exchg.getRequestURI().getPath().equals("/")) {
        try {
          exchg.sendResponseHeaders(200, 0 );
          OutputStream os = exchg.getResponseBody();
          BufferedWriter bw = new BufferedWriter( new OutputStreamWriter( os ) );
          bw.write("<html><head><title>Registered Agents</title></head><body><h1>Registered Agents</h1><ul>");
          for (String context : HttpCommunicationSystem.this.contexts ) {
            bw.write( "<li> " + context + "</li>");
          }
          bw.write("</ul></body></html>");
          bw.flush();
          os.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
      } else {
        logger.log( Level.WARNING, "server does not handle " + exchg.getRequestURI() );
        exchg.sendResponseHeaders( 404, -1 );
      }
      exchg.close();
    }
    
  }


  @Override
  public void addMessageListener(MessageListener listener) {
    messageListeners.add( listener );
  }

  @Override
  public void notifyMessageReceived( PolicyMessage msg ) {
    for (MessageListener listener : messageListeners) {
      listener.messageReceived( msg );
    }
  }
  
  @Override
  public void notifyMessageSent( PolicyMessage msg ) {
    for (MessageListener listener : messageListeners) {
      listener.messageSent( msg );
    }
  }


  public LbaSchemeSocketFactory getSocketFactory() {
    return socketFactory;
  }


  public void setSocketFactory( LbaSchemeSocketFactory socketFactory ) {
    this.socketFactory = socketFactory;
  }

}
