/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm.http;

import java.net.URL;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.core.PolicyPartner;
import risoe.syslab.control.policy.message.PolicyMessage;

/**
 * Implementation of the {@link AbstractAgent} class for the 
 * {@link HttpCommunicationSystem}.
 * This class handles the sending and receiving of messages.
 * The receiving of messages is done by using the web server started
 * by the {@link HttpCommunicationSystem} instance. 
 */
public final class HttpAgent implements AbstractAgent, Runnable {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( HttpAgent.class.getName() );

  private static final long ACTION_SLEEP_MS = 50;
  
  private URL url;
  private Queue<PolicyMessage> inbox;
  private PolicyPartner partner;
  private Thread thread;

  private HttpCommunicationSystem communicationSystem;

  
  /**
   * Constructor to use for creating an instance.
   * This method is package-private!
   * @param url URL where the agent should be reached
   */
  HttpAgent( URL url, CommunicationSystem communicationSystem ) {
    this.url = url;
    this.inbox = new LinkedBlockingQueue<PolicyMessage>();
    this.communicationSystem = (HttpCommunicationSystem) communicationSystem;
  }

  
  @Override
  public AbstractAddress getAddress() {
    return new HttpAddress( url );
  }

  
  @Override
  public synchronized PolicyMessage receiveMessage() {
    // When there is no message available, we wait up to one second
    // TODO Return immediately, and let someone else handle busy loops
    if ( inbox.isEmpty() ) {
      try {
        Thread.sleep(1000);
      } catch ( InterruptedException e ) {
        // TODO Alternative: let receiveMessage() throw InterruptedException 
        Thread.currentThread().interrupt();
        return null;
      }
    }
    if ( inbox.isEmpty() ) return null;
    PolicyMessage msg = inbox.poll();
    logger.log( Level.FINER, msg.getReceiverAddress().getName() + 
        " received " + msg.getClass().getName() +
        " from " + msg.getSenderAddress().getName() );
    logger.log( Level.FINEST, msg.toFipaContent() );
    return msg;
  }

  
  @Override
  public void sendMessage( PolicyMessage message ) {
    if (HttpCommunicationSystem.SEND_IN_BACKGROUND) {
      communicationSystem.messageSender.execute( new HttpMessageSender( communicationSystem, message ) );
    } else {
      new HttpMessageSender( communicationSystem, message ).run();
    }
  }
  
  
  /** 
   * This is an internal method, therefore it has package access!
   * 
   * @return
   */
  PolicyPartner getPartner() {
    return partner;
  }


  
  @Override
  public void setPolicyPartner( PolicyPartner partner ) {
    this.partner = partner;
    if ( thread != null ) {
      thread.interrupt();
      thread = null;
    }
    thread = new Thread( this );
    thread.setDaemon( true );
    thread.start();
  }

  
  @Override
  public void stopAgent() {
    // Stop thread calling partner.action()
    thread.interrupt();
  }

  
  /**
   * Method that is used by {@link HttpCommunicationSystem.AgentHandler#handle(com.sun.net.httpserver.HttpExchange)}
   * to put a received message into the inbox of the {@link PolicyPartner}.
   * 
   * This method is package-private!
   * 
   * @param msg message that was received
   */
  synchronized void putMessage( PolicyMessage msg ) {
    inbox.add( msg );
//    if (inbox.size() > 3 ) {
//      logger.log( Level.WARNING, "inbox gets large: " + this.getAddress().getName() + " " + inbox.size() );
//    }
  }


  /**
   * This method executes the action of its partner, until
   * the thread is stopped
   * 
   * @see Runnable#run()
   */
  @Override
  public void run() {
    Thread.currentThread().setName( "HttpAgent " + url );
    while ( ! partner.done() && ! Thread.interrupted() ) {
      partner.action();
      try {
        if ( inbox.size() == 0 ) {
          Thread.sleep( ACTION_SLEEP_MS );
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        break;
      }
    }
  }


  @Override
  public boolean areMessagesAvailable() {
    return ! inbox.isEmpty();
  }
  
}
