package risoe.syslab.control.policy.comm;

import risoe.syslab.control.policy.message.PolicyMessage;

/**
 * This interface should be implemented when the sending and 
 * receiving of messages should be logged.
 * 
 * @author daku
 */
public interface MessageListener {

  void messageSent( PolicyMessage message );
  void messageReceived( PolicyMessage message );
}
