/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.comm;

/**
 * Address of a communication partner in a policy protocol.
 * 
 * This abstract class encapsulates the address that is actually used
 * by the Communication System. For JADE, this is the agent id, for HTTP
 * this is a URL.
 */
public interface AbstractAddress extends Comparable<AbstractAddress> {

  /**
   * Get the local name of this address.
   * This name is used for looking up the private and public keys for message 
   * signing and for logging.
   * 
   * @return the local name
   */
  String getName();
  
  /**
   * Get the actual underlying address object that this abstract address encapsulates.
   * @return the actual address object
   */
  Object getAddress();

}
