package risoe.syslab.control.policy.vis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import risoe.syslab.gui.wall.displets.flexhouse.TimeSeries;

/**
 * Visualisation of policy controller behaviour. Three plots are shown:
 * <ul>
 * <li>Overall power consumption
 * <li>system frequency and thresholds
 * <li>power price and thresholds
 * <ul>
 * 
 * @author olge
 */

@SuppressWarnings("serial")
public class PolicyControllerVisualisation extends JFrame implements KeyListener, Runnable {

  private static final long REPAINT_TIME = 5000; // ms
  private static final int NUM_PLOTS = 3;

  private PlotCanvas[] plots = new PlotCanvas[NUM_PLOTS];
  private ControllerOutputMonitor controller = new ControllerOutputMonitor();

  private boolean isRunning;

  /** Default constructor */
  public PolicyControllerVisualisation() {
    this("Policy controller visualisation");
  }

  /** Constructor to give a window title */
  public PolicyControllerVisualisation(String title) {
    super(title);

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int xSize = (int) screenSize.getWidth();
    int ySize = (int) screenSize.getHeight();
    boolean bogusSize = (xSize == 3360 && ySize == 1200);
    if (bogusSize) {
      setSize(1024, 768);
    } else {
      setSize(xSize, ySize);
    }

    if (!bogusSize)
      setUndecorated(true);
    getContentPane().setPreferredSize(screenSize);
    getContentPane().setSize(screenSize);
    addKeyListener(this);
    if (!bogusSize)
      setExtendedState(JFrame.MAXIMIZED_BOTH);

    Container mainPanel = getContentPane();
    mainPanel.setLayout(new BorderLayout());
    mainPanel.setLayout(new GridLayout(NUM_PLOTS, 1));

    plots[0] = new PlotCanvas();
    plots[0].addKeyListener(this);
    // plots[0].setTitle("Model Predictive Control");
    plots[0].setLabelX("Time");
    plots[0].setLabelY("Power Consumption");
    //plots[0].setPlotColor(0, Color.GREEN);
    mainPanel.add(plots[0]);

    plots[1] = new PlotCanvas();
    plots[1].addKeyListener(this);
    plots[1].setLabelX("Time");
    plots[1].setLabelY("System frequency & upper/lower threshold");
    plots[1].setBarTypePlot(0, false);
    //plots[1].setPlotColor(0, Color.GREEN);
    mainPanel.add(plots[1]);

    plots[2] = new PlotCanvas();
    plots[2].addKeyListener(this);
    plots[2].setLabelX("Time");
    plots[2].setLabelY("Power price & upper/lower threshold");
    plots[2].setBarTypePlot(0, false);
    //plots[2].setPlotColor(0, Color.GREEN);
    mainPanel.add(plots[2]);

    setContentPane(mainPanel);

    pack();
    setVisible(true);

    new Thread(this).start();
  }

  /** Get new data, and return true when new data has arrived */
  private boolean shouldUpdate() {
    if (controller.hasChanged()) {
      plots[0].setTimeseries(controller.getPowerConsumption());
      TimeSeries[] uf = controller.getUpperFrequencyThreshold();
      TimeSeries[] lf = controller.getLowerFrequencyThreshold();
      TimeSeries[] up = controller.getUpperPriceThreshold();
      TimeSeries[] lp = controller.getLowerPriceThreshold();
      TimeSeries[] plot1 = new TimeSeries[1 + uf.length + lf.length];
      TimeSeries[] plot2 = new TimeSeries[1 + up.length + lp.length];
      for (int i = 0; i < uf.length; i++) {
        plot1[i] = uf[i];
        plot1[uf.length + i] = lf[i];
      }
      for (int i = 0; i < up.length; i++) {
        plot2[i] = up[i];
        plot2[up.length + i] = lp[i];
      }
      plot1[uf.length + up.length] = controller.getSystemFrequency();
      plot2[uf.length + up.length] = controller.getPowerPrice();
      plots[1].setTimeseries(plot1);
      plots[2].setTimeseries(plot2);

      for ( int i=0; i<uf.length; i++ ) plots[1].setPlotColor(i, Color.BLUE);
      for ( int i=uf.length; i<2*uf.length; i++ ) plots[1].setPlotColor(i, Color.RED);
      plots[1].setPlotColor(uf.length+up.length, Color.GREEN);

      for ( int i=0; i<up.length ; i++ ) plots[2].setPlotColor(i, Color.BLUE);
      for ( int i=up.length ; i<2*up.length; i++ ) plots[2].setPlotColor(i, Color.RED);
      plots[2].setPlotColor(uf.length+up.length, Color.GREEN);
      
      return true;
    }
    return false;
  }

  @Override
  public void run() {
    isRunning = true;

    for (PlotCanvas plot : plots) {
      plot.repaint();
    }

    while (isRunning) {
      try {
        Thread.sleep(REPAINT_TIME);
      } catch (Exception e) {
        Thread.currentThread().interrupt();
        isRunning = false;
        break;
      }
      if (shouldUpdate()) {
        for (PlotCanvas plot : plots) {
          plot.repaint();
        }
      }
    }
  }

  /** Stop the thread doing the updating */
  public void kill() {
    isRunning = false;
  }

  @Override
  public void keyPressed(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_ESCAPE && JOptionPane.showConfirmDialog(this, "Do you want to quit?", "EXIT", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
      System.exit(0);
    }
  }

  @Override
  public void keyReleased(KeyEvent arg0) {
    // not used..
  }

  @Override
  public void keyTyped(KeyEvent arg0) {
    // not used..
  }

  /** Entry point for Java application; shows the window */
  public static void main(String[] args) {
    try {
      new PolicyControllerVisualisation();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
