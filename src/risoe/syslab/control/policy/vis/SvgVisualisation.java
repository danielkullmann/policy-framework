package risoe.syslab.control.policy.vis;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.swing.svg.SVGLoadEventDispatcherAdapter;
import org.apache.batik.swing.svg.SVGLoadEventDispatcherEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;

import risoe.syslab.gui.JSVGIgnoreViewBoxCanvas;
import risoe.syslab.gui.wall.AbstractDisplet;
import risoe.syslab.gui.wall.AbstractSelectlet;
import risoe.syslab.gui.wall.AbstractSelectletStub;

/** Try to make a SVG-based visualisation of devices.
 * 
 * Did not manage to get it to work correctly.
 * 
 * @author daku
 */
@SuppressWarnings("serial")
public class SvgVisualisation extends AbstractDisplet implements EventListener,
    Runnable {

  private volatile JSVGCanvas canvas;
//  private volatile Window window;
  private volatile Document document;
  private HashMap<String, Device> devices;
  private Thread myThread;

  private static final String DEVICE_ID = "g3794";

  private volatile int elCounter = 1;
  
  /** Default constructor */
  public SvgVisualisation() {
    super();
    devices = new HashMap<String, SvgVisualisation.Device>();
  }

  @Override
  public void handleEvent(Event evt) {
    // TODO nothing to do here yet..
  }

  @Override
  public void run() {
    int num = 1;
    while (! Thread.interrupted()) {
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        break;
      }
      addDevice( "id" + num, "Device " + num, "status " + num );
      num++;
    }
  }

  /** Add a new device to be displayed */
  private void addDevice(final String id, final String title,
      final String status) {
    Device dev = new Device(id, title, status);
    devices.put(id, dev);
    if (canvas != null) {
      try {
        canvas.getUpdateManager().getUpdateRunnableQueue().invokeAndWait(new AddDevice(dev));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    } else {
      // TODO put on waiting list
    }
  }

  @Override
  public AbstractSelectletStub getSelectletStub() {
    return null;
  }

  @Override
  public void paintDisplet(Graphics2D g) {
    // may be empty; JSVGCanvas handles this
  }

  @Override
  public String getDispletName() {
    return "Policy Visualisation";
  }

  @Override
  public String gainedFocus() {
    setLayout(new BorderLayout());
    canvas = new JSVGIgnoreViewBoxCanvas();
//    canvas = new JSVGCanvas();
    KeyHandler keyHandler = new KeyHandler();

    this.add(canvas);

    // This does not work:
    this.addKeyListener(keyHandler);
    canvas.addKeyListener(keyHandler);
    for (Component c : canvas.getComponents()) {
      c.addKeyListener(keyHandler);
    }

    // Forces the canvas to always be dynamic even if the current
    // document does not contain scripting or animation.
    canvas.setDocumentState(JSVGCanvas.ALWAYS_DYNAMIC);
    canvas
        .addSVGLoadEventDispatcherListener(new SVGLoadEventDispatcherAdapter() {
          @Override
          public void svgLoadEventDispatchStarted(SVGLoadEventDispatcherEvent e) {
            // At this time the document is available...
            document = canvas.getSVGDocument();
            // ...and the window object too.
            /*window =*/ canvas.getUpdateManager().getScriptingEnvironment().createWindow();
            // Registers the listeners on the document
            // just before the SVGLoad event is
            // dispatched.
            registerListeners();
            setupDocument();
            myThread = new Thread( SvgVisualisation.this );
            myThread.start();
          }
        });
    // Set the JSVGCanvas listeners.
    try {
      File f = new File("data/policy-vis.svg");
      canvas.setURI(f.toURI().toString());
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return null;
  }

  private void registerListeners() {
    // TODO implement when needed..

  }

  private void setupDocument() {

//    AffineTransform tf = canvas.getRenderingTransform();
//    tf.translate(-tf.getTranslateX(), -tf.getTranslateY());
//    tf.scale(0.5,0.5);
//    canvas.setRenderingTransform(tf);

    canvas.setRecenterOnResize(true);
    
    try {
      canvas.getUpdateManager().getUpdateRunnableQueue().invokeLater(new Runnable() {

            @Override
            public void run() {
              try {
                document.getElementById(DEVICE_ID).setAttribute("opacity", "0");
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          });
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void lostFocus() {
    myThread.interrupt();
    myThread = null;
    removeAll();
    if (canvas != null) {

      try {
        canvas.stopProcessing();
        canvas.dispose();
        canvas.setURI(null);
      } catch (Exception e) {
        e.printStackTrace();
      }
      canvas = null;
    }

    document = null;
//    window = null;
  }

  @Override
  public Class<? extends AbstractSelectlet> getSelectletClass() {
    return null;
  }

  private class KeyHandler implements KeyListener {

    @Override
    public void keyTyped(KeyEvent e) {
      // ignore
    }

    @Override
    public void keyPressed(KeyEvent e) {
      if (e.getKeyCode() == 0x27) {
        System.exit(0);
      }
    }

    @Override
    public void keyReleased(KeyEvent e) {
      // ignore
    }

  }

  /** Container class for device data */
  private class Device {
    private String id;
    private String title;
    private String status;
    private String svgId;

    
    /** Standard constructor, giving data for all fields except the svg id
     * @param id
     * @param title
     * @param status
     */
    public Device(String id, String title, String status) {
      super();
      this.id = id;
      this.title = title;
      this.status = status;
    }

    /** Getter for id field */
    public String getId() {
      return id;
    }

    /** Getter for title field */
    public String getTitle() {
      return title;
    }

    /** Getter for status field */
    public String getStatus() {
      return status;
    }

    /** Getter for svg id field */
    public String getSvgId() {
      return svgId;
    }

//    public void setId(String id) {
//      this.id = id;
//    }
//
//    public void setTitle(String title) {
//      this.title = title;
//    }
//
//    public void setStatus(String status) {
//      this.status = status;
//    }

    /** Setter for svg id */
    public void setSvgId(String svgId) {
      this.svgId = svgId;
    }

  } // class Device

  /** CLass that provides the functionality to add a device to the svg DOM */
  private class AddDevice implements Runnable {

    private Device dev;

    /** Standard constructor
     * 
     * @param dev data for new device 
     */
    public AddDevice(Device dev) {
      super();
      this.dev = dev;
    }

    @Override
    public void run() {

      try {
        if ( document.getElementById( dev.getId() ) != null ) return;
        
        Element sourceElement = document.getElementById(DEVICE_ID);
        Element el = (Element) sourceElement.cloneNode(true);
        el.setAttribute("opacity", "100");
        sourceElement.getParentNode().appendChild( el );
        NodeList chs = el.getElementsByTagName("text");
        for (int i = 0; i < chs.getLength(); i++) {
          Element ch = (Element) chs.item(i);
          //System.out.println( ch.getLocalName() );
          if (ch.getTextContent().startsWith("Device")) {
            ch.setTextContent( dev.getTitle() );
          } else if (ch.getTextContent().startsWith("Some")) {
            ch.setTextContent( dev.getStatus() );
          }
        }
        
        String svgId = "g48" + elCounter++;
        el.setAttribute("id", svgId);
        dev.setSvgId(svgId);
        
        layout();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    /** layout all existing devices in the svg dom */
    private void layout() {
      Collection<Device> devc = devices.values();
      List<Device> devs;
      if ( devc instanceof List<?> ) {
        devs = (List<SvgVisualisation.Device>) devc;
      } else {
        devs = new ArrayList<SvgVisualisation.Device>( devc );
      }
      Collections.sort(devs, new DeviceComparator());
      int rows = (int) Math.sqrt( devs.size() );
      if ( rows == 0 ) rows = 1;
      int cols = devs.size() / rows + (devs.size() % rows != 0 ? 1 : 0 );
      //System.out.println( "grid: " + cols + "x" + rows );
      int row = 0;
      int col = 0;

      Element ul = document.getElementById("rect1");
      Element lr = document.getElementById("rect3");
      String style = ul.getAttribute("style");
      style.replace("color:#000000", "color:#600000");
      ul.setAttribute("style", style );
      double x0 = Double.parseDouble( ul.getAttribute("x") );
      double y0 = Double.parseDouble( ul.getAttribute("y") );
      double x1 = Double.parseDouble( lr.getAttribute("x") );
      double y1 = Double.parseDouble( lr.getAttribute("y") );
      double elWidth = (x1-x0) / cols;
      double elHeight = (y1-y0) / rows;
      
      //System.out.println( "dim " + x0 + ", " + y0 + " : " + elWidth + ", " + elHeight + 
      //    " " + (int) (cols*elWidth) + ", " + (int) (rows*elHeight) );
      canvas.setBounds( new Rectangle( 0, 0, (int) (cols*elWidth), (int) (rows*elHeight) ) );
      
      for (Device device : devs) {
        Element el = document.getElementById( device.getSvgId());
        double x = x0+col*elWidth;
        double y = y0+row*elHeight;
        el.setAttribute("transform", "translate(" + x + "," + y + ")" );
        //System.out.println( "el: " + el.getAttribute("id") + " " + col + " : " + row + " | " + x + " : " + y );
        col++;
        if ( col >= cols ) {
          row++;
          col = 0;
        }
      }
      
      document.getDocumentElement().setAttribute("width", ""+(cols+1)*elWidth);
      document.getDocumentElement().setAttribute("height", ""+(rows+1)*elHeight);
      
    } // layout()
    
  } // class AddDevice

  /** Comparator class to compare {@link Device} instances by title */
  private class DeviceComparator implements Comparator<Device> {

    @Override
    public int compare(Device o1, Device o2) {
      return o1.getTitle().compareToIgnoreCase(o2.getTitle());
    }
    
  } // class DeviceComparator
  
}
