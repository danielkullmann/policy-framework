package risoe.syslab.control.policy.vis;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.demo.policy.heatercontrol.control.HeaterDataAPI;
import risoe.syslab.demo.policy.heatercontrol.control.RoomInformation;
import risoe.syslab.gui.wall.displets.flexhouse.Measurement;
import risoe.syslab.gui.wall.displets.flexhouse.TimeSeries;

/** 
 * Wrapper class around {@link HeaterDataAPI} server to get at the data
 * provided by the RMI service.
 * 
 * @author olge
 */
public class ControllerOutputMonitor implements Runnable {
  
  private final static Logger logger = Logger.getLogger( ControllerOutputMonitor.class.getName() );
  
  /** Length of {@link TimeSeries} here. */
  private static final int SERIES_LENGTH = 24*3600;

  /** server that provides the data */
	private HeaterDataAPI server = null;
	
	/** URI of server */
  private final String serverURI;
  private boolean running;
  private TimeSeries powerConsumption;
  private TimeSeries systemFrequency;
  private TimeSeries powerPrice;
  private TimeSeries[] upperFreqBounds;
  private TimeSeries[] lowerFreqBounds;
  private TimeSeries[] upperPriceBounds;
  private TimeSeries[] lowerPriceBounds;
  private int numRooms;
  private Thread myThread;
  
  /** Default constructor. Connects to {@link ControllerStateRMIServer} */
	public ControllerOutputMonitor() {
		serverURI = "//flexhouse:1099/" + HeaterDataAPI.SERVICE_NAME;
		checkServer();
		try {
      numRooms=server.getNumberOfRooms();
    } catch (RemoteException e) {
      e.printStackTrace();
      System.exit(0);
    }
    powerConsumption=new TimeSeries(SERIES_LENGTH);
    systemFrequency=new TimeSeries(SERIES_LENGTH);
    powerPrice=new TimeSeries(SERIES_LENGTH);

    upperFreqBounds=new TimeSeries[numRooms];
    lowerFreqBounds=new TimeSeries[numRooms];
    upperPriceBounds=new TimeSeries[numRooms];
    lowerPriceBounds=new TimeSeries[numRooms];
    for (int i=0;i<numRooms;i++) {
      upperFreqBounds[i]=new TimeSeries(SERIES_LENGTH);
      lowerFreqBounds[i]=new TimeSeries(SERIES_LENGTH);
      upperPriceBounds[i]=new TimeSeries(SERIES_LENGTH);
      lowerPriceBounds[i]=new TimeSeries(SERIES_LENGTH);
    }
    myThread=new Thread(this);
    myThread.start();
	}
	
  /** Tries to get a connection to the server */
  private void checkServer() {
    if ( server != null ) return;
    try {
			logger.log( Level.CONFIG, "trying to acquire server interface: " + serverURI );
			Object o = Naming.lookup( serverURI );
			server = (HeaterDataAPI) o;
		} catch ( MalformedURLException e ) {
			e.printStackTrace();
		} catch ( RemoteException e ) {
      e.printStackTrace();
		} catch ( NotBoundException e ) {
      e.printStackTrace();
		}
  }
  
  @Override
  public void run() {
    running=true;
    while (running) {
      if ( server == null ) checkServer();
      if ( server != null ) {
        try {
          powerConsumption.addMeasurement(new Measurement(server.getPowerConsumption()));
          double frequency = server.getFrequency();
          if ( Math.abs(frequency) < 0.1 ) frequency = Double.NaN;
          if ( ! Double.isNaN(frequency) ) systemFrequency.addMeasurement(new Measurement(frequency));
          double price = server.getPowerPrice();
          if ( Math.abs(price) < 0.1 ) price = Double.NaN;
          if ( ! Double.isNaN(price) ) powerPrice.addMeasurement(new Measurement(price));
          RoomInformation[] ri=server.getRoomData();
          for (int i=0;i<ri.length;i++) {
            if ( ri[i].getFreqCtrl() != null ) {
              upperFreqBounds[i].addMeasurement(new Measurement(ri[i].getFreqCtrl().getHigh()));
              lowerFreqBounds[i].addMeasurement(new Measurement(ri[i].getFreqCtrl().getLow()));
            }
            if ( ri[i].getPriceCtrl() != null ) {
              upperPriceBounds[i].addMeasurement(new Measurement(ri[i].getPriceCtrl().getHigh()));
              lowerPriceBounds[i].addMeasurement(new Measurement(ri[i].getPriceCtrl().getLow()));
            }
          }
        }
        catch (RemoteException e) {
          e.printStackTrace();
          try {
            // Wait for a short period (100ms)..
            try { Thread.sleep(100); } catch (InterruptedException e1) {
              Thread.currentThread().interrupt();
              running = false;
            }
            // ..then try again
            server.getFrequency();
          } catch (RemoteException ex) {
            // Still no connection
            server = null;
          }
        }
        catch (NullPointerException e) {
          e.printStackTrace();
        }
      }
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        running=false;
      }
    }
  }
  
  public TimeSeries getPowerConsumption() {
    return powerConsumption;
  }
  
  public TimeSeries getSystemFrequency() {
    return systemFrequency;
  }
  
  public TimeSeries getPowerPrice() {
    return powerPrice;
  }
  
  public TimeSeries[] getUpperFrequencyThreshold() {
    return upperFreqBounds;
  }
  
  public TimeSeries[] getLowerFrequencyThreshold() {
    return lowerFreqBounds;
  }
  
  public TimeSeries[] getUpperPriceThreshold() {
    return upperPriceBounds;
  }
  
  public TimeSeries[] getLowerPriceThreshold() {
    return lowerPriceBounds;
  }
  
	/** Checks whether any new data has arrived since last time */
    public boolean hasChanged() {
      return true;
    }
	
}
