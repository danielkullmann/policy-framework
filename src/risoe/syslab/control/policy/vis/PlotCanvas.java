package risoe.syslab.control.policy.vis;

import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import risoe.syslab.gui.wall.displets.flexhouse.Measurement;
import risoe.syslab.gui.wall.displets.flexhouse.TimeSeries;

/**
 * Widget to plot one or more {@link TimeSeries}.
 * 
 * @author atha
 */
public class PlotCanvas extends Canvas {
  private static final long serialVersionUID = 1128946284071599970L;

  private static final double padXWidth = 0.08f;
  private static final double padYHeight = 0.08f;

  private String title = "";
  private String labelX = "";
  private String labelY = "";
  private ArrayList<Color> plotColor = new ArrayList<Color>();

  private Graphics buffer;

  private int width;
  private int height;

  private int padX = 0;
  private int padY = 0;

  private int minX = 0;
  private int maxX = 1;

  private double minY = -5.f;
  private double maxY = 5.f;

  private ArrayList<TimeSeries> plots = new ArrayList<TimeSeries>();

  private int ticsX = 5;
  private int ticsY = 5;

  private ArrayList<Boolean> barTypePlot = new ArrayList<Boolean>();

  /** Default constructor */
  public PlotCanvas() {
    super();
  }

  @SuppressWarnings("unchecked")
  @Override
  public void update(Graphics g) {
    maxX = Integer.MIN_VALUE;
    minX = Integer.MAX_VALUE;
    maxY = Double.NaN;
    minY = Double.NaN;

    for (TimeSeries timeSeries : (ArrayList<TimeSeries>) plots.clone() ) {
      if (timeSeries != null) {
        Measurement[] data = timeSeries.getAll();
        if ( data == null ) {
          System.err.println("data is null...");
          continue;
        }
        maxY = safeMax( maxY, getMaxValue( timeSeries ) );
        minY = safeMin( minY, getMinValue( timeSeries ) );
        if (data.length > 0)
          minX = Math.min(minX, data[0].getTime());
        if (data.length > 0)
          maxX = Math.max(maxX, data[data.length - 1].getTime());

        if (minY == maxY) {
          minY -= 0.1;
          maxY += 0.1;
        }
      }
      if (minX == maxX) {
        minX--;
        maxX++;
      }
    }

    width = g.getClipBounds().width;
    height = g.getClipBounds().height;
    System.out.println( this );
    System.out.println("min: " + minX + " " + minY );
    System.out.println("max: " + maxX + " " + maxY);

    padX = (int) (padXWidth * width);
    padY = (int) (padYHeight * height);

    Image offscreen = createImage(width, height);

    buffer = offscreen.getGraphics();
    buffer.setColor(getBackground());
    buffer.fillRect(0, 0, width, height);
    buffer.setColor(getForeground());

    paint(buffer);
    g.drawImage(offscreen, 0, 0, this);

  }

  private double getMaxValue(TimeSeries timeSeries) {
    if ( timeSeries.getSize() == 0 ) return Double.NaN;
    double max = timeSeries.getFirstMeasurement().getData();
    for (Measurement m : timeSeries.getAll()) {
      max = Math.max( max, m.getData() );
    }
    return max;
  }

  private double getMinValue(TimeSeries timeSeries) {
    if ( timeSeries.getSize() == 0 ) return Double.NaN;
    double min = timeSeries.getFirstMeasurement().getData();
    for (Measurement m : timeSeries.getAll()) {
      min = Math.min( min, m.getData() );
    }
    return min;
  }

  @Override
  public void paint(Graphics g) {
    Graphics2D g2d = (Graphics2D) g;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    g2d.drawRect(0, 0, width, height);
    paintTimeSeries(g2d);
    paintCoordinateSystem(g2d);
  }

  /** Draw all the plots on the widget */
  @SuppressWarnings("unchecked")
  public void paintTimeSeries(Graphics2D g2d) {
    int i = -1;
    for (TimeSeries timeSeries : (ArrayList<TimeSeries>) plots.clone()) {
      i++;
      if (timeSeries != null) {
        Measurement[] data = timeSeries.getAll();

        if (barTypePlot.get(i) != null && barTypePlot.get(i).booleanValue() ) {
          int[] xPoints = new int[2 * data.length - 1];
          int[] yPoints = new int[2 * data.length - 1];

          xPoints[0] = relativeX(data[0].getTime());
          yPoints[0] = relativeY(data[0].getData());
          for (int j = 1; j < data.length; j++) {
            xPoints[2 * j - 1] = relativeX(data[j].getTime());
            xPoints[2 * j] = xPoints[2 * j - 1];
            yPoints[2 * j - 1] = yPoints[2 * (j - 1)];
            yPoints[2 * j] = relativeY(data[j].getData());
          }
          g2d.setColor(plotColor.get(i));
          g2d.setStroke(new BasicStroke(2.0f));
          g2d.drawPolyline(xPoints, yPoints, xPoints.length);
        } else {
          // draw other type of plot
          int[] xPoints = new int[data.length];
          int[] yPoints = new int[data.length];

          for (int j = 0; j < data.length; j++) {
            xPoints[j] = relativeX(data[j].getTime());
            yPoints[j] = relativeY(data[j].getData());
            // System.out.println(j + ": " + xPoints[j] + ", " + yPoints[j]);
          }
          g2d.setColor(plotColor.get(i));
          g2d.setStroke(new BasicStroke(2.0f));
          g2d.drawPolyline(xPoints, yPoints, xPoints.length);
        }
      }
    }
  }

  /** Draw the coordinate system */
  private void paintCoordinateSystem(Graphics2D g2d) {
    BasicStroke stroke = new BasicStroke(2);
    g2d.setStroke(stroke);
    g2d.setColor(Color.BLACK);

    g2d.drawLine(relativeX(minX), relativeY(maxY), relativeX(minX), relativeY(minY));
    g2d.drawLine(relativeX(minX), relativeY(minY), relativeX(maxX), relativeY(minY));
    g2d.drawLine(relativeX(maxX), relativeY(minY), relativeX(maxX), relativeY(maxY));

    Font font = null;
    Rectangle2D nameBounds = null;

    if (!title.equals("")) {
      font = new Font("Helvetica", Font.BOLD, height / 25);
      g2d.setFont(font);
      nameBounds = font.getStringBounds(title, g2d.getFontRenderContext());
      g2d.drawString(title, (int) ((width - nameBounds.getWidth()) / 2.f), (int) ((padY + 0.7 * nameBounds.getHeight()) / 2.f));
    }

    if (!labelX.equals("")) {
      font = new Font("Helvetica", Font.PLAIN, height / 30);
      g2d.setFont(font);

      nameBounds = font.getStringBounds(labelX, g2d.getFontRenderContext());
      g2d.drawString(labelX, (int) (0.5 * (width - nameBounds.getWidth())), height - (int) (0.5 * (padY - nameBounds.getHeight())));
    }

    font = new Font("Helvetica", Font.BOLD, height / 30);

    if (labelY != null) {
      nameBounds = font.getStringBounds(labelY, g2d.getFontRenderContext());
      int tx = (int) (0.3 * (padX + nameBounds.getHeight()));
      int ty = relativeY(0.5f * (maxY + minY)) + (int) (0.5 * nameBounds.getWidth());
      g2d.translate(tx, ty);
      g2d.rotate(-Math.PI / 2.f);
      g2d.drawString(labelY, 0, 0);
      g2d.setTransform(new AffineTransform());
    }

    /*
     * if(!labelY[1].equals("")){ nameBounds = font.getStringBounds(labelY[1],
     * g2d.getFontRenderContext()); tx =
     * width-(int)(0.3*(padX+nameBounds.getHeight())); ty =
     * relativeY(0.5f*(maxY[1]+minY[1]),1)-(int)(0.5*nameBounds.getWidth());
     * g2d.translate(tx, ty); g2d.rotate(Math.PI/2.f); g2d.drawString(labelY[1],
     * 0, 0); g2d.setTransform(new AffineTransform()); }
     */

    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

    long dt = (maxX - minX) / (ticsX);
    String time;
    int tickSize = (int) (0.07 * padX);
    if (ticsX != 0) {
      stroke = new BasicStroke(1);
      g2d.setStroke(stroke);
      int dx = (relativeX(maxX) - relativeX(minX)) / (ticsX);
      for (int i = 0; i < ticsX; i++) {
        g2d.drawLine(relativeX(minX) + i * dx, relativeY(minY), relativeX(minX) + i * dx, relativeY(minY) - tickSize);
        time = formatter.format(new Date(1000 * (minX + i * dt)));
        nameBounds = font.getStringBounds(time, g2d.getFontRenderContext());
        g2d.drawString(time, (int) (relativeX(minX) + i * dx - 0.5 * nameBounds.getWidth()), (int) (relativeY(minY) + nameBounds.getHeight() + 0.5 * tickSize));
      }
      time = formatter.format(new Date(1000 * (minX + ticsX * dt)));
      nameBounds = font.getStringBounds(time, g2d.getFontRenderContext());
      g2d.drawString(time, (int) (relativeX(maxX) - 0.5 * nameBounds.getWidth()), (int) (relativeY(minY) + nameBounds.getHeight() + 0.5 * tickSize));
    }

    DecimalFormat df = new DecimalFormat("#0.00");

    if (ticsY != 0) {
      double dya = (maxY - minY) / (ticsY);
      int dyr = (relativeY(maxY) - relativeY(minY)) / (ticsY);
      for (int j = 0; j <= ticsY; j++) {
        g2d.drawLine(relativeX(minX), relativeY(minY) + j * dyr, relativeX(minX) + tickSize, relativeY(minY) + j * dyr);
        String value = df.format(minY + j * dya);
        nameBounds = font.getStringBounds(value, g2d.getFontRenderContext());
        g2d.drawString(value, (int) (relativeX(minX) - nameBounds.getWidth()) - tickSize, relativeY(minY) + j * dyr + (int) (0.3 * nameBounds.getHeight()));
      }
    }

    /*
     * if(ticsY[1] != 0){ double dya = (maxY[1] - minY[1])/(ticsY[1]); int dyr =
     * (relativeY(maxY[1],1) - relativeY(minY[1],1))/(ticsY[1]); for (int j = 0;
     * j <= ticsY[1]; j++) { g2d.drawLine(relativeX(maxX),
     * relativeY(minY[1],1)+j*dyr, relativeX(maxX)-tickSize,
     * relativeY(minY[1],1)+j*dyr); String value = df.format(minY[1]+j*dya);
     * nameBounds = font.getStringBounds(value, g2d.getFontRenderContext());
     * g2d.drawString(value, relativeX(maxX)+tickSize,
     * relativeY(minY[1],1)+j*dyr + (int)(0.3*nameBounds.getHeight())); } }
     */
  }

  private int relativeX(int minX2) {
    return (padX + (minX2 - minX) * (width - 2 * padX) / (maxX - minX));
  }

  private int relativeY(double y) {
    return height - (int) (1.5 * padY + (int) ((y - minY) * (height - 2.5 * padY) / (maxY - minY)));
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setLabelX(String labelX) {
    this.labelX = labelX;
  }

  /** Setter for the y labels */
  public void setLabelY(String labelY) {
    this.labelY = labelY;
  }

  /** Setter for the x tics */
  public void setTicsX(int ticsX) {
    this.ticsX = ticsX;
  }

  /** Setter for the y tics */
  public void setTicsY(int ticsY) {
    this.ticsY = ticsY;
  }

  /** Setter for one timeseries */
  public void setTimeseries(TimeSeries plot) {
    this.setTimeseries(plot, null);
  }

  /** Setter for two timeseries */
  public void setTimeseries(TimeSeries plot1, TimeSeries plot2) {
    extendLists(2);
    plots = new ArrayList<TimeSeries>();
    plots.add( plot1 );
    plots.add( plot2 );
  }

  /** Setter for three timeseries */
  public void setTimeseries(TimeSeries plot1, TimeSeries plot2, TimeSeries plot3) {
    extendLists(3);
    plots = new ArrayList<TimeSeries>();
    plots.add( plot1 );
    plots.add( plot2 );
    plots.add( plot3 );
  }

  /** Setter for many timeseries */
  public void setTimeseries(TimeSeries[] plot) {
    extendLists(plot.length);
    plots = new ArrayList<TimeSeries>( Arrays.asList( plot ) );
  }

  /** Setter for the plot color */
  public void setPlotColor(int plot, Color color) {
    extendLists(plot);
    this.plotColor.set( plot, color );
  }

  /** Setter for the type of plot ("function" or bar) */
  public void setBarTypePlot(int plot, boolean isBarType) {
    extendLists(plot);
    barTypePlot.set(plot, isBarType);
  }

  private void extendLists(int index) {
    int size = index+1;
    if ( plotColor.size() < size ) {
      plotColor.ensureCapacity(size);
      for (int i = plotColor.size(); i < size; i++) {
        plotColor.add( null );
      }
    }
    if ( plots.size() < size ) {
      plots.ensureCapacity(size);
      for (int i = plots.size(); i < size; i++) {
        plots.add( null );
      }
    }
    if ( barTypePlot.size() < size ) {
      barTypePlot.ensureCapacity(size);
      for (int i = barTypePlot.size(); i < size; i++) {
        barTypePlot.add( null );
      }
    }
  }

  private double safeMin(double v1, double v2) {
    if ( Double.isNaN( v1 ) ) return v2;
    if ( Double.isNaN( v2 ) ) return v1;
    return Math.min( v1, v2);
  }

  private double safeMax(double v1, double v2) {
    if ( Double.isNaN( v1 ) ) return v2;
    if ( Double.isNaN( v2 ) ) return v1;
    return Math.max( v1, v2);
  }

}
