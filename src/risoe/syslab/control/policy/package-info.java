/**
 * This package contains all the classes that are necessary to implement
 * policy-based communication in SYSLAB.
 * 
 * TODO Implement support for disconnecting clients from the policy system
 * TODO (low priority) Implement name server capability
 * 
 */
package risoe.syslab.control.policy;
