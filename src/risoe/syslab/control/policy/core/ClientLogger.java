package risoe.syslab.control.policy.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;


/** Class logging data for clients. This is used 
 * for logging information abouty the current 
 * policies of clients, and also the state 
 * changes of clients.
 * 
 * @author daku
 */
public class ClientLogger {

  private static ClientLogger _instance;
  private static String stateFileName = "state.log";
  private static String policyFileName = "policies.log";
  private static String useFileName = "use.log";

  private PrintWriter state = null;
  private PrintWriter policy = null;
  private PrintWriter use = null;
  private int count;
  
  /** This is a singleton class! 
   */
  private ClientLogger() {
    super();
  }


  public void logState(String clientName, String clientId, String[] data) {
    log(state, clientName, clientId, data);
  }

  public void logUse(String clientName, String clientId, String[] data) {
    log(use, clientName, clientId, data);
  }

  public void logPolicy(String clientName, String clientId, String[] data) {
    log(policy, clientName, clientId, data);
  }

  public synchronized void openLogFiles() {
    try {
      boolean needsHeaderLine = state == null && ! new File(stateFileName).exists();
      if (this.state == null) this.state = new PrintWriter(stateFileName);
      if ( needsHeaderLine ) {
        state.println("date;timestamp;client;id;d1;d2;d3;d4;d5;d6;d7;d8;d9;");
        state.flush();
      }
    } catch (FileNotFoundException e) {
      throw new IllegalStateException("could not create log file: " + stateFileName);
    }

    try {
      boolean needsHeaderLine = policy == null && ! new File(policyFileName).exists();
      if (this.policy == null) this.policy = new PrintWriter(policyFileName);
      if ( needsHeaderLine ) {
        policy.println("date;timestamp;client;id;d1;d2;d3;d4;d5;d6;d7;d8;d9;");
        policy.flush();
      }
    } catch (FileNotFoundException e) {
      throw new IllegalStateException("could not create log file: " + policyFileName);
    }

    try {
      boolean needsHeaderLine = use == null && ! new File(useFileName).exists();
      if (this.use == null) this.use = new PrintWriter(useFileName);
      if ( needsHeaderLine ) {
        use.println("date;timestamp;client;id;d1;d2;d3;d4;d5;d6;d7;d8;d9;");
        use.flush();
      }
    } catch (FileNotFoundException e) {
      throw new IllegalStateException("could not create log file: " + useFileName);
    }
 }

  public synchronized static ClientLogger instance() {
    if (_instance == null) _instance = new ClientLogger();
    return _instance;
  }
  
  private void log(PrintWriter writer, String clientName, String clientId, String[] data) {
    StringBuilder msg = new StringBuilder();
    Date now = new Date();
    msg.append(now);
    msg.append(";");
    msg.append(now.getTime());
    msg.append(";");
    msg.append(clientName);
    msg.append(";");
    msg.append(clientId);
    msg.append(";");
    for (String d : data) {
      msg.append(d);
      msg.append(";");
    }
    openLogFiles();
    synchronized (this) {
      writer.println(msg.toString());
      count ++;
      if (count > 100) {
        use.flush();
        policy.flush();
        state.flush();
        count = 0;
      }
    }
  }


  public void relocateLogFiles( String loggerDir ) {
    // This creates and opens the ClientLogger log files, so I can move 
    // them to the simulation logging directory
    openLogFiles();
    String[] logFiles = { "log.log", "log.log.lck", stateFileName, policyFileName, useFileName };
    for ( String logFile : logFiles ) {
      File oldLocation = new File( logFile );
      File newLocation = new File( loggerDir + File.separator + logFile );
      if ( oldLocation.exists() ) oldLocation.renameTo( newLocation );
    }
  }

}
