/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;

/**
 * Validity period of a policy.
 * 
 * This consists of two fields:
 * <ul>
 * <li> from, which is the start datetime (inclusive) of the period
 * <li> until, which is the end datetime (exclusive) of the period
 * </ul>
 */
public class TimeFrame {

  /**
   * Key used in FIPA-like content to start the TimeFrame.
   * Used like:
   *   KEY_TIME_FRAME + " " + timeFrame.toFipaContent()
   */
  public static final String KEY_TIME_FRAME = ":time-frame";
  
  
  /** Key used in {@link #toFipaContent()} for <code>from</code> field */
  private static final String KEY_FROM = ":from";

  
  /** Key used in {@link #toFipaContent()} for <code>until</code> field */
  private static final String KEY_UNTIL = ":until";
  
  
  /** Start date and time of time frame */
  private Date from;

  
  /** End date and time of time frame */
  private Date until;
  

  /** Default Constructor 
   * 
   * @param from start date and time of time frame
   * @param until end date and time of time frame
   */
  public TimeFrame( Date from, Date until ) {
    this.from = from;
    this.until = until;
    
    assert( this.from != null );
    assert( this.until != null );
    assert( this.until.after( this.from ) );
  }
  

  /**
   * Creates a time frame object from a start date time and a duration
   * 
   * @param from start date and time of time frame
   * @param days duration: number of days
   * @param hours duration: number of hours
   * @param minutes duration: number of minutes
   * @return the created time frame instance
   */
  public static TimeFrame createTimeFrame( Date from, int days, int hours, int minutes ) {
    Calendar cal = new GregorianCalendar();
    cal.setTime( from );
    cal.add( Calendar.DAY_OF_YEAR, days );
    cal.add( Calendar.HOUR_OF_DAY, hours );
    cal.add( Calendar.MINUTE, minutes );
    Date until = cal.getTime();
    TimeFrame timeframe = new TimeFrame( from, until );
    return timeframe ;
  }

  
  /**
   * Simple getter for ther <code>from</code> field
   * @return the from value
   */
  public Date getFrom() {
    return from;
  }


  
  /**
   * Simple getter for ther <code>until</code> field
   * @return the until value
   */
  public Date getUntil() {
    return until;
  }

  
  /**
   * Checks whether the timeframe is current now
   *  
   * @param now the date to be checked against, normally <code>new Date()</code>
   * @return true, iff the time frame is current (active) now
   */
  public boolean isCurrent( Date now ) {
    return ! now.before( from ) && ! now.after( until );
  }
  
  /**
   * Checks whether the timeframe is current, or will soon 
   * (in the next inSeconds seconds) be current.
   * 
   * @param now the date to be checked against, normally <code>new Date()</code>
   * @param inSeconds what "soon" means, in seconds
   * @return
   */
  public boolean isSoonCurrent( Date now, int inSeconds ) {
    if ( isCurrent( now ) ) return true;
    Calendar cal = new GregorianCalendar();
    cal.setTime( now );
    cal.add( Calendar.SECOND, inSeconds );
    return isCurrent( cal.getTime() );
  }
  
  /**
   * Checks whether the timeframe is expired, i.e. now is after the end of the timeframe.
   * 
   * This is different from !isCurrent(now) in that isExpired(now) also returns false when
   * now is before the start of the time frame.
   *  
   * @param now the date to be checked against, normally <code>new Date()</code>
   * @return true, iff the time frame is current (active) now
   */
  public boolean isExpired( Date now ) {
    return now.after( until );
  }
  
  /**
   * Parses the FIPA-like content of a time frame into a {@link TimeFrame} instance.
   * 
   * The string representation produced by {@link #toFipaContent()} can be converted
   * into a {@link TimeFrame} instance by using this method. 
   * 
   * assertEquals(tf, TimeFrame.parse(new MessageParser().parse(tf.toFipaContent())));
   * 
   * @param value parsed value of time frame, as created by {@link MessageParser#parse(String)}
   * @return the created time frame instance, or null if this is no TimeFrame
   * @throws IllegalArgumentException iff this looks like a time frame, but isn't
   */
  public static TimeFrame parse( Expression value ) {
    if ( value instanceof ExpressionList ) {
      try {
        ExpressionList list = (ExpressionList) value;
        Date from;
        Date until;
        if ( ! list.getHead().toString().equalsIgnoreCase( KEY_FROM ) ) 
          return null;
        from = new Date( Long.valueOf( list.getTail().getHead().toString() ) );
        list = list.getTail().getTail();
        if ( ! list.getHead().toString().equalsIgnoreCase( KEY_UNTIL ) ) 
          return null;
        
        until = new Date( Long.valueOf( list.getTail().getHead().toString() ) );
        list = list.getTail().getTail();
        if ( list != null )
          return null;
        return new TimeFrame( from, until );
      } catch ( NumberFormatException e ) {
        return null;
      } catch ( NullPointerException e ) {
        return null;
      }
    }
    return null;
  }

  
  /** returns the duration of the time frame in hours
   *  @return duration in hours
   */
  public double getDuration() {
    return (until.getTime() - from.getTime()) / (3600.0 * 1000.0); 
  }


  /**
   * Creates a string from this time frame value that can be used in a
   * FIPA message. Example: "(:from 12349532495 :to 1275874857)"
   * @return the FIPA-like content for this time frame
   */
  public String toFipaContent() {
    return "(" + 
      KEY_FROM  + " " + from.getTime()  + " " + 
      KEY_UNTIL + " " + until.getTime() + ")";
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((from == null) ? 0 : from.hashCode());
    result = prime * result + ((until == null) ? 0 : until.hashCode());
    return result;
  }

  
  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    TimeFrame other = (TimeFrame) obj;
    if ( from == null ) {
      if ( other.from != null ) {
        return false;
      }
    } else if ( !from.equals( other.from ) ) {
      return false;
    }
    if ( until == null ) {
      if ( other.until != null ) {
        return false;
      }
    } else if ( !until.equals( other.until ) ) {
      return false;
    }
    return true;
  }


  @Override
  public String toString() {
    return "TimeFrame [from=" + from + ", until=" + until + "]";
  }


}
