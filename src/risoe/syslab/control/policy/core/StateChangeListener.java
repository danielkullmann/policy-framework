package risoe.syslab.control.policy.core;

/**
 * Simple interface to support listening for state changes.
 * This is used in the class {@link ThresholdRunner}.
 *
 * @param <T> type of the state, usually an enum
 * 
 * @author daku
 */
public interface StateChangeListener<T> {

  /** This method is supposed to be called each time
   * the state of the publisher changes.
   *  
   * @param state
   */
  void stateChanged( T state );
}
