/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.message.PolicyMessage;

/**
 * This is the base class for {@link PolicyServer} and {@link PolicyClient},
 * the partners in the policy communication. It provides some common
 * functionality for clients and servers of the policy system. The most
 * important part of that is the ability to send and receive messages.
 * That functionality is in turn provided by the actual {@link CommunicationSystem}
 * that is used.
 * 
 * This should not be confused with {@link ServerClientBehaviour} and 
 * {@link ClientBehaviour}. Those classes are implementing actual policies and
 * are used by {@link PolicyServer} and {@link PolicyClient}.
 */
public abstract class PolicyPartner implements PolicyParser {

  /**
   * This field should be set to false when the behaviour should 
   * be removed
   */
  protected boolean isDone = false;
  
  /**
   * agent we use for communication
   */
  protected AbstractAgent agent;


  /**
   * This is the method that must be overridden.
   * It is called repeatedly and shouold implement 
   * one step of the policy behaviour.
   */
  public abstract void action();
  

  /**
   * {@link AbstractAddress} of this unit (how can this unit be reached?)
   * 
   * @return {@link AbstractAddress} of this unit 
   */
  public abstract AbstractAddress getAgentId();


  /**
   * Sends a message via the communication system.
   * 
   * @param msg message to be sent
   */
  protected void sendMessage( PolicyMessage msg ) {
    agent.sendMessage( msg );
  }

  
  /**
   * Waits for a policy message, blocking for maximally one second
   * 
   * @return a policy message, when one was received, or null
   */
  protected PolicyMessage receivePolicyMessage() {
    return agent.receiveMessage();
  }


  /**
   * Stop the client from participating in the policy communication
   */
  protected void stopClient() {
    agent.stopAgent();
  }


  /**
   * When the partner should be stopped, this method should return true
   * 
   * @return
   */
  public boolean done() {
    return isDone;
  }


  /**
   * Simple setter for the agent used for communication
   * @param agent agent used for communicating
   */
  public void setAgent( AbstractAgent agent ) {
    this.agent = agent;
  }

  
  
}
