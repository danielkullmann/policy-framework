package risoe.syslab.control.policy.core;

import risoe.syslab.control.policy.rules.EventSource;


/**
 * This runner acts according to a Threshold, sending state change events to a
 * listener.
 * 
 * The runner can be used in two modes:
 * <li> threaded: use {@link #start()} and {@link #interrupt()} to start and stop autonomous handling of the state, and {@link #injectValue(double)} to give a new value to the runner
 * <li> non-threaded: use {@link #injectValue(double)} to give a new value to the runner, then {@link #switchState()} to perform the state-handling logic
 * 
 * @author daku
 * 
 */
public class ThresholdRunner implements Runnable {

  /** State the runner can be in: */
  public enum State {
    NORMAL, HIGH, LOW
  };

  /** Threshold to be followed.  */
  private Thresholds threshold;
  
  /** Thread that executes this runner */
  private volatile Thread thread;
  
  /** value that is compared to the thresholds */
  private volatile double value = Double.NaN;

  /** Current state of the runner */
  private State state = State.NORMAL;
  
  /** Object interested in changes from this runner */ 
  private StateChangeListener<State> listener;
  
  /** Thread that retrieves new values that are put into {@link #value}. */
  private Thread sourceThread;

  
  /** Normal constructor
   * 
   * @param threshold low/high thresholds to observe
   * @param listener {@link StateChangeListener} to send vents to
   */
  public ThresholdRunner(Thresholds threshold,
      StateChangeListener<State> listener) {
    super();
    this.threshold = threshold;
    this.thread = null;
    this.listener = listener;
    this.sourceThread = null;
  }

  /** Constructor that additionally accepts an EventSource to be used for
   * retrieving new values to be chekced against the threshold.
   * 
   * @param threshold
   * @param source
   * @param listener
   */
  public ThresholdRunner(Thresholds threshold, EventSource source, StateChangeListener<State> listener) {
    this(threshold,listener);
    if ( source != null ) {
      sourceThread = new Thread( new SourceRunner( source ) );
      sourceThread.setDaemon( true );
      sourceThread.start();
    }
  }

  /** Starts the runner in a spearate thread. */
  public void start() {
    thread = new Thread(this);
    thread.setDaemon(true);
    thread.start();
    thread.setName("ThresholdRunner #" + Thread.currentThread().getId());
  }

  
  /** Stop the runner thread, when it was started */
  public void interrupt() {
    if (thread != null)
      thread.interrupt();
    thread = null;
    if ( sourceThread != null )
      sourceThread.interrupt();
    sourceThread = null;
  }

  
  /** Give a new value to the runner.
   * 
   * When the runner is run in threaded mode, it will automatically
   * {@link #notify()} the thread that is dealing with state management.
   * 
   * Otherwise, you should call {@link #switchState()} manually.
   * 
   * @param value new value for comparison with the thresholds.
   */
  public synchronized void injectValue(double value) {
    this.value = value;
    notify();
  }

  
  /** Do not call! This is for running the ThresholdRunner in threaded mode
   * TODO Move this method into a private child class, so that it is not 
   * accessible anymore. 
   */
  @Override
  public void run() {
    while (!Thread.interrupted()) {
      synchronized (this) {
        try {
          wait();
          switchState();
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
          break;
        }
      }
    }
  }

  
  /** Used for state management.
   * 
   * This method is called automatically in threaded mode, i.e. each time {@link #injectValue(double)}
   * is called.
   * 
   * Call this method manually, when the runner is working in non-threaded mode. I.e.
   * <code>
   *   runner.injectValue( newValue );
   *   runner.switchStates();
   * </code>
   */
  public synchronized void switchState() {
    double value = this.value;
    switch (state) {
    case NORMAL:
      if (value < threshold.getLow()) {
        stateChanged(State.LOW);
      } else if (value > threshold.getHigh()) {
        stateChanged(State.HIGH);
      }
      break;
    case HIGH:
      if (value < threshold.getLow()) {
        stateChanged(State.LOW);
      } else if (value < threshold.getHigh() - threshold.getHysteresis()) {
        stateChanged(State.NORMAL);
      }
      break;
    case LOW:
      if (value > threshold.getHigh()) {
        stateChanged(State.HIGH);
      } else if (value > threshold.getLow() + threshold.getHysteresis()) {
        stateChanged(State.NORMAL);
      }
      break;

    default:
      throw new IllegalArgumentException("unknown state " + state);
    }
  }

  
  /** Simple setter for the current state of the runner.
   * 
   * Will usually only be used when no {@link StateChangeListener} is used. 
   *  
   * @return current state of the runner
   */
  public State getState() {
    return state;
  }

  
  /** Simple getter for the {@link #threshold} field.
   * 
   * @return the threshold used in this instance
   */
  public Thresholds getThreshold() {
    return threshold;
  }

  
  /** Whether the runner is running in threaded mode.
   * 
   * Threaded mode is started with {@link #start()}, and stopped with {@link #interrupt()}.
   * 
   * @return true iff runner is running in threaded mode
   */
  public boolean isRunning() {
    return thread != null;
  }

  
  /** This method handles state changes, i.e. it sets the new state,
   * and it calls the listener to tell it the new state.
   *  
   * @param state new state
   */
  private void stateChanged(State state) {
    this.state = state;
    if ( listener != null ) {
      listener.stateChanged(state);
    }
  }
  

  /** Class that implements retrieving new values from an {@link EventSource}. */
  private class SourceRunner implements Runnable {
    private EventSource source;

    public SourceRunner(EventSource source) {
      this.source = source;
    }

    @Override
    public void run() {
      Thread.currentThread().setName( "ThresholdRunner.SourceRunner #" + Thread.currentThread().getId() );
      while ( !Thread.interrupted() ) {
        synchronized (source) {
          try {
            source.wait();
          } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            break;
          }
          injectValue( source.getValue() );
          switchState();
        }
      }
    }
  }

}
