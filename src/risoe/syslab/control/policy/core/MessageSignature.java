/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import java.util.Arrays;

/**
 * Class that encapsulates a signature for a policy message.
 * The signature as it is returned by the Java crypto system is just
 * a byte array, but we want to have several methods to deal with
 * it, to create a FIPA-compliant content string with it, and to parse 
 * that content string back into a byte array.
 */
public final class MessageSignature {

  /**
   * Helper constant with hex digits.
   */
  private static final String HEXES = "0123456789ABCDEF";

  
  /** signature byte array that this class encapsulates */
  private final byte signature[];

  
  /** Constructor to create an instance from a byte array.
   * 
   * @param signature byte array of signature
   */
  public MessageSignature( byte[] signature ) {
    this.signature = signature;
    if ( signature == null || signature.length < 4 )
      throw new AssertionError("empty signature passed in");
  }

  
  /**
   * Also a "constructor", but for creating an instance from
   * a FIPA content string.
   * @param content content of string, without the '"' characters
   * @return a new {@link MessageSignature} instance
   */
  public static MessageSignature fromString( String content) {
    byte[] signature = new byte[ content.length()/2 ];
    int index = 0;
    while ( index+1 < content.length() ) {
      int n1 = HEXES.indexOf( content.charAt( index ) );
      int n2 = HEXES.indexOf( content.charAt( index+1 ) );
      if ( n1 < 0 || n2 < 0 ) throw new AssertionError();
      signature[index/2] = (byte) (n1*16 + n2);
      index += 2;
    }
    if ( index != content.length() ) throw new AssertionError();
    return new MessageSignature( signature );
  }

  
  /**
   * Simplke getter for encapsulated signature byte array
   * @return byte array of signature
   */
  public byte[] getSignature() {
    return signature;
  }
  

  /** Creates the FIPA-string for use in the FIPA-encoding of 
   * {@link risoe.syslab.control.policy.message.PolicyMessage}s.
   * @return a String with hex content
   */
  public String toFipaContent() {
    return "\"" + getHex( signature ) + "\"";
  }
  
  
  /**
   * Helper method to convert a signature byte array into a hex string
   * @param raw byte array
   * @return hex string
   */
  public static String getHex( byte [] raw ) {
    if ( raw == null ) {
      return null;
    }
    final StringBuilder hex = new StringBuilder( 2 * raw.length );
    for ( final byte b : raw ) {
      hex.append(HEXES.charAt((b & 0xF0) >> 4))
         .append(HEXES.charAt((b & 0x0F)));
    }
    return hex.toString();
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.hashCode( signature );
    return result;
  }
  

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    MessageSignature other = (MessageSignature) obj;
    if ( !Arrays.equals( signature, other.signature ) ) {
      return false;
    }
    return true;
  }

}
