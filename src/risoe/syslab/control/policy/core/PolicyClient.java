/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.message.AcceptPolicy;
import risoe.syslab.control.policy.message.AliveMessage;
import risoe.syslab.control.policy.message.GiveDeviceInfo;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.control.policy.message.ProposePolicy;
import risoe.syslab.control.policy.message.RegisterClient;
import risoe.syslab.control.policy.message.RejectPolicy;
import risoe.syslab.control.policy.message.ServerRequestNegotiation;
import risoe.syslab.control.policy.message.UnregisterClient;
import risoe.syslab.control.policy.message.parser.Expression;

/**
 * This provides the client side of the policy protocol,
 * also known as the "device". The main method is {@link #action()}
 * which handles the state of the client and sends and receives messages.
 */
public final class PolicyClient extends PolicyPartner {

  /**
   * How often is the state of the client checked (in seconds)?
   */
  private static final int CHECK_FREQUENCY = 10;

  
  /**
   * How often is an AliveMessage sent (in seconds)?
   */
  public static int ALIVE_MESSAGE_FREQUENCY = 60;

  
  /** logger instance */
  private static final Logger logger = Logger.getLogger( PolicyClient.class.getName() );

  /**
   * Status of the client in the policy protocol
   */
  public enum ClientStatus {
    /** Client has just started */
    UNKNOWN,
    /** Looking for a server to communicate with */
    SEARCHING_SERVER,
    /** Policy negotiation phase */
    NEGOTIATE,
    /** Waiting to get a policy proposal */
    WAIT_FOR_POLICY,
    /** Waiting for policy to activate */
    WAITING,
    /** A policy has been activated */
    ACTIVE,
  }


  /**
   * Agent ID of this object
   */
  private AbstractAddress agentId;
  
  
  /**
   * Agent ID of server
   */
  private AbstractAddress server;
  
  
  /**
   * Client behaviours that is used
   */
  private ClientBehaviour behaviour;

  
  /**
   * List of policies this client has agreed to follow.
   * The policies in this list are not active; it could be
   * that their timeframe is not come yet.
   */
  private ArrayList<Policy> acceptedPolicies;
  
  
  /**
   * Status of the client
   */
  ClientStatus status = ClientStatus.UNKNOWN;
  
  
  /**
   * Constructor
   * 
   * @param agentId may NOT be null
   * @param server may be null
   */
  public PolicyClient( AbstractAddress agentId, AbstractAddress server ) {
    this.agentId = agentId;
    this.server = server;
    this.behaviour = null;
    this.acceptedPolicies = new ArrayList<Policy>();
    
    assert( this.agentId != null );
  }
  
  
  /** just for debugging... */
  private PolicyClient.ClientStatus lastStatus = ClientStatus.UNKNOWN;


  /** Check every minute or so */ 
  private Date nextCheck = null;
  
  
  /** Check every minute */ 
  private Date nextAliveMessage = null;


  @Override
  public void action() {

    if ( lastStatus != status ) {
      logger.log( Level.FINE, agentId.getName() + ": client status: " + status );
      lastStatus = status;
    }
    
    if ( behaviour.shouldUnregister() ) {
      UnregisterClient msg = new UnregisterClient( UUID.randomUUID(), agentId, server, null );
      sendMessage( msg );
      isDone = true;
      stopClient();
      return;
    }

    Date now = new Date();

    // AliveMessage are regularly sent
    if ( nextAliveMessage == null ) {
      setNextAliveMessage();
    } else if ( now.after( nextAliveMessage ) ) {
      AliveMessage msg = new AliveMessage( UUID.randomUUID(), agentId, server, null );
      sendMessage( msg ); // Ignore failure;
      setNextAliveMessage();
    }

    if ( nextCheck == null || now.after( nextCheck ) ) {
      
      // Check whether the time frame of an accepted policy has come..
      checkPolicies();
      
      // Calculate next time the check has to be done...
      Calendar cal = new GregorianCalendar();
      cal.setTime( now );
      cal.add( Calendar.SECOND, CHECK_FREQUENCY );
      nextCheck = cal.getTime();
    }

    PolicyMessage msg = null;
    
//    // TODO Was a send failure message received?
//    if ( new Random().nextInt( 100 ) < 10 ) {
//      if ( status == ClientStatus.SEARCHING_SERVER ) {
//        // TODO This way, the node can be registered at two servers
//        //      This is not correct
//        server = null;
//      }
//      status = ClientStatus.UNKNOWN;
//    }
    
    switch ( status ) {
    
    case UNKNOWN:
      //if ( server == null ) {
        // TODO Ask name server for a policy server..
        // That's *not* the way to do it:
        //server = AgentRunner.createAgentId( "Policy 01", "syslab-01", "syslab-01" );
      //}
      status = ClientStatus.SEARCHING_SERVER;
      break;
    
    case SEARCHING_SERVER:
      // TODO Receive message that server was found
      if ( server != null ) {
        // Send register request to server; server should respond with ServerRequestNegotiation message
        msg = new RegisterClient( UUID.randomUUID(), agentId, server, null );
        setNextAliveMessage();
        sendMessage( msg );
        status = ClientStatus.NEGOTIATE;
        // TODO Start a timer that triggers when no ServerRequestNegotiation message comes 
      }
      break;
    
    case NEGOTIATE:
      msg = receivePolicyMessage();

      // Receive optional (re-)negotiation messages: those can be almost ignored..
      if ( msg instanceof ServerRequestNegotiation ) {
        ServerRequestNegotiation r = (ServerRequestNegotiation) msg;
        GiveDeviceInfo msg2 = new GiveDeviceInfo( r.getNegotiationId(), agentId, server, behaviour.provideDeviceInfo(), null );
        // How to handle message failure? => server should respond to an upcoming AliveMessage
        setNextAliveMessage();
        sendMessage( msg2 );
        status = ClientStatus.WAIT_FOR_POLICY;
        // TODO Start a timer that triggers when no ProposePolicy message comes 
      }
        
      break;
      
    case WAIT_FOR_POLICY:
      msg = receivePolicyMessage();

      if ( msg instanceof ProposePolicy ) {
        // react to ProposePolicy message
        // does our active behaviour accept this policy?
        if ( behaviour.acceptPolicy( msg.getPolicy() ) ) {
          // Yes, so we accept that policy
          PolicyMessage msg2 = new AcceptPolicy( msg.getNegotiationId(), agentId, server, msg.getPolicy(), null );
          setNextAliveMessage();
          sendMessage( msg2 );
          acceptedPolicies.add( msg.getPolicy() );
          checkPolicies();
          status = behaviour.getActivePolicy() == null ? ClientStatus.WAITING : ClientStatus.ACTIVE;
          logger.log( Level.INFO, agentId.getName() + ": accepted policy: \n" + msg.getPolicy() );
        } else {
          // No, so we reject that policy
          RejectPolicy msg2 = new RejectPolicy( msg.getNegotiationId(), agentId, server, msg.getPolicy(), null );
          setNextAliveMessage();
          sendMessage( msg2 );
          status = ClientStatus.NEGOTIATE;
          logger.log( Level.INFO, agentId.getName() + ": rejected policy: \n" + msg.getPolicy() );
        }
      } else if ( msg instanceof ServerRequestNegotiation ) {
        // Normally, I should just ignore this message, but it might be necessary in the future to 
        GiveDeviceInfo msg2 = new GiveDeviceInfo( msg.getNegotiationId(), agentId, server, behaviour.provideDeviceInfo(), null );
        setNextAliveMessage();
        sendMessage( msg2 );
        status = ClientStatus.WAIT_FOR_POLICY;
      } else if ( msg != null ) {
        logger.log( Level.SEVERE, agentId.getName() + ": PolicyClient.action() received an unknown message " + msg.getClass().getName() );
      }

      break;
    
    case WAITING:
    case ACTIVE:
      msg = receivePolicyMessage();

      if ( msg instanceof ServerRequestNegotiation ) {
        ServerRequestNegotiation r = (ServerRequestNegotiation) msg;
        GiveDeviceInfo msg2 = new GiveDeviceInfo( r.getNegotiationId(), agentId, server, behaviour.provideDeviceInfo(), null );
        setNextAliveMessage();
        sendMessage( msg2 );
        status = ClientStatus.WAIT_FOR_POLICY;
      }

      break;
      
    default:
      throw new IllegalArgumentException(" Unknown client status");
    }
    
  }


  public void setNextAliveMessage() {
    // Calculate next time the check has to be done...
    Date now  = new Date();
    Calendar cal = new GregorianCalendar();
    cal.setTime( now );
    cal.add( Calendar.SECOND, ALIVE_MESSAGE_FREQUENCY );
    nextAliveMessage = cal.getTime();
  }
  

  @Override
  public AbstractAddress getAgentId() {
    return agentId;
  }
  

  /**
   * Simply getter for the server this client is connected to
   * @return AbstractAddress of the server
   */
  public  AbstractAddress getServer() {
    return server;
  }
  
  
  /**
   * Manages behaviours of this client
   * 
   * @param cb client behaviour
   */
  public void setBehaviour( ClientBehaviour cb ) {
    this.behaviour = cb;
  }
  
  
  /**
   * Manages behaviours of this client
   * 
   * @return registered client behaviour
   */
  public ClientBehaviour getBehaviour() {
    return behaviour;
  }
  
  
  /**
   * Looks for an policy to be activated, and removes old policies
   */
  public synchronized void checkPolicies() {
    
//    if ( behaviour.getActivePolicy() != null ) {
//      // We have an active policy; no need to find a new one...
//      if ( behaviour.getActivePolicy().checkTimeFrame( new Date() ) )
//        return;
//    }
    // When policies are renegotiated, we want newer policies to override older policies, 
    // even when those older policies are still current. So we don't skip this if there is
    // already an active policy.
   
    Date now = new Date();
    ArrayList<Policy> newList = new ArrayList<Policy>();
    Policy activePolicy = null;
    
    for ( Policy policy : acceptedPolicies ) {
      if ( policy.getTimeFrame().getUntil().after( now ) ) {
        newList.add( policy );
      }
      if ( policy.getTimeFrame().isCurrent( now ) ) {
        activePolicy  = policy;
      }
      if ( activePolicy != null && activePolicy.getTimeFrame().isExpired( now ) ) {
        activePolicy = null;
      }
      
    }
    // set new list of accepted policies
    acceptedPolicies = newList;
    
    // check if we have a missing policy..
    if ( activePolicy == null && ( status == ClientStatus.ACTIVE || status == ClientStatus.WAITING ) ) {
      status = ClientStatus.UNKNOWN;
    }
    
    // check if we have an active policy
    if ( activePolicy != null && ! activePolicy.equals( behaviour.getActivePolicy()  ) ) {
      status = ClientStatus.ACTIVE;
      if ( ! activePolicy.equals(  behaviour.getActivePolicy() ) ) {
        behaviour.activatePolicy( activePolicy );
      }
    }
    
  }


  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return behaviour.parseDeviceInfo( expression );
  }


  @Override
  public Policy parsePolicy( Expression expression ) {
    return behaviour.parsePolicy( expression );
  }


}
