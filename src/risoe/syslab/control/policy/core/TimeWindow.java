package risoe.syslab.control.policy.core;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.IntegerConstant;
import risoe.syslab.control.policy.message.parser.MessageParser;

/** 
 * This class represents a time window during a given time frame, i.e. the
 * window has a start value and a length value, both between 0 and (frame-1)
 * seconds.
 * 
 * A frame starts at all timeInSeconds values for which timeInSeconds % frame ==
 * 0.
 * 
 * When you have 10 devices, and you want to distribute a 15-minute time frame
 * between them, use a frame of 15*60, and 10 windows of frame/10 seconds
 * length, whose start times are distributed along the length of the frame, one
 * every frame/10 seconds.
 * 
 * @author daku
 */
public class TimeWindow implements Serializable {

  /** */
  private static final long serialVersionUID = -6721861178534654827L;
  
  /** length of frame, i.e. sum of lengths of all TimeWindows. */
  private final int frame;
  
  /** start time of this window inside the frame. */
  private final int startTime;
  
  /** length of this window inside the frame. */
  private final int length;
  

  /** Default constructor.
   * 
   * @param frame length of frame
   * @param startTime start time of this window inside frame
   * @param length length of this window insiide frame
   * @throws IllegalArgumentException when any of the parameters
   *   has an illegal or inconsistent value.
   */
  public TimeWindow(int frame, int startTime, int length) {
    super();
    this.frame = frame;
    this.startTime = startTime;
    this.length = length;
    
    if ( frame <= 0 ) {
      throw new IllegalArgumentException("frame negative");
    }
    if ( length < 0  ) {
      throw new IllegalArgumentException( "length " + length + " < 0 " );
    }
    if ( startTime < 0 || startTime >= frame ) {
      throw new IllegalArgumentException( "range error start: " + startTime );
    }
    if ( length < 0 || startTime+length > frame ) {
      throw new IllegalArgumentException( "range error length: " + length );
    }
  }

  
  /** Constructs a new TimeWindow object from the given parse tree.
   * 
   * assertEquals(tw, TimeWindow.parse(new MessageParser().parse(tw.toFipaContent())));
   * 
   * @param expression the parse tree constructed by {@link MessageParser#parse(String)}
   * @return The constructed TimeWindow, or null if expression is no TimeWindow representation
   * @throws RuntimeException if this looks like a TimeWindow representation, but the parsing fails
   */
  public static TimeWindow parse( Expression expression ) {
    String pattern =  "(time-window " + 
      ":frame ?frame " +
      ":start ?start " +
      ":length ?length " +
      ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    
    long frame  = ((IntegerConstant) match.get( "frame" )).getValue();
    long start  = ((IntegerConstant) match.get( "start" )).getValue();
    long length = ((IntegerConstant) match.get( "length" )).getValue();

    return new TimeWindow((int) frame, (int) start, (int) length);
  }

  
  /** Returns a representation of this object in the format 
   * used for messages here.
   * 
   * The string representation can be converted to an equivalent 
   * TimeWindow object by using the {@link #parse(Expression)} method.
   * 
   * assertEquals(tw, TimeWindow.parse(new MessageParser().parse(tw.toFipaContent())));
   * 
   * @return a String representation of this object
   */
  public String toFipaContent() {
    return "(time-window :frame " + frame + " :start " + startTime + " :length " + length + ")";
  }

  
  /** Checks whether this TimeWindow is current for the given date.
   * 
   * A TimeWindow is current when the time index in the current frame
   * lies between startTime and startTime+length for that TimeWindow.
   * 
   * @param date date to be checked
   * @return true, iff the TimeWindow is current for the given date
   */
  public boolean isCurrent( Date date ) {
    return isCurrent( date.getTime() );
  }
  
  
  /** Checks whether this TimeWindow is current for the given date.
   * @param timeMs time in milliseconds
   * @return true, iff the TimeWindow is current for the given date
   * @see #isCurrent(Date)
   */
  public boolean isCurrent( long timeMs ) {
    int secs = (int) ((timeMs / 1000) % frame);
    return secs >= startTime && secs < startTime+length;
  }

  
  /** Checks whether this TimeWindow is current right now.
   * @return true, iff the TimeWindow is current right now
   * @see #isCurrent(Date)
   */
  public boolean isCurrent() {
    return isCurrent( System.currentTimeMillis() );
  }


  /** Simple getter for the start time field.
   * 
   * This field denotes the start time of this window inside the frame.
   * 
   * @return start time of this window
   */
  public int getStartTime() {
    return startTime;
  }

  
  /** Simple getter for the length field.
   * 
   * This field denotes the length of this window inside the frame
   * 
   * @return length of this window
   */
  public int getLength() {
    return length;
  }
  
  
  /** Simple getter for the frame field.
   * 
   * The frame is the length of all windows together.
   * 
   * @return the length of the whole frame
   */
  public int getFrame() {
    return frame;
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + length;
    result = prime * result + frame;
    result = prime * result + startTime;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    TimeWindow other = (TimeWindow) obj;
    if (length != other.length)
      return false;
    if (frame != other.frame)
      return false;
    if (startTime != other.startTime)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "TimeWindow [frame=" + frame + ", startTime=" + startTime + ", length=" + length + "]";
  }


}
