/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;

/** Class for signing messages using public key cryptography.
 */
public class MessageSigner {

  /** The key store type that is used by this class */
  public static final String KEY_STORE_TYPE = "jks";
  

  /** 
   * Signs a message using @link {@link MessageSigner#sign(String, KeyStore, String, String)}, 
   * taking the signing key from the common conf/keystore.jks file. They also know they
   * key passwords.
   * 
   * @param message message to be signed
   * @param alias alias of the private key
   * @return the signature of the message
   */
  public byte[] sign( String message, String alias ) {
    FileInputStream stream = null;
    try {
      String privateStore = System.getProperty( "risoe.syslab.encryption.private.keystore", "conf/keystore.jks" );
      String privateStorePass = System.getProperty( "risoe.syslab.encryption.private.keypass", "ek4j.t5,rmn.20ef.rdjf" );
      String keyPassPrefix = System.getProperty( "risoe.syslab.encryption.keypass.prefix", "pw-" );
      String keyPassPostfix = System.getProperty( "risoe.syslab.encryption.keypass.postfix", "-2010" );
      KeyStore keystore = KeyStore.getInstance( KEY_STORE_TYPE );
      stream = new FileInputStream( privateStore );
      keystore.load( stream, privateStorePass.toCharArray() );
      String keyPassword = keyPassPrefix + alias.replace( "syslab-", "sys-" ) + keyPassPostfix;
      return sign( message, keystore, alias, keyPassword );
    } catch ( KeyStoreException e ) {
      throw new AssertionError( e );
    } catch ( NoSuchAlgorithmException e ) {
      throw new AssertionError( e );
    } catch ( CertificateException e ) {
      throw new AssertionError( e );
    } catch ( FileNotFoundException e ) {
      throw new AssertionError( e );
    } catch ( IOException e ) {
      throw new AssertionError( e );
    } finally {
      try {
        if ( stream != null ) stream.close();
      } catch ( IOException e ) { /* ignore */ }
    }
  }
  

  /**
   * Signs the given message with a private key.
   * Ther key is found by getting the entry with the given alias from the given keystore,
   * using the given key password.
   * 
   * @param message message to be signed
   * @param keystore key store to be used to retrieve the private key
   * @param alias alias of the private key
   * @param keyPassword password the key is protected with (might be different from the keystore password)
   */
  public byte[] sign( String message, KeyStore keystore, String alias, String keyPassword ) {

    try {
      Signature sig = Signature.getInstance( "SHA1withDSA" );
      PrivateKeyEntry entry = (PrivateKeyEntry) keystore.getEntry( alias, new KeyStore.PasswordProtection( keyPassword.toCharArray() ) );
      if (entry == null ) throw new AssertionError("alias not found in keystore: " + alias );
      PrivateKey privateKey = entry.getPrivateKey();
      sig.initSign( privateKey );
      sig.update( message.getBytes() );
      byte[] signature = sig.sign();
      return signature;

    } catch ( InvalidKeyException e ) {
      throw new AssertionError( e );
    } catch ( NoSuchAlgorithmException e ) {
      throw new AssertionError( e );
    } catch ( SignatureException e ) {
      throw new AssertionError( e );
    } catch ( UnrecoverableEntryException e ) {
      throw new AssertionError( e );
    } catch ( KeyStoreException e ) {
      throw new AssertionError( e );
    }
  }
  
  
  /**
   * Signs the digest of the given message with a private key.
   * Ther key is found by getting the entry with the given alias from the given keystore,
   * using the given key password.
   * 
   * The difference between sign() and signDigest() is that sign() just signs the message, 
   * whereas signDigest() first creates a digest of the message and then signs that. 
   * For large messages, signDigest() will be preferable, because faster.
   * 
   * @param message message to be signed
   * @param keystore key store to be used to retrieve the private key
   * @param alias alias of the private key
   * @param keyPassword password the key is protected with (might be different from the keystore password)
   */
  public byte[] signDigest( String message, KeyStore keystore, String alias, String keyPassword ) {

    try {
      Signature sig = Signature.getInstance( "SHA1withDSA" );
      PrivateKeyEntry entry = (PrivateKeyEntry) keystore.getEntry( alias, new KeyStore.PasswordProtection( keyPassword.toCharArray() ) );
      PrivateKey privateKey = entry.getPrivateKey();
      sig.initSign( privateKey );

      MessageDigest digestInstance = MessageDigest.getInstance( "SHA" );
      digestInstance.update( message.getBytes() );
      byte[] digest = digestInstance.digest();
      sig.update( digest );
      
      byte[] signature = sig.sign();
      
      return signature;

    } catch ( InvalidKeyException e ) {
      throw new AssertionError( e );
    } catch ( NoSuchAlgorithmException e ) {
      throw new AssertionError( e );
    } catch ( SignatureException e ) {
      throw new AssertionError( e );
    } catch ( UnrecoverableEntryException e ) {
      // key not found
      throw new AssertionError( e );
      // return null;
    } catch ( KeyStoreException e ) {
      throw new AssertionError( e );
    }
  }

  /**
   * 
   * @param message
   * @param signature
   * @param alias
   * @return
   */
  public boolean verify( String message, byte[] signature, String alias ) {
    FileInputStream stream = null;
    try {
      String publicStore = System.getProperty( "risoe.syslab.encryption.public.keystore", "conf/keystore-pub.jks" );
      String publicStorePass = System.getProperty( "risoe.syslab.encryption.public.keypass", "sa3l.7kf4dj.klj" );
      KeyStore keystore = KeyStore.getInstance( KEY_STORE_TYPE );
      stream = new FileInputStream( publicStore );
      keystore.load( stream, publicStorePass.toCharArray() );
      return verify( message, signature, keystore, alias );
    } catch ( KeyStoreException e ) {
      throw new AssertionError( e );
    } catch ( NoSuchAlgorithmException e ) {
      throw new AssertionError( e );
    } catch ( CertificateException e ) {
      throw new AssertionError( e );
    } catch ( FileNotFoundException e ) {
      throw new AssertionError( e );
    } catch ( IOException e ) {
      throw new AssertionError( e );
    } finally {
      try {
        if ( stream != null ) stream.close();
      } catch ( IOException e ) { /* ignore */ }
    }
  }
  
  
  /**
   * 
   * @param message
   * @param signature
   * @param keystore
   * @param alias
   * @return
   */
  public boolean verify( String message, byte[] signature, KeyStore keystore, String alias ) {
    try {
      Certificate certificate = keystore.getCertificate( alias );
      PublicKey pubKey = certificate.getPublicKey();
      Signature sig = Signature.getInstance( "SHA1withDSA" );
      sig.initVerify(pubKey);
      sig.update( message.getBytes() );
      return sig.verify( signature );
    } catch ( KeyStoreException e ) {
      throw new AssertionError( e );
    } catch ( NoSuchAlgorithmException e ) {
      throw new AssertionError( e );
    } catch ( InvalidKeyException e ) {
      throw new AssertionError( e );
    } catch ( SignatureException e ) {
      throw new AssertionError( e );
    }
  }
  

  /** 
   */
  public String[] listKeys() {
    FileInputStream stream = null;
    try {
      String publicStore = System.getProperty( "risoe.syslab.encryption.public.keystore", "conf/keystore-pub.jks" );
      String publicStorePass = System.getProperty( "risoe.syslab.encryption.public.keypass", "sa3l.7kf4dj.klj" );
      KeyStore keystore = KeyStore.getInstance( KEY_STORE_TYPE );
      stream = new FileInputStream( publicStore );
      keystore.load( stream, publicStorePass.toCharArray() );
      ArrayList<String> names = new ArrayList<String>();
      Enumeration<String> aliases = keystore.aliases();
      while (aliases.hasMoreElements()) {
        names.add( aliases.nextElement() );
      }
      return names.toArray( new String[ names.size() ] );
    } catch ( KeyStoreException e ) {
      throw new AssertionError( e );
    } catch ( NoSuchAlgorithmException e ) {
      throw new AssertionError( e );
    } catch ( CertificateException e ) {
      throw new AssertionError( e );
    } catch ( FileNotFoundException e ) {
      throw new AssertionError( e );
    } catch ( IOException e ) {
      throw new AssertionError( e );
    } finally {
      try {
        if ( stream != null ) stream.close();
      } catch ( IOException e ) { /* ignore */ }
    }
  }
  

}
