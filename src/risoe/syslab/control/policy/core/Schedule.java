package risoe.syslab.control.policy.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.service.Service;

/** Simple schedule with relative time values and arbitrary values in the schedule.
 * 
 * Use the {@link ScheduleBuilder} class or any of the helper methods 
 * ({@link #makeUniformSchedule(Date, long, Object[])} or
 * {@link #makeSchedule(Date, long[], Object[])}) 
 * to create a schedule. 
 * 
 * @param <T> value type that is stored in the schedule
 * @author daku
 */
public class Schedule<T> implements Controller {

  /** logger */
  private static Logger logger = Logger.getLogger( Schedule.class.getName() );
  
  /** Start of schedule */
  private Date start;
  
  /** List of schedule entries, consisting of a value and a duration */
  private ArrayList<ScheduleEntry<T>> entries; 
  
  /** Normal constructor. Don't use, use {@link ScheduleBuilder},
   * {@link #makeSchedule(Date, Object[], long[])} or 
   * {@link #makeUniformSchedule(Date, Object[], long)} instead!
   * 
   * @param start start date and time of schedule
   * @param entries list of schedule entries
   */
  private Schedule(Date start, ArrayList<ScheduleEntry<T>> entries) {
    super();
    this.start = start;
    this.entries = entries;
  }

  /**
   * Create a schedule where the durations are all the same, e.g. a schedule where
   * all entries have 1 hour duration.
   * 
   * @param <T>
   * @param start start date and time of the schedule
   * @param values values for the schedule
   * @param duration duration in seconds of each schedule entry
   * @return a schedule representing the given values
   */
  public static <T> Schedule<T> makeUniformSchedule(Date start, T[] values, long duration ) {
    ScheduleBuilder<T> builder = new ScheduleBuilder<T>(start);
    for (int i = 0; i < values.length; i++) {
      builder.addEntry( values[i], duration );
    }
    return builder.build();
  }
  
  /**
   * Makes a schedule out of two arrays, one for values, and one for durations.
   * 
   * @param <T> Type of the values in the schedule
   * @param start start of the schedule
   * @param values array with the value parts of the schedule
   * @param durations array with the duration parts of the schedule (in seconds)
   * @return
   */
  public static <T> Schedule<T> makeSchedule(Date start, T[] values, long[] durations ) {
    if ( durations.length != values.length ) throw new IllegalArgumentException("arrays must be of equal length");
    ScheduleBuilder<T> builder = new ScheduleBuilder<T>(start);
    for (int i = 0; i < values.length; i++) {
      builder.addEntry( values[i], durations[i] );
    }
    return builder.build();
  }
  
  /** Simple getter for the start field */
  public Date getStart() {
    return start;
  }

  /** Simple getter for the entries field */
  public ArrayList<ScheduleEntry<T>> getEntries() {
    return entries;
  }
  

  @Override
  public Service getProvidedService() {
    return null;
  }

  @Override
  public boolean wantsToBeActivated() {
    // This object has no means to find out whether it should be activated, 
    // because it does not depend on any external events. That's why it 
    // always returns true.
    return true;
  }

  @Override
  public void start() {
    logger.log( Level.INFO, "use schedule" );
  }

  @Override
  public void stop() {
//    logger.log( Level.INFO, "stop using schedule" );
  }

  /**
   * Class for the schedule entries.
   * 
   * Each entry consists of a value of the given type T and of the duration of this entry in seconds.
   * 
   * @author daku
   *
   * @param <T> Type of the value in the entry
   */
  public static class ScheduleEntry<T> {
    /** value of schedule entry */
    private T value;
    
    /** duration in seconds */
    private long duration;
    
    /** Standard constructor */
    public ScheduleEntry(T value, long duration) {
      super();
      this.value = value;
      this.duration = duration;
    }

    /** Getter for value */
    public T getValue() {
      return value;
    }

    /** Getter for duration */
    public long getDuration() {
      return duration;
    }
    
  }
  
  /** Class for building Schedules, so that Schedule can be an immutable class. */
  public static class ScheduleBuilder<T> {
    
    private Date start;
    private ArrayList<ScheduleEntry<T>> entries;

    /** start building a schedule, giving the start date and time of the schedule */
    public ScheduleBuilder(Date start) {
      this.start = start;
      this.entries = new ArrayList<Schedule.ScheduleEntry<T>>();
    }
    
    /** add a new entry to the schedule, consitising of a value and a duration (in seconds). */    
    public ScheduleBuilder<T> addEntry( T value, long duration ) {
      this.entries.add( new ScheduleEntry<T>(value, duration));
      return this;
    }
    
    /** make a new schedule with the given data */
    public Schedule<T> build() {
      return new Schedule<T>( start,entries );
    }
    
  }

}
