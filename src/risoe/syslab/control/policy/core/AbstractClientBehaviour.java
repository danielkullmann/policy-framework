/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import java.util.ArrayList;
import java.util.Collection;


/**
 * Abstract class implementing {@link ClientBehaviour} that
 * offers the publish/subscribe implementation for {@link ClientListener}s.
 */
public abstract class AbstractClientBehaviour implements ClientBehaviour {

  /** List of registered {@link ClientListener}s. */
  protected final Collection<ClientListener> clientListeners = new ArrayList<ClientListener>();


  @Override
  public void registerClientListener( ClientListener listener ) {
    clientListeners.add( listener );
  }


  @Override
  public void unregisterClientListener( ClientListener listener ) {
    clientListeners.remove( listener );
  }



}
