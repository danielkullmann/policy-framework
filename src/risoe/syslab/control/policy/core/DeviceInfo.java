/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

/** 
 * Interface that must be implemented by all device info classes.
 * 
 * A device info class contains the information about the client
 * that a server needs to create a policy for that client.
 * 
 */
public interface DeviceInfo {

  /** 
   * Returns a String with the message content representation of the
   * device info, something like e.g. "(household ...)"
   * @return a string with the FIPA-like content (a LISP-like syntax)
   */
  public String toFipaContent();


  
}
