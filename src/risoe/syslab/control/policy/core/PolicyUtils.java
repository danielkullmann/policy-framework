/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.apache.log4j.PropertyConfigurator;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.message.AcceptPolicy;
import risoe.syslab.control.policy.message.AliveMessage;
import risoe.syslab.control.policy.message.GiveDeviceInfo;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.control.policy.message.ProposePolicy;
import risoe.syslab.control.policy.message.RegisterClient;
import risoe.syslab.control.policy.message.RejectPolicy;
import risoe.syslab.control.policy.message.RequestNegotiation;
import risoe.syslab.control.policy.message.ServerRequestNegotiation;
import risoe.syslab.control.policy.message.UnregisterClient;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.MessageParser;

/** Helper class that provides several static methods
 * that don't fit anywhere else.
 * 
 * TODO Try to reduce the number of methods in this class
 */
public final class PolicyUtils {

  /** Logger for debug output */
  private static Logger logger = Logger.getLogger( PolicyUtils.class.getName() );
  
  /**
   * List of class names of PolicyMessage
   * classes. They each have a static member, parseMessage(), 
   * that can parse their own message.
   */
  private static ArrayList<String> registeredPolicyMessageParsers;

  /** List of all agents that have been created via {@link #createClientAgent(AbstractAddress, ClientBehaviour, AbstractAddress)}
   * or {@link #createServerAgent(AbstractAddress)}.
   */
  private static ArrayList<AbstractAgent> agents = new ArrayList<AbstractAgent>();

  
  /**
   * Communication system (JADE, HTTP, ..) that is used. Only one such system
   * can be used at the same time.
   */
  private static CommunicationSystem registeredCommunicationSystem = null;

  static {
    registeredPolicyMessageParsers = new ArrayList<String>();
    registeredPolicyMessageParsers.add( AcceptPolicy.class.getName() );
    registeredPolicyMessageParsers.add( AliveMessage.class.getName() );
    registeredPolicyMessageParsers.add( GiveDeviceInfo.class.getName() );
    registeredPolicyMessageParsers.add( ProposePolicy.class.getName() );
    registeredPolicyMessageParsers.add( RegisterClient.class.getName() );
    registeredPolicyMessageParsers.add( RejectPolicy.class.getName() );
    registeredPolicyMessageParsers.add( RequestNegotiation.class.getName() );
    registeredPolicyMessageParsers.add( ServerRequestNegotiation.class.getName() );
    registeredPolicyMessageParsers.add( UnregisterClient.class.getName() );
  }

  
  /**
   * private Constructor.
   * 
   * This class should never be instantiated.
   */
  private PolicyUtils() {
    // empty!
  }


  /**
   * Method setting up the policy system.
   * Right now, it loads the policy-framework.properties file.
   * This file contains the configuration for the public and private keys
   * for key signing, and some other settings regarding e.g. the location
   * of servers for demos.
   * 
   * @param propertiesFileName file that containes the policy framework properties
   */
  public static void setup( String propertiesFileName ) {
    try {
      Properties properties = new Properties();
      properties.load( new FileInputStream( new File( propertiesFileName ) ) );
      // System.setProperties( Properties ) replaces the system properties set,
      // so we can't use this method...
      for ( Object key : properties.keySet() ) {
        if (System.getProperty((String) key) == null) {
          System.setProperty( (String) key, properties.getProperty( (String) key ) );
        }
      }
    } catch ( FileNotFoundException e ) {
      throw new AssertionError( "policy-framework properties file not found" );
    } catch ( IOException e ) {
      throw new AssertionError( "policy-framework properties file could not be loaded" );
    }
    
    if ( System.getProperty( "java.util.logging.config.file" ) != null ) {
      try {
        LogManager.getLogManager().readConfiguration();
      } catch ( Exception e ) {
        throw new RuntimeException( e );
      }
    }

    if ( System.getProperty( "log4j.configuration" ) != null ) {
      PropertyConfigurator.configure( System.getProperty( "log4j.configuration" ) );
    }
      
  }
  
  
  /**
   * "Factory" that creates a {@link PolicyMessage} from String content.
   * 
   * This method tries all the registered {@link PolicyMessage} parsers
   * to see if one can parse the given content.
   * 
   * @param msgSenderId the {@link AbstractAddress} of the sender of that message
   * @param msgReceiverId the {@link AbstractAddress} of the receiver of that message
   * @param content the content string of the policy, as created by {@link Policy#toFipaContent()}
   * @param parser 
   * @return a {@link PolicyMessage} instance, or null if parsing failed
   */
  @SuppressWarnings( "unchecked" )
  public static PolicyMessage parseMessage( AbstractAddress msgSenderId, AbstractAddress msgReceiverId, String content, PolicyParser parser ) {
    MessageParser mp = new MessageParser();
    Expression expr = mp.parse( content );
    PolicyMessage msg = null;
    for ( String className : registeredPolicyMessageParsers ) {
      try {
        Class<PolicyMessage> clazz = (Class<PolicyMessage>) Class.forName( className );
        Method parseMethod = clazz.getMethod( "parse", new Class[] { AbstractAddress.class, AbstractAddress.class, ExpressionList.class, PolicyParser.class } );
        msg = (PolicyMessage) parseMethod.invoke( null, new Object[] { msgSenderId, msgReceiverId, (ExpressionList) expr, parser } );
        if ( msg != null ) return msg;
      } catch ( ClassNotFoundException e ) {
        throw new IllegalArgumentException( "no such PolicyMessage class: " + className );
      } catch ( SecurityException e ) {
        throw new IllegalArgumentException( "class method can't be accessed: " + className );
      } catch ( NoSuchMethodException e ) {
        throw new IllegalArgumentException( "no parse method: " + className );
      } catch ( IllegalArgumentException e ) {
        throw new IllegalArgumentException( "can't call parse method: " + className );
      } catch ( IllegalAccessException e ) {
        throw new IllegalArgumentException( "can't call parse method: " + className );
      } catch ( InvocationTargetException e ) {
        throw new IllegalArgumentException( "can't call parse method: " + className, e.getTargetException() );
      }
    }
    throw new AssertionError( "message " + content.substring(0, 20 ) + "... is not supported" );
  }


  /**
   * Register the {@link CommunicationSystem}. Before any communication can take place, 
   * the {@link CommunicationSystem} first has to be registered using this method
   * 
   * @param commSystem the {@link CommunicationSystem} to be registered
   */
  public static void setCommunicationSystem ( CommunicationSystem commSystem ) {
    registeredCommunicationSystem = commSystem;
    commSystem.startup();
  }
  

  /**
   * Return the {@link CommunicationSystem} that is used.
   * 
   * @return the {@link CommunicationSystem} used
   */
  public static CommunicationSystem getCommunicationSystem () {
    return registeredCommunicationSystem;
  }
  
  
  /**
   * Create a local address using the actual {@link CommunicationSystem}.
   * @param agentName local name of agent (local part of address without container name)
   * @return a new {@link AbstractAddress} instance
   */
  public static AbstractAddress createAddress( String agentName ) {
    return registeredCommunicationSystem.createAddress( agentName );
  }

  
  /**
   * Create a remote address using the actual {@link CommunicationSystem}.
   * @param agentName local name of agent
   * @param containerName cname of container the agent "lives in"
   * @param hostName name of host the agent is on
   * @return a new {@link AbstractAddress} instance
   */
  public static AbstractAddress createAddress( String agentName, String containerName, String hostName ) {
    return getCommunicationSystem().createAddress( agentName, containerName, hostName );
  }


  /**
   * Create a policy server agent on this machine.
   * @param address address of agent
   * @return a new {@link AbstractAgent} instance
   */
  public static AbstractAgent createServerAgent( AbstractAddress address, ServerBehaviour serverImpl ) {
    CommunicationSystem commSystem = getCommunicationSystem();
    PolicyServer policyServer = new PolicyServer( address, serverImpl );
    AbstractAgent agent = commSystem.createandStartAgent( address, policyServer );
    agents.add(agent);
    logger.log( Level.FINE, "server started: " + agent.getAddress().getName() );
    return agent;
  }


  /**
   * Create a policy client agent on this machine.
   * @param address address of agent
   * @param behaviour client behaviour that should be used
   * @param serverAddress address of server this agent should connect to
   * @return a new {@link AbstractAgent} instance
   */
  public static AbstractAgent createClientAgent( AbstractAddress address, ClientBehaviour behaviour, AbstractAddress serverAddress ) {
    CommunicationSystem commSystem = getCommunicationSystem();
    PolicyClient policyClient = new PolicyClient( address, serverAddress );
    policyClient.setBehaviour( behaviour );
    AbstractAgent agent = commSystem.createandStartAgent( address, policyClient );
    agents.add(agent);
    logger.log( Level.FINE, "client started: " + agent.getAddress() );
    return agent;
  }


  /** Stops all agents that have been created via
   * {@link #createClientAgent(AbstractAddress, ClientBehaviour, AbstractAddress)} or
   * {@link #createServerAgent(AbstractAddress)}.
   */
  public static void stopAllAgents() {
    for ( AbstractAgent agent : agents ) {
      agent.stopAgent();
    }
  }

}
