/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;



/**
 * Encapsulates the type-specific part
 * of the policy client.
 * 
 * This class has several tasks:
 * <ul>
 * <li> create a {@link DeviceInfo} instance for the actual device
 * <li> check whether the device accepts a certain policy
 * <li> activate this policy.
 * </ul>
 */
public interface ClientBehaviour extends PolicyParser {


  /**
   * This returns info about the device this client is
   * controlling. The actual return type depends on the 
   * type of policy used
   * 
   * @return a DeviceInfo instance; may not be null!
   */
  DeviceInfo provideDeviceInfo();

  
  /**
   * Checks whether a given policy is accepted by this client.
   * 
   * @param policy Policy to be checked
   * @return true, iff the client accepts the policy; false otherwise
   */
  boolean acceptPolicy( Policy policy );


  /**
   * This method activates the given policy for this device.
   * @param policy current policy that should be activated
   */
  void activatePolicy( Policy policy );


  /** Returns the currently active policy, or null if none has been activated yet
   * 
   * @return the active policy, or null
   */
  Policy getActivePolicy();

  
  /**
   * This method should return true when the client should 
   * unregister from its server.
   * 
   * @return true, iff the client should unregister from the server
   */
  boolean shouldUnregister();


  /**
   * Register a {@link ClientListener} to be notified of changes to the client
   * @param listener {@link ClientListener} to be registered
   */
  void registerClientListener( ClientListener listener );


  /**
   * Unregister a {@link ClientListener} that was registered with {@link #registerClientListener(ClientListener)}.
   * @param listener {@link ClientListener} to be unregistered
   */
  void unregisterClientListener( ClientListener listener );
  
  
  /**
   * Stop this client. This is called when the client is not available anymore
   */
  void stop();
  
}
