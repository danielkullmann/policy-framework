/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;

/**
 * Policy that can be used when the server has not yet
 * calculated an actual policy for the client.
 */
public class IdlePolicy implements Policy {

  /** String that identifies IdlePolicy messages in the FIPA content. */
  private static final String MSG_ID = "idle-policy";
  
  
  /**
   * Time frame (validity period) of policy
   */
  private TimeFrame timeFrame;

  
  /** Tries to parse the given expression into an IdlePolicy instance.
   *  
   * @param expression expression to parse
   * @return the parsed IdlePolicy, or null if that failed
   */
  public static IdlePolicy parse( Expression expression ) {
    
    String pattern =  "(" + MSG_ID + " " + 
      TimeFrame.KEY_TIME_FRAME + " ?tf" +
      ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null ) {
      return null;
    }
    
    TimeFrame timeFrame = TimeFrame.parse( match.get( "tf" ) );
    
    return new IdlePolicy( timeFrame );
  }

  
  /**
   * @param duration duration of policy in minutes
   */
  public IdlePolicy( int duration ) {
    timeFrame = TimeFrame.createTimeFrame( new Date(), 0, 0, duration );
  }
  

  /** Internal constructor, used by {@link #parse(Expression)}
   * @param timeFrame time frame for this policy
   */
  protected IdlePolicy( TimeFrame timeFrame ) {
    this.timeFrame = timeFrame;
  }


  @Override
  public TimeFrame getTimeFrame() {
    return timeFrame;
  }

  
  @Override
  public String toFipaContent() {
    return "(" + MSG_ID + " " + TimeFrame.KEY_TIME_FRAME + " " + timeFrame.toFipaContent() + ")";
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((timeFrame == null) ? 0 : timeFrame.hashCode());
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    IdlePolicy other = (IdlePolicy) obj;
    if ( timeFrame == null ) {
      if ( other.timeFrame != null ) {
        return false;
      }
    } else if ( !timeFrame.equals( other.timeFrame ) ) {
      return false;
    }
    return true;
  }

}
