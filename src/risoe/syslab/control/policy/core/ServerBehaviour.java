/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import risoe.syslab.control.policy.comm.AbstractAddress;

/**
 * Interface where functions dependent on the actual type of
 * the policies used is implemented. The main functions are
 * policy parser, DeviceInfo parser and creating the
 * {@link ServerClientBehaviour}. This is for the server side.
 * 
 * When defining a new policy type, an implementation of this
 * interface has to be provided and to be registered using
 *  {@link PolicyUtils#registerPolicyImplementation(PolicyImplementation)}
 */
public interface ServerBehaviour extends PolicyParser {

  /**
   * The actual PolicyServer is sometimes needed to start a renegotiation,
   * so here the {@link PolicyServer} can be set. 
   * 
   * @param policyServer
   */
  void setPolicyServer( PolicyServer policyServer );
  
  /**
   * Create a new {@link ServerClientBehaviour} instance for that type of policy,
   * for a certain client.
   * 
   * @param clientAddress AbstractAddress of client
   * @param deviceInfo DeviceInfo of client
   * @return the created server behaviour
   */
  ServerClientBehaviour createServerBehaviour( AbstractAddress clientAddress, DeviceInfo deviceInfo );

}
