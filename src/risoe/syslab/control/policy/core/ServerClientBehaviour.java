/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;


/**
 * An implementation of a policy needs to implement this class.
 * For each connected client, an instance of this interface
 * is created, via 
 * {@link ServerBehaviour#createServerBehaviour(risoe.syslab.control.policy.comm.AbstractAddress, DeviceInfo)}.
 */
public interface ServerClientBehaviour {

  
  /** Creates a policy for this client.
   * 
   * @return The policy for this client
   */
  Policy createPolicy();
  
  
  /** Returns the {@link DeviceInfo} that is connected to this client.
   *  
   * @return device info of the client
   */
  DeviceInfo getClientDeviceInfo();

  
  /** When the device info of the client changes, the server 
   * is informed by calling this method.
   *  
   * @param deviceInfo (new) info of the client
   */
  void setClientDeviceInfo( DeviceInfo deviceInfo );

  
  /** Sets the currently active policy
   * 
   * @param policy the policy that is the currently active policy, can be null
   */
  void activatePolicy( Policy policy );
  
  
  /** Simple getter for the currently active policy
   * 
   * @return the currently active policy, can be null
   */
  Policy getActivePolicy();

  
  /** When a policy is rejected by the client, this method is called.
   * 
   * @param policy the policy that was rejected
   */
  void rejectPolicy( Policy policy );
 
}
