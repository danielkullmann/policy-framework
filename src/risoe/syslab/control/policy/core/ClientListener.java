/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import risoe.syslab.control.policy.comm.AbstractAddress;


/**
 * 
 */
public interface ClientListener {

  void deviceInfoChanged( AbstractAddress address, DeviceInfo deviceInfo );
  void activePolicyChanged( AbstractAddress address, Policy policy);
  
}
