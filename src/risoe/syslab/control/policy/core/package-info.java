/**
 * This package contains the core files of the policy protocol,
 * with interfaces that describe the different concepts.
 */
package risoe.syslab.control.policy.core;
