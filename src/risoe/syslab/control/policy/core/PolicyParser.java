package risoe.syslab.control.policy.core;

import risoe.syslab.control.policy.message.parser.Expression;


public interface PolicyParser {

  /**
   * Creates a {@link DeviceInfo} object from 
   * parsed message content.
   * 
   * @param expression the {@link Expression} that should be parsed into a {@link DeviceInfo} object
   * @return the parsed {@link DeviceInfo}, or null if that failed
   */
  DeviceInfo parseDeviceInfo( Expression expression);

  /**
   * Creates a {@link Policy} object from
   * parsed message content.
   * 
   * @param expression
   * @return the parsed {@link Policy}, or null if that failed
   */
  Policy parsePolicy( Expression expression );
  
}
