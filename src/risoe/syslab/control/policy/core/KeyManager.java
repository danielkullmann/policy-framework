package risoe.syslab.control.policy.core;

import java.io.PrintStream;


public class KeyManager {

  /**
   * @param args
   */
  public static void main(String[] args) {
    PolicyUtils.setup( "conf/policy-framework.properties" );
    if ( args.length == 0 ) {
      System.err.println("Usage: java KeyManager <cmd>");
      System.err.println("Commands: ");
      System.err.println(" -l  list all keys" );
      System.exit( 1 );
    }
    
    String command = args[0];
    if ( "-l".equals( command ) ) {
      listKeys();
    }
  }

  private static void listKeys() {
    String[] names = new MessageSigner().listKeys();
    PrintStream out = System.out; 
    for (String name : names) {
      out.println( name );
    }
  }

}
