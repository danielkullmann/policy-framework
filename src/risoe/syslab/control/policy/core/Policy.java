/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;


/**
 * Interface for all policy implementations.
 */
public interface Policy {

  /** Returns a String with the message content representation of the
   * policy, something like e.g. "(schedule ...)"
   * @return string with FIPA-like content
   */
  String toFipaContent();


  /**
   * Returns TimeFrame where policy is valid
   * 
   * @return timeframe of this policy
   */
  TimeFrame getTimeFrame();
  
  
}
