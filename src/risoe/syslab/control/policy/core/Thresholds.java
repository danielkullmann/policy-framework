package risoe.syslab.control.policy.core;

import java.io.Serializable;
import java.util.HashMap;

import risoe.syslab.control.policy.message.parser.DoubleConstant;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;


/**
 * Provides low/high thresholds, plus hysteresis.
 * 
 * You can use this class in connection with a {@link ThresholdRunner}, which calls
 * a callback when the state of the system (NORMAL,HIGH,LOW) changes.
 * 
 * @author daku
 */
public class Thresholds implements Serializable {

  /** generated serial UID */
  private static final long serialVersionUID = 8714490121997202768L;
  
  /** value of low threshold */
  private double low;
  
  /** value of high threshold */
  private double high;
  
  /** hysteresis used for both thresholds */
  private double hysteresis;
  
  
  /** Standard constructor for low and high threshold, and hysteresis.
   * 
   * @param low
   * @param high
   * @param hysteresis
   * @throws IllegalArgumentException if values are inconsistent
   */
  public Thresholds(double low, double high, double hysteresis) {
    super();
    if ( low > high ) {
      throw new IllegalArgumentException("low threshold is larger than high threshold");
    }
    if ( hysteresis < 0 ) {
      throw new IllegalArgumentException("hysteresis must be >= zero");
    }
    this.low = low;
    this.high = high;
    this.hysteresis = hysteresis;
  }

  
  /** Standard constructor for low and high threshold.
   * 
   * Hysteresis is calculated to be 1% of the difference between low and high.
   * 
   * @param low
   * @param high
   * @throws IllegalArgumentException if values are inconsistent
   */
  public Thresholds(double low, double high) {
    super();
    if ( low > high ) {
      throw new IllegalArgumentException("low threshold is larger than high threshold");
    }
    this.low = low;
    this.high = high;
    this.hysteresis = Math.abs(high-low)/100;
  }
  
  
  /** Constructs a {@link Thresholds} instance from a parse tree.
   * 
   * This is the reverse operation to {@link #toFipaContent()}
   * 
   * @param expression parse tree containing a representation of Thresholds.
   * @return
   */
  public static Thresholds parse( Expression expression ) {
    String pattern =  "(thresholds " + 
      ":low ?low " +
      ":high ?high " +
      ":hyst ?hyst " +
      ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    
    double low = ((DoubleConstant) match.get( "low" )).getValue();
    double high = ((DoubleConstant) match.get( "high" )).getValue();
    double hysteresis = ((DoubleConstant) match.get( "hyst" )).getValue();
    return new Thresholds(low, high, hysteresis);
  }

  
  /** Simple getter method for the low value
   * 
   * @return low value
   */
  public double getLow() {
    return low;
  }

  
  /** Simple getter method for the high value
   * 
   * @return high value
   */
  public double getHigh() {
    return high;
  }

  
  /** Simple getter method for the hysteresis value
   * 
   * @return hysteresis value
   */
  public double getHysteresis() {
    return hysteresis;
  }

  
  /** Returns string representation of this object that can be used
   * in messages.
   * 
   * The string representation created by {@link #toFipaContent()} can be used
   * to create a new instance via {@link #parse(Expression)}:
   * 
   * assertEquals(thr, TimeThresholds.parse(new MessageParser().parse(thr.toFipaContent())));
   * 
   * @return string representation
   */
  public String toFipaContent() {
    return "(thresholds :low " + low + " :high " + high + " :hyst " + hysteresis + ")";
  }
  

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(high);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(hysteresis);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(low);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Thresholds other = (Thresholds) obj;
    if (Double.doubleToLongBits(high) != Double.doubleToLongBits(other.high))
      return false;
    if (Double.doubleToLongBits(hysteresis) != Double.doubleToLongBits(other.hysteresis))
      return false;
    if (Double.doubleToLongBits(low) != Double.doubleToLongBits(other.low))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Thresholds [low=" + low + ", high=" + high + ", hysteresis=" + hysteresis + "]";
  }  
  
}
