/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.core;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.message.AcceptPolicy;
import risoe.syslab.control.policy.message.AliveMessage;
import risoe.syslab.control.policy.message.GiveDeviceInfo;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.control.policy.message.ProposePolicy;
import risoe.syslab.control.policy.message.RegisterClient;
import risoe.syslab.control.policy.message.RejectPolicy;
import risoe.syslab.control.policy.message.RequestNegotiation;
import risoe.syslab.control.policy.message.ServerRequestNegotiation;
import risoe.syslab.control.policy.message.UnregisterClient;
import risoe.syslab.control.policy.message.parser.Expression;

/**
 * This is the server side of a policy protocol,
 * also known as the "aggregator".
 * The main method is {@link #action()}
 * which handles the state of the clients and sends and receives messages.
 * 
 * It is usually not used directly; the server is usually started with this code:
 * <pre>
 * CommunicationSystem commSystem = ...;
 * AbstractAddress serverAddress = commSystem.createAddress( "server" );
 * AbstractAgent s = PolicyUtils.createServerAgent( serverAddress );
 * // s can be used to stop the server 
 * </pre>
 */
public final class PolicyServer extends PolicyPartner {

  /** logger instance */
  private static final Logger logger = Logger.getLogger( PolicyServer.class.getName() );

  
  /**
   * Status of a client.
   */
  public enum ClientStatus {
    /**
     * client has just been added to the client list
     */
    NEW,
    /**
     * sent a negotiation request to the client
     */
    NEGOTIATION_SENT,
    /**
     * Client wants a policy, but has not yet gotten one..
     */
    WANTS_POLICY,
    /**
     * proposed a policy to the client
     */
    POLICY_SENT,
    /**
     * the client has accepted a policy
     */
    POLICY_ACCEPTED, 
    /**
     * Client has not sent AliveMessage for a long time...
     */
    MISSING
  };

  
  /**
   * How often is the state of the clients checked (in seconds)?
   */
  private static final int CHECK_FREQUENCY = 30;


  /**
   * When is a warning issued that a client did not send
   * an AliveMessage? (seconds). This value can be changed.
   */
  public static int ALIVE_THRESHOLD = 120;
  

  /**
   * Address of this instance.
   */
  private AbstractAddress serverAddress;
  
  
  /**
   * List of connected client with other info..
   */
  private HashMap<AbstractAddress, PolicyClientStatus> clients;
  
  
  /**
   * The server checks periodically for each client, 
   * whether there is now a policy that has to be activated
   */
  Date nextCheck = null;

  /**
   * Server implementation to use to create {@link ServerClientBehaviour}, and to
   * parse {@link Policy} and {@link DeviceInfo} representations.
   */
  private ServerBehaviour serverImpl;
  
  
  /**
   * Constructor
   * 
   * @param serverAddress may NOT be null
   */
  public PolicyServer( AbstractAddress serverAddress, ServerBehaviour serverImpl ) {
    this.serverAddress = serverAddress;
    this.serverImpl = serverImpl;
    this.clients = new HashMap<AbstractAddress,PolicyClientStatus>();
    serverImpl.setPolicyServer( this );
    assert( this.serverAddress != null );
  }


  /** Start renegotiation of policies for all clients 
   */
  public void startRenegotiation() {
    for ( AbstractAddress clientAddress : clients.keySet() ) {
      startRenegotiation( clientAddress );
    }
  }
  

  /** Start renegotiation of policy for a single client 
   * 
   * @param clientAddress address of client
   */
  public void startRenegotiation(AbstractAddress clientAddress) {
    PolicyClientStatus client = clients.get( clientAddress );
    if ( client == null ) throw new IllegalArgumentException( "unknown client address: " + clientAddress );
    
    // Client is already in the right state -> skip over it
    if ( client.getStatus().equals( ClientStatus.NEGOTIATION_SENT ) ) {
      return;
    }
    
    if ( client.getStatus().equals( ClientStatus.WANTS_POLICY ) ) {
      Policy policy = client.getServerBehaviour().createPolicy();
      if ( policy != null ) {
        ProposePolicy returnMsg = new ProposePolicy( client.getNegotiationUuid(), clientAddress, serverAddress, policy, null );
        sendMessage( returnMsg );
        client.setStatus( ClientStatus.POLICY_SENT );
        return;
      }
    }

    // Send the request to renegotiate
    ServerRequestNegotiation msg = new ServerRequestNegotiation( client.getNegotiationUuid(), 
        clientAddress, serverAddress, null );
    sendMessage( msg );
    client.setStatus( ClientStatus.NEGOTIATION_SENT );
    
  }
  
  
  @Override
  public void action() {
    
    Date now = new Date();
    if ( nextCheck == null || now.after( nextCheck ) ) {
      
      // Do a check
      for ( PolicyClientStatus client : clients.values() ) {
        client.checkPolicies();
      }
      
      // Calculate next time the check has to be done...
      Calendar cal = new GregorianCalendar();
      cal.setTime( now );
      cal.add( Calendar.SECOND, CHECK_FREQUENCY );
      nextCheck = cal.getTime();
    }
    
    PolicyMessage msg = receivePolicyMessage();
    
    for ( AbstractAddress clientAddress : clients.keySet() ) {
      PolicyClientStatus client = clients.get( clientAddress );
      if ( client.getStatus() == ClientStatus.WANTS_POLICY ) {
        Policy policy = client.getServerBehaviour().createPolicy();
        if ( policy != null ) {
          ProposePolicy rmsg = new ProposePolicy( client.getNegotiationUuid(), clientAddress, serverAddress, policy, null );
          sendMessage( rmsg );
          client.setStatus( ClientStatus.POLICY_SENT );
        }
      }
      // NEW clients need a new policy negotiation round 
      if ( client.getStatus() == ClientStatus.NEW ) {
        ServerRequestNegotiation returnMsg = new ServerRequestNegotiation( client.getNegotiationUuid(), 
            clientAddress, serverAddress, null );
        sendMessage( returnMsg );
        client.setStatus( ClientStatus.NEGOTIATION_SENT );
      }
    }

    if ( msg instanceof RegisterClient ) {
      
      PolicyClientStatus client = getInternalClientStatus( msg.getSenderAddress() );
      if ( client == null ) {
        client = new PolicyClientStatus( msg.getSenderAddress() );
        client.setNegotiationUuid( msg.getNegotiationId() );
        clients.put( msg.getSenderAddress(), client );
      }
      
      // When a client register itself again, it means that it died..
      ServerRequestNegotiation returnMsg = new ServerRequestNegotiation( msg.getNegotiationId(), 
          msg.getSenderAddress(), serverAddress, null );
      sendMessage( returnMsg );
      client.setStatus( ClientStatus.NEGOTIATION_SENT );
      client.setAlive( new Date() );
      
    } else if ( msg instanceof RequestNegotiation ) {
      // message is sent anyway, regardless of whether the client is actually known
      ServerRequestNegotiation returnMsg = new ServerRequestNegotiation( msg.getNegotiationId(), 
          msg.getSenderAddress(), serverAddress, null );
      sendMessage( returnMsg );
      // Check whether client already registered
      PolicyClientStatus client = clients.get( msg.getSenderAddress() );
      if ( client == null ) {
        client = new PolicyClientStatus( msg.getSenderAddress() );
        client.setNegotiationUuid( msg.getNegotiationId() );
        clients.put( msg.getSenderAddress(), client );
      }
      client.setStatus( ClientStatus.NEGOTIATION_SENT );
      client.setAlive( new Date() );
      
    } else if ( msg instanceof GiveDeviceInfo ) {

      PolicyClientStatus client = clients.get( msg.getSenderAddress() );
      if ( client == null ) {
        client = new PolicyClientStatus( msg.getSenderAddress() );
        client.setNegotiationUuid( msg.getNegotiationId() );
        clients.put( msg.getSenderAddress(), client );
      }
      if ( client.getServerBehaviour() == null ) {
        client.setServerBehaviour( serverImpl.createServerBehaviour( msg.getSenderAddress(), ((GiveDeviceInfo) msg).getDeviceInfo() ) );
      } else {
        client.getServerBehaviour().setClientDeviceInfo( ((GiveDeviceInfo) msg).getDeviceInfo());
      }
      client.setAlive( new Date() );
      Policy policy = client.getServerBehaviour().createPolicy();
      // if policy returned is NULL, mark client as needing a policy
      if ( policy == null ) {
        client.setStatus( ClientStatus.WANTS_POLICY );
      } else {
        ProposePolicy returnMsg = new ProposePolicy( msg.getNegotiationId(), msg.getSenderAddress(), serverAddress, policy, null );
        sendMessage( returnMsg );
        client.setStatus( ClientStatus.POLICY_SENT );
      }

    } else if ( msg instanceof AcceptPolicy ) {
      
      PolicyClientStatus client = getInternalClientStatus( msg.getSenderAddress() );
      if ( client != null ) {
        client.setAlive( new Date() );
        client.setStatus( ClientStatus.POLICY_ACCEPTED );
        client.addPolicy( msg.getPolicy() );
      } else {
        logger.log( Level.SEVERE, "server: (received AcceptPolicy message) Client ID unknown: " + msg.getSenderAddress() );
        // send appropriate message to client
        ServerRequestNegotiation msg2 = new ServerRequestNegotiation( 
            msg.getNegotiationId(), msg.getSenderAddress(), serverAddress, null );
        sendMessage( msg2 );
      }
      
    } else if ( msg instanceof RejectPolicy ) {
      
      PolicyClientStatus client = getInternalClientStatus( msg.getSenderAddress() );
      if ( client != null ) {
        client.setAlive( new Date() );
        client.getServerBehaviour().rejectPolicy( msg.getPolicy() );
        client.setStatus( ClientStatus.WANTS_POLICY );
      } else {
        logger.log( Level.SEVERE, "server: (received RejectPolicy message) Client ID unknown: " + msg.getSenderAddress() );
        // send appropriate message to client
        ServerRequestNegotiation msg2 = new ServerRequestNegotiation( 
            msg.getNegotiationId(), msg.getSenderAddress(), serverAddress, null );
        sendMessage( msg2 );
      }
      
    } else if ( msg instanceof AliveMessage ) {

      PolicyClientStatus client = getInternalClientStatus( msg.getSenderAddress() );
      if ( client != null ) {
        client.setAlive( new Date() );
        if ( client.status == ClientStatus.MISSING ) {
          logger.log( Level.INFO, "Got AliveMessage from " + client.clientId );
          client.status = ClientStatus.NEW; 
        }
      } else {
        logger.log( Level.SEVERE, "server: (received AliveMessage) Client ID unknown: " + msg.getSenderAddress() );
        // send appropriate message to client
        ServerRequestNegotiation msg2 = new ServerRequestNegotiation( 
            msg.getNegotiationId(), msg.getSenderAddress(), serverAddress, null );
        sendMessage( msg2 );
      }
      
    } else if ( msg instanceof UnregisterClient ) {
      
      PolicyClientStatus client = clients.remove( msg.getSenderAddress() );
      if ( client == null ) {
        logger.log( Level.SEVERE, "server: (received UnregisterCLient message) Client ID unknown: " + msg.getSenderAddress() );
      }

    } else if ( msg != null ){
      logger.log( Level.SEVERE, "server: Unknown messge: " + msg );
    }
    
  }


  /**
   * Inner class that encapsulates the state of a client as seen from the server side.
   * A client has a {@link ClientStatus}, a server behaviour, a list of accepted policies
   * and an active policy (which can be null)
   */
  private class PolicyClientStatus {

    /** Address of that client */
    private AbstractAddress clientId;
    
    /** Status of that client */
    private ClientStatus status;
    
    /** Policies that the client has accepted */
    private ArrayList<Policy> acceptedpolicies;
    
    /** Server behaviour for that client */
    private ServerClientBehaviour serverBehaviour;
    
    /** Last time an alive message was received.. */
    private Date alive;

    /** Negotation {@link UUID} that has been used in the current communication.
     * TODO The UUID of all messages should be checked.
     */
    private UUID negotiationUuid = null;

    
    /** Constructor
     */
    public PolicyClientStatus(AbstractAddress clientId) {
      this.clientId = clientId;
      this.status = ClientStatus.NEW;
      this.acceptedpolicies = new ArrayList<Policy>();
      this.alive = new Date();
    }


    /**
     * Simple setter
     * @param status the status of the client
     */
    public void setStatus( ClientStatus status ) {
      this.status = status;
    }

    
    /** 
     * Add an accepted policy.
     * 
     * @param policy the accepted policy
     */
    public synchronized void addPolicy( Policy policy ) {
      acceptedpolicies.add( policy );
    }

    /**
     * Looks for a policy to be activated, and removes old policies
     */
    public synchronized void checkPolicies() {

      // Emit one log message when client has been inactive
      // for a longer period
      if ( status != ClientStatus.MISSING ) {
        Calendar cal = new GregorianCalendar();
        cal.setTime( getAlive() );
        cal.add( Calendar.SECOND, ALIVE_THRESHOLD );
        if ( new Date().after( cal.getTime() ) ) {
          logger.log( Level.SEVERE, "server: client " + clientId + " did not send an AliveMessage for a long time.." );
          status = ClientStatus.MISSING;
        }
      }

      Date now = new Date();

      Policy activePolicy = null;
      if ( serverBehaviour != null) activePolicy = serverBehaviour.getActivePolicy(); 
      
      if ( activePolicy != null && 
           activePolicy.getTimeFrame().isCurrent( now ) ) {
        // No need to search for a new policy to activate, because the
        // active policy is current.
        return;
      }
      
      // Deactivate active policy if it has expired
      if ( serverBehaviour != null && activePolicy != null && activePolicy.getTimeFrame().isExpired( now ) ) {
        serverBehaviour.activatePolicy( null );
      }

      // Search list of all accepted policy for one that 
      // could be activated; remove expired policies at
      // the same time
      ArrayList<Policy> newList = new ArrayList<Policy>();
      for ( Policy policy : acceptedpolicies ) {
        if ( ! policy.getTimeFrame().isExpired( now ) ) {
          newList.add( policy );
        }
        if ( activePolicy == null && policy.getTimeFrame().isCurrent( now ) ) {
          serverBehaviour.activatePolicy( policy );
          activePolicy = policy;
          status = ClientStatus.POLICY_ACCEPTED;
        }
      }

      // Reset client status if it has no active policy anymore
      acceptedpolicies = newList;
      if ( activePolicy == null && status == ClientStatus.POLICY_ACCEPTED ) {
        status = ClientStatus.NEW;
      }
      
      // TODO Not sure why this is here; I think this is already being done above
      if ( activePolicy != null ) {
        if ( ! activePolicy.equals( serverBehaviour.getActivePolicy() ) ) {
          serverBehaviour.activatePolicy( activePolicy );
          status = ClientStatus.POLICY_ACCEPTED;
        }
      }

    }


    /**
     * Simple getter for the {@link ServerClientBehaviour}
     * @return the server behaviour of that client
     */
    public ServerClientBehaviour getServerBehaviour() {
      return serverBehaviour;
    }

    
    /**
     * Simple setter for the {@link ServerClientBehaviour}
     * @param serverBehaviour the server behaviour of that client
     */
    public void setServerBehaviour( ServerClientBehaviour serverBehaviour ) {
      this.serverBehaviour = serverBehaviour;
    }

    
    /**
     * Get the last date an {@link AliveMessage} was received from this client
     * @return the date of the last {@link AliveMessage}
     */
    public Date getAlive() {
      return alive;
    }
    

    /**
     * Set the last date an {@link AliveMessage} was received from this client
     * @param alive the date of the last {@link AliveMessage}
     */
    public void setAlive( Date alive ) {
      this.alive = alive;
    }

    
    /**
     * Simple getter for {@link ClientStatus} field
     * @return the current status of this client
     */
    public ClientStatus getStatus() {
      return status;
    }
    

    /**
     * Simple getter for the negotiation {@link UUID}
     * @return the {@link UUID} currently used in communication with the client
     */
    public UUID getNegotiationUuid() {
      return negotiationUuid;
    }

    
    /**
     * Simple setter foor the negotiation {@link UUID}
     * @param negotiationUuid the {@link UUID} that is currently used in communication with the client
     */
    public void setNegotiationUuid( UUID negotiationUuid ) {
      this.negotiationUuid = negotiationUuid;
    }

  }
  
  
  /** Return the status of the given client
   * 
   * @param clientAddress address of client we are interested in
   * @return the status (NEW, NEGOTIATION_SENT, WANTS_POLICY, POLICY_SENT, POLICY_ACCEPTED, MISSING)  
   */
  public ClientStatus getClientStatus( final AbstractAddress clientAddress ) {
    return clients.get( clientAddress ).status;
  }

  /**
   * Look up the status of a client by its AbstractAddress.
   * 
   * @param clientAddress the {@link AbstractAddress} of the client we are loooking for
   * @return the status of that client; can be null
   */
  private PolicyClientStatus getInternalClientStatus( final AbstractAddress clientAddress ) {
    return clients.get( clientAddress );
  }


  @Override
  public AbstractAddress getAgentId() {
    return serverAddress;
  }

  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return serverImpl.parseDeviceInfo( expression );
  }


  @Override
  public Policy parsePolicy( Expression expression ) {
    return serverImpl.parsePolicy( expression );
  }

}
