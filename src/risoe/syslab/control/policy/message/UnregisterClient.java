/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message;

import java.util.UUID;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.MessageSignature;
import risoe.syslab.control.policy.core.PolicyParser;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.Symbol;

/**
 * Message class for unregistering a client from its server 
 */
public class UnregisterClient extends PolicyMessage {

  /** the unique message id used to identify this message */
  private static final String MSG_ID = "unregister-client";

  /**
   * Tries to parse an {@link UnregisterClient} message
   * 
   * @param clientAddress the client AbstractAddress of the message
   * @param serverAddress the server AbstractAddress of that message
   * @param parsedContent the pre-parsed content of the content of that message
   * @return The parsed {@link UnregisterClient} instance, or null if parsing failed
   */
  public static UnregisterClient parse( AbstractAddress clientAddress, AbstractAddress serverAddress, 
      ExpressionList parsedContent, PolicyParser parser ) {
    UUID negotiationId = null;
    MessageSignature signature = null;
    
    if ( parsedContent == null || ! parsedContent.getHead().equals( new Symbol( MSG_ID ) ) ) {
      return null;
    }
    ExpressionList current = parsedContent.getTail();
    while ( current != null ) {
      try {
        String key = current.getHead().toString();
        Expression value = current.getTail().getHead();
        if ( key.equals( KEY_NEGOTIATION ) ) negotiationId = UUID.fromString( value.valueString() );
        if ( key.equals( KEY_SIGNATURE ) ) signature = MessageSignature.fromString( value.valueString() );
        current = current.getTail().getTail();
      } catch ( NullPointerException e ) {
        return null;
      } catch ( ClassCastException e ) {
        return null;
      }
    }
    
    return new UnregisterClient( negotiationId, clientAddress, serverAddress, signature );
    
  }
  
  
  /**
   * Normal constructor.
   * 
   * When creating a new message, the parameter signature should always be null!.
   * 
   * @param negotiationId {@link UUID} of this message
   * @param senderId AbstractAddress of sender of message
   * @param receiverId AbstractAddress of receiver of message
   * @param signature signature of message (can be null)
   */
  public UnregisterClient( UUID negotiationId, AbstractAddress senderId, AbstractAddress receiverId, MessageSignature signature ) {
    super( negotiationId, senderId, receiverId, null, signature );
  }
  

  @Override
  public String toFipaContent() {
    return MSG_ID + " " + 
      KEY_NEGOTIATION + " \"" + getNegotiationId() + "\" ";
  }

}
