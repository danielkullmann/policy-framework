/**
 * This package contains the message classes of the policy protocol,
 * with interfaces that describe the different concepts.
 */
package risoe.syslab.control.policy.message;
