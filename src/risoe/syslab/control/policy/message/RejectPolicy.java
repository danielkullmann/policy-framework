/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message;

import java.util.UUID;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.MessageSignature;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyParser;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.Symbol;

/**
 * Message that is sent from client to server to 
 * decline a policy that was sent from the server.
 * 
 */
public class RejectPolicy extends PolicyMessage {

  public static final String MSG_ID = "decline-policy";

  /**
   * "Constructor" for creating a message of this class from parsed
   * FIPA message content. 
   * 
   * @param clientAddress sender of message
   * @param serverAddress receiver of message
   * @param parsedContent parsed content of message
   * @return The parsed {@link RejectPolicy} instance, or null if parsing failed
   */
  public static RejectPolicy parse( AbstractAddress clientAddress, AbstractAddress serverAddress, 
      ExpressionList parsedContent, PolicyParser parser ) {

    UUID negotiationId = null;
    Policy policy = null;
    MessageSignature signature = null;
    
    if ( parsedContent == null || ! parsedContent.getHead().equals( new Symbol( MSG_ID ) ) ) {
      return null;
    }
    ExpressionList current = parsedContent.getTail();
    while ( current != null ) {
      try {
        String key = current.getHead().toString();
        Expression value = current.getTail().getHead();
        if ( key.equals( KEY_NEGOTIATION ) ) negotiationId = UUID.fromString( value.valueString() );
        if ( key.equals( KEY_POLICY ) ) policy = parser.parsePolicy( value );
        if ( key.equals( KEY_SIGNATURE ) ) signature = MessageSignature.fromString( value.valueString() );
        current = current.getTail().getTail();
      } catch ( NullPointerException e ) {
        return null;
      } catch ( ClassCastException e ) {
        return null;
      }
    }
    
    return new RejectPolicy( negotiationId, clientAddress, serverAddress, policy, signature );
  }


  /**
   * Constructor
   * 
   * @param negotiationId UUID used in the policy communication
   * @param clientAddress address of client in policy protocol
   * @param serverAddress address of server in policy protocol
   * @param policy policy that is rejected
   * @param signature signature, may be null
   */
  public RejectPolicy( UUID negotiationId, AbstractAddress clientAddress, AbstractAddress serverAddress, Policy policy, MessageSignature signature ) {
    super(negotiationId, clientAddress, serverAddress, policy, signature);
  }


  @Override
  public String toFipaContent() {
    return  MSG_ID + " " + 
    KEY_NEGOTIATION + " \"" + getNegotiationId() + "\" " +
    KEY_POLICY + " " + getPolicy().toFipaContent();
  }
  

}
