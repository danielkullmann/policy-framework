/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message;

import java.util.UUID;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.MessageSignature;
import risoe.syslab.control.policy.core.PolicyParser;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.Symbol;

/**
 * Message that initiates a policy protocol interaction
 * from the server side.
 */
public class ServerRequestNegotiation extends PolicyMessage {

  /** message id of this message. */
  private static final String MSG_ID = "server-request-negotiation";

  
  /**
   * "Constructor" for creating a message of this class from parsed
   * FIPA message content. 
   * 
   * @param clientAddress sender of message
   * @param serverAddress receiver of message
   * @param parsedContent parsed content of message
   * @return The parsed {@link ServerRequestNegotiation} instance, or null if parsing failed
   */
  public static ServerRequestNegotiation parse( AbstractAddress clientAddress, AbstractAddress serverAddress, 
      ExpressionList parsedContent, PolicyParser parser ) {

    UUID negotiationId = null;
    MessageSignature signature = null;
    
    if ( parsedContent == null || ! parsedContent.getHead().equals( new Symbol( MSG_ID ) ) ) {
      return null;
    }
    ExpressionList current = parsedContent.getTail();
    while ( current != null ) {
      try {
        String key = current.getHead().toString();
        Expression value = current.getTail().getHead();
        if ( key.equals( KEY_NEGOTIATION ) ) negotiationId = UUID.fromString( value.valueString() );
        if ( key.equals( KEY_SIGNATURE ) ) signature = MessageSignature.fromString( value.valueString() );
        current = current.getTail().getTail();
      } catch ( NullPointerException e ) {
        return null;
      } catch ( ClassCastException e ) {
        return null;
      }
    }
    
    return new ServerRequestNegotiation( negotiationId, clientAddress, serverAddress, signature );
  }


  /**
   * Constructor that sets all fields of this class.
   * 
   * @param negotiationId
   * @param clientAddress
   * @param serverAddress
   * @param signature
   */
  public ServerRequestNegotiation( UUID negotiationId, AbstractAddress clientAddress, AbstractAddress serverAddress, MessageSignature signature ) {
    super(negotiationId, serverAddress, clientAddress, null, signature);
  }
  

  @Override
  public String toFipaContent() {
    return MSG_ID + " " + 
      KEY_NEGOTIATION + " \"" + getNegotiationId() + "\" ";
  }
  

}
