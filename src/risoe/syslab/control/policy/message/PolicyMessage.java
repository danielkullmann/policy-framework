/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message;

import java.util.UUID;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.MessageSignature;
import risoe.syslab.control.policy.core.MessageSigner;
import risoe.syslab.control.policy.core.Policy;

/**
 * This is the base class for all message contents that are sent 
 * for the policy protocol. 
 * 
 * This class is NOT a child of the ACLMessage class.
 */
public abstract class PolicyMessage {

  public static boolean USE_SIGNATURES = true;
  
  /** key that is used in the FIPA message content for the negotiation id value */
  static final String KEY_NEGOTIATION = ":negotiation-id";
  /** key that is used in the FIPA message content for the policy data */
  static final String KEY_POLICY = ":policy";
  /** key that is used in the FIPA message content for the signature value */
  static final String KEY_SIGNATURE = ":signature";

  
  /** negotiation id of message; this should be a unique conversation id. */
  private UUID negotiationId;
  /** id of sender of thiss message */
  private AbstractAddress senderAddress;
  /** id of receiver of this message */
  private AbstractAddress receiverAddress;
  /** the policy that this message contains; sometimes this can be empty */
  private Policy policy;
  /** the signature that signs this message */
  protected MessageSignature signature;

  
  /**
   * Constructor that sets all the fields of this class.
   * 
   * @param negotiationId UUID used in the policy communication
   * @param senderAddress address of message sender
   * @param receiverAddress address of message receiver
   * @param policy policy; may be null, depending on message
   * @param signature signature; may be null
   */
  protected PolicyMessage( UUID negotiationId, AbstractAddress senderAddress, AbstractAddress receiverAddress, Policy policy, MessageSignature signature ) {
    super();
    this.negotiationId = negotiationId;
    this.senderAddress = senderAddress;
    this.receiverAddress = receiverAddress;
    this.policy = policy;
    this.signature = signature;
    
    assert( this.negotiationId != null );
    assert( this.senderAddress != null );
    assert( this.receiverAddress != null );
  }


  /**
   * Creates a FIPA-compliant content string of this message,
   * to be put into the content field of an ACLMessage.
   * This uses {@link #toFipaContent()} to create the 
   * message-specific content, and adds the signature.
   * 
   * @return {@link ACLMessage}
   */
  public final String toSignedFipaContent() {
    StringBuilder sb = new StringBuilder();
    sb.append( "(" );
    sb.append( toFipaContent() );
    if ( USE_SIGNATURES ) {
	    sb.append( " :signature " );
	    createSignature();
	    sb.append( getSignature().toFipaContent() );
    }
    sb.append( ")" );
    return sb.toString();
  }
  

  /**
   * Creates a FIPA-compliant content string of this message.
   * This method is used by {@link #toSignedFipaContent()}.
   * 
   * @return {@link ACLMessage}
   */
  public abstract String toFipaContent();
  

  /** Creates a signature for this message type.
   * All fields must already be existing
   */
  public final void createSignature() {
    if ( signature != null ) throw new IllegalArgumentException( "Not allowed to overwrite an existing signature" );
    if ( USE_SIGNATURES ) {
      String message = ":" + getNegotiationId() + ":" + getSenderAddress() + ":" + getReceiverAddress() + ":" + toFipaContent(); 
      signature = new MessageSignature( new MessageSigner().sign( message, getSenderAddress().getName() ) );
    }
  }


  /**
   * This method checks whether the signature is valid
   * @return true, iff the signature is valid
   */
  public final boolean checkSignature() {
	if ( USE_SIGNATURES ) {
	  String message = ":" + getNegotiationId() + ":" + getSenderAddress() + ":" + getReceiverAddress() + ":" + toFipaContent(); 
	  return new MessageSigner().verify( message, signature.getSignature(), getSenderAddress().getName() );
	}
	// Not using signatures..
	return true;
  }

  
  /**
   * Simple getter for the negotiation ID of the message.
   * @return negotiation id of message
   */
  public final UUID getNegotiationId() {
    return negotiationId;
  }
  
  
  /**
   * Simple getter for the sender address of the message.
   * @return address of sender of message
   */
  public final AbstractAddress getSenderAddress() {
    return senderAddress;
  }
  
  
  /**
   * Simple getter for the receiver address of the message.
   * @return address of receiver of message 
   */
  public final AbstractAddress getReceiverAddress() {
    return receiverAddress;
  }
  
  
  /**
   * Simple getter for the policy of the message.
   * @return policy of message
   */
  public final Policy getPolicy() {
    return policy;
  }
  
  
  /**
   * Simple getter for the signature of the message.
   * @return signature of message
   */
  public final MessageSignature getSignature() {
    return signature;
  }


}
