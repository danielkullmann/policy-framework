/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message;

import java.util.UUID;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.MessageSignature;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyParser;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.Symbol;


/**
 * Message to accept a policy; it is sent from the client to the server
 */
public class AcceptPolicy extends PolicyMessage {

  public static final String MSG_ID = "accept-policy";

  /**
   * "Constructor" for creating a message of this class from parsed
   * FIPA message content. 
   * 
   * @param clientAddress sender of message
   * @param serverAddress receiver of message
   * @param parsedContent parsed content of message
   * @return The parsed {@link AcceptPolicy} instance, or null if parsing failed
   */
  public static AcceptPolicy parse( AbstractAddress clientAddress, AbstractAddress serverAddress, 
      ExpressionList parsedContent, PolicyParser parser ) {

    UUID negotiationId = null;
    Policy policy = null;
    MessageSignature signature = null;
    
    if ( parsedContent == null || ! parsedContent.getHead().equals( new Symbol( MSG_ID ) ) ) {
      return null;
    }
    ExpressionList current = parsedContent.getTail();
    while ( current != null ) {
      try {
        String key = current.getHead().toString();
        Expression value = current.getTail().getHead();
        if ( key.equals( KEY_NEGOTIATION ) ) negotiationId = UUID.fromString( value.valueString() );
        if ( key.equals( KEY_POLICY ) ) policy = parser.parsePolicy( value );
        if ( key.equals( KEY_SIGNATURE ) ) signature = MessageSignature.fromString( value.valueString() );
        current = current.getTail().getTail();
      } catch ( NullPointerException e ) {
        throw new AssertionError( e );
      } catch ( ClassCastException e ) {
        throw new AssertionError( e );
      }
    }
    
    return new AcceptPolicy( negotiationId, clientAddress, serverAddress, policy, signature );
  }


  /**
   * Constructor
   * 
   * @param negotiationId
   * @param clientAddress
   * @param serverAddress
   * @param policy
   * @param signature
   */
  public AcceptPolicy( UUID negotiationId, AbstractAddress clientAddress, AbstractAddress serverAddress, Policy policy, MessageSignature signature ) {
    super(negotiationId, clientAddress, serverAddress, policy, signature );
    assert( policy != null );
  }


  @Override
  public String toFipaContent() {
    return MSG_ID + " " + 
      KEY_NEGOTIATION + " \"" + getNegotiationId() + "\" " +
      KEY_POLICY + " " + getPolicy().toFipaContent();
  }


}
