/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

/**
 * Represents a symbol in a message, e.g.
 * (symbol-1 symbol-2) is a list of two symbols,
 * symbol-1 and symbol-2.
 */
public final class Symbol implements Expression {

  /** name of symbol; the name of the symbol is 
   * the printed name. For example, the name of
   * symbol-1 mentioned above is the string "symbol-1"
   */
  String name;
  
  
  /**
   * Default constructor; should never be called.
   */
  @SuppressWarnings( "unused" )
  private Symbol() {
    throw new IllegalAccessError();
  }

  /**Normal constructor, giving the name of the symbol as string
   * @param name name of symbol
   */
  public Symbol( String name ) {
    this.name = name;
  }
  

  /**
   * Returns the name of the symbol
   * @return
   */
  public String getName() {
    return name;
  }


  @Override
  public String valueString() {
    return name;
  }


  @Override
  public String toString() {
    return name;
  }
  
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  
  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    Symbol other = (Symbol) obj;
    if ( name == null ) {
      if ( other.name != null ) {
        return false;
      }
    } else if ( !name.equals( other.name ) ) {
      return false;
    }
    return true;
  }

  
}
