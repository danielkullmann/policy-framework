/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

/**
 * Represents a keyword in a message. A keyword
 * is a symbol that startes with a ":" character. 
 * 
 * TODO Should this be a child of Symbol?
 */
public final class Keyword implements Expression {

  /** name of keyword, i.e. without the colon */
  private String name;

  
  /** Defualt constructor; should NEVER be called. */
  @SuppressWarnings( "unused" )
  private Keyword() {
    throw new IllegalAccessError();
  }
  
  
  /** Normal constructor giving the name of the keyword
   * @param name name of keyword, i.e. without the colon
   */
  public Keyword( String name ) {
    while ( name.startsWith( ":" ) ) name = name.substring( 1 );
    this.name = name;
  }

  /**
   * Returns the name of the keyword
   * @return name of keyword, i.e. without the colon
   */
  public String getName() {
    return name;
  }


  @Override
  public String valueString() {
    return name;
  }
  
  
  @Override
  public String toString() {
    return ":" + name;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    Keyword other = (Keyword) obj;
    if ( name == null ) {
      if ( other.name != null ) {
        return false;
      }
    } else if ( !name.equals( other.name ) ) {
      return false;
    }
    return true;
  }

}
