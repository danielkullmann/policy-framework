/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

/**
 * Represents a string in a message, i.e.
 * the two " characters and the text they 
 * enclose.
 */
public final class StringConstant implements Expression {

  /** The content of the string, i.e. without the quote characters (") */
  private String value;

  
  /** Default constructor, should NEVER be used */
  @SuppressWarnings( "unused" )
  private StringConstant() {
    throw new IllegalAccessError();
  }
  
  
  /** Normal constructor, giving the string content
   * @param value content of string, without the quotes
   */
  public StringConstant( String value ) {
    this.value = value;
  }

  
  /** Returns the content of this string,
   * without the quotes.
   */
  public String getValue() {
    return value;
  }

  
  @Override
  public String valueString() {
    return value;
  }

  
  @Override
  public String toString() {
    String escaped = value;
    escaped = escaped.replace( "\\", "\\\\" );
    escaped = escaped.replace( "\"", "\\\"" );
    escaped = escaped.replace( "\n", "\\n" );
    escaped = escaped.replace( "\r", "" );
    return "\"" + escaped + "\"";
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    StringConstant other = (StringConstant) obj;
    if ( value == null ) {
      if ( other.value != null ) {
        return false;
      }
    } else if ( !value.equals( other.value ) ) {
      return false;
    }
    return true;
  }

}
