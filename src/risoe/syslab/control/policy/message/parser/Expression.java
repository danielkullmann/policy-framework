/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

/**
 * Interface for all classes representing message content.
 * Messages contain of lists, symbols, keywords, placeholders,
 * numbers and strings. Lists can contain the same elements.
 */
public interface Expression {

  /**
   * This is similar to {@link #toString()}, but it
   * returns the actual string value of the expression.
   * This is basically only different for strings and keywords:
   * <li> toString()     <=> valueString()
   * <li> "string-value" <=> string-value
   * <li> :keyword       <=> keyword
   * <li> ("a" "b" :k)   <=> (a b k)
   * 
   * @return value string of that expression
   */
  String valueString();

}
