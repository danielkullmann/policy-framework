/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

import java.util.List;

/**
 *  Represents a list of expressions in parsed
 *  message content.
 *  
 *  This class is implemented in LISP style, as a 
 *  single-linked list.
 */
public final class ExpressionList implements Expression {

  /** First element of this list */
  private Expression head;
  
  /** Remaining elements of this list */
  private ExpressionList tail;

  
  /** Default constructor; should NEVER be called */
  @SuppressWarnings( "unused" )
  private ExpressionList() {
    throw new IllegalAccessError();
  }
  

  /** Normal constructor, giving first element of list, and rest of list.
   * 
   * @param head first element of list
   * @param tail rest of list, or NULL if head was the last (and only) element of this list
   */
  public ExpressionList( Expression head, ExpressionList tail ) {
    this.head = head;
    this.tail = tail;
  }

  
  /**
   * Static helper method that creates an {@link ExpressionList} from a {@link List}
   * 
   * @param list List we want as an {@link ExpressionList}
   * @return a newly created {@link ExpressionList}
   */
  public static ExpressionList makeList( List<Expression> list) {
    ExpressionList result = null;
    for( int index = list.size()-1; index >= 0; index-- ) {
      result = new ExpressionList( list.get(  index ), result );
    }
    return result;
  }
  
  /**
   * Static helper method that creates an {@link ExpressionList} from a {@link List} of {@link String} values
   * 
   * @param list List we want as an {@link ExpressionList}
   * @return a newly created {@link ExpressionList}
   */
  public static ExpressionList makeStringList(List<String> list) {
    ExpressionList result = null;
    for( int index = list.size()-1; index >= 0; index-- ) {
      result = new ExpressionList( new StringConstant( list.get(  index ) ), result );
    }
    return result;
  }

  
  /** 
   * Static helper method that creates an {@link ExpressionList} from a boolean array.
   * True and false are mapped to integers 1 and 0, respectively.
   * 
   * @param array array we want as expression list
   * @return a newly created {@link ExpressionList}
   */
  public static ExpressionList makeList( boolean[] array) {
    ExpressionList result = null;
    for( int index = array.length-1; index >= 0; index-- ) {
      result = new ExpressionList( new IntegerConstant( array[index] ? 1 : 0 ), result );
    }
    return result;
  }

  /** Simple getter for first element of list
   * 
   * @return First element of list
   */
  public Expression getHead() {
    return head;
  }

  
  /** Simple getter for rest of list
   * 
   * @return Rest of list, or null
   */
  public ExpressionList getTail() {
    return tail;
  }

  
  @Override
  public String valueString() {
    StringBuilder sb = new StringBuilder();
    sb.append( "(" );
    sb.append( head.valueString() );
    ExpressionList c = tail;
    while ( c != null ) {
      sb.append( " " );
      sb.append( c.head.valueString() );
      c = c.tail;
    }
    sb.append( ")" );
    return sb.toString();
  }

  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append( "(" );
    sb.append( head );
    ExpressionList c = tail;
    while ( c != null ) {
      sb.append( " " );
      sb.append( c.head );
      c = c.tail;
    }
    sb.append( ")" );
    return sb.toString();
  }
  

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((head == null) ? 0 : head.hashCode());
    result = prime * result + ((tail == null) ? 0 : tail.hashCode());
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    ExpressionList other = (ExpressionList) obj;
    if ( head == null ) {
      if ( other.head != null ) {
        return false;
      }
    } else if ( !head.equals( other.head ) ) {
      return false;
    }
    if ( tail == null ) {
      if ( other.tail != null ) {
        return false;
      }
    } else if ( !tail.equals( other.tail ) ) {
      return false;
    }
    return true;
  }


  /** Calculates the length of the list, and returns
   * it. NOTE: This is not an O(1) operation!
   * 
   * @return the calculated length of the list
   */
  public int getLength() {
    int length = 1;
    ExpressionList tail = getTail();
    while ( tail != null ) {
      length++;
      tail = tail.getTail();
    }
    return length;
  }


}
