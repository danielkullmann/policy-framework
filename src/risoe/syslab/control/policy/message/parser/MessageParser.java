/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Parses the message content of ACLMessages, and returns
 * an {@link Expression}. 
 * 
 */
public final class MessageParser {

  /** Logger used by this class */
  private static final Logger logger = Logger.getLogger( MessageParser.class.getName() );
  
  /** String escapes supported by the parser */
  private static final HashMap<Character, Character> StringEscapes;
  
  /** setup the string escapes */
  static {
    StringEscapes = new HashMap<Character, Character>();
    // Java knows those escapes: 
    // \b \t \n \f \r \" \' \\
    StringEscapes.put( 'n', '\n' );
    StringEscapes.put( 'r', '\r' );
    StringEscapes.put( 'b', '\b' );
    StringEscapes.put( 't', '\t' );
    StringEscapes.put( 'f', '\f' );
    // \" \' \\ are handled in parseString() directly
  }
  
  /**
   * Parses a string into a {@link Expression}.
   * 
   * One of the two main methods of this class.
   * 
   * @param content message content, like (policy :start-charge 1230038233)
   * @return an expression that represents the parsed string, or null if parsing failed.
   */
  public Expression parse( String content ) {
    Pair<Expression, Integer> result = parseExpression( content, 0 );
    if ( result != null )
      return result.getFirst();
    return null;
  }

  
  /** 
   * Matches a parsed expression against a pattern string.
   * 
   * As an example, take the expression (add 2 5). We now could
   * match this expression against the pattern string "(add ?a ?b)".
   * As a result, we would get a HashMap with two entries: "a"
   * has the value 2 as parsed message, and "b" has the value 5
   * as parsed message.
   * 
   * @param pattern like normal message content, but can contain {@link PlaceHolder}s
   * @param expression the expression to match against
   * @return A HashMap containing the parts of the expression that were matched by placeholders
   */
  public HashMap<String, Expression> match( String pattern, Expression expression ) {
    
    Expression pExpression = parse( pattern );
    if ( pExpression == null )
      throw new IllegalArgumentException( "Could not parse pattern" );
    
    return match( pExpression, expression, new HashMap<String, Expression>() );
  }
  
    
  /**
   * Internal parsing method; this method is called recursively.
   * 
   * @param pattern the parsed pattern to match
   * @param expression the expression to match against
   * @param result the result HashMap; it is given as parameter also so that it is easy to call this method recursivley
   * @return The third parameter, with any found matches added, or null if matching failed.
   */
  private HashMap<String, Expression> match( Expression pattern, Expression expression, HashMap<String, Expression> result ) {
    
    if ( pattern instanceof ExpressionList ) {
      if ( expression instanceof ExpressionList ) {
        ExpressionList pList = (ExpressionList) pattern;
        ExpressionList eList = (ExpressionList) expression;
        while ( pList != null ) {
          result = match( pList.getHead(), eList.getHead(), result );
          if ( result == null ) 
            return result;
          pList = pList.getTail();
          eList = eList.getTail();
        }
        if ( eList != null ) 
          return result;
        
      } else {
        return null;
      }
    } else if ( pattern instanceof PlaceHolder ){
      result.put( ((PlaceHolder) pattern).getName(), expression );
    } else {
      // match constants (symbols, keywords, numbers, strings, etc...
      if ( ! pattern.equals( expression ) ) 
        return null;
    }
    
    return result;
  }
  
  
  /** 
   * Main worker method to parse an expression; can be called recursively.
   * 
   * @param content string content that has to be parsed
   * @param index index into content string, where parsing should start.
   * @return a pair of (parsed expression, index into content where parsing ended), or null if parsing failed
   */
  public Pair<Expression, Integer> parseExpression( String content, int index ) {

    index = skipWhitespace( content, index );

    Pair<Expression, Integer>  result;
    
    result = parseSymbol( content, index );
    if ( result != null ) return result;

    result = parseKeyword( content, index );
    if ( result != null ) return result;

    result = parsePlaceHolder( content, index );
    if ( result != null ) return result;

    result = parseString( content, index );
    if ( result != null ) return result;

    result = parseNumber( content, index );
    if ( result != null ) return result;

    result = parseList( content, index );
    if ( result != null ) return result;

    return null;
  }

  
  /**
   * CLass needed to return two values from a method call
   * 
   * @param <T1> type of first part of pair 
   * @param <T2> type of second part of pair
   */
  public class Pair<T1, T2> {
    private T1 first;
    private T2 second;

    public Pair( T1 o1, T2 o2 ) {
      first = o1;
      second = o2;
    }

    public T1 getFirst() {
      return first;
    }

    public T2 getSecond() {
      return second;
    }

  }

  
  /**
   * Method that parses a symbol.
   * 
   * @param content see {@link MessageParser#parseExpression(String, int)}
   * @param index see {@link MessageParser#parseExpression(String, int)}
   * @return see {@link MessageParser#parseExpression(String, int)}
   */
  private Pair<Expression, Integer> parseSymbol( String content, int index ) {
    if ( Character.isLetter( content.charAt( index ) ) ) {
      int start = index;
      char ch = content.charAt( index );
      while ( isSymbolLetter( ch ) ) {
        index++;
        if ( index == content.length() ) {
          break;
        }
        ch = content.charAt( index );
      }
      return new Pair<Expression, Integer>( new Symbol( content.substring( start, index ) ), index );
    } 

    return null;
  }

  
  /**
   * @param ch
   * @return
   */
  private boolean isSymbolLetter( char ch ) {
    if ( Character.isLetter( ch ) || Character.isDigit( ch ) ) return true;
    String allowedExtra = "+-=*!#&/";
    if ( allowedExtra.indexOf( ch ) >= 0 ) return true;
    return false;
  }


  /**
   * Method that parses a keyword.
   * 
   * @param content see {@link MessageParser#parseExpression(String, int)}
   * @param index see {@link MessageParser#parseExpression(String, int)}
   * @return see {@link MessageParser#parseExpression(String, int)}
   */
  private Pair<Expression, Integer> parseKeyword( String content, int index ) {
    if ( ':' == content.charAt( index ) ) {
      index++;
      if ( index == content.length() ) {
        throw new IllegalArgumentException( "Empty Keyword constant" );
      }
      int start = index;
      char ch = content.charAt( index );
      while ( Character.isLetter( ch ) || Character.isDigit( ch ) || ch == '-' ) {
        index++;
        if ( index == content.length() ) {
          break;
        }
        ch = content.charAt( index );
      }
      return new Pair<Expression, Integer>( new Keyword( content.substring( start, index ) ), index );
    } 

    return null;
  }

  
  /**
   * Method that parses a placeholder.
   * 
   * @param content see {@link MessageParser#parseExpression(String, int)}
   * @param index see {@link MessageParser#parseExpression(String, int)}
   * @return see {@link MessageParser#parseExpression(String, int)}
   */
  private Pair<Expression, Integer> parsePlaceHolder( String content, int index ) {
    if ( '?' == content.charAt( index ) ) {
      index++;
      if ( index == content.length() ) {
        throw new IllegalArgumentException( "Empty Placeholder constant" );
      }
      int start = index;
      char ch = content.charAt( index );
      while ( Character.isLetter( ch ) || Character.isDigit( ch ) || ch == '-' ) {
        index++;
        if ( index == content.length() ) {
          break;
        }
        ch = content.charAt( index );
      }
      return new Pair<Expression, Integer>( new PlaceHolder( content.substring( start, index ) ), index );
    } 

    return null;
  }

  
  /**
   * Method that parses a number (integer or double).
   * 
   * @param content see {@link MessageParser#parseExpression(String, int)}
   * @param index see {@link MessageParser#parseExpression(String, int)}
   * @return see {@link MessageParser#parseExpression(String, int)}
   */
  private Pair<Expression, Integer> parseNumber( String content, int index ) {
    try  {
      int sign = 1;
      if ( '+' == content.charAt( index ) || '-' == content.charAt( index ) ) {
        if ('-' == content.charAt( index )) sign = -1 * sign;
        index++;
        if ( index == content.length() ) {
          throw new IllegalArgumentException( "Number ends at EOF after sign" );
        }
      }
      
      if ( Character.isDigit( content.charAt( index ) ) ) {
        String numStr = "";
        char ch = content.charAt( index );
        while ( Character.isDigit( ch ) ) {
          numStr = numStr + ch;
          index++;
          if ( index == content.length() ) {
            break;
          }
          ch = content.charAt( index );
        }
        if ( ch == '.' ) {
          // Now parse the part after the decimal point
          numStr = numStr + ch;
          index ++;
          if ( index == content.length() ) {
            return null;
          }
          ch = content.charAt( index );
          while ( Character.isDigit( ch ) ) {
            numStr = numStr + ch;
            index++;
            if ( index == content.length() ) {
              break;
            }
            ch = content.charAt( index );
          }
          if ( ch == 'e' || ch == 'E' ) {
            numStr = numStr + ch;
            index ++;
            if ( index == content.length() ) {
              return null;
            }
            ch = content.charAt( index );
            if ( ch == '-' ) {
              numStr = numStr + ch;
              index ++;
              if ( index == content.length() ) {
                return null;
              }
              ch = content.charAt( index );
            }
            while ( Character.isDigit( ch ) ) {
              numStr = numStr + ch;
              index++;
              if ( index == content.length() ) {
                break;
              }
              ch = content.charAt( index );
            }
          }
          return new Pair<Expression, Integer>( new DoubleConstant( sign*Double.parseDouble( numStr ) ), index );
        } 
        
        return new Pair<Expression, Integer>( new IntegerConstant( sign*Long.parseLong( numStr ) ), index );
      }
    } catch ( IndexOutOfBoundsException e ) {
      // That exception happens when the input has been fully consumed, and one of the checks is missing
      // The reaction to this is to return null;
      logger.log( Level.SEVERE, "This shoud never happen (MessageParser.parseNumber()" );
      e.printStackTrace();
    }
      
    return null;
  }

  
  /**
   * Method that parses a string.
   * 
   * @param content see {@link MessageParser#parseExpression(String, int)}
   * @param index see {@link MessageParser#parseExpression(String, int)}
   * @return see {@link MessageParser#parseExpression(String, int)}
   */
  private Pair<Expression, Integer> parseString( String content, int index ) {
    if ( '"' == content.charAt( index ) ) {
      index++;
      if ( index == content.length() ) {
        throw new IllegalArgumentException( "String constant ends at EOF" );
      }
      StringBuilder sb = new StringBuilder();
      char ch = content.charAt( index );
      while ( ch != '"' ) {
        if ( ch == '\\' ) {
          // Escaped character..
          index++;
          if ( index == content.length() ) {
            throw new IllegalArgumentException( "String constant ends at EOF" );
          }
          ch = content.charAt( index );
          // String escapes like \0x1b, \033 etc. are \u0008 are not supported yet..
          if ( StringEscapes.containsKey( ch ) )
            sb.append( StringEscapes.get( ch ) );
          else
            sb.append( ch );
        } else {
          sb.append( ch );
        }
        index++;
        if ( index == content.length() ) {
          throw new IllegalArgumentException( "String constant ends at EOF" );
        }
        ch = content.charAt( index );
      }
      return new Pair<Expression, Integer>( new StringConstant( sb.toString() ), index+1 );
    } 

    return null;
  }


  /**
   * Method that parses a list.
   * 
   * @param content see {@link MessageParser#parseExpression(String, int)}
   * @param index see {@link MessageParser#parseExpression(String, int)}
   * @return see {@link MessageParser#parseExpression(String, int)}
   */
  private Pair<Expression, Integer> parseList( String content, int index ) {
    if ( content.charAt( index ) == '(' ) {
      index ++;
      ExpressionList list = null;
      while ( true ) {
        if ( index == content.length() ) {
          throw new IllegalArgumentException( "List finished by end of string" );
        }
        index = skipWhitespace( content, index );
        char ch = content.charAt( index );
        if ( ch == ')' ) {
          index += 1;
          break;
        }
        Pair<Expression,Integer> expr = parseExpression( content, index );
        if ( expr == null ) {
          throw new IllegalArgumentException( "Can't parse list element: " + content + ":" + index );
        }
        list = new ExpressionList( expr.first, list );
        index = expr.second.intValue();
      }
      return new Pair<Expression, Integer>( reverseList(list), index );
    } 

    return null;
  }

  
  /** Helper mehtod that reverses the given list, needed by {@link MessageParser#parseList(String, int)}
   * @param list list to be reversed
   * @return reversed list; a copy is created
   */
  private Expression reverseList( ExpressionList list ) {
    ExpressionList result = null;
    while ( list != null ) {
      result = new ExpressionList( list.getHead(), result );
      list = list.getTail();
    }
    
    return result;
  }


  /** Helper method that skips over whitespace in message content
   * 
   * @param content message content
   * @param index index into content string where to start skipping whitespace
   * @return index of the next character in content that is no whitespace
   */
  private int skipWhitespace( String content, int index ) {
    while ( index < content.length() && Character.isWhitespace( content.charAt( index ) ) ) {
      index++;
    }
    return index;
  }

}
