/**
 * This package contains the class that can parse the FIPA-like
 * content of message (MessageParser) and the supporting classes.
 * 
 * The main entry point in this package is the {@link risoe.syslab.control.policy.message.parser.MessageParser} class,
 * namely the {@link risoe.syslab.control.policy.message.parser.MessageParser#parse(String)} method. 
 * It returns an {@link risoe.syslab.control.policy.message.parser.Expression} object, which can 
 * be a tree of other Expression objects, such as a list of expressions, 
 * {@link risoe.syslab.control.policy.message.parser.ExpressionList}.
 * 
 */
package risoe.syslab.control.policy.message.parser;

