/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

/**
 * Represents a floating point value in message content.
 * 
 * I do not distinguish between floats and doubles.
 */
public final class DoubleConstant implements Expression {
  
  /** Value of double */
  private double value;

  
  /** Default constructor; should NEVER be called */
  @SuppressWarnings( "unused" )
  private DoubleConstant() {
    throw new IllegalAccessError();
  }
  
  /** Normal constructor, giving the value of the float
   * @param value value of float
   */
  public DoubleConstant( double value ) {
    this.value = value;
  }

  
  /** Gets the value of the double */
  public double getValue() {
    return value;
  }

  
  @Override
  public String valueString() {
    return ""+value;
  }

  
  @Override
  public String toString() {
    return ""+value;
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits( value );
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    DoubleConstant other = (DoubleConstant) obj;
    if ( Double.doubleToLongBits( value ) != Double.doubleToLongBits( other.value ) ) {
      return false;
    }
    return true;
  }

  
}
