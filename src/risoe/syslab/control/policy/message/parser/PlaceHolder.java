/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

/**
 * Represents a Prolog-like placeholder in a message
 * pattern. A placeholder is a symbol that starts with a
 * question mark. 
 * 
 * Used by {@link MessageParser#match(String, Expression)}
 * 
 * TODO Should this be a child of Symbol?
 */
public final class PlaceHolder implements Expression {

  /** Name of placeholder, without the question mark. */
  private String name;

  
  /** Default constructor; should NEVER be called */
  @SuppressWarnings( "unused" )
  private PlaceHolder() {
    throw new IllegalAccessError();
  }
  
  
  /**
   * Normal constructor
   * @param name name of place holder, i.e. without the question mark
   */
  public PlaceHolder( String name ) {
    super();
    this.name = name;
  }

  
  /** Returns the name of the placeholder */
  public String getName() {
    return name;
  }
  

  @Override
  public String valueString() {
    return name;
  }


  @Override
  public String toString() {
    return "?" + name;
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    PlaceHolder other = (PlaceHolder) obj;
    if ( name == null ) {
      if ( other.name != null ) {
        return false;
      }
    } else if ( !name.equals( other.name ) ) {
      return false;
    }
    return true;
  }

}
