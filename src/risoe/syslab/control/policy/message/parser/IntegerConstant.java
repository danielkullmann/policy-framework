/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.control.policy.message.parser;

/**
 * Represents an integer in message content.
 * 
 * I do not distinguish between int and long.
 */
public final class IntegerConstant implements Expression {

  /** value of the integer */
  private long value;

  
  /** Default constructor; should NEVER be called. */
  @SuppressWarnings( "unused" )
  private IntegerConstant() {
    throw new IllegalAccessError();
  }
  
  
  /** Normal constructor, giving the integer value
   * @param value value of integer
   */
  public IntegerConstant( long value ) {
    this.value = value;
  }


  /**
   * Returns value of the integer
   * @return value of integer
   */
  public long getValue() {
    return value;
  }


  @Override
  public String valueString() {
    return ""+value;
  }
  
  
  @Override
  public String toString() {
    return ""+value;
  }

  
@Override
  public int hashCode() {
    final int prime = 31;
    long result = 1;
    result = prime * result + value;
    return (int) result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    IntegerConstant other = (IntegerConstant) obj;
    if ( value != other.value ) {
      return false;
    }
    return true;
  }

}
