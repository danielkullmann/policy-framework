package risoe.syslab.control.policy.rules;

/** Interface abstracting an event source. 
 * An event source is something that provides a value,
 * in our case a double value.
 * 
 * Examples for this are the system frequenecy, a power price,
 * or also a schedule activation signal.
 * 
 * @author daku
 */
public interface EventSource {

  /** Get the current value of the event source */
  public abstract double getValue();
  
}
