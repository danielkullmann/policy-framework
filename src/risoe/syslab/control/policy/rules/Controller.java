package risoe.syslab.control.policy.rules;

import java.util.Properties;

import risoe.syslab.control.policy.service.Service;

/** Interface definition for service controllers.
 * 
 * @author daku
 *
 */
public interface Controller {

  /** Returns the service that is provided by this controller
   * @return
   */
  Service getProvidedService();
  
  /** Whether this controller would like to be activated. E.g. for a droop curve-based controller, this would happen to be the case when the 
   * observed value is outside the deadband of the droop curve.
   * 
   * @return
   */
  boolean wantsToBeActivated();
  
  /** Start this controller, i.e. it will react actively to {@link #provideCurrentData(Properties)}. */
  void start();
  
  /** Stop this controller, i.e. it will cease to react actively to {@link #provideCurrentData(Properties)}.
   * But it will still be possible to call {@link #wantsToBeActivated()}. */
  void stop();
  
}
