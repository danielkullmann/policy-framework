package risoe.syslab.control.policy.rules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;


/** Class for creating Drools rule bases for clients 
 * TODO Remove need for activeTest in rule conditions {@link #addRule(String, String, String)}
 * 
 * @author daku
 */
public class RuleBaseCreator {

  /** name for {@link Registrar} object in rule base */
  private String regName = "registrar";
  private String fileTemplate;
  /** name for activation group in rule base to make rules mutually exclusive */
  private String activationGroup = "activation-group";
  
  /** for numbering of rules in rule base */
  private int id = 1;
  
  /** list of rules */
  private ArrayList<String> rules   = new ArrayList<String>();
  /** list of when-parts of setup rule */
  private ArrayList<String> setupWhens  = new ArrayList<String>();
  /** list of then-parts of setup rule */
  private ArrayList<String> setupThens  = new ArrayList<String>();

  /** Default constructor 
   * @param fileTemplate basic template to use for the rule base
   */
  public RuleBaseCreator( String fileTemplate ) {
    super();
    this.fileTemplate = fileTemplate;
  }
  
  /** Set the name of the registrar variable name to use
   * 
   * @param regName
   */
  public void setRegVarName( String regName ) {
    this.regName = regName;
  }
  
  /** Add a new rule to the rulebase
   * @param ruleTemplate filename of rule template to be used
   * @param typeName class name of controller to be used in rule
   * @param activeTest test to be used for the controller; Drools needs this...
   */
  public void addRule( String ruleTemplate, String typeName, String activeTest ) {
    if ( activationGroup == null ) throw new AssertionError("activationGroup may not be null!");
    String content = readFile( ruleTemplate );
    content = content.replace( "$$id$$", ""+id );
    id++;
    content = content.replace( "$$activation-group$$", activationGroup );
    content = content.replace( "$$type-name$$", typeName );
    content = content.replace( "$$active-test$$", activeTest );
    rules.add( content );
  }
  

  /** Add a new part to the setup rule
   * 
   * @param setupWhen "when" part for this setup part
   * @param setupThen "then" part for this setup part
   */
  public void addSetup( String setupWhen, String setupThen) {
    setupWhens.add( setupWhen );
    setupThens.add( regName + ".register( " + setupThen + " );" );
  }

  /** Create the rules base from the add rules and setup parts */
  public String createRuleBase() {
    String content = readFile( fileTemplate );
    content = content.replace( "$$regname$$" , regName );
    
    content = content.replace( "$$rules$$" , StringUtils.join( rules, "" ) );
    content = content.replace( "$$setup-when$$" , StringUtils.join( setupWhens, " and\n    " ) );
    content = content.replace( "$$setup-then$$" , StringUtils.join( setupThens, "\n    " ) );

    return content;
  }

  /** Helper method that reads in a file completely into a String
   * 
   * @param template name of file whose contents should be read 
   * @return contents of file
   * @throws IllegalArgumentException whne file could not be read
   */
  private String readFile( String template ) {
    StringBuilder sb = new StringBuilder();
    File tFile = new File( template );
    BufferedReader br = null;
    if ( tFile.exists() ) {
      try {
        br = new BufferedReader( new FileReader( tFile ) );
      } catch (FileNotFoundException e) {
        throw new IllegalArgumentException( "template file not found: " + template );
      }
    } else {
      InputStream is = getClass().getClassLoader().getResourceAsStream( template );
      if ( is == null ) {
        throw new IllegalArgumentException( "template resource not found: " + template );
      }
      br = new BufferedReader( new InputStreamReader( is ) ); 
    }
    try {
      for ( String line = br.readLine(); line != null; line = br.readLine() ) {
        sb.append( line );
        sb.append( System.getProperty( "line.separator" ) );
      }
    } catch (IOException e) {
      throw new IllegalArgumentException( "problem reading template file: " + template );
    }
    try {
      br.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return sb.toString();
  }

  /** Simple getter for activationGroup field */
  public String getActivationGroup() {
    return activationGroup;
  }

  /** Simple setter for activationGroup field */
  public void setActivationGroup(String activationGroup) {
    this.activationGroup = activationGroup;
  }

  /** Simple getter for regName field */
  public String getRegName() {
    return regName;
  }

  /** Simple setter for regName field */
  public void setRegName(String regName) {
    this.regName = regName;
  }

  
}
