package risoe.syslab.control.policy.rules;


public class IdGenerator {

  private static IdGenerator instance;
  private long id = 0;
  
  public synchronized final long next() {
    return ++id;
  }

  public synchronized static IdGenerator instance() {
    if ( instance == null ) {
      instance = new IdGenerator();
    }
    return instance;
  }

  private IdGenerator() {
    super();
  }
  
  
}
