package risoe.syslab.control.policy.rules;

import java.util.HashMap;

import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.rule.FactHandle;

/** Registrar for facts in a rule base. An object of this class
 * must be present in all policy rule bases. It is used in the setup
 * rule of a rule base to register new facts with the rule base.
 * 
 * @author daku
 *
 */
public class Registrar {

  /** session the Registrar belongs to */
  private StatefulKnowledgeSession ksession;
  /** Map of fact handles and the facts they refer to. Necessary for updating the rule base */
  private HashMap<FactHandle, Object> facts = new HashMap<FactHandle, Object>();
  /** Dummy value, needed by Drools. Drools does not fire on facts with no constraints set, so the use flag is used for such constraints */
  private boolean use = true;

  /** Standard constructor */
  public Registrar(StatefulKnowledgeSession ksession) {
    super();
    this.ksession = ksession;
  }

  /** Registers a new rule base fact. This is called from the stetup rule of a rule base.
   * @param fact fact object to be registered.
   */
  public FactHandle register( Object fact ) {
    FactHandle factHandle = ksession.insert( fact );
    facts.put( factHandle, fact );
    use = false;
    return factHandle;
  }

  /** Get all registered facts
   * @return the Map with facts
   */
  public HashMap<FactHandle, Object> getFacts() {
    return facts;
  }
  
  /** dummy method needed for Drools rule bases... */
  public boolean isUse() {
    return use ;
  }
}
