package risoe.syslab.control.policy.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.rule.FactHandle;


/** Executes a rule base that is contained in a string 
 * 
 * @author daku
 *
 */
public class RuleBaseExecutor implements Runnable {

  public static final Logger logger = Logger.getLogger( RuleBaseExecutor.class.getName() );
  
  public static final IdGenerator idGenerator = IdGenerator.instance();
  
  /** Rule base that is executed */
  private String ruleBase;
  /** Thread in which the rule base is executed */
  private Thread thread;
  /** List of event sources for this rule base */
  private ArrayList<EventSource> eventSources;
  /** Controller Scheduler that is used in the rule base */
  private Object controller;
  /** Registrar that is used in the rule base */
  private Registrar registrar;
  
  /** session containing the data for executing the rulebase */
  private StatefulKnowledgeSession ksession;
  
  /** Standard constructor
   * 
   * @param ruleBase rule base to be used
   * @param controller controller scheduler to be used
   * @param eventSources list of event sources for the rule base
   */
  public RuleBaseExecutor(String ruleBase, Object controller, ArrayList<EventSource> eventSources) {
    super();
    this.ruleBase = ruleBase;
    this.eventSources = eventSources;
    this.controller = controller;

    this.thread = new Thread( this );
  }

  /** Start executing the rule base */
  public void start() {
    this.thread.start();
  }

  /** Interrupt the execution of the rule base */
  public void interrupt() {
    if ( registrar != null ) {
      HashMap<FactHandle, Object> facts = registrar.getFacts();
      for (Object obj : facts.values() ) {
        if ( obj instanceof Controller ) {
          ((Controller) obj).stop();
        }
      }
    }
    if ( this.thread != null ) {
      this.thread.interrupt();
    }
  }

  /** Runs the rule base. Don't call this method yourself!!! */
  @Override
  public void run() {
    
    Thread.currentThread().setName( "RuleBaseExecutor " + ruleBase.hashCode() );

    setup();
    while ( ! Thread.interrupted() ) {
     runOnce();
      try {
        Thread.sleep( 50 );
      } catch (InterruptedException e) {
        break;
      }
    }
    
    ksession.dispose();
  }

  /** If the executor is not run in a thread, this method should be run
   * every time one wants a reaction of the rulebase to external events.
   */
	public void runOnce() {
		// update all facts (except registrar), so that all rules can fire
		HashMap<FactHandle, Object> facts = registrar.getFacts();
		for (FactHandle fact : facts.keySet()) {
		  logger.log( Level.FINEST, "" );
			ksession.update(fact, facts.get(fact));
		}

		// fire all rules...
		ksession.fireAllRules();
	}

  /** If the executor is not run in a thread, this method must be run
   * exactly once after the creation of the {@link RuleBaseExecutor}
   * instance.
   */
  public void setup() {
    KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
    kbuilder.add( ResourceFactory.newByteArrayResource( ruleBase.getBytes() ), ResourceType.DRL );
    KnowledgeBuilderErrors errors = kbuilder.getErrors();
    if ( errors.size() > 0 ) {
      for ( KnowledgeBuilderError error : errors ) {
        System.err.println( error );
      }
      throw new IllegalArgumentException( "Could not parse knowledge." );
    }
    KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
    kbase.addKnowledgePackages( kbuilder.getKnowledgePackages() );

    ksession = kbase.newStatefulKnowledgeSession();
    registrar = new Registrar( ksession );
    ksession.insert( registrar );
    registrar.register( controller );

    // setup all event sources
    for ( EventSource eventSource : eventSources ) {
      registrar.register( eventSource );
    }
  }

  public Registrar getRegistrar() {
    return registrar;
  }

  public StatefulKnowledgeSession getKsession() {
    return ksession;
  }

  public String getRuleBase() {
    return ruleBase;
  }

  public ArrayList<EventSource> getEventSources() {
    return eventSources;
  }

  public Object getController() {
    return controller;
  }

}
