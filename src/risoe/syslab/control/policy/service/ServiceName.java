package risoe.syslab.control.policy.service;

/**
 * A service name consists of three parts:
 * <ul>
 * <li> The type of service that is provided, like "frequency-control", "dynamic-power-price"
 * <li> The sub type of service, i.e. how the service is described, e.g. for frequency-control it could be "droop" or "thresholds+hysteresis"
 * <li> The version of that particular type/sub type combination that is used, like "1.0"
 * </ul>
 * 
 * The service name is combined into in single string by joining the two parts with colon (":") characters, e.g.
 * "frequency-control:thresholds-hysteresis:1.0" 
 * 
 * The names of the parts may contain any printable character, except colon (":") and semicolon (";"), those are reserved for other purposes.
 * Colon is used to concatenate the three tokens of a service name, and semicolon is to concatenate several service names together
 * 
 * @author daku
 *
 */
public interface ServiceName {

  static final String TOKEN_SEPARATOR = ":";
  static final String LIST_SEPARATOR  = ";";
  
  String getType();
  String getSubType();
  String getVersion();
  
}
