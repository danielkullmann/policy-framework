package risoe.syslab.control.policy.service;

import java.util.Properties;

import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.Symbol;

/** An activation of a {@link Service}. A service activation
 * consists of the name of the service to be activated, and a 
 * description of the parameters needed to activate that service.
 * 
 * @author daku
 */
public class ServiceActivation {

  /** Service to be activated
   * {@link Service#getActivationProperties()} must have the same keys as {@link #parameters} 
   */
  private Service service;
  /** Parameters of the service */
  private Properties parameters;
  
  /** Standard constructor
   * 
   * @param service
   * @param parameters
   */
  public ServiceActivation(Service service, Properties parameters) {
    super();
    this.service = service;
    this.parameters = parameters;
  }
  
  /** Simple getter for {@link #service} field.
   * @return the service field
   */
  public Service getService() {
    return service;
  }
  
  /** Simple getter for {@link #parameters} field.
   * @return the parameters field
   */
  public Properties getParameters() {
    return parameters;
  }

  /** Parses an expression representing a ServiceActivation.
   * 
   * @param expr {@link Expression}, as created by {@link MessageParser#parse(String)}
   * @return the parsed ServiceActivation instance, or null if that failed
   */
  public static ServiceActivation parse(Expression expr) {
    ExpressionList exprList = (ExpressionList) expr;
    if ( ! ( exprList.getHead() instanceof Symbol ) ) return null;
    if ( ! ((Symbol) exprList.getHead()).getName().equals("service-activation")  ) return null;
    exprList = exprList.getTail();
    Service service = Service.parse( exprList.getHead() );
    exprList = exprList.getTail();
    Properties parameters = Service.parseProperties( exprList.getHead() );
    return new ServiceActivation(service, parameters);
  }

  /** Creates a string representation of this object, for use in messages
   * @return String with a lisp-like syntax
   */
  public String toFipaContent() {
    StringBuilder sb = new StringBuilder();
    sb.append("(service-activation ");
    sb.append(service.toFipaContent() + " ");
    sb.append(Service.toFipaContent( parameters ));
    sb.append(")");
    
    return sb .toString();
  }
  
}
