package risoe.syslab.control.policy.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

/**
 * List of service names
 * 
 * @author daku
 */
public class ServiceNameList {

  /** The list of {@link ServiceNameImpl} instances */
  private List<ServiceName> list = new ArrayList<ServiceName>();

  /** Simple constructor */
  public ServiceNameList() {
    super();
  }
  
  /** Constructor that parses a {@link ServiceNameList} string
   *  Such a string looks like this: "a:b:c;d:e:f;g:h:i", which contains the three
   *  service names "a:b:c", "d:e:f", and "g:h:i"
   */
  public ServiceNameList( String str) {
    super();
    if ( str == null ) throw new NullPointerException();
    if ( ! str.equals("") ) {
      StringTokenizer tokenizer = new StringTokenizer(str, ServiceNameImpl.LIST_SEPARATOR );
      while (tokenizer.hasMoreTokens()) {
        String token = tokenizer.nextToken();
        if ( ! token.equals("") ) {
          list.add( new ServiceNameImpl( token ) );
        }
      }
    }
  }
  
  /** Add a service to the list */
  public void addService( ServiceNameImpl service ) {
    list.add( service );
  }
  
  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    for (Iterator<ServiceName> it = list.iterator(); it.hasNext();) {
      ServiceName service = it.next();
      sb.append( service.toString() );
      if ( it.hasNext() ) sb.append( ServiceNameImpl.LIST_SEPARATOR );
    }
    return sb.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((list == null) ? 0 : list.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ServiceNameList other = (ServiceNameList) obj;
    if (list == null) {
      if (other.list != null)
        return false;
    } else if (!list.equals(other.list))
      return false;
    return true;
  }
  
}
