package risoe.syslab.control.policy.service;

import java.util.Properties;

import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.StringConstant;
import risoe.syslab.control.policy.message.parser.Symbol;

/** A service consists of a name, the {@link ServiceNameImpl}, 
 * and a list of properties that {@link ServiceActivation}s need,
 * a {@link Properties} instance.
 * 
 * @author daku
 *
 */
public class Service {

  /** "name" of service */
  private final ServiceNameImpl serviceName;
  /** properties for service activations */
  private final Properties activationProperties;
  /** properties for service descriptions */
  private final Properties descriptionProperties;

  /** Standard constructor
   * @param serviceName
   * @param activationProperties
   */
  public Service( ServiceNameImpl serviceName, Properties activationProperties, Properties descriptionProperties) {
    super();
    this.serviceName = serviceName;
    this.activationProperties = activationProperties;
    this.descriptionProperties = descriptionProperties;
  }

  /** Simple getter for {@link #getServiceName()} */
  public ServiceNameImpl getServiceName() {
    return serviceName;
  }

  /** Simple getter for {@link #activationProperties} */
  public Properties getActivationProperties() {
    return activationProperties;
  }
  
  /** Simple getter for {@link #descriptionProperties} */
  public Properties getDescriptionProperties() {
    return descriptionProperties;
  }
  

  /** Creates a string representation of this object, for use in messages
   * @return String with a lisp-like syntax
   */
  public String toFipaContent() {
    
    StringBuilder sb = new StringBuilder();
    sb.append("(service ");
    sb.append("\"" + serviceName.toString() + "\"");
    sb.append( " " );
    sb.append( toFipaContent(getDescriptionProperties()) );
    sb.append( " " );
    sb.append( toFipaContent(getActivationProperties()) );
    sb.append(")");
    
    return sb.toString();
  }
  
  /** Helper method creating a string representation of a {@link Properties} object, for use in messages
   * @return String with a lisp-like syntax
   */
  public static Object toFipaContent(Properties parameters) {
    StringBuilder sb = new StringBuilder();
    sb.append("(");
    for (Object key : parameters.keySet()) {
      Object value = parameters.get(key);
      sb.append("\"" + key + "\" ");
      sb.append("\"" + value + "\" ");
    }
    sb.append(")");
    return sb.toString();
  }
  
  /** Parses an {@link Expression} into a {@link Service} instance
   * 
   * @param expr {@link Expression}, as created by {@link MessageParser#parse(String)}
   * @return the parsed Service instance, or null if that failed
   */
  public static Service parse( Expression expr ) {
    
    if ( ! ( expr instanceof ExpressionList ) ) return null;
    ExpressionList exprList = (ExpressionList) expr;
    if ( ! ( exprList.getHead() instanceof Symbol ) ) return null;
    if ( ! ((Symbol) exprList.getHead()).getName().equals("service")  ) return null;
    ExpressionList tail = exprList.getTail();
    if ( tail == null ) return null;
    String serviceNameStr = ((StringConstant) tail.getHead()).getValue();
    ServiceNameImpl serviceName = new ServiceNameImpl( serviceNameStr );
    tail = tail.getTail();
    Properties dProperties = parseProperties( tail.getHead());
    if ( dProperties == null ) return null;
    tail = tail.getTail();
    Properties aProperties = parseProperties( tail.getHead());
    if ( aProperties == null ) return null;
    if ( tail.getTail() != null ) return null;
    return new Service( serviceName, aProperties, dProperties );
  }

  /** Helper function for parsing an expression representing a {@link Properties} object
   *  
   * @param expr {@link Expression}, as created by {@link MessageParser#parse(String)}
   * @return the parsed {@link Properties} instance, or null if that failed
   */
  public static Properties parseProperties(Expression expr) {
    Properties properties = new Properties();
    ExpressionList tail = (ExpressionList) expr;
    while ( tail != null ) {
      String key = ((StringConstant) tail.getHead()).getValue();
      tail = tail.getTail();
      String value = ((StringConstant) tail.getHead()).getValue();
      tail = tail.getTail();
      properties.put(key, value);
    }
    return properties;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime
        * result
        + ((activationProperties == null) ? 0 : activationProperties
            .hashCode());
    result = prime * result
        + ((serviceName == null) ? 0 : serviceName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Service other = (Service) obj;
    if (activationProperties == null) {
      if (other.activationProperties != null)
        return false;
    } else if (!activationProperties.equals(other.activationProperties))
      return false;
    if (serviceName == null) {
      if (other.serviceName != null)
        return false;
    } else if (!serviceName.equals(other.serviceName))
      return false;
    return true;
  }

}
