package risoe.syslab.control.policy.service;

import java.util.StringTokenizer;

/** Class describing a service. A service has three parts:
 * <li> type, e.g. "frequency control"
 * <li> sub-type, e.g. "thresholds" or "droop" (how is the service implemented
 * <li> version, e.g "1.0"
 * 
 * @author daku
 */
public class ServiceNameImpl implements ServiceName {

  /** separator character between the three parts of a {@link ServiceNameImpl} */
  public static final String TOKEN_SEPARATOR = ":";
  
  /** separator between the different {@link ServiceNameImpl}s in a {@link ServiceNameList} */
  public static final String LIST_SEPARATOR  = ";";
  
  
  private String type;
  private String subType;
  private String version;

  
  /** Standard constructor */
  public ServiceNameImpl(String type, String subType, String version) {
    super();
    this.type = type;
    this.subType = subType;
    this.version = version;

    checkServiceName();
  }

  /** Constructor that parses a String.
   * @param name String representation of ServiceNameImpl, as created by the {@link #toString()} method
   */
  public ServiceNameImpl(String name) {
    if ( name == null ) throw new NullPointerException();
    
    StringTokenizer tokenizer = new StringTokenizer( name, TOKEN_SEPARATOR );
    this.type = tokenizer.nextToken();
    if ( tokenizer.hasMoreTokens() ) {
      this.subType = tokenizer.nextToken();
      if ( tokenizer.hasMoreTokens() ) {
        this.version = tokenizer.nextToken();
      } else {
        this.version = "";
      }
    } else {
      this.subType = "";
      this.version = "";
    }
    
    checkServiceName();
  }

  /** Simple getter for type (first part) of the service name.
   * @return a String
   */
  @Override
  public String getType() {
    return type;
  }

  /** Simple getter for sub-type (second part) of the service name.
  * @return a String
  */
  @Override
  public String getSubType() {
    return subType;
  }

  /** Simple getter for version (third part) of the service name.
   * @return a String
   */
  @Override
  public String getVersion() {
    return version;
  }
  
  /** Method that checks the validity of the {@link ServiceNameImpl}.
   * @throws NullPointerException when any of the fields are null
   * @throws IllegalArgumentException if any of the fields contain wrong values
   */
  private void checkServiceName() {
    if ( type == null ) throw new NullPointerException();
    if ( subType == null ) throw new NullPointerException();
    if ( version == null ) throw new NullPointerException();
    if ( type.equals("") ) throw new IllegalArgumentException( "ServiceNameImpl: type may not be empty" );
    if ( type.contains( TOKEN_SEPARATOR ) ) throw new IllegalArgumentException( "ServiceNameImpl: type may not contain '" + TOKEN_SEPARATOR + "'" );
    if ( subType.contains( TOKEN_SEPARATOR ) ) throw new IllegalArgumentException( "ServiceNameImpl: sub type may not contain '" + TOKEN_SEPARATOR + "'" );
    if ( version.contains( TOKEN_SEPARATOR ) ) throw new IllegalArgumentException( "ServiceNameImpl: version may not contain '" + TOKEN_SEPARATOR + "'" );

    if ( type.contains( LIST_SEPARATOR ) ) throw new IllegalArgumentException( "ServiceNameImpl: type may not contain '" + LIST_SEPARATOR + "'" );
    if ( subType.contains( LIST_SEPARATOR ) ) throw new IllegalArgumentException( "ServiceNameImpl: sub type may not contain '" + LIST_SEPARATOR + "'" );
    if ( version.contains( LIST_SEPARATOR ) ) throw new IllegalArgumentException( "ServiceNameImpl: version may not contain '" + LIST_SEPARATOR + "'" );
  }

  /**
   * This creates a string representation of the ServiceNameImpl,
   * which can be used to 
   * 
   */
  @Override
  public String toString() {
    return type + TOKEN_SEPARATOR + subType + TOKEN_SEPARATOR + version;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((subType == null) ? 0 : subType.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    result = prime * result + ((version == null) ? 0 : version.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ServiceNameImpl other = (ServiceNameImpl) obj;
    if (subType == null) {
      if (other.subType != null)
        return false;
    } else if (!subType.equals(other.subType))
      return false;
    if (type == null) {
      if (other.type != null)
        return false;
    } else if (!type.equals(other.type))
      return false;
    if (version == null) {
      if (other.version != null)
        return false;
    } else if (!version.equals(other.version))
      return false;
    return true;
  }

  
}
