package risoe.syslab.comm.lba;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import javax.net.ServerSocketFactory;

/**
 * This class responds to messages from {@link LbaCheckClient}.
 * 
 * It will wait for messages, create reply messages of requested size and will
 * send them back to the client.
 * 
 * @author daku
 * 
 */
public class LbaCheckServer implements LbaCheck {

  private TCP tcp;
  private Thread tcpThread;
  private ServerSocketFactory socketFactory;
  private PrintStream out = System.out;

  public LbaCheckServer() {
    super();
  }

  public ServerSocketFactory getSocketFactory() {
    return socketFactory;
  }

  public void setSocketFactory( ServerSocketFactory socketFactory ) {
    this.socketFactory = socketFactory;
  }

  public void start() {
    tcp = new TCP();

    tcpThread = new Thread( tcp );
    tcpThread.start();
  }

  public void stop() {
    tcpThread.interrupt();
    tcp.close();
    tcp = null;
  }

  public static void main( String[] args ) {
    LbaCheckServer server = new LbaCheckServer();
    if ( args.length > 2 ) {
      double latency = Double.parseDouble( args[0] );
      double upbw = Double.parseDouble( args[1] );
      double dnbw = Double.parseDouble( args[2] );
      server.setSocketFactory( new LbaServerSocketFactory( latency, upbw, dnbw ) );
    }
    server.start();
  }

  /**
   * Parses the first part of an incoming packet
   * 
   * @param data
   * @return
   */
  private byte[] parse( InputStream in ) {
    try {
      byte first = (byte) in.read();
      DataInputStream dis = new DataInputStream( in );
      
      // set other parameters?
      if ( first == MSG_MAGIC_PARAMS ) {
        if ( in.read() != MSG_MAGIC_PARAMS ) {
          throw new IllegalArgumentException( "not a legal data packet (x)" );
        }
        int latency = dis.readInt();
        double upBandwidth = dis.readFloat();
        double downBandwidth = dis.readFloat();
        if ( socketFactory == null || ! (socketFactory instanceof LbaServerSocketFactory) ) {
          throw new IllegalArgumentException( "no socket factory!" );
        }
        out.println( "params: " + latency + " : " + upBandwidth + " " + downBandwidth );
        ((LbaServerSocketFactory) socketFactory).setLatency( latency, 0 );
        ((LbaServerSocketFactory) socketFactory).setUpBandwidth( upBandwidth, 0 );
        ((LbaServerSocketFactory) socketFactory).setDownBandwidth( downBandwidth, 0 );
      }
      
      if ( first != MSG_MAGIC_REQUEST ) {
        throw new IllegalArgumentException( "not a legal data packet (0) " + first );
      }
      byte second = (byte) dis.read();
      if ( second != MSG_MAGIC_REQUEST ) {
        throw new IllegalArgumentException( "not a legal data packet (1) " + second );
      }
      int dataLength = BLOCK_SIZE * dis.readInt();
      if (dataLength == 0 ) dataLength = 10;

      int replyLength = BLOCK_SIZE * dis.readInt();
      if (replyLength==0) replyLength=10;

      // why 10? => number of bytes already read (2 magic bytes and two 4-byte integers)
      byte[] buf = new byte[ dataLength-10 ];
      dis.readFully( buf  );
      for ( int i = 0; i < buf.length; i++ ) {
        if ( buf[i] != MSG_MAGIC_REQUEST ) {
          throw new IllegalArgumentException( buf[i] + " != " + MSG_MAGIC_REQUEST );
        }
      }
      byte[] newData = new byte[replyLength];
      for ( int i = 0; i < newData.length; i++ ) {
        newData[i] = MSG_MAGIC_REPLY;
      }
      return newData;
    } catch (IOException e) {
      throw new IllegalStateException( e );
    }
  }
  
  private class TCP implements Runnable {

    private ServerSocket ss;

    public TCP() {
      super();
    }

    @Override
    public void run() {
      try {
        if ( socketFactory != null ) {
          ss = socketFactory.createServerSocket( TCP_PORT );
        } else {
          ss = new ServerSocket( TCP_PORT );
        }
      } catch (SocketException e) {
        throw new IllegalStateException( e );
      } catch (IOException e) {
        throw new IllegalStateException( e );
      }

      try {
        while( ! Thread.interrupted() ) {
          Socket cs = ss.accept();
          byte[] reply = parse( cs.getInputStream() );
          cs.getOutputStream().write( reply );
          cs.close();
        }
      } catch (SocketException e) {
        if ( ! ss.isClosed() ) 
          throw new IllegalStateException(e);
      } catch ( IOException e ) {
        throw new IllegalStateException(e);
      } finally {
        try {
          ss.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    public void close() {
      try {
        ss.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

}
