package risoe.syslab.comm.lba;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.core.PolicyUtils;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class LbaTestServer {

  private static final Logger logger = Logger.getLogger( LbaTestServer.class.getName() );

  private BindException lastException;

  public LbaTestServer( int port ) throws IOException {
    super();
    ArrayList<InetAddress> addresses = new ArrayList<InetAddress>();
    ArrayList<NetworkInterface> nics = Collections.list( NetworkInterface.getNetworkInterfaces() );
    for ( NetworkInterface nic : nics ) {
      List<InterfaceAddress> addrs = nic.getInterfaceAddresses();
      for ( InterfaceAddress addr : addrs ) {
        addresses.add( addr.getAddress() );
      }
    }
    HttpHandler handler = new Handler();
    System.out.println( "Listening on port: " + port );
    for ( InetAddress address : addresses ) {
      System.out.println( "Listening on address: " + address );
      setupServerAtAddress( port, handler, address );
    }

  }

  private void setupServerAtAddress( int port, HttpHandler handler, InetAddress address ) throws IOException {
    logger.log( Level.FINE, "bind to: " + address.getHostAddress() + ":" + port );
    InetSocketAddress inetaddress = new InetSocketAddress( address.getHostAddress(), port );
    try {
      HttpServer server = HttpServer.create( inetaddress, -1 );
      server.createContext( "/", handler );
      server.start();
    } catch ( BindException e ) {
      logger.log( Level.SEVERE, "Could not bind to " + address.getHostAddress() + ":" + port );
      e.printStackTrace();
      lastException = e;
    }
  }

  public BindException getLastException() {
    return lastException;
  }

  /**
   * Class handling all requests that can't be mapped to an agent URL
   */
  private class Handler implements HttpHandler {

    @Override
    public void handle( HttpExchange exchg ) throws IOException {

      if ( exchg.getRequestURI().getPath().equals( "/" ) ) {
        try {
          String query = exchg.getRequestURI().getRawQuery();
          Map<String, Object> params = parseQuery( query );
          int length = 100;
          if (params.containsKey("length")) {
            length = Integer.parseInt((String) params.get("length"));
          }
          exchg.getResponseHeaders().add( "Content-Type", "text/plain" );
          exchg.sendResponseHeaders( 200, 0 );
          OutputStream os = exchg.getResponseBody();
          BufferedWriter bw = new BufferedWriter( new OutputStreamWriter( os ) );
          for ( int i = 0; i < length-1; i++ ) {
            bw.write("a");
          }
          bw.write("\n");
          bw.flush();
          os.close();
        } catch ( Exception e ) {
          e.printStackTrace();
        }
      } else {
        logger.log( Level.WARNING, "server does not handle " + exchg.getRequestURI() );
        exchg.sendResponseHeaders( 404, -1 );
      }
      exchg.close();
    }

    private Map<String, Object> parseQuery( String query ) throws UnsupportedEncodingException {

      Map<String, Object> parameters = new HashMap<String, Object>();

      if ( query != null ) {
        String pairs[] = query.split( "[&]" );
        for ( String pair : pairs ) {
          String param[] = pair.split( "[=]" );

          String key = null;
          String value = null;
          if ( param.length > 0 ) {
            key = URLDecoder.decode( param[0], "UTF-8" );
          }

          if ( param.length > 1 ) {
            value = URLDecoder.decode( param[1], "UTF-8" );
          }

          if ( parameters.containsKey( key ) ) {
            Object obj = parameters.get( key );
            if ( obj instanceof List<?> ) {
              @SuppressWarnings( "unchecked" )
              List<String> values = (List<String>) obj;
              values.add( value );
            } else if ( obj instanceof String ) {
              List<String> values = new ArrayList<String>();
              values.add( (String) obj );
              values.add( value );
              parameters.put( key, values );
            }
          } else {
            parameters.put( key, value );
          }
        }
      }
      return parameters;
    }

  }

  /**
   * @param args
   */
  public static void main( String[] args ) {
    try {
      if (args.length != 1) {
        System.out.println("Usage: ./bin/runClass.sh " + LbaTestServer.class.getName() + " port");
        System.exit(1);
      }
      System.setProperty( "java.util.logging.config.file", "conf/logging-verbose.properties" );
      System.setProperty( "log4j.configuration", "conf/log4j.properties" );
      PolicyUtils.setup( "conf/policy-framework.properties" );

      new LbaTestServer( Integer.parseInt( args[0] ) );
      while ( true ) {
        Thread.sleep( Long.MAX_VALUE );
      }
    } catch ( NumberFormatException e ) {
      e.printStackTrace();
    } catch ( IOException e ) {
      e.printStackTrace();
    } catch ( InterruptedException e ) {
      // ignore, breaks out of loop
    }
  }

}
