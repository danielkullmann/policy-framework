package risoe.syslab.comm.lba;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.SchemeSocketFactory;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;


public class LbaSchemeSocketFactory implements SchemeSocketFactory {

  private LbaSocketFactory factory;

  public LbaSchemeSocketFactory() {
    super();
    factory = new LbaSocketFactory( 0, Double.NaN, Double.NaN );
  }

  /**
   * 
   * @param latency [ms]
   * @param upBandwidth [byte/ms]
   * @param downBandwidth [byte/ms]
   */
  public void setParams( double latency, double upBandwidth, double downBandwidth ) {
    if ( ! Double.isNaN( latency       ) ) factory.setLatency(       latency,       0 );
    if ( ! Double.isNaN( upBandwidth   ) ) factory.setUpBandwidth(   upBandwidth,   0 );
    if ( ! Double.isNaN( downBandwidth ) ) factory.setDownBandwidth( downBandwidth, 0 );
  }
  
  /**
   * 
   * @param latency
   * @param lr random part of latency
   * @param upBandwidth
   * @param ur random part of upBandwidth
   * @param downBandwidth
   * @param dr random part of downBandwidth
   */
  public void setParams( double latency, double lr, 
      double upBandwidth, double ur, 
      double downBandwidth, double dr ) {
    if ( ! Double.isNaN( latency       ) ) factory.setLatency(       latency,       lr );
    if ( ! Double.isNaN( upBandwidth   ) ) factory.setUpBandwidth(   upBandwidth,   ur );
    if ( ! Double.isNaN( downBandwidth ) ) factory.setDownBandwidth( downBandwidth, dr );
  }
  


  @Override
  public Socket connectSocket( Socket socket, InetSocketAddress remoteAddress, InetSocketAddress localAddress, HttpParams params ) throws IOException, UnknownHostException, ConnectTimeoutException {
    if (localAddress != null) {
      socket.setReuseAddress(HttpConnectionParams.getSoReuseaddr(params));
      socket.bind(localAddress);
    }
    int connTimeout = HttpConnectionParams.getConnectionTimeout(params);
    int soTimeout = HttpConnectionParams.getSoTimeout(params);

    try {
        socket.setSoTimeout(soTimeout);
        socket.connect(remoteAddress, connTimeout );
    } catch (SocketTimeoutException ex) {
        throw new ConnectTimeoutException("Connect to " + remoteAddress + " timed out");
    }

    return socket;
  }

  @Override
  public Socket createSocket( HttpParams params ) throws IOException {
    return factory.createSocket();
  }

  @Override
  public boolean isSecure( Socket socket ) throws IllegalArgumentException {
    return false;
  }

}
