package risoe.syslab.comm.lba;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

import javax.net.ServerSocketFactory;

/** Please note that the directions for up and down bandwidth are
 * as seen from the client, i.e.
 * <ul>
 * <li> down: server -> client
 * <li> up: client -> server
 * </ul>
 * @author daku
 *
 */
public class LbaServerSocketFactory extends ServerSocketFactory {

  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  private double latency;

  /** simulated up bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
   private double upBandwidth;
  
   /** simulated down bandwidth [ms/byte].
    * The unit of this value is inverted bandwidth, ms/byte.
    * This is additional to the inherent bandwidth delay!
    */
   private double downBandwidth;
  
   private double latencyRandom;
   private double upBandwidthRandom;
   private double downBandwidthRandom;

   public LbaServerSocketFactory( double latency, double upBandwidth, double downBandwidth ) {
    super();
    this.latency = latency;
    this.upBandwidth = upBandwidth;
    this.downBandwidth = downBandwidth;
  }

  @Override
  public ServerSocket createServerSocket( int port ) throws IOException {
    SimulatedServerSocket s = new SimulatedServerSocket( port );
    s.setLatency( latency, latencyRandom );
    // bandwidth directions are reversed, see class comment!
    s.setUpBandwidth( downBandwidth, downBandwidthRandom );
    s.setDownBandwidth( upBandwidth, upBandwidthRandom );
    return s;
  }

  @Override
  public ServerSocket createServerSocket( int port, int backlog ) throws IOException {
    SimulatedServerSocket s = new SimulatedServerSocket( port, backlog );
    s.setLatency( latency, latencyRandom );
    // bandwidth directions are reversed, see class comment!
    s.setUpBandwidth( downBandwidth, downBandwidthRandom );
    s.setDownBandwidth( upBandwidth, upBandwidthRandom );
    return s;
  }

  @Override
  public ServerSocket createServerSocket( int port, int backlog, InetAddress inetAddress ) throws IOException {
    
    SimulatedServerSocket s = new SimulatedServerSocket( port, backlog, inetAddress );;
    s.setLatency( latency, latencyRandom );
    // bandwidth directions are reversed, see class comment!
    s.setUpBandwidth( downBandwidth, downBandwidthRandom );
    s.setDownBandwidth( upBandwidth, upBandwidthRandom );
    return s;
  }

  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  public double getLatency() {
    return latency;
  }

  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  public void setLatency( double latency, double random ) {
    this.latency = latency;
    this.latencyRandom = random;
  }

  /** simulated up bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public double getUpBandwidth() {
    return upBandwidth;
  }

  /** simulated up bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public void setUpBandwidth( double upBandwidth, double random ) {
    this.upBandwidth = upBandwidth;
    this.upBandwidthRandom = random;
  }

  /** simulated down bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public double getDownBandwidth() {
    return downBandwidth;
  }

  /** simulated down bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public void setDownBandwidth( double downBandwidth, double random ) {
    this.downBandwidth = downBandwidth;
    this.downBandwidthRandom = random;
  }

}
