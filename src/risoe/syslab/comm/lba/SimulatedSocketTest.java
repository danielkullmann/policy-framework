package risoe.syslab.comm.lba;

import junit.framework.TestCase;

public class SimulatedSocketTest extends TestCase {

  public void testLatency() {
    LbaSocket s = new LbaSocket();
    s.setLatency( 235.0, 0 );
    assertEquals( 235.0, s.getLatency() );
  }
  
  public void testBandwidth() {
    LbaSocket s = new LbaSocket();
    s.setUpBandwidth( 1000*1000.0, 0 );
    assertEquals( 1000*1000.0, s.getUpBandwidth() );
    s.setDownBandwidth( 1100*1000.0, 0 );
    assertEquals( 1100*1000.0, s.getDownBandwidth() );
  }
  
  /**
   * @param args
   */
  public static void main( String[] args ) {
    try {
      SimulatedSocketTest t = new SimulatedSocketTest();
      t.testLatency();
      t.testBandwidth();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
