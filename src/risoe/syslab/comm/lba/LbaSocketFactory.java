package risoe.syslab.comm.lba;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.net.SocketFactory;

public class LbaSocketFactory extends SocketFactory {

  private double latency;
  private double upBandwidth;
  private double downBandwidth;
  private double latencyRandom;
  private double upBandwidthRandom;
  private double downBandwidthRandom;

  /** Constructor for the socket factory
   * 
   * @param latency latency in [ms]
   * @param bandwidth bandwidth in [byte/ms]
   */
  public LbaSocketFactory( double latency, double upBandwidth, double downBandwidth ) {
    this.latency = latency;
    this.upBandwidth = upBandwidth;
    this.downBandwidth = downBandwidth;
  }

  
  @Override
  public Socket createSocket() throws IOException {
    LbaSocket s = new LbaSocket();
    s.setLatency( latency, latencyRandom );
    s.setUpBandwidth( upBandwidth, upBandwidthRandom );
    s.setDownBandwidth( downBandwidth, downBandwidthRandom );
    return s;
  }
  
  
  @Override
  public Socket createSocket( String arg0, int arg1 ) throws IOException, UnknownHostException {
    LbaSocket s = new LbaSocket( arg0, arg1 );
    s.setLatency( latency, latencyRandom );
    s.setUpBandwidth( upBandwidth, upBandwidthRandom );
    s.setDownBandwidth( downBandwidth, downBandwidthRandom );
    return s;
  }

  @Override
  public Socket createSocket( InetAddress address, int port ) throws IOException {
    LbaSocket s = new LbaSocket( address, port );
    s.setLatency( latency, latencyRandom );
    s.setUpBandwidth( upBandwidth, upBandwidthRandom );
    s.setDownBandwidth( downBandwidth, downBandwidthRandom );
    return s;
  }

  @Override
  public Socket createSocket( String arg0, int arg1, InetAddress arg2, int arg3 ) throws IOException, UnknownHostException {
    LbaSocket s = new LbaSocket( arg0, arg1, arg2, arg3 );
    s.setLatency( latency, latencyRandom );
    s.setUpBandwidth( upBandwidth, upBandwidthRandom );
    s.setDownBandwidth( downBandwidth, downBandwidthRandom );
    return s;
  }

  @Override
  public Socket createSocket( InetAddress address, int port, InetAddress localAddress, int localPort ) throws IOException {
    LbaSocket s = new LbaSocket( address, port, localAddress, localPort );
    s.setLatency( latency, latencyRandom );
    s.setUpBandwidth( upBandwidth, upBandwidthRandom );
    s.setDownBandwidth( downBandwidth, downBandwidthRandom );
    return s;
  }


  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  public double getLatency() {
    return latency;
  }


  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  public void setLatency( double latency, double random ) {
    this.latency = latency;
    this.latencyRandom = random;
  }


  /** simulated up bandwidth [byte/ms]. 
   * This is additional to the inherent bandwidth delay!
   */
  public double getUpBandwidth() {
    return upBandwidth;
  }


  /** simulated up bandwidth [byte/ms]. 
   * This is additional to the inherent bandwidth delay!
   */
  public void setUpBandwidth( double upBandwidth, double random ) {
    this.upBandwidth = upBandwidth;
    this.upBandwidthRandom = random;
  }


  /** simulated down bandwidth [byte/ms]. 
   * This is additional to the inherent bandwidth delay!
   */
  public double getDownBandwidth() {
    return downBandwidth;
  }


  /** simulated down bandwidth [byte/ms]. 
   * This is additional to the inherent bandwidth delay!
   */
  public void setDownBandwidth( double downBandwidth, double random ) {
    this.downBandwidth = downBandwidth;
    this.downBandwidthRandom = random;
  }




}
