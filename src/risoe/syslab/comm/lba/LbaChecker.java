package risoe.syslab.comm.lba;

import java.io.PrintStream;

public class LbaChecker {

  public static void main( String[] args ) {
    LbaCheckServer server = new LbaCheckServer();
    PrintStream out = System.out;
    try {
      server.start();
      LbaCheckClient client = new LbaCheckClient( "localhost" );
      
      for ( int i = 1; i <= 16; i++ ) {
        for ( int j = 1; j <= 16; j++ ) {
          long t = client.message( 16*i, 16*j );
          out.println( "" + i + " : " + j + " : " + t );
        }
      }
    } finally {
      server.stop();
    }
  }
}
