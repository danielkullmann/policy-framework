package risoe.syslab.comm.lba;

public interface LbaCheck {

  static final int BLOCK_SIZE = 256;

  static final int TCP_PORT = 31975;

  static final byte MSG_MAGIC_REQUEST = 0x24;
  static final byte MSG_MAGIC_REPLY   = 0x42;
  static final byte MSG_MAGIC_PARAMS  = 0x7e;

}
