package risoe.syslab.comm.lba;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class LbaCheckClient implements LbaCheck {

  public InetAddress ipAddress;
  private LbaSocketFactory socketFactory;

  /** Constructor
   * @param ipAddress ipAddress of server; can be left null if localhost should be used.
   */
  public LbaCheckClient( String ipAddress ) {
    super();
    try {
      this.ipAddress = InetAddress.getByName( ipAddress );
      if ( ipAddress == null ) {
        this.ipAddress = InetAddress.getLocalHost();
      }
    } catch (UnknownHostException e) {
      throw new IllegalStateException( e );
    }
  }

  /** Measures how long it takes to send and receive repetitions packets of
   * sendSizeBlocks and recvSizeBlocks size, respectively
   * @param repetitions
   * @param sendSizeBlocks
   * @param recvSizeBlocks
   * @return time spent in milliseconds
   */
  public long message( int repetitions, int sendSizeBlocks, int recvSizeBlocks ) {
    long sum = 0;
    for ( int i = 0; i < repetitions; i++ ) {
      long time = message( sendSizeBlocks, recvSizeBlocks );
      sum += time;
    }
    return sum;

  }
  
  /** Measures how long it takes to send and receive a packet of
   * sendSizeBlocks and recvSizeBlocks size, respectively.
   * @param sendSizeBlocks
   * @param recvSizeBlocks
   * @return time spent in milliseconds
   */
  public long message( int sendSizeBlocks, int recvSizeBlocks ) {

    int sendSize = sendSizeBlocks * BLOCK_SIZE;
    int recvSize = recvSizeBlocks * BLOCK_SIZE;
    
    if ( sendSize == 0 ) {
      sendSize = 10;
    }

    if ( recvSize == 0 ) {
      recvSize = 10;
    }

    Socket s = null;
    try {
      ByteBuffer data = ByteBuffer.allocate( sendSize );
      data.put( MSG_MAGIC_REQUEST );
      data.put( MSG_MAGIC_REQUEST );
      data.putInt( sendSizeBlocks );
      data.putInt( recvSizeBlocks );
      for ( int i = data.position(); i < sendSize; i++ ) {
        data.put( MSG_MAGIC_REQUEST );
      }
      long startTime = System.currentTimeMillis();
      if ( socketFactory != null )
        s = socketFactory.createSocket();
      else
        s = new Socket();
      s.connect( new InetSocketAddress( ipAddress, TCP_PORT ) );
      s.getOutputStream().write( data.array() ); 
      byte[] reply = new byte[ recvSize ];
      int received = 0;
      while ( received < recvSize ) { 
        int n = s.getInputStream().read( reply, received, recvSize-received );
        if ( n < 0 ) {
          throw new IllegalStateException( "" + received );
        }
        received += n;
      }
      
      s.close();
      s = null;
      long endTime = System.currentTimeMillis();
      
      for ( int i = 0; i < reply.length; i++ ) {
        if ( reply[i] != MSG_MAGIC_REPLY ) {
          throw new IllegalArgumentException( reply[i] + " != " + MSG_MAGIC_REPLY );
        }
      }
      
      return endTime - startTime;

    } catch (IOException e) {
      throw new IllegalStateException( e );
    } finally {
      if ( s != null ) {
        try {
          s.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void useSocketFactory( LbaSocketFactory socketFactory ) {
    this.socketFactory = socketFactory;    
  }


}
