package risoe.syslab.comm.lba;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import risoe.syslab.control.policy.core.PolicyUtils;

public class LbaTestClient {

  private String hostName;
  private int port;
  private LbaSchemeSocketFactory sf;
  private Scheme scheme;

  public LbaTestClient( String hostName, int port ) {
    super();
    this.hostName = hostName;
    this.port = port;
    this.sf = new LbaSchemeSocketFactory();
    this.scheme = new Scheme( "http", port, sf );
  }

  /**
   * @param args
   */
  public static void main( String[] args ) {
    if ( args.length != 6 && args.length != 10 ) {
      System.err.println( "Usage: " );
      System.err.println( "./bin/runClass.sh " + LbaTestClient.class.getName() + " hostname port length latency up-bandwidth down-bandwidth" );
      System.err.println( "./bin/runClass.sh " + LbaTestClient.class.getName() + " hostname port length steps latency-from latency-to up-bw-from up-bw-to down-bw-from down-bw-to" );
      System.err.println();
      System.err.println( "Latency is in ms, bandwidths are in kByte/s" );
      System.exit( 1 );
    }

    System.setProperty( "java.util.logging.config.file", "conf/logging-verbose.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );

    try {
      String hostName = args[0];
      int port = Integer.parseInt( args[1] );
      int length = Integer.parseInt( args[2] );
      LbaTestClient c = new LbaTestClient( hostName, port );

      if ( args.length == 6 ) {
        double latency = Double.parseDouble( args[3] );
        double upBandwidth = convertBw( Double.parseDouble( args[4] ) );
        double downBandwidth = convertBw( Double.parseDouble( args[5] ) );
        long t = c.request( length, latency, upBandwidth, downBandwidth );
        System.err.println( t + " ms" );
      } else if ( args.length == 10 ) {
        double steps = Integer.parseInt( args[3] ) - 1;
        double latencyFrom = Double.parseDouble( args[4] );
        double latencyTo = Double.parseDouble( args[5] );
        double lDiff = (latencyTo - latencyFrom) / steps;
        double upBandwidthFrom = Double.parseDouble( args[6] );
        double upBandwidthTo = Double.parseDouble( args[7] );
        double ubDiff = (upBandwidthTo - upBandwidthFrom) / steps;
        double downBandwidthFrom = Double.parseDouble( args[8] );
        double downBandwidthTo = Double.parseDouble( args[9] );
        double dbDiff = (downBandwidthTo - downBandwidthFrom) / steps;
        System.out.println( "length;latency;up-bandwidth;down-bandwidth;time" );
        for ( double latency = latencyFrom; latency <= latencyTo; latency += lDiff ) {
          for ( double upBandwidth = upBandwidthFrom; upBandwidth <= upBandwidthTo; upBandwidth += ubDiff ) {
            for ( double downBandwidth = downBandwidthFrom; downBandwidth <= downBandwidthTo; downBandwidth += dbDiff ) {
              long t = c.request( length, latency, convertBw( upBandwidth ), convertBw( downBandwidth ) );
              System.out.println( length + ";" + f(latency) + ";" + f(convertBw( upBandwidth )) + ";" + f(convertBw( downBandwidth )) + ";" + t );
              if (downBandwidth >= downBandwidthTo) break;
            }
            if (upBandwidth >= upBandwidthTo) break;
          }
          if (latency >= latencyTo) break;
        }
      } else {
        System.err.println( "Wrong arguments" );
        System.exit( 1 );
      }
    } catch ( Exception e ) {
      e.printStackTrace();
    }
  }

  private static String f(double v) {
    return String.format(Locale.ROOT, "%.2f", v);
  }

  /** Converts between different ways to put bandwidth
   * 
   * @param bw bandwidth in kByte/s
   * @return bandwidth in byte/ms
   */
  private static double convertBw( double bw ) {
    return bw*1024/1024;
  }

  private long request( int length, double latency, double upBandwidth, double downBandwidth ) throws ClientProtocolException, IOException {
    DefaultHttpClient client = new DefaultHttpClient();
    client.getConnectionManager().getSchemeRegistry().register( scheme );
    sf.setParams( latency, upBandwidth, downBandwidth );
    URL url = new URL( "http", hostName, port, "/?length=" + length );
    HttpPost request = new HttpPost( url.toString() );
    byte[] buf = new byte[length];
    long t0 = System.currentTimeMillis();
    HttpResponse r = client.execute( request );
    InputStream stream = r.getEntity().getContent();
    int read = 0;
    while ( read < length ) {
      int n = stream.read( buf, read, length - read );
      if ( n < 0 ) {
        System.err.println( "Could not read from stream: " + n + "(" + read + " bytes read)" );
        System.exit( 1 );
      }
      read += n;
    }
    long t1 = System.currentTimeMillis();
    stream.close();
    EntityUtils.consume( r.getEntity() );
    return t1 - t0;
  }
}
