package risoe.syslab.comm.lba;

  import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

/** Socket that simulates the effects of certain latency 
 * and upstream/downstream parameters.
 * 
 * TODO: Support availability as well, as a probability
 * 
 * @author daku
 *
 */
public class LbaSocket extends Socket {

//  /** A TCP packet over ethernet has 1518 bytes payload:
//   *    1500 byte ethernet frame size 
//   *    - 42 byte ethernet frame size 
//   *    - 20 byte ip header 
//   *    - 20 byte tcp header
//   *  = 1518 bytes payload )
//   */
//  private static final int PACKET_SIZE = 1518;
  
  /** 
   * Default TCP window size
   */
  private static final int TCP_WINDOW_SIZE = 64*1024;
  
  /** Flag for switching internal debugging on and off */
  private static final boolean DEBUG = false;
  
  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  private volatile double latency = 0; 

  /** simulated up bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  private volatile double upBandwidth = 0;

  /** simulated down bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  private volatile double downBandwidth = 0;

  private volatile double failureProbability = 0;

  private WrappedInputStream wis = null;
  private WrappedOutputStream wos = null;

  /** Random part of latency; default is zero */
  private double latencyRandom = 0;

  /** Random part of up bandwidth; default is zero */
  private double upBandwidthRandom = 0;

  /** Random part of down bandwidth; default is zero */
  private double downBandwidthRandom = 0;
  
  private static Random r = new Random();
  
  public LbaSocket() {
    super();
  }

  public LbaSocket( InetAddress address, int port, InetAddress localAddr, int localPort ) throws IOException {
    super( address, port, localAddr, localPort );
  }

  public LbaSocket( InetAddress address, int port ) throws IOException {
    super( address, port );
  }

  public LbaSocket( String host, int port, InetAddress localAddr, int localPort ) throws IOException {
    super( host, port, localAddr, localPort );
  }

  public LbaSocket( String host, int port ) throws UnknownHostException, IOException {
    super( host, port );
  }

  
  /** Get the set latency [ms]
   * 
   * @return latency in milliseconds
   */
  public double getLatency() {
    return latency;
  }

  /** Set the latency [ms]
   * 
   * @param latency latency in milliseconds
   */
  public void setLatency( double latency, double random ) {
    this.latency = latency;
    this.latencyRandom = random;
  }

  /** Get the set upstream bandwidth [byte/ms]
   * 
   * @return upstream bandwidth in bytes per millisecond
   */
  public double getUpBandwidth() {
    return 1 / upBandwidth;
  }

  /** Set the upstream bandwidth [byte/ms]
   * 
   * @param bandwidth upstream bandwidth in bytes per millisecond
   */
  public void setUpBandwidth( double upBandwidth, double random ) {
    this.upBandwidth = 1 / upBandwidth;
    this.upBandwidthRandom = random;
    if ( Double.isInfinite( this.upBandwidth ) || Double.isNaN( this.upBandwidth ) )
      this.upBandwidth = 0;
  }

  /** Get the set downstream bandwidth [byte/ms]
   * 
   * @return downstream bandwidth in bytes per millisecond
   */
  public double getDownBandwidth() {
    return 1 / downBandwidth;
  }

  /** Set the downstream bandwidth [byte/ms]
   * 
   * @param bandwidth downstream bandwidth in bytes per millisecond
   */
  public void setDownBandwidth( double downBandwidth, double random ) {
    this.downBandwidth = 1 / downBandwidth;
    this.downBandwidthRandom = random;
    if ( Double.isInfinite( this.downBandwidth ) || Double.isNaN( this.downBandwidth ) )
      this.downBandwidth = 0;
  }

  @Override
  public synchronized InputStream getInputStream() throws IOException {
    if ( wis == null ) 
      wis = new WrappedInputStream( super.getInputStream() );
    return wis;
  }

  @Override
  public synchronized OutputStream getOutputStream() throws IOException {
    if (wos == null )
      wos = new WrappedOutputStream( super.getOutputStream() );
    return wos;
  }
  
  
  class WrappedInputStream extends InputStream {

    private InputStream is;
    private boolean first = true;
    private int dataSent = 0;
    private double toWait = 0.0;

    public WrappedInputStream( InputStream is ) {
      super();
      this.is = is;
    }

    /** This is the method responsible for simulating 
     * latency and bandwidth.
     * @param bufLength length of data that has been received
     * @throws IOException might happen when #failureProbability is larger than zero 
     */
    private void mayWait( int bufLength ) throws IOException {
      
      if ( failureProbability > 0 ) {
        if ( r.nextDouble() < failureProbability ) {
          throw new IOException( "failure to comply.." );          
        }
      }
      
      // TODO: What about this? setup of IP connection?
      if ( first ) {
        toWait += latency + rnd(latencyRandom);
      }
      first = false;

      toWait += (downBandwidth +  rnd(downBandwidthRandom)) * bufLength;
      
      dataSent += bufLength;
      while ( dataSent > TCP_WINDOW_SIZE ) {
        toWait += latency;
        dataSent -= TCP_WINDOW_SIZE;
      }
      
      if ( toWait > 1.0 ) {
        try {
          if (DEBUG) System.err.println("Waiting for: " + (long) toWait + " (down) [" + latency + "," + upBandwidth + "," + downBandwidth + "]");
          Thread.sleep( (long) toWait );
          toWait -= (int) toWait;
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }
    }

    @Override
    public int read() throws IOException {
      int result = is.read();
      mayWait( 1 );
      return result;
    }

    @Override
    public int read( byte[] arg0, int arg1, int arg2 ) throws IOException {
      int result = is.read( arg0, arg1, arg2 );
      if ( result > 0 ) mayWait( result );
      return result;
    }

    @Override
    public int read( byte[] b ) throws IOException {
      int result = is.read( b );
      if ( result > 0 ) mayWait( result );
      return result;
    }

    @Override
    public int available() throws IOException {
      return is.available();
    }

    @Override
    public void close() throws IOException {
      is.close();
    }

    @Override
    public boolean equals( Object obj ) {
      return is.equals( obj );
    }

    @Override
    public int hashCode() {
      return is.hashCode();
    }

    @Override
    public void mark( int readlimit ) {
      is.mark( readlimit );
    }

    @Override
    public boolean markSupported() {
      return is.markSupported();
    }

    @Override
    public void reset() throws IOException {
      is.reset();
    }

    @Override
    public long skip( long arg0 ) throws IOException {
      return is.skip( arg0 );
    }

    @Override
    public String toString() {
      return is.toString();
    }

  }

  class WrappedOutputStream extends OutputStream {

    private OutputStream os;
    private boolean first = true;
    private int dataSent = 0;
    private double toWait = 0.0;

    public WrappedOutputStream( OutputStream os ) {
      super();
      this.os = os;
    }

    /** This is the method responsible for simulating 
     * latency and bandwidth.
     * @param bufLength length of data that is to be sent
     * @throws IOException might happen when #failureProbability is larger than zero 
     */
    private void mayWait( int bufLength ) throws IOException {

      if ( failureProbability > 0 ) {
        if ( r.nextDouble() < failureProbability ) {
          throw new IOException( "failure to comply.." );          
        }
      }
      
      // TODO: What about this? setup of IP connection?
      if ( first ) {
        toWait += latency + rnd(latencyRandom);
      }
      first = false;

      toWait += (upBandwidth + rnd(upBandwidthRandom)) * bufLength;
      
      dataSent += bufLength;
      while ( dataSent > TCP_WINDOW_SIZE ) {
        toWait += latency;
        dataSent -= TCP_WINDOW_SIZE;
      }
      
      if ( toWait > 1.0 ) {
        try {
          if (DEBUG) System.err.println("Waiting for: " + (long) toWait + " (up)[" + latency + "," + upBandwidth + "," + downBandwidth + "]");
          Thread.sleep( (long) toWait );
          toWait -= (int) toWait;
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }
    }

    @Override
    public void write( byte[] arg0, int arg1, int arg2 ) throws IOException {
      mayWait( arg2 );
      os.write( arg0, arg1, arg2 );
    }

    @Override
    public void write( byte[] b ) throws IOException {
      mayWait( b.length );
      os.write( b );
    }

    @Override
    public void write( int arg0 ) throws IOException {
      mayWait( 1 );
      os.write( arg0 );
    }

    @Override
    public void close() throws IOException {
      os.close();
    }

    @Override
    public boolean equals( Object obj ) {
      return os.equals( obj );
    }

    @Override
    public void flush() throws IOException {
      os.flush();
    }

    @Override
    public int hashCode() {
      return os.hashCode();
    }

    @Override
    public String toString() {
      return os.toString();
    }

  }

  /** Generates a random value between -val and val.
   * 
   * @param val maximum absolute value that is returned
   * @return value betwenn -val and val
   */
  public double rnd( double val ) {
    if (Double.isNaN( val )) return 0;
    if (val < 0 ) throw new AssertionError( "assertion not true: val >= 0" );
    if (val == 0.0) return 0;
    return 2.0*val*r.nextDouble() - val;
  }

}
