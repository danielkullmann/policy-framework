package risoe.syslab.comm.lba;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;


/**
 *
 * Conversion from ms/packet into kbyte/s 
 * t   = [ ms/packet ]
 * 1/t = [packet / ms ]
 *     * 1000 ms / 1 s
 *     * 256*BLOCK_SIZE byte / packet
 *     * kbyte / 1000 byte
 * =>
 * 256*BLOCK_SIZE / t = [kbyte/s]
 *
 * @author daku
 *
 */
public class LbaMeasure {

  private static int REPETITIONS = 5000;
  private static LbaCheckClient client;
  private static PrintStream out = System.out;

  public static void main( String[] args ) {
    if (args.length < 1 ) {
      System.err.println( "Usage: java " + LbaMeasure.class.getName() + " hostname");
      System.exit( 1 );
    }

    client = new LbaCheckClient( args[0] );
    LbaSocketFactory socketFactory = null;
    double latency = 0;
    double endLatency = 0;
    double stepLatency = 0;
    double upBandwidth = 0;
    double endUpBandwidth = 0;
    double stepUpBandwidth = 0;
    double downBandwidth = 0;
    double endDownBandwidth = 0;
    double stepDownBandwidth = 0;
    boolean multiple = false;

    ArrayList<String> latencies = new ArrayList<String>();
    ArrayList<String> bandwidths = new ArrayList<String>();
    HashMap<String, double[]> measurements = new HashMap<String, double[]>();
    
    if (args.length > 2 ) {
      latency = Double.parseDouble( args[1] );
      upBandwidth = Double.parseDouble( args[2] );
      downBandwidth = Double.parseDouble( args[3] );
      socketFactory = new LbaSocketFactory( latency, upBandwidth, downBandwidth );
      client.useSocketFactory( socketFactory  );
    }

    if (args.length > 9 ) {
      endLatency = Double.parseDouble( args[4] );
      stepLatency = Double.parseDouble( args[5] );
      
      endUpBandwidth = Double.parseDouble( args[6] );
      stepUpBandwidth = Double.parseDouble( args[7] );

      endDownBandwidth = Double.parseDouble( args[8] );
      stepDownBandwidth = Double.parseDouble( args[9] );
      
      REPETITIONS = 100;
      multiple = true;
    }
    
    if ( multiple ) {
      double startLatency = latency;
      while( true ) {
        out.println( "== latency: " + latency + "; up: " + upBandwidth + "; down: " + downBandwidth );
        if ( socketFactory == null ) throw new IllegalStateException();
        socketFactory.setLatency( latency, 0 );
        socketFactory.setUpBandwidth( upBandwidth, 0 );
        socketFactory.setDownBandwidth( downBandwidth, 0 );
        
        double[] data = measure();
        
        if ( ! latencies.contains( ""+latency ) ) latencies.add( ""+latency );
        if ( ! bandwidths.contains( upBandwidth + "-" + downBandwidth ) ) bandwidths.add( upBandwidth + "-" + downBandwidth  );
        measurements.put( latency + "-" + upBandwidth + "-" + downBandwidth, data );
        
        if ( latency >= endLatency ) {
          latency = startLatency;
          if ( upBandwidth >= endUpBandwidth && downBandwidth >= endDownBandwidth )
            break;
          upBandwidth += stepUpBandwidth;
          downBandwidth += stepDownBandwidth;
        } else {
          latency += stepLatency;
        }
        
        if ( latency > endLatency ) latency = endLatency;
        if ( upBandwidth > endUpBandwidth ) upBandwidth = endUpBandwidth;
        if ( downBandwidth > endDownBandwidth ) downBandwidth = endDownBandwidth;
      }
      
      // Output 3 tables for latency, upstream bandwidth and downstream bandwidth
      for ( int i = 0; i < 3; i++ ) {
        try {
          File content = new File( "measurements/table-lba/table-lba-measurements-" + i + ".tex" );
          BufferedWriter w = new BufferedWriter( new FileWriter( content ) );
          for ( String lat : latencies ) {
            w.write( " & \\multicolumn{1}{c}{" + lat + "}" );
          }
          w.write(" \\\\ \\hline \n");
          for ( String bw : bandwidths ) {
            int idx = bw.indexOf( "-" );
            // conversion from byte/ms into kbyte/s: 
            // byte/s = byte/ms * kbyte/(1000*byte) * (1000*ms)/s = kbyte/s;
            // => no conversion needed
            double up = Double.parseDouble( bw.substring( 0, idx ) );
            double down = Double.parseDouble( bw.substring( idx+1 ) );
            w.write( "" + up + " / " + down);
            
            for ( String lat : latencies ) {
              w.write( " & " + String.format( "%8.2f", measurements.get( lat + "-" + bw )[i] ) );
            }
            w.write(" \\\\\n");
          }
          w.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      
    } else {
      measure();
    }
    
  }
  
  private static double[] measure() {
    long t = client.message( REPETITIONS, 0, 0 );
    double latency = (double) t / REPETITIONS;
    out.println( "latency: " + latency + " ms" );
    
    long t2 = client.message( REPETITIONS, 256, 0 );
    double timeUpstream = (double) t2/REPETITIONS;
    double bwUpstream = (256*LbaCheck.BLOCK_SIZE)/timeUpstream;
    out.println( "upstream bandwidth: " + bwUpstream  + " kbyte/s" );
    
    long t3 = client.message( REPETITIONS, 0, 256 );
    double timeDownstream = (double) t3/REPETITIONS;
    double bwDownstream = (256*LbaCheck.BLOCK_SIZE)/timeDownstream;
    out.println( "downstream bandwidth: " + bwDownstream + " kbyte/s" );
    
    return new double[] { latency, bwUpstream, bwDownstream };
  }
}
