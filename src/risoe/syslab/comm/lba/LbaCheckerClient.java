package risoe.syslab.comm.lba;

import java.io.PrintStream;

public class LbaCheckerClient {

  public static void main( String[] args ) {
    LbaCheckClient client = new LbaCheckClient( args[0] );
    PrintStream out = System.out; 
    for ( int i = 1; i <= 16; i++ ) {
      for ( int j = 1; j <= 16; j++ ) {
        long t = client.message( 100, 16*i, 16*j );
        out.println( "" + i + " : " + j + " : " + t );
      }
    }
  }
}

