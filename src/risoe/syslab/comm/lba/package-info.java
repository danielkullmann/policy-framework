/**
 * This package contains all classes that are related to 
 * measuring latency, bandwidth and availability effects.
 */
package risoe.syslab.comm.lba;
