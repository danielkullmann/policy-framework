package risoe.syslab.comm.lba;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class SimulatedServerSocket extends ServerSocket {

  private double latency;
  private double upBandwidth;
  private double downBandwidth;
  private double latencyRandom;
  private double upBandwidthRandom;
  private double downBandwidthRandom;

  public SimulatedServerSocket() throws IOException {
    super();
  }

  public SimulatedServerSocket( int arg0, int arg1, InetAddress arg2 ) throws IOException {
    super( arg0, arg1, arg2 );
  }

  public SimulatedServerSocket( int port, int backlog ) throws IOException {
    super( port, backlog );
  }

  public SimulatedServerSocket( int port ) throws IOException {
    super( port );
  }

  @Override
  public Socket accept() throws IOException {
    if (isClosed()) throw new SocketException("Socket is closed");
    if (!isBound()) throw new SocketException("Socket is not bound yet");
    LbaSocket s = new LbaSocket();
    s.setLatency( latency, latencyRandom );
    s.setUpBandwidth( upBandwidth, upBandwidthRandom );
    s.setDownBandwidth( downBandwidth, downBandwidthRandom );
    implAccept(s);
    return s;
  }

  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  public double getLatency() {
    return latency;
  }

  /** simulated latency [ms]
   * This is additional to the inherent latency!
   */
  public void setLatency( double latency, double random ) {
    this.latency = latency;
    this.latencyRandom = random;
  }

  /** simulated up bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public double getUpBandwidth() {
    return upBandwidth;
  }

  /** simulated up bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public void setUpBandwidth( double upBandwidth, double random ) {
    this.upBandwidth = upBandwidth;
    this.upBandwidthRandom = random;
  }

  /** simulated down bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public double getDownBandwidth() {
    return downBandwidth;
  }

  /** simulated down bandwidth [ms/byte].
   * The unit of this value is inverted bandwidth, ms/byte.
   * This is additional to the inherent bandwidth delay!
   */
  public void setDownBandwidth( double downBandwidth, double random ) {
    this.downBandwidth = downBandwidth;
    this.downBandwidthRandom = random;
  }

}
