/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.syslabdemo1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.message.parser.DoubleConstant;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.IntegerConstant;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.StringConstant;

/**
 * One DeviceInfo class for the sd1 implementation
 */
public final class Sd1DeviceInfo implements DeviceInfo, Serializable {

  /** */
  private static final long serialVersionUID = 1314370086910411543L;

  /** String that starts the FIPA content off this class */
  public static final String MSG_ID = "sd1";

  /** key used in FIPA content */
  public static final String KEY_ID = ":id";
  /** key used in FIPA content */
  public static final String KEY_SOC = ":soc";
  /** key used in FIPA content */
  public static final String KEY_TSOC = ":tsoc";
  /** key used in FIPA content */
  public static final String KEY_CAPACITY = ":capacity";
  /** key used in FIPA content */
  public static final String KEY_NEXT = ":next";
  /** key used in FIPA content */
  public static final String KEY_SERVICES = ":services";
  

  /** Unique ID of device */
  private String id;
  
  /** Current SOC (state Of Charge) */
  private double soc;
  
  /** target SOC */
  private double tsoc;
  
  /** How much kWH capacity has the battery? */
  private double capacity;
  
  /** When is the car needed again? */
  private Date nextUse;
  
  /** List of services that this device supports */
  private ArrayList<String> services;


  /** Default constructor; should NEVER be called  */
  @SuppressWarnings( "unused" )
  private Sd1DeviceInfo() {
    throw new AssertionError();
  }
  
  
  /**
   * Standard constructor, giving values for all the fields.
   * 
   * @param id
   * @param soc
   * @param tsoc
   * @param capacity
   * @param nextUse
   * @param services
   */
  public Sd1DeviceInfo( String id, double soc, double tsoc, double capacity, 
      Date nextUse, ArrayList<String> services ) {
    super();
    this.id = id;
    this.soc = soc;
    this.tsoc = tsoc;
    this.capacity = capacity;
    this.nextUse = nextUse;
    this.services = services;
  }


  /**
   * Calculate the energy needed to charge the EV to the target SOC.
   * 
   * @return energy in kWh
   */
  public double calcEnergyNeeded() {
    return (getTsoc() - getSoc()) / 100.0 * getCapacity();
  }
  
  
  /** Method to parse an object of this class fromString content
   * created by {@link #toFipaContent()}
   * @param expression Expression returned by {@link MessageParser#parse(String)}
   * @return the created {@link Sd1DeviceInfo}, or null if parsing failed
   */
  public static Sd1DeviceInfo parse( Expression expression ) {
    
    String pattern =  "(" + Sd1DeviceInfo.MSG_ID + " " + 
    Sd1DeviceInfo.KEY_ID       + " ?id " + 
    Sd1DeviceInfo.KEY_SOC      + " ?soc " + 
    Sd1DeviceInfo.KEY_TSOC     + " ?tsoc " + 
    Sd1DeviceInfo.KEY_CAPACITY + " ?capacity " + 
    Sd1DeviceInfo.KEY_NEXT     + " ?nextUse" + 
    Sd1DeviceInfo.KEY_SERVICES + " ?services" + 
    ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    String id = ((StringConstant) match.get( "id" )).getValue();
    double soc = ((DoubleConstant) match.get( "soc" )).getValue();
    double tsoc = ((DoubleConstant) match.get( "tsoc" )).getValue();
    double capacity = ((DoubleConstant) match.get( "capacity" )).getValue();
    Date nextUse = new Date( ((IntegerConstant) match.get( "nextUse" )).getValue() );
    ArrayList<String> services = new ArrayList<String>();
    ExpressionList el = (ExpressionList) match.get("services");
    while (el != null) {
      services.add( el.getHead().valueString() );
      el = el.getTail();
    }
    return new Sd1DeviceInfo( id, soc, tsoc, capacity, nextUse, services );
  }

  
  @Override
  public String toFipaContent() {
    return "(" + MSG_ID + " " + 
      KEY_ID       +  " " + id   + " " + 
      KEY_SOC      + " " + soc  + " " + 
      KEY_TSOC     + " " + tsoc + " " + 
      KEY_CAPACITY + " " + capacity + " " + 
      KEY_NEXT     + " " + nextUse.getTime() + 
      KEY_SERVICES + " " + ExpressionList.makeStringList( services ) + 
      ")";
  }


  /** Simple getter for id of this "car"
   * @return id of this object
   */
  public String getId() {
    return id;
  }


  /** Simple getter
   * @return SOC (state-of-charge) of this object
   */
  public double getSoc() {
    return soc;
  }


  /** Simple getter
   * @return target SOC of this object
   */

  public double getTsoc() {
    return tsoc;
  }


  /** Simple getter
   * @return date of next use of EV for this object
   */
  public Date getNextUse() {
    return nextUse;
  }


  /** Simple getter
   * @return capacity of the EV for this object
   */
  public double getCapacity() {
    return capacity;
  }


  /**
   * Simple getter
   * @return list of services this device supports
   */
  public ArrayList<String> getServices() {
    return services;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits(capacity);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((nextUse == null) ? 0 : nextUse.hashCode());
    result = prime * result + ((services == null) ? 0 : services.hashCode());
    temp = Double.doubleToLongBits(soc);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(tsoc);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Sd1DeviceInfo other = (Sd1DeviceInfo) obj;
    if (Double.doubleToLongBits(capacity) != Double
        .doubleToLongBits(other.capacity))
      return false;
    if (id != other.id)
      return false;
    if (nextUse == null) {
      if (other.nextUse != null)
        return false;
    } else if (!nextUse.equals(other.nextUse))
      return false;
    if (services == null) {
      if (other.services != null)
        return false;
    } else if (!services.equals(other.services))
      return false;
    if (Double.doubleToLongBits(soc) != Double.doubleToLongBits(other.soc))
      return false;
    if (Double.doubleToLongBits(tsoc) != Double.doubleToLongBits(other.tsoc))
      return false;
    return true;
  }


}
