package risoe.syslab.demo.policy.syslabdemo1.gui;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Rectangle;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

/** Try to get a visualisation for the demo data.
 *  
 * @author daku
 */
@SuppressWarnings("serial")
public class Visualisation extends JFrame {

  public Visualisation() throws HeadlessException {
    super();
    createGui();
  }

  /**
   */
  private void createGui() {
    
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] gs = ge.getScreenDevices();
    Rectangle fullscreenBounds = gs[0].getDefaultConfiguration().getBounds();

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    setUndecorated(true);

    JPanel rootPanel = new JPanel();
    rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.Y_AXIS));
    rootPanel.setOpaque(true); // content panes must be opaque
    rootPanel.setBorder(new BevelBorder(5));

    setContentPane(rootPanel);
    
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

    setTitle("Visualisation");
    setMinimumSize( new Dimension(240,160) );

    setBounds( fullscreenBounds );
    setPreferredSize( new Dimension( fullscreenBounds.width, fullscreenBounds.height ) );
    
    rootPanel.add( panel );
    getContentPane().validate();
    pack();
    setVisible( true );

  }

  public static void main( String[] args ) {
    new Visualisation();
  }

}
