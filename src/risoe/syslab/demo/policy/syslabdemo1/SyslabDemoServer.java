package risoe.syslab.demo.policy.syslabdemo1;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.ServerBehaviour;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.core.ServerClientBehaviour;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.demo.policy.evcharging1.Client;
import risoe.syslab.demo.policy.evcharging1.EVCharging1DeviceInfo;

/** PolicyServer for syslabdemo1 */
public class SyslabDemoServer implements ServerBehaviour {

  /** Logger used by the class */
  private static final Logger logger = Logger.getLogger( SyslabDemoServer.class.getName() );

  /** Map of all connected clients */
  private HashMap<AbstractAddress, Client> clients;
  
  /** referemnce to data collector */
  private CollectorInterface collector;
  
  /** name of host where {@link CollectorInterface} server runs */
  private String host;


  /** Standard constructor
   * 
   * @param host name of host where {@link CollectorInterface} server runs
   */
  public SyslabDemoServer(String host) {
    super();
    this.host = host;

    PolicyUtils.setup( "conf/policy-framework.properties" );

    CommunicationSystem commSystem = new HttpCommunicationSystem( "" );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    AbstractAddress serverAddress = commSystem.createAddress( "syslab-demo-server" );
    PolicyUtils.createServerAgent( serverAddress, this );
    logger.log( Level.INFO, serverAddress.getAddress().toString() );
    connect();
  }

  @Override
  public DeviceInfo parseDeviceInfo(Expression expression) {
    Sd1DeviceInfo info = Sd1DeviceInfo.parse(expression);
    if ( info != null ) return info;
    // TODO add more supported device info classes
    throw new IllegalArgumentException("device info type is unknown!");
  }

  @Override
  public ServerClientBehaviour createServerBehaviour(AbstractAddress clientAddress,
      DeviceInfo deviceInfo) {
    SyslabDemoServerClientBehaviour behaviour = new SyslabDemoServerClientBehaviour( this, clientAddress, deviceInfo );
    connect();
    
    return behaviour;
  }

  @Override
  public Policy parsePolicy(Expression expression) {
    SimpleOrderedPolicy soPolicy = SimpleOrderedPolicy.parse(expression);
    if ( soPolicy != null ) return soPolicy;
    throw new IllegalArgumentException( "Unknown policy" );
  }

  /** Create a policy for the client with the given address
   * 
   * @param address address of client for which policy should be created
   * @return Created policy, or null if none was created
   */
  public Policy createPolicy(AbstractAddress address) {
    
    Client client = clients.get( address );
    if ( client == null ) {
      throw new AssertionError( "Client has not registered itself.." );
    }
    
    Date now = new Date();
    
    if ( client.getActivePolicy() != null ) {
      // check validity period of active policy
      if ( client.getActivePolicy().getTimeFrame().isCurrent( now ) )
        return client.getActivePolicy();
      // else..
      client.setActivePolicy( null );
    }
    
    if ( client.getNextPolicy() != null ) {
      // check validity period of next policy
      if ( client.getNextPolicy().getTimeFrame().isCurrent( now ) )
        return client.getNextPolicy();
      // else..
      client.setNextPolicy( null );
    }
    
    // Instead of sending an IdlePolicy, I return null
    // here, and the PolicyServer waits until a policy
    // for the client becomes available.
    return null;

  }

  /** Getter for device info of client with given address
   * 
   * @param address address of client we are interested in
   * @return device info for that client
   */
  public DeviceInfo getDeviceInfo(AbstractAddress address) {
    try {
      return clients.get( address ).getDeviceInfo();
    } catch (NullPointerException e) {
      throw new IllegalArgumentException( "unknown address" );
    }
  }

  /** activates policy for client with given address
   * 
   * @param address address of client that should get a new policy
   * @param policy policy for that client
   */
  public void activatePolicy(AbstractAddress address, Policy policy) {
    try {
      clients.get( address ).setActivePolicy( policy );
    } catch (NullPointerException e) {
      throw new IllegalArgumentException( "unknown address" );
    }
  }

  /** Getter for currently active of client with given address
   * 
   * @param address address of client we are interested in
   * @return currently active policy for that client
   */
  public Policy getActivePolicy(AbstractAddress address) {
    try {
      return clients.get( address ).getActivePolicy();
    } catch (NullPointerException e) {
      throw new IllegalArgumentException( "unknown address" );
    }
  }

  /** reject policy for a client
   * 
   * @param address address of client
   * @param policy policy that should be rejected
   */
  public void rejectPolicy( AbstractAddress address, Policy policy) {
    Client client = clients.get( address );
    if ( client == null )
      throw new IllegalArgumentException( "unknown address" );
    
    if ( client.getNextPolicy().equals( policy ) ) {
      client.setNextPolicy( null );
    }
  }

  /** connect to {@link CollectorInterface} server */
  private void connect() {
    if ( this.collector != null ) return; 
    try {
      this.collector = RMIHelper.getCollectorInterface(host);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (RemoteException e) {
      e.printStackTrace();
    } catch (NotBoundException e) {
      System.err.println("CollectorInterface not available..");
    }
  }

  /** Entry point for Java application; starts the server */ 
  public static void main( String[] args ) {
    if ( args.length <= 1 ) {
      System.err.println("Usage: java SyslabDemoServer <host name>");
      System.exit(1);
    }
    new SyslabDemoServer( args[0] );
  }

  @Override
  public void setPolicyServer(PolicyServer policyServer) {
    // TODO Implement
    throw new RuntimeException("not yet supported..");
  }

  /** Set device info for a certain client
   * 
   * @param address address of client
   * @param deviceInfo new device info for that client
   */
  public void setDeviceInfo(AbstractAddress address, EVCharging1DeviceInfo deviceInfo) {
    clients.get( address ).setDeviceInfo( deviceInfo );
  }
  
}
