package risoe.syslab.demo.policy.syslabdemo1;

import risoe.syslab.control.policy.core.Policy;

/**
 * Class encapsualting information about a device
 * 
 * @todo not used yet!
 * @author daku
 */
public class Client {

  private Sd1DeviceInfo deviceInfo;
  private Policy activePolicy;
  private Policy nextPolicy;
  
  /** Simple getter for deviceinfo */
  public Sd1DeviceInfo getDeviceInfo() {
    return deviceInfo;
  }

  /** Simple getter for currently active policy */
  public Policy getActivePolicy() {
    return activePolicy;
  }
  
  /** Simple getter for next policy to be activated */
  public Policy getNextPolicy() {
    return nextPolicy;
  }
  
}
