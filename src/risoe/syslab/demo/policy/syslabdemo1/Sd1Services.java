package risoe.syslab.demo.policy.syslabdemo1;

import java.util.Properties;

import risoe.syslab.control.policy.core.Schedule;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.control.policy.service.ServiceNameImpl;

/** Contains all services supported by this example */
public class Sd1Services {

  /** name of frequency control service using thresholds */
  public static final String FREQUENCY_CONTROL_THRESHOLD = "frequency-control:threshold:1.0";
  
  /** name of frequency control service using droop curve */
  public static final String FREQUENCY_CONTROL_DROOP = "frequency-control:droop:1.0";
  
  /** name of price-based service using thresholds */
  public static final String DYNAMIC_PRICE = "dynamic-price-reaction:threshold:1.0";
  
  /** name of schedule-based service */
  public static final String SCHEDULE = "schedule:simple:1.0";

  /** schedule-based service */
  public static final Service schService;
  
  /** price-based service using thresholds */
  public static final Service dppService;
  
  /** frequency control service using droop curve */
  public static final Service fcdService;
  
  /** frequency control service using thresholds */
  public static final Service fctService;
  
  /** Creates the Service fields */
  static {
    ServiceNameImpl schServiceName = new ServiceNameImpl( Sd1Services.SCHEDULE );
    Properties schPropertiesDescription = new Properties();
    schPropertiesDescription.put( "schedule", Schedule.class );
    schService = new Service( schServiceName, schPropertiesDescription, new Properties() );

    ServiceNameImpl dppServiceName = new ServiceNameImpl( Sd1Services.DYNAMIC_PRICE );
    Properties dppPropertiesDescription = new Properties();
    dppPropertiesDescription.put( "low", Double.class );
    dppPropertiesDescription.put( "high", Double.class );
    dppPropertiesDescription.put( "hysteresis", Double.class );
    dppService = new Service( dppServiceName, dppPropertiesDescription, new Properties() );
    
    ServiceNameImpl fcdServiceName = new ServiceNameImpl( Sd1Services.FREQUENCY_CONTROL_DROOP );
    Properties fcdPropertiesDescription = new Properties();
    fcdPropertiesDescription.put( "droop", Double.class );
    fcdPropertiesDescription.put( "deadband", Double.class );

    fcdService = new Service( fcdServiceName, fcdPropertiesDescription, new Properties() );

    ServiceNameImpl fctServiceName = new ServiceNameImpl( Sd1Services.FREQUENCY_CONTROL_THRESHOLD );
    Properties fctPropertiesDescription = new Properties();
    fctPropertiesDescription.put( "low", Double.class );
    fctPropertiesDescription.put( "high", Double.class );
    fctPropertiesDescription.put( "hysteresis", Double.class );
    fctService = new Service( fctServiceName, fctPropertiesDescription, new Properties() );
  }

  /** Disable default constructor */
  private Sd1Services() {
    throw new IllegalStateException("do not instantiate");
  }
  
}
