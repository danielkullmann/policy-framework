package risoe.syslab.demo.policy.syslabdemo1;

import java.io.Serializable;
import java.util.HashMap;

import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.StringConstant;

/** One policy class for the syslabdemo1.
 * 
 *  This contains the rule base for a client.
 * 
 * @author daku
 */
public class SdPolicy1 implements Policy, Serializable {

  /** */
  private static final long serialVersionUID = -1252163546374240394L;
  
  private static final String MSG_ID = "sd1-policy";
  private TimeFrame timeFrame;
  private String ruleBase;
  
  /** Parse method
   * 
   * @param expression Expression, as returned by {@link MessageParser#parse(String)} 
   * @return the parsed {@link SdPolicy1}, or null if that failed
   */
  public static SdPolicy1 parse( Expression expression ) {
    String pattern =  "(" + MSG_ID + " " + 
    ":rulebase ?rulebase " +
    " " + TimeFrame.KEY_TIME_FRAME + " ?tf" +
    ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    
    // Need to convert an ExpressionList into a boolean[]
    StringConstant rulebase = (StringConstant) match.get( "rulebase" );
    TimeFrame timeFrame = TimeFrame.parse( match.get( "tf" ) );
    
    return new SdPolicy1( rulebase.getValue(), timeFrame );

  }
  
  /** Standard constructor
   * 
   * @param ruleBase rule base for client 
   * @param timeFrame time frame of policy
   */
  public SdPolicy1(String ruleBase, TimeFrame timeFrame) {
    super();
    this.timeFrame = timeFrame;
    this.ruleBase = ruleBase;
  }

  @Override
  public String toFipaContent() {
    return "(" + MSG_ID + " " +
      ":rulebase \"" + ruleBase.replace("\\", "\\\\").replace("\"", "\\\"") + "\"" + " " +
      TimeFrame.KEY_TIME_FRAME + " " + timeFrame.toFipaContent() + 
      ")";
  }

  @Override
  public TimeFrame getTimeFrame() {
    return timeFrame;
  }

  /** Simple getter for ruleBase field */
  public String getRuleBase() {
    return ruleBase;
  }

}
