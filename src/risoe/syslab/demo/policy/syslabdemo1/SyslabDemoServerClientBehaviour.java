package risoe.syslab.demo.policy.syslabdemo1;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.ServerClientBehaviour;
import risoe.syslab.demo.policy.evcharging1.EVCharging1DeviceInfo;

/** server bahviour for a single client 
 * 
 * @author daku
 */
public class SyslabDemoServerClientBehaviour implements ServerClientBehaviour {

  /** address of client managed by this behaviour */
  private AbstractAddress address;
  
  /** server managing all the clients */
  private SyslabDemoServer server;

  /** STandard constructor
   * 
   * @param server server managing all the clients
   * @param address address of client managed by this behaviour
   * @param deviceInfo device info for that client; is ignored
   */
  public SyslabDemoServerClientBehaviour( SyslabDemoServer server, AbstractAddress address,
      DeviceInfo deviceInfo) {
    this.server = server;
    this.address = address;
  }

  @Override
  public Policy createPolicy() {
    return server.createPolicy( address );
  }

  @Override
  public DeviceInfo getClientDeviceInfo() {
    return server.getDeviceInfo( address );
  }

  @Override
  public void activatePolicy(Policy policy) {
    server.activatePolicy( address, policy );
  }

  @Override
  public Policy getActivePolicy() {
    return server.getActivePolicy( address );
  }

  @Override
  public void rejectPolicy(Policy policy) {
    server.rejectPolicy( address, policy );
  }

  @Override
  public void setClientDeviceInfo(DeviceInfo deviceInfo) {
    this.server.setDeviceInfo(address, (EVCharging1DeviceInfo) deviceInfo);
  }

}
