package risoe.syslab.demo.policy.syslabdemo1;

import java.util.ArrayList;
import java.util.HashMap;

import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.service.ServiceActivation;

/**
 * This is one of the ways to define a policy:
 * As an ordered list of {@link ServiceActivation} instances.
 *  
 * This enables the use of policies without any complicated 
 * rule systems, but it is also much less flexible than a
 * rule-based system.
 * 
 * @author daku
 */
public class SimpleOrderedPolicy implements Policy {

  private static final String MSG_ID = "simple-ordered-policy";

  /** time frame of policy */
  private TimeFrame timeFrame;
  
  /** list of service activations of this policy */
  private ArrayList<ServiceActivation> serviceActivations;

  /** Parses a {@link SimpleOrderedPolicy} from FIPA content
   * @param expression Expression, as returned by {@link MessageParser#parse(String)}
   * @return parsed policy, or null if that failed
   */
  public static SimpleOrderedPolicy parse( Expression expression ) {
    
    String pattern =  "(" + MSG_ID + " " + 
    ":services ?services " +
    " " + TimeFrame.KEY_TIME_FRAME + " ?tf" +
    ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    
    // Need to convert an ExpressionList into a boolean[]
    ExpressionList services = (ExpressionList) match.get( "services" );
    TimeFrame timeFrame = TimeFrame.parse( match.get( "tf" ) );
    
    ArrayList<ServiceActivation> serviceList = new ArrayList<ServiceActivation>();
    while ( services != null ) {
      serviceList.add( ServiceActivation.parse( services.getHead() ) );
      services = services.getTail();
    }
    
    return new SimpleOrderedPolicy( timeFrame, serviceList );
  }

  
  /** Standard constructor
   * 
   * @param timeFrame time frame of policy
   * @param serviceActivations list of service activations of this policy
   */
  public SimpleOrderedPolicy(TimeFrame timeFrame,
      ArrayList<ServiceActivation> serviceActivations) {
    super();
    this.timeFrame = timeFrame;
    this.serviceActivations = serviceActivations;
  }

  @Override
  public String toFipaContent() {
    StringBuilder sb = new StringBuilder();
    sb.append("(" + MSG_ID + " ");
    sb.append(":services " );
    sb.append("(" );
    for (ServiceActivation activation : serviceActivations) {
      sb.append( activation.toFipaContent() );
      sb.append( " " );
    }
    sb.append(")" );
    sb.append( TimeFrame.KEY_TIME_FRAME );
    sb.append( timeFrame.toFipaContent() );
    sb.append(")");
    
    return sb.toString();
    
  }

  @Override
  public TimeFrame getTimeFrame() {
    return timeFrame;
  }

  /** Simple getter for list of service activations */
  public ArrayList<ServiceActivation> getServiceActivations() {
    return serviceActivations;
  }

}
