package risoe.syslab.demo.policy.syslabdemo1;

import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * This class aggregates several clients (up to 10)
 * to use the heaters as consumption devices. 
 *  
 * TODO reconnect to demoServer when connection is lost..
 * 
 * @author daku
 */
public class FlexhouseAggregator {

  private static final int MAX_DEVICES = 10;

  /** connection to data collector service */
  private CollectorInterface demoServer;
  
  /** managed devices/clients */
  private ArrayList<FlexhouseDevice> devices;
  
  /** Standard constructor 
   * 
   * @param serverHostName host name of data collector service
   */
  public FlexhouseAggregator(String serverHostName) {
    super();
    try {
      demoServer = RMIHelper.getCollectorInterface(serverHostName);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /** add a new client; used locally
   * 
   * @param device {@link FlexhouseDevice}
   */
  private void addDevice( FlexhouseDevice device ) {
    if ( devices.size() >= MAX_DEVICES ) {
      throw new IllegalStateException("too many devices already");
    }
    devices.add( device );
    try {
      demoServer.addDevice( device.getDeviceInfo() );
    } catch (RemoteException e) {
      // TODO What can we do here? Reconnect?
      e.printStackTrace();
    }
  }

  /** Entry point for Java application; starts the server */
  public static void main( String[] args ) {
    
    if ( args.length < 2 ) {
      System.err.println("Usage: java ... FlexhouseAggregator <demoServer> <numDevices>");
    }
    String serverHostName = args[0];
    int numDevices = Integer.parseInt( args[1] );
    if ( numDevices > MAX_DEVICES ) {
      numDevices = MAX_DEVICES;
      System.err.println("Only " + MAX_DEVICES + " devices supported" );
    }
    
    FlexhouseAggregator aggr = new FlexhouseAggregator(serverHostName);
    
    for (int i = 0; i < numDevices; i++) {
      FlexhouseDevice device = new FlexhouseDevice( i );
      aggr.addDevice(device );
    }
  }
}
