package risoe.syslab.demo.policy.syslabdemo1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/** One of the clients managed by the server.
 * 
 * This is used by the {@link FlexhouseAggregator} to keep track of Flexhouse clients.
 * 
 * @author daku
 */
public class FlexhouseDevice {

  /** device info of client that server keeps */
  private ServerDeviceInfo serverDeviceInfo;

  /** Standard constructor
   * 
   * @param num local id of this client 
   */
  public FlexhouseDevice(int num) {
    // Next use is at 8:00 at the next day..
    Calendar nextUse = new GregorianCalendar();
    nextUse.setTime( new Date() );
    nextUse.add( Calendar.DAY_OF_MONTH, 1 );
    nextUse.set( Calendar.HOUR_OF_DAY, 8 );
    nextUse.set( Calendar.MINUTE, 30 );
    nextUse.set( Calendar.SECOND, 0 );
    nextUse.set( Calendar.MILLISECOND, 0 );
    
    ArrayList<String> services = new ArrayList<String>();
    // TODO: make device info more random, or else make it configurable..
    Sd1DeviceInfo deviceInfo = new Sd1DeviceInfo( "flxh" + num, 20, 90, 24, nextUse.getTime() , services );
    serverDeviceInfo = new ServerDeviceInfo();
    serverDeviceInfo.setDeviceInfo( deviceInfo );
    // TODO: Should know the address
    serverDeviceInfo.setAddress( null );
  }

  /** Simple getter for serverDeviceInfo field */
  public ServerDeviceInfo getDeviceInfo() {
    return serverDeviceInfo;
  }

  // TODO What other methods are needed?
  
}
