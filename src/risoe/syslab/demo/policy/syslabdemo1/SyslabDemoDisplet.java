package risoe.syslab.demo.policy.syslabdemo1;

import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import risoe.syslab.demo.policy.syslabdemo1.gui.DeviceDisplay;
import risoe.syslab.gui.wall.AbstractDisplet;
import risoe.syslab.gui.wall.AbstractSelectlet;
import risoe.syslab.gui.wall.AbstractSelectletStub;
import risoe.syslab.gui.wall.DispletTest;

/**
 * First try at Displet for displaying data from experiment
 * 
 * @author daku
 */
@SuppressWarnings("serial")
public class SyslabDemoDisplet extends AbstractDisplet implements Runnable {

  /** host the collector interface lives on. */
  private String host = "localhost";
  
  private double aspectRatio;
  
  /** list of managed devices */
  private ArrayList<ServerDeviceInfo> devices = new ArrayList<ServerDeviceInfo>();
  
  /** map for {@link DeviceDisplay} instances, one for each managed device */
  HashMap<String,DeviceDisplay> devDisplays = new HashMap<String, DeviceDisplay>();

  /** connection to {@link CollectorInterface} */
  CollectorInterface server;
  
  /** Thread for refreshing data */
  private Thread myThread;

  @Override
  public AbstractSelectletStub getSelectletStub() {
    return null;
  }

  @Override
  public void paintDisplet(Graphics2D g) {
    // TODO implement!
  }

  @Override
  public String getDispletName() {
    return getClass().getName();
  }

  @Override
  public String gainedFocus() {
    aspectRatio = ((double) myScreen.getScreenWidth()) / myScreen.getScreenHeight();
    // Get connection to server
    try {
      if ( server == null)
        server = RMIHelper.getCollectorInterface(host);
    } catch (Exception e) {
      e.printStackTrace();
    }
    // Setup display
    if ( myThread == null ) {
      myThread = new Thread( this );
      myThread.start();
    }
    return null;
  }

  @Override
  public void lostFocus() {
    if ( myThread != null ) {
      myThread.interrupt();
      myThread = null;
    }
    // Lose connection to server
    server = null;
  }

  @Override
  public Class<? extends AbstractSelectlet> getSelectletClass() {
    return null;
  }
  
  /** method for displaying device info */
  private void layoutDevices() {
    if ( devices.isEmpty() ) return;
    
    double numDevices = devices.size();
    int rows = (int) Math.sqrt( numDevices / aspectRatio );
    if ( rows == 0 ) rows = 1;
    int cols = (int) (numDevices / rows);
    if ( cols == 0 ) cols = 1;
    int width = myScreen.getScreenWidth() / cols;
    int height = myScreen.getScreenHeight() / rows;
    int i = 0;
    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        if ( i >= numDevices ) break;
        ServerDeviceInfo device = devices.get(i);
        DeviceDisplay devDisplay = devDisplays.get( device.getDeviceInfo().getId() );
        if ( devDisplay == null ) {
          devDisplay = new DeviceDisplay();
          devDisplays.put(device.getDeviceInfo().getId(), devDisplay);
          add( devDisplay ); 
        }
        // TODO test..
        devDisplay.setBounds(col*width, row*width, width, height);
        i++;
      }
    }
    validate();
    
    for (ServerDeviceInfo device : devices ) {
      DeviceDisplay devDisplay = devDisplays.get( device.getDeviceInfo().getId() );
      devDisplay.activePolicyChanged( device.getAddress(), device.getPolicy() );
      devDisplay.deviceInfoChanged( device.getAddress(), device.getDeviceInfo() );
    }
  }

  @Override
  public void run() {
    while ( ! Thread.interrupted() ) {
      if ( server == null ) {
        try {
          server = RMIHelper.getCollectorInterface(host);
        } catch (Exception e) {
          e.printStackTrace();
          server = null;
        }
      }
      
      if ( server != null ) {
        try {
          devices = server.getDevices();
        } catch (RemoteException e) {
          e.printStackTrace();
          // TODO server = null; ?
        }
        
        // Remove devices from display that are not longer connected..
        for (String key : devDisplays.keySet()) {
          boolean found = false;
          for (ServerDeviceInfo devInfo : devices) {
            if ( devInfo.getDeviceInfo().getId().equals(key)) found = true;
          }
          if ( ! found ) {
            remove( devDisplays.get( key ) );
            devDisplays.remove(key);
          }
        }
        layoutDevices();
      }
    }
  }

  /** Simple setter for hostname the {@link CollectorInterface} runs on
   * 
   * @param host
   */
  public void setHost(String host) {
    this.host = host;
  }

  
  /** Entry point for Java application; runs the displet */
  public static void main(String args[]) {
    try {
      GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice[] gs = ge.getScreenDevices();
      DispletTest display = new DispletTest(gs[0].getDefaultConfiguration());
      SyslabDemoDisplet displet = new SyslabDemoDisplet();

      // TODO Remove this debugging code, or move it to a test class...
      SyslabDemoCollector s = new SyslabDemoCollector();
      ServerDeviceInfo dev = new ServerDeviceInfo();
      ArrayList<String> services = new ArrayList<String>();
      services.add( Sd1Services.fctService.getServiceName().toString() );
      dev.setDeviceInfo( new Sd1DeviceInfo("1", 80, 100, 86, new Date(), services ));
      display.setDisplet( displet );
      s.addDevice( dev );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
