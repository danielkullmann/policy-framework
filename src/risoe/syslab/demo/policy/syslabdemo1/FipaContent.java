package risoe.syslab.demo.policy.syslabdemo1;

import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.core.Schedule;
import risoe.syslab.control.policy.core.Schedule.ScheduleBuilder;
import risoe.syslab.control.policy.core.Schedule.ScheduleEntry;
import risoe.syslab.control.policy.message.parser.DoubleConstant;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.IntegerConstant;
import risoe.syslab.control.policy.message.parser.MessageParser;

/** Helper class providing a couple of methods for converting
 * several standard classes to and from FIPA content.
 * 
 * FIPA content is the data format used in the policy messages;
 * it has a lisp-like syntax.
 * 
 * @author daku
 */
public class FipaContent {

  /** message id for {@link Schedule} instances */
  private static final String KEY_SCHEDULE = ":schedule"; 
  
  /** Method creatuing FIPA content string from a {@link Schedule} object.
   * 
   * @param schedule schedule we want to have FIPA content for
   * @return FIPA content string for schedule
   */
  public static String toFipaContent( Schedule<Double> schedule ) {

    StringBuilder sb = new StringBuilder( "(" + KEY_SCHEDULE + " " );
    sb.append( ":start " + schedule.getStart().getTime() + " " );
    sb.append( ":entries (" );
    for (ScheduleEntry<Double> entry : schedule.getEntries()) {
      sb.append("(" + entry.getValue() + " " + entry.getDuration() + ")");
    }
    sb.append(")");
    sb.append(")");
    return sb.toString();
    
  }
  
  
  /** Parses a schedule from FIPA content.
   * 
   * @param expression Expression, as returned by {@link MessageParser#parse(String)}
   * @return The parsed {@link Schedule} object, or null if that failed
   */
  public static Schedule<Double> scheduleFromFipaContent( Expression expression ) {

    String pattern =  "(" + KEY_SCHEDULE + " " + 
    ":start ?start " +
    ":entries ?entries " +
    ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    
    // Need to convert an ExpressionList into a boolean[]
    IntegerConstant start = (IntegerConstant) match.get( "start" );
    ExpressionList entries = (ExpressionList) match.get( "entries" );
    
    ScheduleBuilder<Double> builder = new ScheduleBuilder<Double>( new Date( start.getValue() ) );
    while ( entries != null ) {
      ExpressionList entry = (ExpressionList) entries.getHead(); 
      Double value = ((DoubleConstant) entry.getHead()).getValue();
      long duration = ((IntegerConstant) entry.getTail().getHead()).getValue();
      builder.addEntry(value, duration);
      entries = entries.getTail();
    }
    
    return builder.build();
    
  }
  
  /** Disable default constructor */
  private FipaContent() {
    throw new IllegalArgumentException("do not instantiate!"); 
  }

  
}
