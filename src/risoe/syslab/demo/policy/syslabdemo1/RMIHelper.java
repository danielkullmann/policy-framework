package risoe.syslab.demo.policy.syslabdemo1;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/** Class providing helper methods for RMI connections
 * 
 * @author daku
 */
public class RMIHelper {

  /** Connect to {@link CollectorInterface} running on given host
   * 
   * @param host name of host where {@link CollectorInterface} runs
   * @return {@link CollectorInterface}
   * @throws MalformedURLException
   * @throws RemoteException
   * @throws NotBoundException
   */
  static CollectorInterface getCollectorInterface( String host) throws MalformedURLException, RemoteException, NotBoundException {
    String fullName = "//" + host +":" + CollectorInterface.PORT + "/" + CollectorInterface.NAME;
    Object object = Naming.lookup( fullName );
    return (CollectorInterface) object;
  }

  
  /** Registers a RMI service
   * 
   * @param service service to register
   * @param bindPort port that RMI registry should use
   * @param interfaceName name of service interface
   * @param hostName hostname that RMI registry should listen on
   */
  public static void register(Remote service, int bindPort, String interfaceName,  String hostName) {
    String serverURI = "//" + hostName + ":" + bindPort + "/" + interfaceName;
    try {
      Registry reg;
      try {
        // LocateRegistry.getRegistry() just returns a handle, but does not check 
        // for existence of the registry:
        reg = LocateRegistry.getRegistry(bindPort);
        // This means that we have to call a method on the registry to check whether
        // it actually exists:  
        reg.list();
      } catch (RemoteException e) {
        // This exception is thrown when no registry has been started yet, 
        // so a registry is started now:
        reg = java.rmi.registry.LocateRegistry.createRegistry(bindPort);
      }
      Remote stub = UnicastRemoteObject.exportObject( service, 0 );
      Naming.rebind(serverURI, stub);
    } catch (RemoteException e) {
      throw new IllegalStateException( e );
    } catch (MalformedURLException e) {
      throw new IllegalStateException( e );
    }
  }

  /** Disbale default constructor */
  private RMIHelper() {
    throw new RuntimeException("Can't instantiate this class");
  }
}
