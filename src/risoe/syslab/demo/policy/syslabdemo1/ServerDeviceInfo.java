package risoe.syslab.demo.policy.syslabdemo1;

import java.io.Serializable;

import risoe.syslab.control.policy.comm.AbstractAddress;

/** Container class with data that the server keeps for each connected client.
 * 
 * @author daku
 */
public class ServerDeviceInfo implements Serializable {

  /** */
  private static final long serialVersionUID = 7546640376293209569L;
  
  /** address of client */
  private AbstractAddress address;
  
  /** device info of client */
  private Sd1DeviceInfo deviceInfo;
  
  /** (active?) policy of client */
  private SdPolicy1 policy;
  
  /** Simple getter for deviceInfo field */
  public Sd1DeviceInfo getDeviceInfo() {
    return deviceInfo;
  }

  /** Simple setter for deviceInfo field */
  public void setDeviceInfo(Sd1DeviceInfo deviceInfo) {
    this.deviceInfo = deviceInfo;
  }

  /** Simple getter for policy field */
  public SdPolicy1 getPolicy() {
    return policy;
  }

  /** Simple setter for policy field */
  public void setPolicy(SdPolicy1 policy) {
    this.policy = policy;
  }
  
  /** Simple getter for client address field */
  public AbstractAddress getAddress() {
    return address;
  }

  /** Simple setter for client address field */
  public void setAddress(AbstractAddress address) {
    this.address = address;
  }
  
}
