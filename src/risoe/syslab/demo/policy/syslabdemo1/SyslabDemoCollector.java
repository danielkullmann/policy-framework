package risoe.syslab.demo.policy.syslabdemo1;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.ArrayList;

/** Implementation of {@link CollectorInterface}; collects data
 * from the running experiment, and provides it to visualisation
 * components.
 * 
 * @author daku
 */
public class SyslabDemoCollector implements CollectorInterface {

  /** list of managed devices */
  private ArrayList<ServerDeviceInfo> devices = new ArrayList<ServerDeviceInfo>();
  
  /** Default constructor. Registers the RMI service. */
  public SyslabDemoCollector() {
    super();
    try {
      String hostName = InetAddress.getLocalHost().getHostName();
      RMIHelper.register( this, CollectorInterface.PORT, CollectorInterface.NAME, hostName);
    } catch (UnknownHostException e) {
      throw new IllegalStateException( e );
    }
  }
  
  @Override
  public ArrayList<ServerDeviceInfo> getDevices() throws RemoteException {
    return devices;
  }
  
  @Override
  public void addDevice(ServerDeviceInfo deviceInfo) throws RemoteException {
    devices.add(deviceInfo);
  }

  @Override
  public void removeDevice(ServerDeviceInfo deviceInfo) throws RemoteException {
    devices.remove(deviceInfo);
  }

}
