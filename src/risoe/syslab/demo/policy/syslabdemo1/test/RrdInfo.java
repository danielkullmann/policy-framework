package risoe.syslab.demo.policy.syslabdemo1.test;

import java.io.IOException;

import org.rrd4j.core.RrdDb;

/** Class trying out rrd4j */
public class RrdInfo {

  /** Java application reading the data written by {@link RrdCreate}.
   * 
   * @param args
   */
  public static void main(String[] args) {
    try {
      RrdDb db = new RrdDb( RrdCreate.PATH_FREQUENCY, true );
      String dump = db.dump();
      System.out.println( fix(dump) );
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("");
    try {
      RrdDb db = new RrdDb( RrdCreate.PATH_PRICE, true );
      String dump = db.dump();
      System.out.println( fix(dump) );
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static String fix(String dump) {
    String[] list = dump.split("\n");
    StringBuilder sb = new StringBuilder();
    for (String line : list) {
      if ( line.startsWith("Robin ") ) {
        line = line.substring( 0, line.indexOf( ":" )+1 ); 
      }
      sb.append( line + "\n" );
    }
    return sb.toString();
  }

}
