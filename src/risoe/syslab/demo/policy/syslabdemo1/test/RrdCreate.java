package risoe.syslab.demo.policy.syslabdemo1.test;

import java.io.IOException;
import java.util.Random;

import org.rrd4j.ConsolFun;
import org.rrd4j.DsType;
import org.rrd4j.core.RrdDb;
import org.rrd4j.core.RrdDef;
import org.rrd4j.core.Sample;

/** Class trying out rrd4j */
public class RrdCreate {

  public static final String PATH_FREQUENCY = "rrd-test-frequency.rrd";
  public static final String PATH_PRICE     = "rrd-test-power-price.rrd";

  /** Java application creating a rrd file and writing data to it.
   * @param args
   */
  public static void main(String[] args) {

    int num = 240;
    if ( args.length == 1 ) {
      num = Integer.parseInt( args[0] );
    }

    try {
      // Create databases
      new RrdDb( defFrequencyDb() );
      new RrdDb( defPriceDb() );
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    RrdDb freqDb = null;
    RrdDb priceDb = null;
    try {
      
      Random r = new Random();
      long start = System.currentTimeMillis()/1000;
      
      freqDb = new RrdDb( RrdCreate.PATH_FREQUENCY );
      priceDb = new RrdDb( RrdCreate.PATH_PRICE );
      for ( int i=1; i<=num; i++ ) {
        {
          Sample sample = freqDb.createSample();
          sample.setTime( start + i );
          sample.setValue( "frequency", 49 + 2*r.nextDouble() );
          sample.update();
        }

        {
          Sample sample = priceDb.createSample();
          sample.setTime( start + i );
          sample.setValue( "power-price", 40 + 40*r.nextDouble() );
          sample.update();
        }
        
//        try { Thread.sleep(1000); } catch (InterruptedException e) { return; }
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try { if (freqDb  != null) freqDb.close();  } catch (IOException e) { e.printStackTrace(); }
      try { if (priceDb != null) priceDb.close(); } catch (IOException e) { e.printStackTrace(); }
    }
    
  }

  /** define frequency db */
  public static RrdDef defFrequencyDb() {
    RrdDef freqDef = new RrdDef( PATH_FREQUENCY, 1l );
    freqDef.addDatasource( "frequency", DsType.GAUGE, 2l, 30, 70 );
    freqDef.addArchive( ConsolFun.AVERAGE, 0.5, 1, 24*60*60 ); // values for a whole day..
    freqDef.addArchive( ConsolFun.AVERAGE, 0.5, 20*60, 24*3 ); // 20-minutes average for a whole day..
    return freqDef;
  }

  /** define price db */
  public static RrdDef defPriceDb() {
    RrdDef priceDef = new RrdDef( PATH_PRICE, 60l );
    priceDef.addDatasource( "power-price", DsType.GAUGE, 120l, Double.NaN, Double.NaN );
    priceDef.addArchive( ConsolFun.AVERAGE, 0.5, 1, 24*60 ); // values for a whole day..
    priceDef.addArchive( ConsolFun.AVERAGE, 0.5, 20, 24*3 ); // 20-minutes average for a whole day..
    return priceDef;
  }

}
