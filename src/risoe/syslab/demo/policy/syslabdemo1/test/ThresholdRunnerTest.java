package risoe.syslab.demo.policy.syslabdemo1.test;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.StateChangeListener;
import risoe.syslab.control.policy.core.ThresholdRunner;
import risoe.syslab.control.policy.core.ThresholdRunner.State;
import risoe.syslab.control.policy.core.Thresholds;

/** Test class for {@link ThresholdRunner}
 * 
 * @author daku
 */
public class ThresholdRunnerTest extends TestCase implements StateChangeListener<State> {

  private boolean change;
  private State state;

  /** test for for {@link ThresholdRunner} */
  public void testRunner() {
    double low = 10;
    double high = 25;
    double hysteresis = 2;
    Thresholds t = new Thresholds(low, high, hysteresis );
    
    ThresholdRunner tr = new ThresholdRunner( t, this );
    
    change = false;
    tr.injectValue( 20 );
    tr.switchState();
    assertFalse( change );
    assertEquals( State.NORMAL, tr.getState() );

    change = false;
    tr.injectValue( 30 );
    tr.switchState();
    assertTrue( change );
    assertEquals( State.HIGH, tr.getState() );
    assertEquals( State.HIGH, state );

    change = false;
    tr.injectValue( 24 );
    tr.switchState();
    assertFalse( change );
    assertEquals( State.HIGH, tr.getState() );
    assertEquals( State.HIGH, state );

    change = false;
    tr.injectValue( 22.99 );
    tr.switchState();
    assertTrue( change );
    assertEquals( State.NORMAL, tr.getState() );
    assertEquals( State.NORMAL, state );

    change = false;
    tr.injectValue( 7 );
    tr.switchState();
    assertTrue( change );
    assertEquals( State.LOW, tr.getState() );
    assertEquals( State.LOW, state );

    change = false;
    tr.injectValue( 11.9 );
    tr.switchState();
    assertFalse( change );
    assertEquals( State.LOW, tr.getState() );
    assertEquals( State.LOW, state );

    change = false;
    tr.injectValue( 21.1 );
    tr.switchState();
    assertTrue( change );
    assertEquals( State.NORMAL, tr.getState() );
    assertEquals( State.NORMAL, state );
  }

  @Override
  public void stateChanged(State state) {
    change = true;
    this.state = state;
  }
}
