package risoe.syslab.demo.policy.syslabdemo1.test;

import java.util.Date;
import java.util.Iterator;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.Schedule;
import risoe.syslab.control.policy.core.Schedule.ScheduleBuilder;
import risoe.syslab.control.policy.core.Schedule.ScheduleEntry;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.demo.policy.syslabdemo1.FipaContent;
import risoe.syslab.demo.policy.syslabdemo1.SdPolicy1;

/** Test class for various classes
 * 
 * @author daku
 */
public class ContentTest extends TestCase {

  /** Test for {@link Thresholds} class */
  public void testThreshold() {
    Thresholds threshold = new Thresholds( 24.0, 38.0 );
    String str = threshold.toFipaContent();
    Thresholds result = Thresholds.parse( new MessageParser().parse( str ) );
    assertEquals( threshold.getLow(), result.getLow() );
    assertEquals( threshold.getHigh(), result.getHigh() );
    assertEquals( threshold.getHysteresis(), result.getHysteresis() );
  }
  
  /** Test for {@link Schedule} class */
  public void testSchedule() {
    ScheduleBuilder<Double> builder = new ScheduleBuilder<Double>( new Date() );
    builder.addEntry( 14.0, 10 );
    builder.addEntry( 0.0, 10 );
    builder.addEntry( 8.0, 15 );
    builder.addEntry( 12.0, 10 );
    builder.addEntry( 14.0, 20 );
    
    Schedule<Double> schedule = builder.build();
    
    String str = FipaContent.toFipaContent(schedule);
    Schedule<Double> result = FipaContent.scheduleFromFipaContent( new MessageParser().parse( str ) );
    
    assertEquals( schedule.getStart(), result.getStart() );
    Iterator<ScheduleEntry<Double>> it1 = schedule.getEntries().iterator();
    Iterator<ScheduleEntry<Double>> it2 = schedule.getEntries().iterator();
    while ( it1.hasNext() && it2.hasNext() ) {
      ScheduleEntry<Double> e1 = it1.next();
      ScheduleEntry<Double> e2 = it2.next();
      assertEquals(e1.getDuration(), e2.getDuration());
      assertEquals(e1.getValue(), e2.getValue());
    }
    
    assertFalse( it1.hasNext() || it2.hasNext() );
  }
  
  /** Test for {@link SdPolicy1} class */
  public void testSdPolicy1() {
    SdPolicy1 sdPolicy1 = new SdPolicy1("hallo(\"24\")", TimeFrame.createTimeFrame(new Date(), 0, 1, 0));
    String str = sdPolicy1.toFipaContent();
    SdPolicy1 result = SdPolicy1.parse( new MessageParser().parse( str ) );
    assertNotNull( result.getTimeFrame() );
    assertEquals( sdPolicy1.getTimeFrame(), result.getTimeFrame() );
    assertEquals( sdPolicy1.getRuleBase(), result.getRuleBase() );
  }
}
