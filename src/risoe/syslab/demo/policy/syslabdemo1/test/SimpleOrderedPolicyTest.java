package risoe.syslab.demo.policy.syslabdemo1.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.control.policy.service.ServiceActivation;
import risoe.syslab.control.policy.service.ServiceNameImpl;
import risoe.syslab.demo.policy.syslabdemo1.SimpleOrderedPolicy;
import junit.framework.TestCase;

/** Test class for {@link SimpleOrderedPolicy}
 * 
 * @author daku
 */
public class SimpleOrderedPolicyTest extends TestCase {

  /** test for {@link SimpleOrderedPolicy} */
  public void testPolicy() {
    ServiceNameImpl sn = new ServiceNameImpl("a", "b", "c");
    Properties props = new Properties();
    props.put("k1", "v1");
    props.put("k2", "v2");
    
    Properties props2 = new Properties();
    props2.put("k1", "vv1");
    props2.put("k2", "vv2");

    Service service1 = new Service( sn, props, new Properties() );
    ServiceActivation activation1 = new ServiceActivation( service1, props2 );
    
    ArrayList<ServiceActivation> serviceActivations = new ArrayList<ServiceActivation>();
    serviceActivations.add( activation1 );
    TimeFrame timeFrame = TimeFrame.createTimeFrame(new Date(), 0, 6, 0);
    SimpleOrderedPolicy policy = new SimpleOrderedPolicy(timeFrame , serviceActivations );

    String str = policy.toFipaContent();
    
    Expression expr = new MessageParser().parse( str );
    
    SimpleOrderedPolicy result = SimpleOrderedPolicy.parse( expr );
    ServiceActivation aresult = result.getServiceActivations().get(0);
    
    assertNotNull( result );
    assertEquals( policy.getTimeFrame(), result.getTimeFrame() );
    assertEquals( activation1.getService().getServiceName(), aresult.getService().getServiceName() );
    assertEquals( activation1.getService().getActivationProperties(), aresult.getService().getActivationProperties() );
    assertEquals( activation1.getParameters(), aresult.getParameters() );
  }
}
