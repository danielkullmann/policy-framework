package risoe.syslab.demo.policy.syslabdemo1;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/** Interface for class responsible for collecting
 * data.
 * 
 * This interface provides following data:
 * <ul>
 * <li> list of devices: devices add and remove themselves from the list
 * <li> current status of devices: devices regularly send their status 
 *      to the {@link CollectorInterface}; this should probably happen every
 *      second or few seconds
 * <li> logging of status: with the same time granularity as status updates
 *      this could also be done locally
 * </ul>
 * 
 * Status of the devices contains:
 * <ul>
 * <li> Relation to Policy Server? (UNKNOWN, REGISTERED, NEGOTIATING, POLICY_ACTIVE)
 * <li> Policy the devices are using right now (maybe just a summary of the policy)
 * <li> status of the device (which part of the policy does it act upon?)
 *      examples: low-freq, high-freq, low-price, high-price, schedule
 * </ul>
 * 
 * Logging: 
 * <ul>
 * <li> could be done e.g. with CSV 
 * <li> Problem: how to account for different lifetimes of devices?
 * <li> Solution 1: use prefix for each cell in the csv: deviceId-field:value
 * <li> Solution 2: use one line per update message from a device; one column is a device-id, other columns are the other values
 *      (maybe we need the field:value system here as well 
 * <li> This way, the data can be postprocessed in a relatively 
 *      straightforward manner.
 * <li> Global state: one special client sends global state (frequency, ???)
 * <li> Important: time synchronisation
 * </ul>
 * 
 * Data format of log:
 * <ul>
 * <li> Device sends its deviceId, a timestamp, and a Properties instance
 * </ul>
 * 
 * How to represent policies in simple format?
 * 
 * @author daku
 */
public interface CollectorInterface extends Remote {

  /** Name the collector interface is registered as on its server */
  static final String NAME = CollectorInterface.class.getName();
  
  /** port the RMI registry will use */
  static final int PORT = java.rmi.registry.Registry.REGISTRY_PORT;
  
  /** Get the list of managed devices */
  ArrayList<ServerDeviceInfo> getDevices() throws RemoteException;

  /** Add a new device to the list */
  void addDevice(ServerDeviceInfo deviceInfo) throws RemoteException;

  /** Remove a device from the list */
  void removeDevice(ServerDeviceInfo deviceInfo) throws RemoteException;
  
}
