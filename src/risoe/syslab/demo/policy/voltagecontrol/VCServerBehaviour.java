package risoe.syslab.demo.policy.voltagecontrol;

import java.net.InetAddress;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.core.ServerBehaviour;
import risoe.syslab.control.policy.core.ServerClientBehaviour;
import risoe.syslab.control.policy.message.parser.Expression;

public class VCServerBehaviour implements ServerBehaviour {

  //private PolicyServer policyServer;

  public VCServerBehaviour(CommunicationSystem commSystem) {
    AbstractAddress serverAddress = commSystem.createAddress("server");
    PolicyUtils.createServerAgent(serverAddress, this);
  }

  @Override
  public DeviceInfo parseDeviceInfo(Expression expression) {
    return VCDeviceInfo.parse(expression);
  }

  @Override
  public Policy parsePolicy(Expression expression) {
    return VCPolicy.parse(expression);
  }

  @Override
  public void setPolicyServer(PolicyServer policyServer) {
    //this.policyServer = policyServer;
  }

  @Override
  public ServerClientBehaviour createServerBehaviour(AbstractAddress clientAddress, DeviceInfo deviceInfo) {
    return new VCServerClientBehaviour(clientAddress, deviceInfo);
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    try {
      String serverName = InetAddress.getLocalHost().getHostName();

      PolicyUtils.setup("conf/policy-framework.properties");
      CommunicationSystem commSystem = new HttpCommunicationSystem("", serverName);
      PolicyUtils.setCommunicationSystem(commSystem);

      new VCServerBehaviour(commSystem);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

}
