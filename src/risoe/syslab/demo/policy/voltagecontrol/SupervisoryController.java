package risoe.syslab.demo.policy.voltagecontrol;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import risoe.syslab.util.NanoHTTPD;

public class SupervisoryController {
  
  public SupervisoryController() {
    super();
    try {
      new HttpServer(Config.getFreeHttpPort());
    }
    catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public class HttpServer extends NanoHTTPD {

    public HttpServer(int port) throws IOException {
      super(port);
    }

    @Override
    public Response serve(String uri, String method, Properties header, Properties parms) {
      return new Response("404", "text/html", "<html><title>Not Found</title><body>"+uri+"</body></html>");
    }

    @Override
    public Response serveFile(String uri, Properties header, File homeDir, boolean allowDirectoryListing) {
      return new Response("404", "text/html", "<html><title>Not Found</title><body>"+uri+"</body></html>");
    }
    
  }
  
  public static void main(String[] args) {
    new SupervisoryController();
    while (true) {
      try {
        Thread.sleep(3600*1000);
      }
      catch (InterruptedException e) {
        break;
      }
    }
  }
}
