package risoe.syslab.demo.policy.voltagecontrol;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.ServerClientBehaviour;

public class VCServerClientBehaviour implements ServerClientBehaviour {

  //private AbstractAddress address;
  private DeviceInfo deviceInfo;
  private VCPolicy policy;

  public VCServerClientBehaviour(AbstractAddress clientAddress, DeviceInfo deviceInfo) {
    //this.address = clientAddress;
    this.deviceInfo = deviceInfo;
    // TODO Implement
  }

  @Override
  public Policy createPolicy() {
    // TODO Implement
    throw new IllegalStateException("Not yet implemented");
  }

  @Override
  public DeviceInfo getClientDeviceInfo() {
    return deviceInfo;
  }

  @Override
  public void setClientDeviceInfo(DeviceInfo deviceInfo) {
    this.deviceInfo = deviceInfo;
  }

  @Override
  public void activatePolicy(Policy policy) {
    this.policy = (VCPolicy) policy;
  }

  @Override
  public Policy getActivePolicy() {
    return policy;
  }

  @Override
  public void rejectPolicy(Policy policy) {
    // TODO Implement
    throw new IllegalStateException("Not yet implemented");
  }

}
