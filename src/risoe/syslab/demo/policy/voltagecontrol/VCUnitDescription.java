package risoe.syslab.demo.policy.voltagecontrol;

import java.util.HashMap;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.StringConstant;
import risoe.syslab.demo.policy.voltagecontrol.Config.House;
import risoe.syslab.labs.syslab.SyslabComponent;

public class VCUnitDescription {

  private SyslabComponent comp;
  private House house;

  public VCUnitDescription(SyslabComponent comp) {
    this.comp = comp;
  }

  public VCUnitDescription(House house) {
    this.house = house;
  }

  public String toFipaContent() {
    String type = comp != null ? comp.getClass().getName() : house.getClass().getName();
    String value = comp != null ? ""+comp : ""+house;
    return "(:vcunitdescription \"" + type + "\" \"" + value + "\")";
  }

  public static VCUnitDescription parse(Expression expression) {
    String pattern = "(:vcunitdescription ?type ?value )";
    HashMap<String, Expression> match = new MessageParser().match(pattern, expression);
    if (match == null)
      return null;
    String type = ((StringConstant) match.get("type")).getValue();
    String value = ((StringConstant) match.get("value")).getValue();
    if (type.equals(House.class.getName())) {
      return new VCUnitDescription(House.valueOf(value));
    } else if (type.equals(SyslabComponent.class.getName())) {
      return new VCUnitDescription(SyslabComponent.valueOf(value));
    } else {
      throw new IllegalArgumentException("no such type: " + type);
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((comp == null) ? 0 : comp.hashCode());
    result = prime * result + ((house == null) ? 0 : house.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    VCUnitDescription other = (VCUnitDescription) obj;
    if (comp != other.comp)
      return false;
    if (house != other.house)
      return false;
    return true;
  }

}
