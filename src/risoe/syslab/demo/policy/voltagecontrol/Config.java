package risoe.syslab.demo.policy.voltagecontrol;

import java.util.ArrayList;
import java.util.HashMap;
import risoe.syslab.labs.syslab.SyslabComponent;

public class Config {
  
  private static int httpPort = 9000;

  public enum House {
    H117("117", SyslabComponent.VRBBattery),
    H319("319", SyslabComponent.Dumpload),
    H715("715", SyslabComponent.Flexhouse);
    
    public final String location;
    public final SyslabComponent comp;

    private House(String location, SyslabComponent comp) {
      this.location = location;
      this.comp = comp;
    }
  };
  
  public static class VoltageMeasurement {
    
    public final SyslabComponent comp;
    public final String bay;

    public VoltageMeasurement(SyslabComponent comp, String bay) {
      super();
      this.comp = comp;
      this.bay = bay;
    }
  };
  
  public static final HashMap<String,ArrayList<VCUnitDescription>> units = new HashMap<String, ArrayList<VCUnitDescription>>();
  static {
    units.put("117", new ArrayList<VCUnitDescription>());
    units.put("319", new ArrayList<VCUnitDescription>());
    units.put("715", new ArrayList<VCUnitDescription>());

    units.get("117").add(new VCUnitDescription(SyslabComponent.PV117));
    units.get("117").add(new VCUnitDescription(House.H117));

    units.get("319").add(new VCUnitDescription(SyslabComponent.PV319));
    units.get("319").add(new VCUnitDescription(House.H319));
    
    units.get("715").add(new VCUnitDescription(SyslabComponent.PV715));
    units.get("715").add(new VCUnitDescription(House.H715));
  }
  
  public static final HashMap<String,VoltageMeasurement> vm = new HashMap<String, VoltageMeasurement>();
  static {
    vm.put("117", new VoltageMeasurement(SyslabComponent.SwitchBoard117_2, "Solar"));
    vm.put("319", new VoltageMeasurement(SyslabComponent.SwitchBoard319_2, "Solar"));
    vm.put("715", new VoltageMeasurement(SyslabComponent.SwitchBoard715_2, "Solar"));
  }
  
  public static synchronized int getFreeHttpPort() {
    return httpPort++;
  }
}
