package risoe.syslab.demo.policy.voltagecontrol;

import java.util.HashMap;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;

public class VCPolicy implements Policy {

  private TimeFrame timeFrame;
  private VCService service;

  public VCPolicy(TimeFrame timeFrame, VCService service) {
    super();
    this.timeFrame = timeFrame;
    this.service = service;
  }

  public static VCPolicy parse(Expression expression) {
    String pattern = "(:vcpolicy ?tf ?svc )";
    HashMap<String, Expression> match = new MessageParser().match(pattern, expression);
    if (match == null)
      return null;
    TimeFrame timeFrame = TimeFrame.parse(match.get("tf"));
    VCService service = VCService.parse(match.get("svc"));
    return new VCPolicy(timeFrame, service);
  }

  @Override
  public String toFipaContent() {
    return "(:vcpolicy " + timeFrame.toFipaContent() +
        " " + service.toFipaContent() + ")";
  }

  @Override
  public TimeFrame getTimeFrame() {
    return timeFrame;
  }

  public VCService getService() {
    return service;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((service == null) ? 0 : service.hashCode());
    result = prime * result + ((timeFrame == null) ? 0 : timeFrame.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    VCPolicy other = (VCPolicy) obj;
    if (service == null) {
      if (other.service != null)
        return false;
    }
    else if (!service.equals(other.service))
      return false;
    if (timeFrame == null) {
      if (other.timeFrame != null)
        return false;
    }
    else if (!timeFrame.equals(other.timeFrame))
      return false;
    return true;
  }

}
