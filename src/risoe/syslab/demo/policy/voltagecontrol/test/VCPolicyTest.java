package risoe.syslab.demo.policy.voltagecontrol.test;

import java.net.URL;
import java.util.Date;
import junit.framework.TestCase;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.demo.policy.voltagecontrol.VCPolicy;
import risoe.syslab.demo.policy.voltagecontrol.VCService;

public class VCPolicyTest extends TestCase {

  public void testParsing() throws Exception {
    URL url = new URL("http://vea-e02.risoe.dk/lalala");
    VCService s = new VCService(url);
    VCPolicy p = new VCPolicy(TimeFrame.createTimeFrame(new Date(), 1, 2, 3), s);
    VCPolicy t = VCPolicy.parse(new MessageParser().parse(p.toFipaContent()));
    assertEquals(p, t);
  }
  
}
