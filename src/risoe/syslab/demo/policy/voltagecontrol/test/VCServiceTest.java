package risoe.syslab.demo.policy.voltagecontrol.test;

import java.net.URL;
import junit.framework.TestCase;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.demo.policy.voltagecontrol.VCService;

public class VCServiceTest extends TestCase {

  public void testParsing() throws Exception {
    URL url = new URL("http://vea-e02.risoe.dk/lalala");
    VCService s = new VCService(url);
    VCService p = VCService.parse(new MessageParser().parse(s.toFipaContent()));
    assertEquals(s, p);
  }
  
}
