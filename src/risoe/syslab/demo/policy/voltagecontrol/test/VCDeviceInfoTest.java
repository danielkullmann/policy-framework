package risoe.syslab.demo.policy.voltagecontrol.test;

import junit.framework.TestCase;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.demo.policy.voltagecontrol.Config;
import risoe.syslab.demo.policy.voltagecontrol.VCDeviceInfo;
import risoe.syslab.demo.policy.voltagecontrol.VCUnitDescription;

public class VCDeviceInfoTest extends TestCase {

  public void testParsing() throws Exception {
    String location = "117";
    VCDeviceInfo d = new VCDeviceInfo(location, Config.units.get(location));
    VCDeviceInfo t = VCDeviceInfo.parse(new MessageParser().parse(d.toFipaContent()));
    assertEquals(d, t);
  }
  
  public void testUnitParsing() {
    VCUnitDescription u = Config.units.get("117").get(0);
    VCUnitDescription t = VCUnitDescription.parse(new MessageParser().parse(u.toFipaContent()));
    assertEquals(u, t);
  }
  
}
