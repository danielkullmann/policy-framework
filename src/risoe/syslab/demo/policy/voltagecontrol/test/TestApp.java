package risoe.syslab.demo.policy.voltagecontrol.test;

import java.net.InetAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.demo.policy.voltagecontrol.LocalController;
import risoe.syslab.demo.policy.voltagecontrol.VCServerBehaviour;

public class TestApp {

  /**
   * @param args
   */
  public static void main(String[] args) {

    try {
      String serverName = InetAddress.getLocalHost().getHostName();

      PolicyUtils.setup("conf/policy-framework.properties");
      CommunicationSystem commSystem = new HttpCommunicationSystem("", serverName);
      PolicyUtils.setCommunicationSystem(commSystem);

      new VCServerBehaviour(commSystem);
      new LocalController(serverName, "117", commSystem);
      new LocalController(serverName, "319", commSystem);
      new LocalController(serverName, "715", commSystem);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

}
