package risoe.syslab.demo.policy.voltagecontrol;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.ClientBehaviour;
import risoe.syslab.control.policy.core.ClientListener;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.message.parser.Expression;

public class VCClientBehaviour implements ClientBehaviour {

  private VCPolicy policy;
  private VCDeviceInfo deviceInfo;
  private LocalController lc;
  //private AbstractAddress clientAddress;

  public VCClientBehaviour(LocalController lc, AbstractAddress clientAddress) {
    super();
    this.lc = lc;
    //this.clientAddress = clientAddress;
  }

  @Override
  public DeviceInfo parseDeviceInfo(Expression expression) {
    return VCDeviceInfo.parse(expression);
  }

  @Override
  public Policy parsePolicy(Expression expression) {
    return VCPolicy.parse(expression);
  }

  @Override
  public DeviceInfo provideDeviceInfo() {
    if (deviceInfo == null) {
      deviceInfo = new VCDeviceInfo(lc.getLocation(), Config.units.get(lc.getLocation()));
    }
    return deviceInfo;
  }

  @Override
  public boolean acceptPolicy(Policy policy) {
    // TODO Implement
    throw new IllegalStateException("Not yet implemented");
  }

  @Override
  public void activatePolicy(Policy policy) {
    this.policy = (VCPolicy) policy;
  }

  @Override
  public Policy getActivePolicy() {
    return policy;
  }

  @Override
  public boolean shouldUnregister() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public void registerClientListener(ClientListener listener) {
    // TODO Implement; not needed?
    throw new IllegalStateException("Not yet implemented");
  }

  @Override
  public void unregisterClientListener(ClientListener listener) {
    // TODO Implement; not needed?
    throw new IllegalStateException("Not yet implemented");
  }

  @Override
  public void stop() {
    lc.stop();
  }

  public void setDeviceInfo(VCDeviceInfo deviceInfo) {
    this.deviceInfo = deviceInfo;
  }

}
