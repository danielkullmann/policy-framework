package risoe.syslab.demo.policy.voltagecontrol;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;

public class LocalController {

  private String location;

  public LocalController(String server, String location, CommunicationSystem commSystem) {
    this.location = location;
    PolicyUtils.setup("conf/policy-framework.properties");

    AbstractAddress clientAddress = commSystem.createAddress(location);
    PolicyUtils.createClientAgent(clientAddress, new VCClientBehaviour(this, clientAddress),
        PolicyUtils.createAddress("server", "", server));
    
    // TODO Implement: Connect to local units
  }

  public void stop() {
    // TODO Implement; not needed?
    throw new IllegalStateException("Not yet implemented");
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    if (args.length!=2) {
      System.out.println("Usage: ./bin/runClass.sh " + LocalController.class.getName() + " servername location");
      System.exit(1);
    }
    String server = args[0];
    String location = args[1];

    CommunicationSystem commSystem = new HttpCommunicationSystem("");
    PolicyUtils.setCommunicationSystem(commSystem);

    new LocalController(server, location, commSystem);
  }

  public String getLocation() {
    return location;
  }

}
