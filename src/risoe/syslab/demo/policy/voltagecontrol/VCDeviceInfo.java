package risoe.syslab.demo.policy.voltagecontrol;

import java.util.ArrayList;
import java.util.HashMap;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.StringConstant;

public class VCDeviceInfo implements DeviceInfo {

  private String location;
  private ArrayList<VCUnitDescription> units;
  
  
  public VCDeviceInfo(String location, ArrayList<VCUnitDescription> units) {
    super();
    this.location = location;
    this.units = units;
  }

  @Override
  public String toFipaContent() {
    String result = "(:vcdeviceinfo \"" + location + "\" (";
    for (VCUnitDescription unit : units) {
      result += unit.toFipaContent() + " ";
    }
    result += "))";
    return result;
  }

  public static VCDeviceInfo parse(Expression expression) {
    String pattern = "(:vcdeviceinfo ?location ?units )";
    HashMap<String, Expression> match = new MessageParser().match(pattern, expression);
    if (match == null)
      return null;
    String location = ((StringConstant) match.get("location")).getValue();
    ArrayList<VCUnitDescription>units = new ArrayList<VCUnitDescription>();
    ExpressionList list = (ExpressionList) match.get("units");
    while (list != null) {
      units.add(VCUnitDescription.parse(list.getHead()));
      list = list.getTail();
    }
    return new VCDeviceInfo(location, units);
  }

  public String getLocation() {
    return location;
  }

  public ArrayList<VCUnitDescription> getUnits() {
    return units;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((location == null) ? 0 : location.hashCode());
    result = prime * result + ((units == null) ? 0 : units.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    VCDeviceInfo other = (VCDeviceInfo) obj;
    if (location == null) {
      if (other.location != null)
        return false;
    }
    else if (!location.equals(other.location))
      return false;
    if (units == null) {
      if (other.units != null)
        return false;
    }
    else if (!units.equals(other.units))
      return false;
    return true;
  }

}
