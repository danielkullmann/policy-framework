package risoe.syslab.demo.policy.voltagecontrol;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.StringConstant;

public class VCService {

  public static final String SERVICE_NAME = "voltage-control:supervised:0";

  private URL serverAddress;

  public VCService(URL serverAddress) {
    super();
    this.serverAddress = serverAddress;
  }

  public static VCService parse(Expression expression) {
    String pattern = "(:vcservice ?url )";
    HashMap<String, Expression> match = new MessageParser().match(pattern, expression);
    if (match == null)
      return null;
    URL url;
    try {
      url = new URL(((StringConstant) match.get("url")).getValue());
    }
    catch (MalformedURLException e) {
      throw new IllegalArgumentException(e);
    }
    return new VCService(url);
  }

  public String toFipaContent() {
    return "(:vcservice \"" + serverAddress.toString() + "\")";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((serverAddress == null) ? 0 : serverAddress.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    VCService other = (VCService) obj;
    if (serverAddress == null) {
      if (other.serverAddress != null)
        return false;
    }
    else if (!serverAddress.equals(other.serverAddress))
      return false;
    return true;
  }

}
