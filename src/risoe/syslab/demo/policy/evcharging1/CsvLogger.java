package risoe.syslab.demo.policy.evcharging1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Logs client data to a csv file.
 * 
 * Singleton class.
 * 
 * @author daku
 */
public class CsvLogger {

  private static Logger logger = Logger.getLogger(  CsvLogger.class.getName() );
  
  private static CsvLogger instance = null;

  public static final DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
  private static final DateFormat fdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
  
  // setup DateFormat to use GMT as timezone
  static {
    TimeZone tz = TimeZone.getTimeZone( "GMT" );
    df.setTimeZone( tz );
    fdf.setTimeZone( tz );
  }

  private BufferedWriter bos;
  
  /** Constructor. Since this is a singleton class,
   * the constructor is private.
   */
  private CsvLogger() {
    super();
    String fileName = "./simulations/evc1-" + fdf.format( new Date() ) + ".csv";
    logger.log( Level.INFO, "csv log is written into file \"" + fileName + "\"" );
    try {
      bos = new BufferedWriter( new FileWriter( fileName ) );
    } catch ( IOException e ) {
      throw new IllegalArgumentException( e );
    }
  }

  public synchronized void log( String tag, Object[] data ) {
    
    StringBuilder str = new StringBuilder();
    str.append( tag );
    for ( int i = 0; i < data.length; i++ ) {
      str.append(";");
      str.append( data[i] );
    }
    str.append( "\n" );

    try {
      bos.write( str.toString() );
      bos.flush();
    } catch ( IOException e ) {
      throw new IllegalStateException(e);
    }
  }

  public static CsvLogger getInstance() {
    if (instance == null) {
      instance = new CsvLogger();
    }
    return instance;
  }

  
}
