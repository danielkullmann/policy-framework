/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

/**
 * Simple container object for a solution of
 * the MIP problem. 
 * This class has no setter methods; this is intentional!
 */
public final class Solution {

  /** Quarterhour this solution refers to */
  private final QuarterHour quarterHour;
  
  /** Whether the EV should be charging in that quarter hour */
  private final boolean charging;
  
  
  /**
   * Normal constructor, giving values for all the fields
   * 
   * @param quarterHour
   * @param charging
   */
  public Solution( QuarterHour quarterHour, boolean charging ) {
    super();
    this.quarterHour = quarterHour;
    this.charging = charging;
  }
  
  
  /**
   * Simple getter for the quarterhour field
   * @return value of quarterhour field
   */
  public QuarterHour getQuarterHour() {
    return quarterHour;
  }
  
  
  /**
   * Simple getter for the charging field
   * @return value of charging field
   */
  public boolean isCharging() {
    return charging;
  }
  
}
