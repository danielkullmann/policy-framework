/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Date;
import java.util.Random;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * 
 */
@SuppressWarnings( "serial" )
public final class ChargingDisplay extends JComponent {

  Date startDate;
  boolean[] chargingData;

  public ChargingDisplay() {
    super();
    setMinimumSize( new Dimension( 4 * 24, 10 ) );
    setPreferredSize( new Dimension( 4 * 24, 10 ) );
  }

  @Override
  public void paint( Graphics g ) {
    super.paint( g );
    
    if ( chargingData == null ) return;
    
    int numItems = chargingData.length;
    // TODO respect startDate

    int itemWidth = 2;
//    int itemWidth = getWidth() / numItems;
//    assert (itemWidth > 0);

    int startX = (getWidth() - numItems * itemWidth) / 2;

    for ( int i = 0; i < chargingData.length; i++ ) {
      if ( chargingData[i] ) {
        g.setColor( Color.BLUE );
      } else {
        g.setColor( Color.YELLOW.brighter() );
      }

      g.fillRect( startX + i * itemWidth, 0, itemWidth, getHeight() );

      // Full hour?
      if ( (i % 4) == 0 ) {
        g.setColor( Color.BLACK );
        g.drawLine( startX + i * itemWidth, 0, startX + i * itemWidth, getHeight() );
      }

    }
  }

  /**
   * Sets the data to be displayed..
   * 
   * @param startDate
   * @param chargingData
   */
  public void setData( Date startDate, boolean[] chargingData ) {
    this.startDate = startDate;
    this.chargingData = chargingData;
    // Supports not more than one day of charging in advance
    assert (chargingData.length <= 24 * 15);

    repaint();
  }

  public static void main( String args[] ) {
    JFrame outer = new JFrame( "test" );
    ChargingDisplay cd = new ChargingDisplay();
    outer.add( cd );
    outer.getContentPane().validate();
    outer.pack();
    outer.setVisible( true );
    outer.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );

    while ( true ) {
      Random r = new Random();
      boolean[] chargingData = new boolean[10 + r.nextInt( 24 * 4 - 10 )];
      for ( int i = 0; i < chargingData.length; i++ ) {
        chargingData[i] = r.nextBoolean();
      }
      cd.setData( new Date(), chargingData );
      
      try {
        Thread.sleep( 3000 );
      } catch ( InterruptedException e ) {
        // can be ignored
      }
    }


  }

}
