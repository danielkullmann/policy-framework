/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.ClientListener;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.demo.policy.evcharging1.EVCharging1DeviceInfo;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Policy;

/**
 * 
 */
@SuppressWarnings( "serial" )
public final class DeviceDisplay extends JPanel implements ClientListener {

  private JLabel name;
  private JLabel nextUse;
  private JLabel chargingNeeded;
  private JLabel chargingQuarters;
  private ChargingDisplay chargingDisplay;
  private double quartersNeeded = 0.0;
  
  public DeviceDisplay() throws HeadlessException {
    super();
    createGui();
  }
  
  
  @Override
  public void activePolicyChanged( AbstractAddress address, Policy policy ) {
    if ( policy instanceof EVCharging1Policy ) {
      boolean[] charging = ((EVCharging1Policy) policy).getCharging();
      chargingDisplay.setData( policy.getTimeFrame().getFrom(), charging );
      int numQC = 0;
      for ( int i = 0; i < charging.length; i++ ) {
        if ( charging[i] ) numQC++;
      }
      chargingQuarters.setText( numQC + " of " + charging.length );
      if ( numQC != quartersNeeded ) {
        chargingQuarters.setForeground( Color.red.darker() );
      } else {
        chargingQuarters.setForeground( Color.black );
      }
      repaint();
    }
  }



  @Override
  public void deviceInfoChanged( AbstractAddress address, DeviceInfo info ) {
    EVCharging1DeviceInfo deviceInfo = (EVCharging1DeviceInfo) info;
    name.setText( "" + deviceInfo.getId() );
    DateFormat df = new SimpleDateFormat( "MM-dd HH:mm" );
    nextUse.setText( df.format( deviceInfo.getNextUse() ) );
    quartersNeeded  = Math.ceil( 4*deviceInfo.calcEnergyNeeded() );
    chargingNeeded.setText( String.format( "%.3f (%.0f)", deviceInfo.calcEnergyNeeded(), quartersNeeded ) );
    repaint();
  }


  private void createGui() {
    setMinimumSize( new Dimension( 200, 100 ) );
    setPreferredSize( new Dimension( 200, 150 ) );
    
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    setOpaque(true); // content panes must be opaque
    setBorder(new BevelBorder(5));

    JPanel namePanel = new JPanel();
    namePanel.setLayout( new BoxLayout( namePanel, BoxLayout.X_AXIS ) );
    JLabel nameLabel = new JLabel( "Name " );
    name = new JLabel( "<unknown>" );
    namePanel.add( nameLabel );
    namePanel.add( name );

    JPanel nextUsePanel = new JPanel();
    nextUsePanel.setLayout( new BoxLayout( nextUsePanel, BoxLayout.X_AXIS ) );
    JLabel nextUseLabel = new JLabel( "Next " );
    nextUse = new JLabel( "<unknown>" );
    nextUsePanel.add( nextUseLabel );
    nextUsePanel.add( nextUse );

    JPanel chargingNeededPanel = new JPanel();
    BoxLayout layout = new BoxLayout( chargingNeededPanel, BoxLayout.X_AXIS );
    chargingNeededPanel.setLayout( layout );
    JLabel chargingNeededLabel = new JLabel( "Energy " );
    chargingNeeded = new JLabel( "<unknown>" );
    chargingNeededPanel.add( chargingNeededLabel );
    chargingNeededPanel.add( chargingNeeded );

    JPanel chargingQuartersPanel = new JPanel();
    chargingQuartersPanel.setLayout( new BoxLayout( chargingQuartersPanel, BoxLayout.X_AXIS ) );
    JLabel chargingQuartersLabel = new JLabel( "Quarters " );
    chargingQuarters = new JLabel( "<unknown>" );
    chargingQuartersPanel.add( chargingQuartersLabel );
    chargingQuartersPanel.add( chargingQuarters );

    chargingDisplay = new ChargingDisplay();
    
    add( namePanel );
    add( nextUsePanel );
    add( chargingNeededPanel );
    add( chargingQuartersPanel );
    add( chargingDisplay );
       
    
  }

  

  public static void main( String args[] ) {
    
    // Setting up GUI...
    JFrame outer = new JFrame();
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    panel.setOpaque(true); // content panes must be opaque
    panel.setBorder(new BevelBorder(5));
    outer.setContentPane( panel );

    DeviceDisplay display = new DeviceDisplay();
    outer.add( display );

    outer.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
    outer.getContentPane().validate();
    outer.pack();
    outer.setVisible( true );
    
    // Setting up data to be displayed...
    EVCharging1DeviceInfo info = new EVCharging1DeviceInfo( 1, 10, 30, 9, new Date() );
    
    display.deviceInfoChanged( null, info );
    
    while ( true ) {
      Random r = new Random();
      boolean[] chargingData = new boolean[10 + r.nextInt( 24 * 4 - 10 )];
      for ( int i = 0; i < chargingData.length; i++ ) {
        chargingData[i] = r.nextBoolean();
      }
      EVCharging1Policy policy = new EVCharging1Policy( 
          TimeFrame.createTimeFrame( new Date(), 0, 1, 0 ), chargingData );
      display.activePolicyChanged( null, policy );
      
      try {
        Thread.sleep( 3000 );
      } catch ( InterruptedException e ) {
        // can be ignored
      }
    }

  }


}
