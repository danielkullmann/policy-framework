/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.ServerBehaviour;
import risoe.syslab.control.policy.message.parser.Expression;

/**
 * Policy implementation for the EV Charging 1 example.
 */
public final class EVCharging1Server implements ServerBehaviour {

  /** map of connected clients */
  private HashMap<AbstractAddress, Client> clients = new HashMap<AbstractAddress, Client>();
  
  /** planner creating the policies */
  private EVCharging1Planner planner;
  
  
  /** Default constructor */
  public EVCharging1Server() {
    super();
    planner = new EVCharging1Planner( this );
  }

  public void start() {
    new Thread( planner ).start();
  }

  

  @Override
  public EVCharging1ServerClient createServerBehaviour( AbstractAddress clientAddress, DeviceInfo deviceInfo ) {
    return new EVCharging1ServerClient( this, clientAddress, deviceInfo );
  }

  
  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return EVCharging1DeviceInfo.parse( expression );
  }

  
  @Override
  public Policy parsePolicy( Expression expression ) {
    return EVCharging1Policy.parse( expression );
  }


  /**
   * Here, the server can register a client to be managed.
   * 
   * @param clientAddress AbstractAddress of the client to be managed
   * @param deviceInfo DeviceInfo of the client to be managed
   */
  public void register( AbstractAddress clientAddress, EVCharging1DeviceInfo deviceInfo ) {
    Client device = clients.get(clientAddress);
    if ( device == null ) {
      device = new Client( clientAddress );
      device.setDeviceInfo( deviceInfo );
      clients.put( clientAddress, device );
    } else {
      device.setDeviceInfo( deviceInfo );
    }
    synchronized (planner) {
      planner.notify();
    }
  }


  /** Activates a policy for the given client
   * @param clientAddress
   * @param policy
   */
  public void activatePolicy( AbstractAddress clientAddress, Policy policy ) {
    Client client = clients.get(clientAddress);
    if ( client == null ) {
      throw new AssertionError( "client unknown: " + clientAddress );
    } 
    client.setActivePolicy( policy );
    
    if ( policy.equals( client.getNextPolicy() ) ) {
      client.setNextPolicy( null );
    }
    
  }


  /** Creates a policy for the given client
   * @param clientAddress
   * @return
   */
  public Policy createPolicy( AbstractAddress clientAddress ) {
    
    Client client = clients.get( clientAddress );
    if ( client == null ) {
      throw new AssertionError( "Client has not registered itself.." );
    }
    
    Date now = new Date();
    
    if ( client.getActivePolicy() != null ) {
      // check validity period of active policy
      if ( client.getActivePolicy().getTimeFrame().isCurrent( now ) )
        return client.getActivePolicy();
      // else..
      client.setActivePolicy( null );
    }
    
    if ( client.getNextPolicy() != null ) {
      // check validity period of next policy
      if ( client.getNextPolicy().getTimeFrame().isCurrent( now ) )
        return client.getNextPolicy();
      // else..
      client.setNextPolicy( null );
    }
    
    // Instead of sending an IdlePolicy, I return null
    // here, and the PolicyServer waits until a policy
    // is available to the client
    return null;
  }


  /** Get the currently active policy of the given client
   * @param clientAddress
   * @return
   */
  public Policy getActivePolicy( AbstractAddress clientAddress ) {
    Client device = clients.get(clientAddress);
    if ( device == null ) {
      return null;
    } 
    
    return device.getActivePolicy();
  }


  /** Get the device info of the given client
   * @param clientAddress
   * @return
   */
  public EVCharging1DeviceInfo getDeviceInfo( AbstractAddress clientAddress ) {
    Client device = clients.get(clientAddress);
    if ( device == null ) {
      return null;
    } 
    
    return device.getDeviceInfo();
    
  }


  /** Return the client with the given AbstractAddress
   * @param clientAddress the AbstractAddress of the wanted client
   */
  public Client getClient( AbstractAddress clientAddress ) {
    return clients.get( clientAddress );
  }


  /** Return the current state of the client list
   * @return the current client list
   */
  public Collection<Client> getClients() {
    return clients.values();
  }


  /** Stops the server, ie.e. stops the planner */
  public void stop() {
    planner.stop();
  }


  /** When the policy was rejected at the client..
   * @param clientAddress
   */
  public void rejectPolicy( AbstractAddress clientAddress, Policy policy) {
    Client client = getClient( clientAddress );
    if ( client.getNextPolicy().equals( policy ) ) {
      client.setNextPolicy( null );
    }
  }


  @Override
  public void setPolicyServer(PolicyServer policyServer) {
    // we don't need that information here..
  }


  public EVCharging1Planner getPlanner() {
    return planner;
  }



}
