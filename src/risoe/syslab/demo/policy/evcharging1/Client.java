/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.Policy;

/**
 * Class that encapuslates all info for a single client
 * 
 * Only visible in this package.
 */
public final class Client {

  private EVCharging1DeviceInfo deviceInfo;
  private EVCharging1Policy nextPolicy;
  private Policy activePolicy;
  private AbstractAddress clientAddress;


  @SuppressWarnings( "unused" )
  private Client() {
    super();
    throw new IllegalAccessError();
  }

  
  public Client( AbstractAddress clientAddress ) {
    super();
    this.clientAddress = clientAddress;
  }


  /** Simple getter
   * @return the active policy
   */
  public Policy getActivePolicy() {
    return activePolicy;
  }

  /** Simple setter
   * @param policy the active policy
   */
  public void setActivePolicy( Policy policy ) {
    this.activePolicy = policy;
  }

  /** Simple getter
   * @return the device info
   */
  public EVCharging1DeviceInfo getDeviceInfo() {
    return deviceInfo;
  }

  /** Simple setter
   * @param deviceInfo the device info
   */
  public void setDeviceInfo( EVCharging1DeviceInfo deviceInfo ) {
    this.deviceInfo = deviceInfo;
  }

  /** Simple getter
   * @return the next policy
   */
  public Policy getNextPolicy() {
    return nextPolicy;
  }

  
  /** Simple setter
   * @param nextPolicy the next policy
   */
  public void setNextPolicy( EVCharging1Policy nextPolicy ) {
    this.nextPolicy = nextPolicy;
  }

  /**
   * @return
   */
  public AbstractAddress getClientAddress() {
    return clientAddress;
  }


} // end of class Device
