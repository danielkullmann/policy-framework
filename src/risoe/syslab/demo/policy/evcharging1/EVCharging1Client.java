/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.AbstractClientBehaviour;
import risoe.syslab.control.policy.core.ClientListener;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.IdlePolicy;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.message.parser.Expression;

/**
 * Client side of EVC1 example behaviour.
 * An instance of this class is created for every 
 * connected client.
 */
public final class EVCharging1Client extends AbstractClientBehaviour {

  /** logger for this class */
  private static Logger logger = Logger.getLogger( EVCharging1Client.class.getName() );
  
  public static final String LOG_TAG = "POLICY";
  
  private static CsvLogger csvLogger = CsvLogger.getInstance();

  /** AbstractAddress of this client */
  private AbstractAddress clientAddress;
  
  /** DeviceInfo of this client */
  private EVCharging1DeviceInfo deviceInfo;
  
  /** aqctive policy of this client */
  private Policy activePolicy;

  
  /** Standard constructor
   * @param clientAddress
   */
  public EVCharging1Client( AbstractAddress clientAddress ) {
    this.clientAddress = clientAddress;
  }


  @Override
  public boolean acceptPolicy( Policy policy ) {
    // This client accepts an EVCharging1Policy and an IdlePolicy
    // (though IdlePolicy is not used right now..)
    return policy.getClass().equals( EVCharging1Policy.class ) ||
           policy instanceof IdlePolicy;
  }


  @Override
  public void activatePolicy( Policy policy ) {
    activePolicy = policy;

    // Tell interested parties..
    for ( ClientListener listener : clientListeners ) {
      listener.activePolicyChanged( clientAddress, policy );
    }
    
    if (policy instanceof EVCharging1Policy) {
      csvLogger.log( LOG_TAG, toData((EVCharging1Policy) policy) );
    }
    logger.log( Level.INFO, "== CLIENT " + clientAddress.getName() + ": policy activated: \n" + policy.toFipaContent() );
  }

  
  private String[] toData( EVCharging1Policy policy ) {
    String[] result = new String[3+policy.getCharging().length];
    result[0] = clientAddress.toString();
    result[1] = CsvLogger.df.format( policy.getTimeFrame().getFrom() );
    result[2] = CsvLogger.df.format( policy.getTimeFrame().getUntil() );
    for ( int i = 0; i < policy.getCharging().length; i++ ) {
      result[3+i] = ""+policy.getCharging()[i];
    }
    return result;
  }


  @Override
  public Policy getActivePolicy() {
    return activePolicy;
  }

  
  @Override
  public EVCharging1DeviceInfo provideDeviceInfo() {
    // Check whether an existing device info is obsolete
    if ( deviceInfo != null && deviceInfo.getNextUse().before( new Date() ) ) {
      deviceInfo = null;
    }

    // Check whether a new device info has to be created
    if ( deviceInfo == null ) {
      // Create random DeviceInfo
      Random r = new Random();
      int id = Integer.parseInt( clientAddress.getName().substring( "client-".length() ) );
      double soc = 30 + r.nextInt( 40 );
      double tsoc = 70 + r.nextInt( 30 );
      if ( tsoc < soc ) tsoc = soc;
      Calendar cal = new GregorianCalendar();
      cal.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
      // Next days power prices should be available at 13:00
      if ( cal.get( Calendar.HOUR_OF_DAY ) >= 14 ) {
        cal.add( Calendar.DAY_OF_YEAR, 1 );
        cal.set( Calendar.HOUR_OF_DAY, 5+r.nextInt(4) );
      } else {
        cal.add( Calendar.HOUR_OF_DAY, 7+r.nextInt(4) );
      }
      cal.set( Calendar.MINUTE, 30*r.nextInt( 2 ) );
      Date nextUse = cal.getTime();
      double timeForCharging = (nextUse.getTime() - new Date().getTime()) / 1000.0 / 3600.0;
      while ( timeForCharging < (tsoc - soc)/100.0 * 9.0 ) {
        tsoc -= 1.0;
      }
      deviceInfo = new EVCharging1DeviceInfo( id, soc, tsoc, 9.0, nextUse );
    }

    // Tell interested parties..
    for ( ClientListener listener : clientListeners ) {
      listener.deviceInfoChanged( clientAddress, deviceInfo );
    }
    
    return deviceInfo;
  }

  
  @Override
  public boolean shouldUnregister() {
    // TODO Is that always so?
    return false;
  }


  @Override
  public void stop() {
    // nothing to be done..
  }


  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return EVCharging1DeviceInfo.parse( expression );
  }


  @Override
  public Policy parsePolicy( Expression expression ) {
    return EVCharging1Policy.parse( expression );
  }

  
}
