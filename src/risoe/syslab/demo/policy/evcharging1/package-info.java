/**
 * This package contains a first implementation of a somewhat realistic policy. 
 * The policy is really simple, it is just a schedule. The server part uses 
 * a n already quite sophisticated system: It uses LP programming to calculate
 * the optimal charging schedule for all connected EVs.
 * 
 * TODO setup is very slow: planner waits too long for things to change
 * TODO Implement pushing policies to clients
 * TODO recalculate policies only when deviceinfos have changed
 * TODO schedules should be logged into a csv file, so they can be analysed later.
 *      this should be done in the clients, to increase usability
 * TODO log power prices as well
 * TODO Compare old and new versions of lp problem solver:
 *      data/evc1-lp-problem.template.txt and data/evc1-lp-problem.template2.txt
 */
package risoe.syslab.demo.policy.evcharging1;