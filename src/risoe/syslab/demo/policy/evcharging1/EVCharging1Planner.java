/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections15.bidimap.TreeBidiMap;
import org.gnu.glpk.GLPK;
import org.gnu.glpk.GLPKConstants;
import org.gnu.glpk.glp_iocp;
import org.gnu.glpk.glp_prob;
import org.gnu.glpk.glp_tran;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.price.nordpool.NordPoolParser;
import risoe.syslab.price.nordpool.PriceForecastData;

/**
 * Planner for EVC1 example. It uses the GLPK library to solve the charging
 * problem as an MIP (mixed-integer-programming) linear programming problem.
 */
public final class EVCharging1Planner implements Runnable {

  /**
   * How long does the planner wait maximally between two planning runs? (in
   * seconds)
   */
  public static int SLEEP_TIME = 15;

  /**
   * Time limit for optimization run (milliseconds)
   */
  private static final int TIME_LIMIT = 60 * 1000;

  /**
   * For debugging: whether to keep the problem definition and solution files
   * after planning. For production purposes, this should be false.
   */
  private static boolean KEEP_FILES = true;

  /**
   * constant used by {@link GLPK#glp_mpl_read_model(glp_tran, String, int)}:
   * skip data section; this should always be zero
   */
  private static final int SKIP = 0;

  /**
   * constant for round-trip battery efficiency; this is used in the GLPK
   * solution to account for losses in storing and returning energy.
   */
  private static final double batteryEfficiency = 0.6;

  public static final String PRICE_LOG_TAG = "PRICE_FORECAST";

  /** Logger for this class */
  private static Logger logger = Logger.getLogger(EVCharging1Planner.class.getName());

  /**
   * maximum number of clients that can be charged at the same time
   */
  private int maxNumClients = 2;

  /**
   * policy implementation used by this planner. The implementation has a list
   * of all connected clients.
   */
  private EVCharging1Server impl;

  /**
   * Set to true by {@link #stop()}, to make the planner thread stop
   */
  private boolean stop = false;

  private int expectedClients = 0;

  /**
   * Set up glpk library
   */
  static {
    // Unfortunately, this code does not work
    // (see http://stackoverflow.com/questions/5419039/):
    // System.setProperty( "java.library.path", new
    // File("jni").getAbsolutePath() );
    try {
      // try to load Linux library
      System.loadLibrary("glpk_java");
    } catch (UnsatisfiedLinkError e) {
      // try to load Windows library
      System.loadLibrary("glpk_4_43");
      System.loadLibrary("glpk_4_43_java");
    }
  }

  /** private default constructor; should NEVER be called! */
  @SuppressWarnings("unused")
  private EVCharging1Planner() {
    throw new AssertionError();
  }

  /**
   * Normal constructor, giving the implementation
   * 
   * @param impl
   *          value for the {@link #impl} field
   */
  public EVCharging1Planner(EVCharging1Server impl) {
    this.impl = impl;
  }

  /**
   * Calls {@link #runOnce()} repeatedly
   */
  @Override
  public void run() {
    while (!stop) {
      try {
        runOnce();
      } catch (IllegalArgumentException e) {
        logger.log(Level.WARNING, "Caught an exception from planner: " + e.getMessage(), e);
        stop = true;
        break;
      } catch (Exception e) {
        logger.log(Level.WARNING, "Caught an exception from planner: " + e.getMessage(), e);
      }

      try {
        synchronized (this) {
          wait(SLEEP_TIME * 1000);
        }
      } catch (InterruptedException ie) {
        Thread.currentThread().interrupt();
        stop = true;
      }
    }
  }

  /**
   * This runs the planner one time, resulting in a set of policies for all
   * connected clients
   */
  public void runOnce() {

    if (expectedClients <= 0) {
      // Sleep before trying to calculate a new policy
      // Do this at the start of the method to give clients
      // time to register themselves..
      try {
        synchronized (this) {
          wait(10 * 1000);
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        stop = true;
        return;
      }
      // Wait until all clients have connected..
    } else {
      while (impl.getClients().size() < expectedClients) {
        try {
          synchronized (this) {
            wait(1 * 1000);
          }
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
          stop = true;
          return;
        }
      }
    }

    // Wait a sec (actually, two) to make it possible for other clients to
    // connect
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e1) {
      /* empty.. */ }

    // Get the list of clients that is valid for now..
    // make it a copy so we don't get additions we are not prepared for..
    Collection<Client> clients = new ArrayList<Client>(impl.getClients());

    // Are any clients registered yet?
    if (clients.size() == 0) {
      // => No, nothing has to be done (yet)
      return;
    }

    logger.log(Level.INFO, "Planning for " + clients.size() + " clients");

    // Template of gmpl problem file; has some placeholders of the
    // form $name$ that are replaced by strings put together in this method.
    File problemTemplate = new File("data/evc1-lp-problem.template.txt");
    String problemDefinition = slurp(problemTemplate);

    // Map for the mapping between quarter hours and their indices
    TreeBidiMap<String, Integer> timeIndices = new TreeBidiMap<String, Integer>();
    // Map for converting between client tags and their AbstractAddress
    TreeBidiMap<AbstractAddress, String> clientTags = new TreeBidiMap<AbstractAddress, String>();

    // Strings to be put into the GMPL problem file
    StringBuilder timeStr = new StringBuilder();
    StringBuilder timeIndicesStr = new StringBuilder();
    StringBuilder clientsStr = new StringBuilder();
    StringBuilder powerPriceStr = new StringBuilder();
    StringBuilder chargeNeededStr = new StringBuilder();
    StringBuilder chargeEndStr = new StringBuilder();

    // start and end date for the optimization process
    QuarterHour startQuarter = new QuarterHour(new Date());
    Date endDate = null;

    // Load power price from http://nordpool.com and put the values into a list
    // to be put into the GMPL file
    NordPoolParser forecastParser = new NordPoolParser();
    PriceForecastData[] forecast = forecastParser.retrieveForecast(startQuarter.previousHour().getDate());

    CsvLogger csvLogger = CsvLogger.getInstance();
    String data[] = new String[forecast.length + 1];
    data[0] = CsvLogger.df.format(forecast[0].getDate());
    for (int i = 0; i < forecast.length; i++) {
      data[i + 1] = "" + forecast[i].getValue();
    }
    csvLogger.log(PRICE_LOG_TAG, data);

    // Go through the list of clients and put several pieces
    // of information into strings that are put into the GMPL file.
    // Those pieces of info are:
    // * list of client tags (used because AbstractAddresses are not in a format
    // that can be used as a symbol in the GMPL file)
    // * amount of energy needed for charging each client
    //
    // Also, the mapping between client AbstractAddress and client tag
    // is created, and the end time for the optimization is
    // calculated
    int clientNo = 1;
    for (Client client : clients) {
      if (client.getDeviceInfo() == null) {
        logger.log(Level.SEVERE, "client " + client.getClientAddress().getName() + " has no device info ");
        continue;
      }

      // Data about client
      EVCharging1DeviceInfo deviceInfo = client.getDeviceInfo();
      AbstractAddress clientAddress = client.getClientAddress();
      String clientTag = "cl_" + clientNo;
      clientNo++;

      // Tag name of client
      clientTags.put(clientAddress, clientTag);

      // List of clients
      clientsStr.append(clientTag);
      clientsStr.append(" ");

      // amount of energy needed for that client
      chargeNeededStr.append(clientTag);
      chargeNeededStr.append(" ");
      // TODO Not an exact solution..
      // Factor 4 is due to the fact that we use quarter hours..
      chargeNeededStr.append(Math.ceil(4 * deviceInfo.calcEnergyNeeded()));
      chargeNeededStr.append("\n");

      // end date and time of optimization run
      if (endDate == null || endDate.before(deviceInfo.getNextUse())) {
        endDate = deviceInfo.getNextUse();
      }

    }

    QuarterHour endQuarter = new QuarterHour(endDate);

    // loop through all quarter hours, giving an index to each
    // quarter hour, so they can be compared in the GMPL problem.
    QuarterHour current = startQuarter;
    int timeIndex = 1;
    while (!current.after(endQuarter)) {
      timeStr.append(current.tag());
      timeStr.append(" ");
      timeIndices.put(current.tag(), timeIndex);
      timeIndicesStr.append(current.tag());
      timeIndicesStr.append(" ");
      timeIndicesStr.append(timeIndex);
      timeIndicesStr.append("\n");
      current = current.nextQuarter();
      timeIndex++;
    }

    logger.log(Level.FINE, "Optimization run from " + startQuarter.tag() + " until " + endQuarter.tag());

    // Loop again through list of client, to get the data
    // when charging of a certain client has to be finished
    for (Client client : clients) {
      if (client.getDeviceInfo() == null) {
        continue;
      }

      // Data about client
      EVCharging1DeviceInfo deviceInfo = client.getDeviceInfo();
      String clientTag = clientTags.get(client.getClientAddress());

      // end of charging for the client
      chargeEndStr.append(clientTag);
      chargeEndStr.append(" ");
      String nextUseTag = new QuarterHour(deviceInfo.getNextUse()).tag();
      Integer timeIndexO = timeIndices.get(nextUseTag);
      if (timeIndexO == null) {
        logger.log(Level.WARNING, "nothing found for " + nextUseTag + " " + deviceInfo.getNextUse());
      }
      chargeEndStr.append(timeIndexO);
      chargeEndStr.append("\n");

    }

    double minPrice = Double.MAX_VALUE;
    double maxPrice = Double.MIN_VALUE;

    HashMap<String, Object> usedTagsForPrice = new HashMap<String, Object>();
    for (int i = 0; i < forecast.length; i++) {
      double value = forecast[i].getValue();
      if (value < minPrice)
        minPrice = value;
      if (value > maxPrice)
        maxPrice = value;
      QuarterHour quarterHour = new QuarterHour(forecast[i].getDate());
      String tag1 = quarterHour.tag();
      quarterHour = quarterHour.nextQuarter();
      String tag2 = quarterHour.tag();
      quarterHour = quarterHour.nextQuarter();
      String tag3 = quarterHour.tag();
      quarterHour = quarterHour.nextQuarter();
      String tag4 = quarterHour.tag();

      if (timeIndices.containsKey(tag1)) {
        powerPriceStr.append(tag1);
        powerPriceStr.append(" ");
        powerPriceStr.append(value);
        powerPriceStr.append("\n");
        usedTagsForPrice.put(tag1, null);
      }

      if (timeIndices.containsKey(tag2)) {
        powerPriceStr.append(tag2);
        powerPriceStr.append(" ");
        powerPriceStr.append(value);
        powerPriceStr.append("\n");
        usedTagsForPrice.put(tag2, null);
      }

      if (timeIndices.containsKey(tag3)) {
        powerPriceStr.append(tag3);
        powerPriceStr.append(" ");
        powerPriceStr.append(value);
        powerPriceStr.append("\n");
        usedTagsForPrice.put(tag3, null);
      }

      if (timeIndices.containsKey(tag4)) {
        powerPriceStr.append(tag4);
        powerPriceStr.append(" ");
        powerPriceStr.append(value);
        powerPriceStr.append("\n");
        usedTagsForPrice.put(tag4, null);
      }
    }

    // Check price forecast for completeness
    for (String tag : timeIndices.keySet()) {
      if (!usedTagsForPrice.containsKey(tag)) {
        throw new AssertionError("ERROR: not found: " + tag);
      }
    }

    // Replace placeholders in template with the real data
    problemDefinition = problemDefinition.replace("$time$", timeStr);
    problemDefinition = problemDefinition.replace("$clients$", clientsStr);
    problemDefinition = problemDefinition.replace("$time_index$", timeIndicesStr);
    problemDefinition = problemDefinition.replace("$power_price$", powerPriceStr);
    problemDefinition = problemDefinition.replace("$charge_needed$", chargeNeededStr);
    problemDefinition = problemDefinition.replace("$charge_end$", chargeEndStr);
    problemDefinition = problemDefinition.replace("$num_clients$", "" + clients.size());
    problemDefinition = problemDefinition.replace("$num_timesteps$", "" + timeIndices.size());
    problemDefinition = problemDefinition.replace("$max_num_clients$", "" + getMaxNumClients());
    problemDefinition = problemDefinition.replace("$battery_efficiency$", "" + batteryEfficiency);
    problemDefinition = problemDefinition.replace("$weight$", "" + 1); // TODO: Real weight
    double scaledPriceFactor = 1 / (maxPrice - minPrice);
    double scaledPriceSummand = -minPrice / (maxPrice - minPrice);
    problemDefinition = problemDefinition.replace("$scaled_price_factor$", "" + scaledPriceFactor);
    problemDefinition = problemDefinition.replace("$scaled_price_summand$", "" + scaledPriceSummand);

    HashMap<AbstractAddress, ArrayList<Solution>> result = null;
    Date now = new Date();
    int randomNo = new Random().nextInt(1000000);
    File solutionFile;
    // Create the GMPL problem file, and run the GLPK optimizer on that problem
    try {
      File tempFile = new File(new File("data"), "test-evc1-p-" + now.getTime() + "-" + randomNo + ".mod");
      solutionFile = new File(new File("data"), "test-evc1-s-" + now.getTime() + "-" + randomNo + ".txt");
      BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(tempFile));
      PrintWriter writer = new PrintWriter(stream);
      writer.append(problemDefinition.toString());
      writer.flush();
      writer.close();
      result = plan(tempFile, solutionFile, clientTags, clients);
      logger.log(Level.FINE, "planned problem from " + tempFile.getName());
      if (!KEEP_FILES)
        tempFile.delete();
    } catch (IOException e) {
      throw new AssertionError("Could not create temporary model file");
    }

    // calculate policy for all clients
    QuarterHour qhNow = new QuarterHour(now);
    logger.log(Level.WARNING, "now:" + now);
    for (AbstractAddress clientAddress : result.keySet()) {
      ArrayList<Solution> solutions = result.get(clientAddress);
      EVCharging1DeviceInfo deviceInfo = impl.getDeviceInfo(clientAddress);
      TimeFrame timeFrame = new TimeFrame(now, deviceInfo.getNextUse());
      int duration = (int) ((timeFrame.getDuration() + 0.25) * 4) + 1;
      boolean[] charging = new boolean[duration];
      for (int i = 0; i < duration; i++)
        charging[i] = false;
      for (Solution solution : solutions) {
        if (solution.isCharging()) {
          // TODO check if the right boolean flag is set here
          QuarterHour qh = qhNow;
          int index = 0;
          while (!qh.equals(solution.getQuarterHour())) {
            qh = qh.nextQuarter();
            index++;
            // Security check: does the solution include a time step that
            // is not allowed for this client?
            if (qh.after(new QuarterHour(deviceInfo.getNextUse()))) {
              // Yes it does...
              logger.log(Level.SEVERE, "solution for " + clientAddress.getName()
                  + " contains a time step that is too late: " + qh.getDate() + " vs. " + deviceInfo.getNextUse());
              qh = null;
              break;
            }
          }
          // if ( qh == null ) {
          // throw new AssertionError();
          // }
          if (index >= charging.length) {
            String msg = "== ERROR for " + clientAddress.getName() + " ";
            msg = msg + duration + " <= " + index + " " + timeFrame.getDuration() + " ";
            msg = msg + qhNow.tag() + " " + new QuarterHour(deviceInfo.getNextUse()).tag() + " ";
            msg = msg + timeFrame.getFrom() + " => " + timeFrame.getUntil() + " ";
            logger.log(Level.SEVERE, msg);
            break;
          }
          charging[index] = true;
        }
      }
      EVCharging1Policy policy = new EVCharging1Policy(timeFrame, charging);
      impl.getClient(clientAddress).setNextPolicy(policy);
      logger.log(Level.INFO, "next policy set for " + clientAddress.getName() + " " + policy.getTimeFrame().getFrom()
          + ": \n" + policy.toFipaContent());

      if (!KEEP_FILES)
        solutionFile.delete();

    }

  }

  /**
   * Does the actual planning using GLPK
   * 
   * @param problemFile
   * @param solutionFile
   * @param clientTags
   * @param clients
   * @return a Map containing a list of Solution instances for each client
   */
  private HashMap<AbstractAddress, ArrayList<Solution>> plan(File problemFile, File solutionFile,
      TreeBidiMap<AbstractAddress, String> clientTags, Collection<Client> clients) {
    glp_prob problem = GLPK.glp_create_prob();
    glp_tran translatorWorkspace = GLPK.glp_mpl_alloc_wksp();
    int ret = GLPK.glp_mpl_read_model(translatorWorkspace, problemFile.getAbsolutePath(), SKIP);
    if (ret != 0) {
      throw new IllegalArgumentException("Could not read model");
    }
    ret = GLPK.glp_mpl_generate(translatorWorkspace, null);
    if (ret != 0) {
      throw new IllegalArgumentException("Could not generate model");
    }
    GLPK.glp_mpl_build_prob(translatorWorkspace, problem);
    GLPK.glp_simplex(problem, null);
    glp_iocp iocp = new glp_iocp();
    GLPK.glp_init_iocp(iocp);
    iocp.setTm_lim(TIME_LIMIT);
    GLPK.glp_intopt(problem, iocp);
    // TODO: when solutions are consistent, saving the solution file should not
    // be done anymore
    if (solutionFile != null) {
      GLPK.glp_print_sol(problem, solutionFile.getAbsolutePath());
    }
    GLPK.glp_mpl_postsolve(translatorWorkspace, problem, GLPKConstants.GLP_MIP);

    HashMap<AbstractAddress, ArrayList<Solution>> result = new HashMap<AbstractAddress, ArrayList<Solution>>();

    // For creating debug output
    HashMap<AbstractAddress, Double> chargePerClient = new HashMap<AbstractAddress, Double>();
    double wanted = 0.0;
    for (Client client : clients) {
      // Factor 4 is due to the fact that we use quarter hours..
      wanted += Math.ceil(4 * client.getDeviceInfo().calcEnergyNeeded());
      // Initialize result..
      result.put(client.getClientAddress(), new ArrayList<Solution>());
      // For creating debug output
      chargePerClient.put(client.getClientAddress(), new Double(4 * client.getDeviceInfo().calcEnergyNeeded()));
    }

    double provided = 0.0;
    int n = GLPK.glp_get_num_cols(problem);
    int status = GLPK.glp_get_status(problem);
    if (status != GLPKConstants.GLP_OPT) {
      logger.log(Level.WARNING, "solution is not optimal: " + getGLPKStatusName(status));
    }
    for (int i = 1; i <= n; i++) {
      String name = GLPK.glp_get_col_name(problem, i);
      double val = GLPK.glp_get_col_prim(problem, i);
      // TODO What is the level from where we assume that the client should be
      // charged?
      // (The MIP problem might have only a LP (non-integer) solution)))
      if (val < 0.9)
        continue;
      if (!name.startsWith("charging[")) {
        continue;
      }
      Matcher match = Pattern.compile("charging\\[([^,]*),\\s*([^\\]]*)\\]").matcher(name);
      if (match.matches()) {
        // First comes client tag
        String clientTag = match.group(1);
        // Second comes quarter hour tag
        String quarterTag = match.group(2);
        // Collect result
        result.get(clientTags.getKey(clientTag)).add(new Solution(new QuarterHour(quarterTag), val >= 0.9));
        // Collect debug output
        chargePerClient.put(clientTags.getKey(clientTag), chargePerClient.get(clientTags.getKey(clientTag)) - val);
        provided += val;
      } else {
        logger.log(Level.SEVERE, "no match for solution: " + name);
      }
    }

    // Debug output
    for (AbstractAddress clientAddress : chargePerClient.keySet()) {
      double val = chargePerClient.get(clientAddress).doubleValue();
      Double needed = null;
      for (Client cl : clients) {
        if (cl.getClientAddress().equals(clientAddress)) {
          needed = Math.ceil(4 * cl.getDeviceInfo().calcEnergyNeeded());
          break;
        }
      }
      logger.log(Level.INFO, "client " + clientAddress.getName() + " charges " + result.get(clientAddress).size()
          + " times for " + needed);
      if (val > 0) {
        logger.log(Level.SEVERE, "client " + clientAddress.getName() + " missing charge of " + val);
      }
    }

    if (provided < wanted) {
      logger.log(Level.SEVERE, "ERROR: Could not provide wanted energy (" + provided + " < " + wanted + ")");
    } else {

      logger.log(Level.INFO, "providing wanted energy (" + provided + " >= " + wanted + ")");
    }

    return result;
  }

  /**
   * Makes a string out of the GLPK solution status
   * 
   * @param status
   *          one of the constants that are returned from
   *          {@link GLPK#glp_get_status(glp_prob)}
   * @return a string representation of that status
   */
  private static String getGLPKStatusName(int status) {
    if (status == GLPKConstants.GLP_OPT)
      return "Optimal";
    if (status == GLPKConstants.GLP_FEAS)
      return "Feasible";
    if (status == GLPKConstants.GLP_INFEAS)
      return "Infeasible";
    if (status == GLPKConstants.GLP_NOFEAS)
      return "No feasible";
    if (status == GLPKConstants.GLP_UNBND)
      return "Unbounded";
    if (status == GLPKConstants.GLP_UNDEF)
      return "Undefined";

    return "Unknown-" + status;
  }

  /**
   * Method to read in an entire file. The method name derived hacker jargon
   * (http://catb.org/jargon/html/S/slurp.html; the good hacker, of course).
   * 
   * @param file
   *          Which file to read in
   * @return The entire content of the file in a string
   * @throws AssertionError
   *           when file is not found, or data can't be read
   */
  private static String slurp(File file) {
    try {
      StringBuffer buffer = new StringBuffer();
      BufferedReader reader = new BufferedReader(new FileReader(file));
      char[] buf = new char[1024];
      int numRead = 0;
      while ((numRead = reader.read(buf)) != -1) {
        buffer.append(String.valueOf(buf, 0, numRead));
      }
      reader.close();
      return buffer.toString();
    } catch (FileNotFoundException e) {
      throw new AssertionError();
    } catch (IOException e) {
      throw new AssertionError();
    }
  }

  /**
   * Simple getter for {@link #maxNumClients} field
   * 
   * @return the current value of {@link #maxNumClients},
   *         {@value #maxNumClients}
   */
  public int getMaxNumClients() {
    return maxNumClients;
  }

  /**
   * Simple setter for {@link #maxNumClients} field
   * 
   * @return
   */
  public void setMaxNumClients(int maxNumClients) {
    this.maxNumClients = maxNumClients;
  }

  /**
   * Stops the planning thread. The thread is not killed, but told to stop at
   * the next possible moment.
   */
  public void stop() {
    stop = true;
  }

  public void setExpectedClients(int expectedClients) {
    this.expectedClients = expectedClients;
  }

  public boolean isStopped() {
    return stop;
  }

  public static double getBatteryEfficiency() {
    return batteryEfficiency;
  }

}
