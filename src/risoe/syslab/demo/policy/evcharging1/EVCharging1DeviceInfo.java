/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.message.parser.DoubleConstant;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.IntegerConstant;
import risoe.syslab.control.policy.message.parser.MessageParser;

/**
 * DeviceInfo class for the EVC1 example implementation
 */
public final class EVCharging1DeviceInfo implements DeviceInfo {

  /** String that starts the FIPA content off this class */
  public static final String MSG_ID = "ev-charging-1";

  /** key used in FIPA content */
  public static final String KEY_ID = ":id";
  /** key used in FIPA content */
  public static final String KEY_SOC = ":soc";
  /** key used in FIPA content */
  public static final String KEY_TSOC = ":tsoc";
  /** key used in FIPA content */
  public static final String KEY_CAPACITY = ":capacity";
  /** key used in FIPA content */
  public static final String KEY_NEXT = ":next";
  

  /** Unique ID of car */
  private int id;
  
  /** Current SOC (state Of Charge) */
  private double soc;
  
  /** target SOC */
  private double tsoc;
  
  /** How much kWH capacity has the battery? */
  private double capacity;
  
  /** When is the car needed again? */
  private Date nextUse;


  /** Default constructor; should NEVER be called  */
  @SuppressWarnings( "unused" )
  private EVCharging1DeviceInfo() {
    throw new AssertionError();
  }
  
  
  /**
   * Standard constructor, giving values for all the fields.
   * 
   * @param id
   * @param soc
   * @param tsoc
   * @param capacity
   * @param nextUse
   */
  public EVCharging1DeviceInfo( int id, double soc, double tsoc, double capacity, Date nextUse ) {
    super();
    this.id = id;
    this.soc = soc;
    this.tsoc = tsoc;
    this.capacity = capacity;
    this.nextUse = nextUse;
  }


  /**
   * Calculate the energy needed to charge the EV to the target SOC.
   * 
   * @return energy in kWh
   */
  public double calcEnergyNeeded() {
    return (getTsoc() - getSoc()) / 100.0 * getCapacity();
  }
  
  
  /** Method to parse an object of this class fromString content
   * created by {@link #toFipaContent()}
   * @param expression Expression returned by {@link MessageParser#parse(String)}
   * @return the created {@link EVCharging1DeviceInfo}, or null if parsing failed
   */
  public static EVCharging1DeviceInfo parse( Expression expression ) {
    
    String pattern =  "(" + EVCharging1DeviceInfo.MSG_ID + " " + 
    EVCharging1DeviceInfo.KEY_ID       + " ?id " + 
    EVCharging1DeviceInfo.KEY_SOC      + " ?soc " + 
    EVCharging1DeviceInfo.KEY_TSOC     + " ?tsoc " + 
    EVCharging1DeviceInfo.KEY_CAPACITY + " ?capacity " + 
    EVCharging1DeviceInfo.KEY_NEXT     + " ?nextUse" + 
    ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    int id = (int) ((IntegerConstant) match.get( "id" )).getValue();
    double soc = ((DoubleConstant) match.get( "soc" )).getValue();
    double tsoc = ((DoubleConstant) match.get( "tsoc" )).getValue();
    double capacity = ((DoubleConstant) match.get( "capacity" )).getValue();
    Date nextUse = new Date( ((IntegerConstant) match.get( "nextUse" )).getValue() );
    return new EVCharging1DeviceInfo( id, soc, tsoc, capacity, nextUse );
  }

  
  @Override
  public String toFipaContent() {
    return "(" + MSG_ID + " " + 
      KEY_ID       +  " " + id   + " " + 
      KEY_SOC      + " " + soc  + " " + 
      KEY_TSOC     + " " + tsoc + " " + 
      KEY_CAPACITY + " " + capacity + " " + 
      KEY_NEXT     + " " + nextUse.getTime() + 
      ")";
  }


  /** Simple getter for id of this "car"
   * @return id of this object
   */
  public int getId() {
    return id;
  }


  /** Simple getter
   * @return SOC (state-of-charge) of this object
   */
  public double getSoc() {
    return soc;
  }


  /** Simple getter
   * @return target SOC of this object
   */

  public double getTsoc() {
    return tsoc;
  }


  /** Simple getter
   * @return date of next use of EV for this object
   */
  public Date getNextUse() {
    return nextUse;
  }


  /** Simple getter
   * @return capacity of the EV for this object
   */
  public double getCapacity() {
    return capacity;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits( capacity );
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + id;
    result = prime * result + ((nextUse == null) ? 0 : nextUse.hashCode());
    temp = Double.doubleToLongBits( soc );
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits( tsoc );
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }



  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    EVCharging1DeviceInfo other = (EVCharging1DeviceInfo) obj;
    if ( Double.doubleToLongBits( capacity ) != Double.doubleToLongBits( other.capacity ) ) {
      return false;
    }
    if ( id != other.id ) {
      return false;
    }
    if ( nextUse == null ) {
      if ( other.nextUse != null ) {
        return false;
      }
    } else if ( !nextUse.equals( other.nextUse ) ) {
      return false;
    }
    if ( Double.doubleToLongBits( soc ) != Double.doubleToLongBits( other.soc ) ) {
      return false;
    }
    if ( Double.doubleToLongBits( tsoc ) != Double.doubleToLongBits( other.tsoc ) ) {
      return false;
    }
    return true;
  }



}
