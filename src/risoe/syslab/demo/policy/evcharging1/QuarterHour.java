/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Since EVC1 plans for each quarter hour, we need 
 * a class representing a single quarter hour.
 */
public final class QuarterHour implements Comparable<QuarterHour> {

  private static TimeZone timeZone = TimeZone.getTimeZone( "UTC" );

  /** start date and time of this quarter hour, 
   * falls always on an exact boundary of a 
   * quarter hour. */
  private Date date;
  
  
  /** Default constructor; SHOULD never be used */
  @SuppressWarnings( "unused" )
  private QuarterHour() {
    throw new AssertionError();
  }
  
  
  /** Normal constructor, giving a date that is converted
   * to the start of a quarter hour (the quarter hour that 
   * contains the given date)
   * @param date date that defines the resulting quarter hour
   */
  public QuarterHour( Date date ) {
    this.date = calculateQuarterHour( date );
    invariant();
  }
  
  
  /** Constructor getting a tag String, as created by {@link #tag()}
   * 
   * @param tag
   */
  public QuarterHour( String tag ) {
    Matcher match = Pattern.compile( "q_(\\d+)_(\\d+)_(\\d+)_(\\d+)_(\\d+)" ).matcher( tag );
    if ( match.matches() ) {
      Calendar cal = new GregorianCalendar();
      cal.setTimeZone( timeZone );
      cal.set( Calendar.YEAR, Integer.parseInt( match.group( 1 ), 10 ) );
      cal.set( Calendar.MONTH, Integer.parseInt( match.group( 2 ), 10 ) - 1 );
      cal.set( Calendar.DAY_OF_MONTH, Integer.parseInt( match.group( 3 ), 10 ) );
      cal.set( Calendar.HOUR_OF_DAY, Integer.parseInt( match.group( 4 ), 10 ) );
      cal.set( Calendar.MINUTE, Integer.parseInt( match.group( 5 ), 10 ) );
      cal.set( Calendar.SECOND, 0 );
      cal.set( Calendar.MILLISECOND, 0 );
      this.date = cal.getTime();
    } else {
      throw new IllegalArgumentException( "Not in tag format: " + tag);
    }
    invariant();
  }

  
  /** Gets the start date of this quarter hour
   * @return
   */
  public Date getDate() {
    return date;
  }
  
  
  /** Calculates the {@link QuarterHour} that comes
   * directly after this quarter hour.
   * 
   * @return a newly created quarter hour instance; is NEVER null
   */
  public QuarterHour nextQuarter() {
    Calendar cal = new GregorianCalendar();
    cal.setTimeZone( timeZone );
    cal.setTime( this.date );
    cal.set( Calendar.SECOND, 0 );
    cal.set( Calendar.MILLISECOND, 0 );
    cal.add( Calendar.MINUTE, 15 );
    return new QuarterHour( cal.getTime() );
  }
  
  
  /** Calculates the {@link QuarterHour} that comes
   * one hour before this quarter hour.
   * 
   * @return a newly created quarter hour instance; is NEVER null
   */
  public QuarterHour previousHour() {
    Calendar cal = new GregorianCalendar();
    cal.setTimeZone( timeZone );
    cal.setTime( this.date );
    cal.set( Calendar.SECOND, 0 );
    cal.set( Calendar.MILLISECOND, 0 );
    cal.add( Calendar.HOUR_OF_DAY, -1 );
    return new QuarterHour( cal.getTime() );
  }
  

  /** Returns a tag for this quarter that can be used
   * as name in a LP problem, and can also be used to create
   * the QuarterHour object from it, using {@link QuarterHour#QuarterHour(String)}.
   * 
   * @return the newly created tag
   */
  public String tag() {
    Calendar cal = new GregorianCalendar();
    cal.setTimeZone( timeZone );
    cal.setTime( date );
    int year = cal.get( Calendar.YEAR );
    int month = cal.get( Calendar.MONTH ) + 1; // First month is represented by zero!
    int day = cal.get( Calendar.DAY_OF_MONTH );
    int hour = cal.get( Calendar.HOUR_OF_DAY );
    int minute = cal.get( Calendar.MINUTE );
    return String.format( "q_%d_%02d_%02d_%02d_%02d", year, month, day, hour, minute );
  }
  
  
  /** Checks whether this quarter hour lies strictly before the
   * given parameter
   * @param other quarter hour to compare with
   * @return true, iff this quarter hour lies strictly before the other quarter hour
   */
  public boolean after( QuarterHour other ) {
    return date.after( other.getDate() );
  }
  

  /** Checks whether this quarter hour lies strictly after the
   * given parameter
   * @param other quarter hour to compare with
   * @return true, iff this quarter hour lies strictly after the other quarter hour
   */
  public boolean before( QuarterHour other ) {
    return date.before( other.getDate() );
  }
  

  /** Helper method that calculates the actual value for the
   * date field, given any date.
   * 
   * @param date any date, defines the quarter hour
   * @return a date with restricted minute, second and millisecond fields
   */
  private static Date calculateQuarterHour( Date date ) {
    Calendar cal = new GregorianCalendar();
    cal.setTimeZone( timeZone );
    cal.setTime( date );
    cal.set( Calendar.SECOND, 0 );
    cal.set( Calendar.MILLISECOND, 0 );
    int minute = cal.get( Calendar.MINUTE );
    minute = minute - (minute%15);
    cal.set( Calendar.MINUTE, minute );
    return cal.getTime();
  }

  
  /** Checks the invariant of this class, i.e. that the date member
   * is directly at the boundary of a quarter hour, e.g.
   * 12.01.2010 17:30:00
   */
  private void invariant() {
    if ( date == null ) 
      throw new AssertionError( "Date is null" );
    Calendar cal = new GregorianCalendar();
    cal.setTimeZone( timeZone );
    cal.setTime( date );
    if ( cal.get( Calendar.SECOND ) != 0 ) 
      throw new AssertionError( "Seconds: " + date );
    if ( cal.get( Calendar.MILLISECOND ) != 0 ) 
      throw new AssertionError( "Milliseconds: " + date );
    if ( cal.get( Calendar.MINUTE ) % 15 != 0 ) 
      throw new AssertionError( "Minutes: " + date );
  }

  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((date == null) ? 0 : date.hashCode());
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    QuarterHour other = (QuarterHour) obj;
    if ( date == null ) {
      if ( other.date != null ) {
        return false;
      }
    } else if ( !date.equals( other.date ) ) {
      return false;
    }
    return true;
  }


  @Override
  public int compareTo( QuarterHour o ) {
    return date.compareTo( o.getDate() );
  }


}
