/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1.app;

import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.ClientListener;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Client;
import risoe.syslab.demo.policy.evcharging1.EVCharging1DeviceInfo;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Planner;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Server;
import risoe.syslab.demo.policy.evcharging1.QuarterHour;

/**
 * Application for trying out {@link EVCharging1PolicyImplementation}.
 */
public class EVC1App implements Runnable, ClientListener {

  private static final int NUM_CLIENTS = 10;

  /**
   * Starts up the application
   */
  @Override
  public void run() {
    PolicyUtils.setup( "conf/policy-framework.properties" );
    EVCharging1Server serverImpl = new EVCharging1Server();
    EVCharging1Planner planner = serverImpl.getPlanner();
    planner.setExpectedClients( NUM_CLIENTS );
    planner.setMaxNumClients( NUM_CLIENTS/2 );
    
    String hostname;
    hostname = "localhost";
//    try {
//      hostname = InetAddress.getLocalHost().getHostName();
//    } catch ( UnknownHostException e ) {
//      throw new AssertionError( "Can't determine local hostname" );
//    }

    serverImpl.start();
    
    CommunicationSystem commSystem = new HttpCommunicationSystem( "evc1", hostname );
    PolicyUtils.setCommunicationSystem( commSystem );

    AbstractAddress serverAddress = commSystem.createAddress( "evc1" );
    PolicyUtils.createServerAgent( serverAddress, serverImpl );

    for ( int number = 1; number < NUM_CLIENTS+1; number++ ) {
      AbstractAddress clientAddress = commSystem.createAddress( "client-" + number );
      EVCharging1Client client = new EVCharging1Client( clientAddress );
      client.registerClientListener(this);
      PolicyUtils.createClientAgent( clientAddress , client, serverAddress );
    }
  }


  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    new EVC1App().run();
  }


  private HashMap<String, EVCharging1DeviceInfo> deviceInfoMap = new HashMap<>();
  
  @Override
  public synchronized void deviceInfoChanged(AbstractAddress address, DeviceInfo deviceInfo) {
    EVCharging1DeviceInfo hcInfo = (EVCharging1DeviceInfo) deviceInfo;
    deviceInfoMap.put(address.getName(), hcInfo);
    if (deviceInfoMap.keySet().size() == NUM_CLIENTS) {
      Date startDate = new QuarterHour(new Date()).getDate();
      double chargeSum = 0;
      double hourSum = 0;
      for (EVCharging1DeviceInfo info : deviceInfoMap.values()) {
        chargeSum += info.calcEnergyNeeded();
        Date endDate = new QuarterHour(info.getNextUse()).getDate();
        hourSum += (endDate.getTime() - startDate.getTime()) / 1000.0 / 3600.0;
        //System.out.println(info.calcEnergyNeeded() + ", " + ((endDate.getTime() - startDate.getTime()) / 1000.0 / 3600.0));
      }
      System.out.println("kWh: " + chargeSum);
      System.out.println("h: " + hourSum);
      System.out.println(hourSum + " > " + (chargeSum / EVCharging1Planner.getBatteryEfficiency()));
    }
  }


  @Override
  public void activePolicyChanged(AbstractAddress address, Policy policy) {
    // Ignore
  }

}
