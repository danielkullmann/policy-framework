/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1.app;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.ClientBehaviour;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Client;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Planner;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Server;
import risoe.syslab.demo.policy.evcharging1.gui.DeviceDisplay;

/**
 * Test application for the EVC1 planner, that also contains a GUI 
 * for displaying the state of the clients.
 * It uses the HTTP Communication System.
 */
@SuppressWarnings( "serial" )
public class EVC1PlannerApp extends JFrame {

  
  /**
   * Number of clients that are created
   */
  private static final int NUM_CLIENTS = 48;
  
  /**
   * GUI setting: How many {@link DeviceDisplay} objects in one row?
   */
  private static final int ROW_SIZE = 6;


  /** List of created client behaviours */
  private ArrayList<ClientBehaviour> clientBehaviours;
  
  
  /** Starts up the app */
  public EVC1PlannerApp() {
    super();
    this.clientBehaviours = new ArrayList<ClientBehaviour>();
    
    EVCharging1Server serverImpl = new EVCharging1Server();
    EVCharging1Planner planner = serverImpl.getPlanner();
    planner.setExpectedClients( NUM_CLIENTS );
    planner.setMaxNumClients( NUM_CLIENTS/2 );

    PolicyUtils.setup( "conf/policy-framework.properties" );

    int port = 1024 + new Random().nextInt( 1024 );
    CommunicationSystem commSystem = new HttpCommunicationSystem( port, "", null );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    AbstractAddress serverAddress = commSystem.createAddress( "evc1" );
    PolicyUtils.createServerAgent( serverAddress, serverImpl );
    
    for ( int i=1; i<=NUM_CLIENTS; i++ ) {
      AbstractAddress clientAddress = commSystem.createAddress( "client-"+i );
      EVCharging1Client clientBehaviour = new EVCharging1Client( clientAddress );
      PolicyUtils.createClientAgent( clientAddress, clientBehaviour, serverAddress );
      clientBehaviours.add( clientBehaviour );
    }
    
    
    createGui();
  }
  

  /**
   * Create the GUI
   */
  private void createGui() {
    
    JPanel rootPanel = new JPanel();
    rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.Y_AXIS));
    rootPanel.setOpaque(true); // content panes must be opaque
    rootPanel.setBorder(new BevelBorder(5));

    setContentPane(rootPanel);
    
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

//    boolean initialized = false;
//    while ( ! initialized ) {
//      initialized = true;
//      for ( ClientAgent client : clients ) {
//        initialized = initialized && client.getClientBehaviour() != null;
//      }
//      if ( ! initialized ) {
//        try { Thread.sleep( 500 ); } catch ( InterruptedException e ) { /* may be ignored */ }
//      }
//    }

    int count = 0;
    for ( ClientBehaviour cb : clientBehaviours ) {
      DeviceDisplay display = new DeviceDisplay();
      panel.add( display );
      cb.registerClientListener( display );
      count ++;
      if ( count == ROW_SIZE ) {
        count = 0;
        rootPanel.add( panel );
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
      }
    }
    
    rootPanel.add( panel );
    getContentPane().validate();
    pack();
    setVisible( true );
    
  }


  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    final EVC1PlannerApp app = new EVC1PlannerApp();
    app.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent we) {
        System.exit(0);
      }
    });

  }
  
  
}
