/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.comm.lba.LbaSchemeSocketFactory;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.ClientBehaviour;
import risoe.syslab.control.policy.core.PolicyClient;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Client;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Planner;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Server;

/**
 * Test application for the EVC1 planner, that is not using a gui.
 * This is meant to be used for large scale simulations.
 * It uses the HTTP Communication System, and it introduces delays
 * using the LbaSchemeSocketFactory.
 * 
 * TODO: Add more clients to the key database
 */
public class EVC1PlannerSimulatorDelayApp {

  /** Logger for this class */
  private static Logger logger = Logger.getLogger( EVC1PlannerSimulatorDelayApp.class.getName() );

  /**
   * Number of clients that are created
   */
  private static final int NUM_CLIENTS = 500;
  
  /** List of created client behaviours */
  private ArrayList<ClientBehaviour> clientBehaviours;
  
  
  /** Starts up the application */
  public EVC1PlannerSimulatorDelayApp( int numClients, int[] params ) {
    super();
    // Disable the use of message signing; this is done to reduce the load
    // on the system
    if (numClients <= 0) {
      numClients = NUM_CLIENTS;
    }
    PolicyMessage.USE_SIGNATURES = true;
    EVCharging1Planner.SLEEP_TIME = 600; // 10 minutes;
    // Don't send (too many) alive messages, to reduce the load on the system
    PolicyClient.ALIVE_MESSAGE_FREQUENCY = 600;
    PolicyServer.ALIVE_THRESHOLD = 1200;
    this.clientBehaviours = new ArrayList<ClientBehaviour>();
    
    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );

    PolicyUtils.setup( "conf/policy-framework.properties" );

    logger.log( Level.INFO, "this is never displayed. WHY?"); 

    int port = 1024 + new Random().nextInt( 1024 );
    HttpCommunicationSystem commSystem = new HttpCommunicationSystem( port, "", null );

    // Setup a socket factory that introduces additional latencies
    LbaSchemeSocketFactory socketFactory = new LbaSchemeSocketFactory();
    double latency = 100.0;       // 100 +/- 50 ms latency [ms]
    double lr = 0;
    double upBandwidth = 1000;    // 1 +/- 0.1 MB/s client -> server [byte/ms]
    double ubr = 0;
    double downBandwidth = 10000; // 10 +/- 1 MB/s server -> client [byte/ms]
    double dbr = 0;
    if (params == null || params.length == 0) {
      // keep default params
    } else if (params.length == 3) {
      // no random values
    } else if (params.length == 6) {
      // with random values
    } else {
      throw new IllegalArgumentException( "params has the wrong format: " + Arrays.toString(params) );
    }
    socketFactory.setParams( latency, upBandwidth, downBandwidth, lr, ubr, dbr );
    logger.log( Level.WARNING, "lb settings: " + 
      latency + ", " + lr + ", " + 
      upBandwidth + ", " + ubr + ", " + 
      downBandwidth + ", " + dbr
    );
//    System.out.println( "lb settings: " + 
//      latency + ", " + lr + ", " + 
//      upBandwidth + ", " + ubr + ", " + 
//      downBandwidth + ", " + dbr
//    );


    commSystem.setSocketFactory( socketFactory  );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    AbstractAddress serverAddress = commSystem.createAddress( "evc1" );
    EVCharging1Server serverImpl = new EVCharging1Server();
    
    int maxClients = numClients / 2;
    EVCharging1Planner planner = serverImpl.getPlanner();
    planner.setMaxNumClients( maxClients );
    planner.setExpectedClients( numClients );
    logger.log( Level.INFO, "max num clients set to: " + planner.getMaxNumClients() );
    
    serverImpl.start();
    PolicyUtils.createServerAgent( serverAddress, serverImpl );
    
    for ( int i=1; i<=numClients; i++ ) {
      AbstractAddress clientAddress = commSystem.createAddress( "client-"+i );
      EVCharging1Client clientBehaviour = new EVCharging1Client( clientAddress );
      PolicyUtils.createClientAgent( clientAddress, clientBehaviour, serverAddress );
      clientBehaviours.add( clientBehaviour );
    }

    while (true) {
      try {
        Thread.sleep(1);
      } catch ( InterruptedException e ) {
        Thread.currentThread().interrupt();
        break;
      }
      if (planner.isStopped()) {
        break;
      }
      boolean allClients = true;
      for ( ClientBehaviour client : clientBehaviours ) {
        if ( client.getActivePolicy() == null ) {
          allClients = false;
          break;
        }
      }
      if (allClients) break;
    }
    
    System.exit(0);
    
  }
  

  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    int numClients = 0;
    if (args.length > 0) {
      numClients = Integer.parseInt( args[0] );
    }
    int[] params = null;
    if (args.length > 1 ) {
      String[] vals = args[1].split( "," );
      params = new int[vals.length];
      for ( int i = 0; i < vals.length; i++ ) {
        params[i] = Integer.parseInt( vals[i] );
      }
    }
    new EVC1PlannerSimulatorDelayApp(numClients, params);
  }
  
  
}
