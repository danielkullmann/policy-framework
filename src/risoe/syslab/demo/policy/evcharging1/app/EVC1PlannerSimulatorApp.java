/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1.app;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.comm.lba.LbaSchemeSocketFactory;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.ClientBehaviour;
import risoe.syslab.control.policy.core.PolicyClient;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Client;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Planner;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Server;

/**
 * Test application for the EVC1 planner, that is not using a gui.
 * This is meant to be used for large scale simulations.
 * It uses the HTTP Communication System.
 */
public class EVC1PlannerSimulatorApp {

  /** Logger for this class */
  private static Logger logger = Logger.getLogger( EVC1PlannerSimulatorApp.class.getName() );

  /**
   * Number of clients that are created
   */
  private static final int NUM_CLIENTS = 500;
  
  /** List of created client behaviours */
  private ArrayList<ClientBehaviour> clientBehaviours;
  
  
  /** Starts up the application */
  public EVC1PlannerSimulatorApp( int numClients, String delayType ) {
    super();
    // Disable the use of message signing; this is done to reduce the load
    // on the system
    if (numClients <= 0) {
      numClients = NUM_CLIENTS;
    }
    PolicyMessage.USE_SIGNATURES = true;
    EVCharging1Planner.SLEEP_TIME = 600; // 10 minutes;
    // Don't send (too many) alive messages, to reduce the load on the system
    PolicyClient.ALIVE_MESSAGE_FREQUENCY = 600;
    PolicyServer.ALIVE_THRESHOLD = 1200;
    this.clientBehaviours = new ArrayList<ClientBehaviour>();
    
    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );

    PolicyUtils.setup( "conf/policy-framework.properties" );

    int port = 1024 + new Random().nextInt( 1024 );
    HttpCommunicationSystem commSystem = new HttpCommunicationSystem( port, "", null );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    if (delayType != null && ! "".equals( delayType )) {
      LbaSchemeSocketFactory socketFactory = new LbaSchemeSocketFactory();
      double latency = 0.0;
      double lr = 0.0;
      double upBandwidth = 0.0;
      double ubr = 0.0;
      double downBandwidth = 0.0;
      double dbr = 0.0;
      if ( "a".equalsIgnoreCase( delayType ) ) {
        // This is the default used by EVC1PlannerSimulatorDelayApp
        latency = 100.0;
        lr = 0;
        upBandwidth = 1000;
        ubr = 0;
        downBandwidth = 10000;
        dbr = 0;
      } else if ( "b".equalsIgnoreCase( delayType ) ) {
        // TODO
      } else if ( "c".equalsIgnoreCase( delayType ) ) {
        // TODO
      } else if ( "d".equalsIgnoreCase( delayType ) ) {
        // TODO
      } else {
        System.exit( 1 );
      }
      socketFactory.setParams( latency, upBandwidth, downBandwidth, lr, ubr, dbr );
      logger.log( Level.WARNING, "lb settings: " + 
        latency + ", " + lr + ", " + 
        upBandwidth + ", " + ubr + ", " + 
        downBandwidth + ", " + dbr
      );
      commSystem.setSocketFactory( socketFactory );
    }
    AbstractAddress serverAddress = commSystem.createAddress( "evc1" );
    EVCharging1Server serverImpl = new EVCharging1Server();
    
    int maxClients = numClients / 2;
    EVCharging1Planner planner = serverImpl.getPlanner();
    planner.setMaxNumClients( maxClients );
    planner.setExpectedClients( numClients );
    logger.log( Level.INFO, "max num clients set to: " + planner.getMaxNumClients() );
    
    serverImpl.start();
    PolicyUtils.createServerAgent( serverAddress, serverImpl );
    
    for ( int i=1; i<=numClients; i++ ) {
      AbstractAddress clientAddress = commSystem.createAddress( "client-"+i );
      EVCharging1Client clientBehaviour = new EVCharging1Client( clientAddress );
      PolicyUtils.createClientAgent( clientAddress, clientBehaviour, serverAddress );
      clientBehaviours.add( clientBehaviour );
    }

    while (true) {
      try {
        Thread.sleep(1);
      } catch ( InterruptedException e ) {
        Thread.currentThread().interrupt();
        break;
      }
      if (planner.isStopped()) {
        break;
      }
      boolean allClients = true;
      for ( ClientBehaviour client : clientBehaviours ) {
        if ( client.getActivePolicy() == null ) {
          allClients = false;
          break;
        }
      }
      if (allClients) break;
    }
    
    System.exit(0);
    
  }
  

  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    int numClients = 0;
    if (args.length > 0) {
      numClients = Integer.parseInt( args[0] );
    }
    String delayType = null;
    if (args.length > 1) {
      delayType = args[1];
    }
    
    new EVC1PlannerSimulatorApp(numClients, delayType);
  }
  
}
