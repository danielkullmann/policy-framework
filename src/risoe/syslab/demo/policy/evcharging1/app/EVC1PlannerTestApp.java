/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1.app;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Client;
import risoe.syslab.demo.policy.evcharging1.EVCharging1DeviceInfo;
import risoe.syslab.demo.policy.evcharging1.EVCharging1Server;

/**
 * First test application for the EVC1 planner.
 * Starts a number of clients and gives them random 
 * device info.
 */
public class EVC1PlannerTestApp {

  private static final int NUM_CLIENTS = 3;

  /** Default constructor */
  public EVC1PlannerTestApp() {
    super();
    PolicyUtils.setup( "conf/policy-framework.properties" );
  }
  
  /** Runs the application for 30 seconds, then stops */
  public void run() {
    EVCharging1Server serverImpl = new EVCharging1Server();
    PolicyUtils.setCommunicationSystem( new HttpCommunicationSystem( "syslab-00", "syslab-00" ) );
    
    for ( int i=0; i<NUM_CLIENTS; i++ ) {
      AbstractAddress clientAddress = PolicyUtils.createAddress( "client-"+i );
      EVCharging1DeviceInfo deviceInfo = new EVCharging1Client( clientAddress ).provideDeviceInfo();
      serverImpl.register( clientAddress, deviceInfo );
    }
    try {
      Thread.sleep( 30000 );
    } catch ( InterruptedException e ) {
      // can be ignored..
    }
    serverImpl.stop();
  }
  
  /** Entry point for Java application */
  public static void main( String[] args ) {
    new EVC1PlannerTestApp().run();
  }
  
  
}
