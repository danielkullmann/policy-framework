/**
 * This package contains all the executable classes for this example,
 * i.e. they contain a <code>public static void main(String[])</code> method
 * 
 */
package risoe.syslab.demo.policy.evcharging1.app;
