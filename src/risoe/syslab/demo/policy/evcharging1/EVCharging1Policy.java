/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import java.util.Arrays;
import java.util.HashMap;

import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.IntegerConstant;
import risoe.syslab.control.policy.message.parser.MessageParser;

/** Policy used in the EVC1 example implementation */
public final class EVCharging1Policy implements Policy {

  private static final String MSG_ID = "ev-charging-policy-1";

  private TimeFrame timeFrame;

  /** starting with the quarter hour of GetTimeFrame().getFrom(),
   * this array contains a true if the EV should charge at that
   * time.
   */
  private boolean[] charging;
  
  @SuppressWarnings( "unused" )
  private EVCharging1Policy() {
    throw new IllegalAccessError("");
  }
  
  /** Standard constructor
   * 
   * @param charging
   * @param maxPrice
   * @param maxPriceSOC
   */
  public EVCharging1Policy( TimeFrame timeFrame, boolean[] charging ) {
    super();
    this.timeFrame = timeFrame;
    this.charging = charging;
  }

  
  /** parse an {@link EVCharging1Policy}
   * @param expression Expression, as returned from {@link MessageParser#parse(String)}
   * @return the parsed {@link EVCharging1Policy}, or null if that failed
   */
  public static EVCharging1Policy parse( Expression expression ) {
    
    String pattern =  "(" + MSG_ID + " " + 
    ":chrg ?chrg " +
    " " + TimeFrame.KEY_TIME_FRAME + " ?tf" +
    ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    
    // Need to convert an ExpressionList into a boolean[]
    ExpressionList chrg = (ExpressionList) match.get( "chrg" );
    boolean[] charging = new boolean[ chrg.getLength() ];
    int index = 0;
    while ( chrg != null ) {
      charging[index] = ((IntegerConstant) chrg.getHead()).getValue() == 1;
      index++;
      chrg = chrg.getTail();
    }
    
    TimeFrame timeFrame = TimeFrame.parse( match.get( "tf" ) );
    
    return new EVCharging1Policy( timeFrame, charging );
  }

  
  @Override
  public TimeFrame getTimeFrame() {
    return timeFrame;
  }

  
  @Override
  public String toFipaContent() {
    return "(" + MSG_ID + 
      " :chrg " + ExpressionList.makeList( charging ) +
      " " + TimeFrame.KEY_TIME_FRAME + " " + timeFrame.toFipaContent() +
      ")";
  }


  /** Simple getter for charging array
   * @return charging array
   */
  public boolean[] getCharging() {
    return charging;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + Arrays.hashCode( charging );
    result = prime * result + ((timeFrame == null) ? 0 : timeFrame.hashCode());
    return result;
  }

  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    EVCharging1Policy other = (EVCharging1Policy) obj;
    if ( !Arrays.equals( charging, other.charging ) ) {
      return false;
    }
    if ( timeFrame == null ) {
      if ( other.timeFrame != null ) {
        return false;
      }
    } else if ( !timeFrame.equals( other.timeFrame ) ) {
      return false;
    }
    return true;
  }

  
}
