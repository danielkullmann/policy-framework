package risoe.syslab.demo.policy.evcharging1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.core.TimeFrame;

public class CsvLogProcessor {

  private String fullFileName;
  /** path of fullFileName */
  private String dirName;
  /** fullFileName name without the path */
  private String fileName;
  private Date priceDateStart;
  private double[] prices;
  private HashMap<String, EVCharging1Policy> policies = new HashMap<String, EVCharging1Policy>();

  public CsvLogProcessor( String fileName ) {
    this.fullFileName = fileName;
    this.dirName = new File(fileName).getParent();
    if (this.dirName == null) this.dirName = ".";
    this.fileName = new File(fileName).getName();
  }

  /**
   * @param args
   */
  public static void main( String[] args ) {
    new CsvLogProcessor( args[0] ).process();
  }

  private void process() {
    BufferedReader br = null;
    try {
      br = new BufferedReader( new FileReader( fullFileName ) );
      String line;
      while ((line = br.readLine()) != null) {
        String[] cols = line.split( ";" );
        if (cols[0].equals( EVCharging1Planner.PRICE_LOG_TAG )) {
          processPrice(cols);
        } else if (cols[0].equals( EVCharging1Client.LOG_TAG )) {
          processPolicy(cols);
        } else {
          throw new IllegalArgumentException("Could not recognize this tag: " + cols[0]);
        }
      }
      
      createOutputFiles();
      
    } catch ( FileNotFoundException e ) {
      throw new RuntimeException(e);
    } catch ( IOException e ) {
      throw new RuntimeException(e);
    } catch ( ParseException e ) {
      throw new RuntimeException(e);
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch ( IOException e ) {
          e.printStackTrace();
        }
      }
    }
  }

  private void createOutputFiles() throws IOException {
    
    // determine file names
    String base = fullFileName;
    if (base.endsWith( ".csv" )) {
      base = base.substring( 0, base.length()-4 );
    }
    String priceFile = base + "-price.dat";
    String consumptionFile = base + "-consumption.dat";
    String costFile = base + "-cost.dat";
    String plotFile = base + ".plot";

    // version without directory names
    String sbase = fileName;
    if (sbase.endsWith( ".csv" )) {
      sbase = sbase.substring( 0, sbase.length()-4 );
    }
    String spriceFile = sbase + "-price.dat";
    String sconsumptionFile = sbase + "-consumption.dat";
    String scostFile = sbase + "-cost.dat";

    // process policy data, convert to datetime -> consumption map
    HashMap<QuarterHour, Double> consumption = new HashMap<QuarterHour, Double>();
    for ( EVCharging1Policy policy : policies.values() ) {
      QuarterHour policyDate = new QuarterHour( policy.getTimeFrame().getFrom() );
      boolean[] charging = policy.getCharging();
      for ( int i = 0; i < charging.length; i++ ) {
        double charge = charging[i] ? 1 : 0;
        if ( ! consumption.containsKey( policyDate ) ) {
          consumption.put( policyDate, 0.0 );
        }
        consumption.put( policyDate, consumption.get( policyDate ) + charge );
        policyDate = policyDate.nextQuarter();
      }
    }

    // Sort dates in consumption data, find start date of policies
    ArrayList<QuarterHour> keys = new ArrayList<QuarterHour>(consumption.keySet());
    Collections.sort( keys );
    long minDate = keys.get( 0 ).getDate().getTime();
    
    QuarterHour priceDate = new QuarterHour( priceDateStart );
    HashMap<QuarterHour, Double> priceMap = new HashMap<QuarterHour, Double>();
    BufferedWriter pw = new BufferedWriter( new FileWriter( priceFile ) );
    for ( int i = 0; i < prices.length; i++ ) {
      for ( int j = 0; j < 4; j++ ) {
        if (priceDate.getDate().getTime() > minDate ) {
          pw.write( (priceDate.getDate().getTime()-minDate)/1000 + ";" + prices[i] + "\n" );
          priceMap.put( priceDate, prices[i] );
        }
        priceDate = priceDate.nextQuarter();
      }
    }
    pw.close();
    

    BufferedWriter mw = new BufferedWriter( new FileWriter( costFile ) );
    
    BufferedWriter cw = new BufferedWriter( new FileWriter( consumptionFile ) );
    for ( QuarterHour date : keys ) {
      cw.write( (date.getDate().getTime()-minDate)/1000 + ";" + consumption.get( date ) + "\n" );
      if (priceMap.get( date ) == null ||  consumption.get( date ) == null ) {
        System.out.println("ERROR: " + date.getDate() + " " + priceMap.get( date ) + " " + consumption.get( date ));
      } else {
        double cost = priceMap.get( date ) * consumption.get( date );
        mw.write( (date.getDate().getTime()-minDate)/1000 + ";" + cost + "\n" );
      }
    }
    
    cw.close();
    mw.close();
    
    BufferedWriter plw = new BufferedWriter( new FileWriter( plotFile ) );
    plw.write( "set datafile separator \";\"\n" );
    plw.write( "set terminal png\n" );
    plw.write( "set key left bottom\n" );
    plw.write( "set xlabel \"Time [hour]\"\n" );
    plw.write( "set ylabel \"Price [EUR]\"\n" );
    plw.write( "set y2label \"Consumption [kWh]\"\n" );
    plw.write( "set y2tics\n" );
    plw.write( "set ytics nomirror\n" );
    plw.write( "set output \"" + sbase + "-consumption.png\"\n" );
    plw.write( "set title \"Price vs. Consumption\"\n" );
    plw.write( "plot \"" + spriceFile + "\" using ($1/3600):2 title \"Price\" with lines, \\\n" );
    plw.write( "     \"" + sconsumptionFile + "\" using ($1/3600):2 title \"Consumption\" axes x1y2 with lines \\\n" );
    plw.write( "\n" );
    plw.write( "set output \"" + sbase + "-cost.png\"\n" );
    plw.write( "set title \"Price vs. Cost\"\n" );
    plw.write( "set y2label \"Cost [EUR]\"\n" );
    plw.write( "plot \"" + spriceFile + "\" using ($1/3600):2 title \"Price\" with lines, \\\n" );
    plw.write( "     \"" + scostFile + "\" using ($1/3600):2 title \"Cost\" axes x1y2 with lines \n" );
    plw.close();
    
    try {
      new ProcessBuilder( "gnuplot", plotFile ).directory( new File(dirName) ).start().waitFor();
    } catch ( InterruptedException e ) {
      Thread.currentThread().interrupt();
      return;
    }
    
  }

  private void processPolicy( String[] cols ) throws ParseException {
    Date from = CsvLogger.df.parse( cols[2] );
    Date until = CsvLogger.df.parse( cols[3] );
    boolean[] charging = new boolean[cols.length-4];
    for ( int i = 4; i < cols.length; i++ ) {
      charging[i-4] = Boolean.parseBoolean( cols[i] );
    }
    EVCharging1Policy policy = new EVCharging1Policy( new TimeFrame( from, until ), charging );
    // Only first policy of each client is read from log file
    // TODO It is possible to get a later policy from a client that was not 
    //      involved in the first round of policies; this should not happen
    if ( ! policies.containsKey( cols[1] ) ) {
      policies.put(cols[1],policy);
    }
  }

  private void processPrice( String[] cols ) throws ParseException {
    priceDateStart = CsvLogger.df.parse( cols[1] );
    prices = new double[cols.length-2];
    for ( int i = 2; i < cols.length; i++ ) {
      prices[i-2] = Double.parseDouble( cols[i] );
    }
  }

}
