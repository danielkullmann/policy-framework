/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.evcharging1;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.ServerClientBehaviour;

/**
 * 
 */
public final class EVCharging1ServerClient implements ServerClientBehaviour {

  private AbstractAddress clientAddress;
  private EVCharging1Server impl;
  
  /** Normal constructor
   * @param impl 
   * @param clientAddress
   * @param deviceInfo
   */
  public EVCharging1ServerClient( EVCharging1Server impl, AbstractAddress clientAddress, DeviceInfo deviceInfo ) {
    this.impl = impl;
    this.clientAddress = clientAddress;
    this.impl.register( clientAddress, (EVCharging1DeviceInfo) deviceInfo );
  }

  @Override
  public void activatePolicy( Policy policy ) {
    impl.activatePolicy( clientAddress, policy );
  }

  @Override
  public Policy createPolicy() {
    return impl.createPolicy( clientAddress );
  }

  @Override
  public Policy getActivePolicy() {
    return impl.getActivePolicy( clientAddress );
  }

  @Override
  public DeviceInfo getClientDeviceInfo() {
    return impl.getDeviceInfo( clientAddress );
  }

  
  @Override
  public void rejectPolicy( Policy policy ) {
    this.impl.rejectPolicy( clientAddress, policy );
  }


  @Override
  public void setClientDeviceInfo(DeviceInfo deviceInfo) {
    impl.getClient( clientAddress ).setDeviceInfo( (EVCharging1DeviceInfo) deviceInfo );
  }
}
