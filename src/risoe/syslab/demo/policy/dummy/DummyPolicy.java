/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import java.util.HashMap;

import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.DoubleConstant;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;

/**
 * Policy for the dummy policy implementation
 */
public final class DummyPolicy implements Policy {

  /** message id for this kind of policies */
  public static final String POLICY_ID = "dummy-policy";
  
  
  private TimeFrame timeFrame;
  private double level;

  
  /** Standard constructor
   * 
   * @param level payload of this policy
   * @param timeframe timeframe for this policy
   */
  public DummyPolicy( double level, TimeFrame timeframe ) {
    this.level = level;
    this.timeFrame = timeframe;
  }
  
  
  /** Parses an Expression into a dummy polcy, if possible
   * 
   * @param expression
   * @return
   */
  public static DummyPolicy parse(Expression expression) {

    String pattern = "(" + POLICY_ID + " :level ?level " + TimeFrame.KEY_TIME_FRAME + " ?tf)";
    HashMap<String, Expression> result = new MessageParser().match(pattern , expression);
    if ( result == null ) return null;
    try {
      double level = ((DoubleConstant) result.get( "level" )).getValue();
      TimeFrame timeframe = TimeFrame.parse( result.get( "tf" ) );
      return new DummyPolicy( level, timeframe );
    } catch (Exception e) {
      return null;
    }
  }

  /** Getter for level field */
  public double getLevel() {
    return level;
  }

  
  @Override
  public String toFipaContent() {
    return "(" + POLICY_ID + " :level " + level + " " + TimeFrame.KEY_TIME_FRAME + " " + timeFrame.toFipaContent() + ")";
  }

  @Override
  public TimeFrame getTimeFrame() {
    return timeFrame;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    long temp;
    temp = Double.doubleToLongBits( level );
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((timeFrame == null) ? 0 : timeFrame.hashCode());
    return result;
  }


  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    DummyPolicy other = (DummyPolicy) obj;
    if ( Double.doubleToLongBits( level ) != Double.doubleToLongBits( other.level ) ) {
      return false;
    }
    if ( timeFrame == null ) {
      if ( other.timeFrame != null ) {
        return false;
      }
    } else if ( !timeFrame.equals( other.timeFrame ) ) {
      return false;
    }
    return true;
  }


  @Override
  public String toString() {
    return "DummyPolicy [\n level=" + level + ",\n timeFrame=" + timeFrame.toFipaContent() + "\n]";
  }



}
