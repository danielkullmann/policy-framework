/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.message.PolicyMessage;

/**
 * This application is a proof of concept for the policy protocol.
 * When it prints "== policy activated: " and some text for the policy, 
 * it means that the protocol has been successfully worked through.
 */
public class DummyScalingApp implements Runnable {

  private static final Logger logger = Logger.getLogger( DummyScalingApp.class.getName() );

  private static final int NUM_CLIENTS = 512;

  private ArrayList<DummyClientBehaviour> behaviours = new ArrayList<DummyClientBehaviour>();
  
  /** Starts the application up */
  @Override
  public void run() {

    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );
    
    PolicyMessage.USE_SIGNATURES = false;

    // Calculate new policies after some minutes..
    DummyServerClientBehaviour.POLICY_VALIDITY_PERIOD = 60;
    // No automatic shutdown of clients
    DummyClientBehaviour.AUTO_UNREGISTER = -1;
    
    DummyServerBehaviour server = new DummyServerBehaviour();
    
    int port = 2048 + new Random().nextInt( 10240 );
    CommunicationSystem commSystem = new HttpCommunicationSystem( port, "", null );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    AbstractAddress serverAddress = commSystem.createAddress( "syslab-00" );
    PolicyUtils.createServerAgent( serverAddress, server );
    
    for ( int i = 0; i < NUM_CLIENTS; i++ ) {
      AbstractAddress clientAddress = commSystem.createAddress( "client-" + (i+1) );
      DummyClientBehaviour behaviour = new DummyClientBehaviour( clientAddress );
      behaviours.add(behaviour);
      PolicyUtils.createClientAgent( clientAddress, behaviour, serverAddress );
    }
    
    while (true) {
      boolean allFinished = true;
      for ( DummyClientBehaviour behaviour : behaviours ) {
        if ( behaviour.getActivePolicy() == null || behaviour.getActivePolicy().getLevel() > 5.0 ) {
          allFinished = false;
          break;
        }
      }
      if (allFinished) {
        logger.log( Level.WARNING, "shutdown");
        commSystem.shutdown();
        for ( DummyClientBehaviour behaviour : behaviours ) {
          behaviour.stop();
        }
        break;
      }
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        break;
      }
    }
    
    // TODO It's not possible yet to shutdown the JADE system.
    System.exit(1);
  }


  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    new DummyScalingApp().run();
  }

}
