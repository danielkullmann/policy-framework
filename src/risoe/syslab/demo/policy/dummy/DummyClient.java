/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;

/**
 * This application is a proof of concept for the policy protocol.
 * It uses the HTTP communication system and implements the client 
 * side of the policy protocol for the {@link DummyPolicyClientImplementation} 
 */
public class DummyClient implements Runnable {

  /** agent name of client */
  private String clientName;
  
  /** hostname of server agent, and agent name of server agent **/
  private String serverHostName;

  
  /** Default constructor
   * 
   * @param clientName name of client agent
   * @param serverHostName hostname of server agent, and agent name of server agent
   */
  public DummyClient( String clientName, String serverHostName ) {
    super();
    this.clientName = clientName;
    this.serverHostName = serverHostName;
  }


  /**
   * Starts up the application
   */
  @Override
  public void run() {
    PolicyUtils.setup( "conf/policy-framework.properties" );
    
    CommunicationSystem commSystem = new HttpCommunicationSystem( "" );
    PolicyUtils.setCommunicationSystem( commSystem  );
    
    AbstractAddress clientAddress = commSystem.createAddress( clientName );
    PolicyUtils.createClientAgent( clientAddress, new DummyClientBehaviour( clientAddress ), PolicyUtils.createAddress( serverHostName, "", serverHostName ) );
  }

  


  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    if ( args.length == 2 ) {
      new DummyClient( args[0], args[1] ).run();
    } else {
      System.err.println( "Usage: java " + DummyClient.class.getName() + " <client name> <server host name>" );
    }
  }

}
