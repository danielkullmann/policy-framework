/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import java.util.ArrayList;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.ServerBehaviour;
import risoe.syslab.control.policy.core.ServerClientBehaviour;
import risoe.syslab.control.policy.message.parser.Expression;

/**
 * Implementation of the policy for the "dummy" policy.
 * The dummy policy is need to have a proof of concept
 * that the framework functions.
 * 
 */
public final class DummyServerBehaviour implements ServerBehaviour {

  private ArrayList<DummyServerClientBehaviour> clients = new ArrayList<DummyServerClientBehaviour>();

  /**
   * Default constructor
   */
  public DummyServerBehaviour() {
    super();
    // empty right now..
  }

  
  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return DummyDeviceInfo.parse( expression );
  }

  
  @Override
  public ServerClientBehaviour createServerBehaviour( AbstractAddress clientAddress, DeviceInfo deviceInfo ) {
    DummyServerClientBehaviour dummyServerBehaviour = new DummyServerClientBehaviour( clientAddress, deviceInfo );
    clients.add( dummyServerBehaviour );
    return dummyServerBehaviour;
  }

  
  @Override
  public Policy parsePolicy( Expression expression ) {
    return DummyPolicy.parse(expression);
  }


  @Override
  public void setPolicyServer(PolicyServer policyServer) {
    // we don't need that information here..
  }


  
  public ArrayList<DummyServerClientBehaviour> getClients() {
    return clients;
  }

  
}
