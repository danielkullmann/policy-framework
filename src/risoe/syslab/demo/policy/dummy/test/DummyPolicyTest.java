/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy.test;

import java.util.Date;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.demo.policy.dummy.DummyDeviceInfo;
import risoe.syslab.demo.policy.dummy.DummyPolicy;
import risoe.syslab.demo.policy.dummy.DummyServerBehaviour;

/**
 * Test class for the dummy policy implementation
 */
public final class DummyPolicyTest extends TestCase {

  /** test parsing of {@link DummyPolicy} */
  public void testPolicyParsing() {
    double level = 24.25;
    TimeFrame timeframe = TimeFrame.createTimeFrame( new Date(), 0, 1, 0 );
    DummyPolicy p1 = new DummyPolicy( level, timeframe );
    DummyServerBehaviour server = new DummyServerBehaviour();
    DummyPolicy p2 = (DummyPolicy) server.parsePolicy( new MessageParser().parse( p1.toFipaContent() ) );
    assertNotNull( p2 );
    assertEquals( p1, p2 );
  }
  
  /** test parsing of {@link DummyDeviceInfo} */
  public void testInfoParsing() {
    String value = "Toyota Prius";
    DummyDeviceInfo i1 = new DummyDeviceInfo( value );
    DummyDeviceInfo i2 = (DummyDeviceInfo) new DummyServerBehaviour().parseDeviceInfo( new MessageParser().parse( i1.toFipaContent() ) );
    assertNotNull( i2 );
    assertEquals( i1, i2 );
  }

  /** test parsing of {@link DummyPolicy} and {@link DummyDeviceInfo} */
  public void testPolicy() {

    double level = 24.25;
    TimeFrame timeframe = TimeFrame.createTimeFrame( new Date(), 0, 1, 0 );
    String value = "Toyota Prius";

    DummyPolicy p1 = new DummyPolicy( level, timeframe );
    DummyDeviceInfo i1 = new DummyDeviceInfo( value );
    
    DummyServerBehaviour server = new DummyServerBehaviour();
    
    Policy p2 = server.parsePolicy( new MessageParser().parse( p1.toFipaContent() ) );
    DeviceInfo i2 = server.parseDeviceInfo( new MessageParser().parse( i1.toFipaContent() ) );
    
    assertEquals( p1, p2 );
    assertEquals( i1, i2 );
    
  }
  
}
