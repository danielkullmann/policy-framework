/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.AbstractClientBehaviour;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.message.parser.Expression;

/**
 * Client behaviour for the dummy policy implementation
 */
public final class DummyClientBehaviour extends AbstractClientBehaviour {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( DummyClientBehaviour.class.getName() );

  /** Configuration option: How long after receiving their first policy 
   * should the client unregister itself? values <= 0 mean "no automatic"
   * unregistering. 
   */
  public static int AUTO_UNREGISTER = 120;

  /** device info of client */
  private DummyDeviceInfo deviceInfo = null;
  
  /** active policy of client */
  private DummyPolicy activePolicy = null;
  
  /** Standard constructor */
  public DummyClientBehaviour( AbstractAddress clientAddress ) {
    // clientAddress is not important here..
  }
  

  @Override
  public boolean acceptPolicy( Policy policy ) {
    return policy instanceof DummyPolicy;
  }

  
  @Override
  public DeviceInfo provideDeviceInfo() {
    // Has device info already been created?
    if ( deviceInfo != null )
      return deviceInfo;
    
    // Create new DeviceInfo..
    deviceInfo = new DummyDeviceInfo( "dummy" );
    return deviceInfo;
  }


  @Override
  public void activatePolicy( Policy policy ) {
    activePolicy  = (DummyPolicy) policy;
    logger.log( Level.INFO, "CLIENT: policy activated " + activePolicy.getLevel() + " " + 
        deviceInfo.getValue() + " " + activePolicy.getTimeFrame() );
  }


  @Override
  public DummyPolicy getActivePolicy() {
    return activePolicy;
  }


  @Override
  public boolean shouldUnregister() {
    // No policy yet: don't unregister yet
    if ( activePolicy == null )
      return false;
    
    // We have a policy, and I want to unregister 
    // this client omne minute after the policy
    // has been activated (Just for testing the 
    // feature).
    if (AUTO_UNREGISTER > 0 ) {
      Calendar cal = new GregorianCalendar();
      cal.setTime( activePolicy.getTimeFrame().getFrom() );
      cal.add( Calendar.SECOND, AUTO_UNREGISTER );
      if ( new Date().after( cal.getTime() ) )
        return true;
    }
    
    return false;
  }


  @Override
  public void stop() {
    // nothing to be done; no thread is running..
  }


  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return DummyDeviceInfo.parse( expression );
  }


  @Override
  public Policy parsePolicy( Expression expression ) {
    return DummyPolicy.parse( expression );
  }


}
