/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.ServerClientBehaviour;
import risoe.syslab.control.policy.core.TimeFrame;

/**
 * Server behaviour for the dummy policy implementation
 */
public final class DummyServerClientBehaviour implements ServerClientBehaviour {

  /** length of policy validity [s]
   * In seconds, but only minute resolution is used.
   */
  public static int POLICY_VALIDITY_PERIOD = 60;

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( DummyServerClientBehaviour.class.getName() );

  /** device info of the connected client */
  private DummyDeviceInfo clientInfo;
  
  /** active policy of the connected client */
  private DummyPolicy policy;

  /** policy creation is delayed by 5 seconds */
  private Date waitUntil;
  
  private double level = 11.0;

  /** Standard constructor
   * 
   * @param clientAddress address of client
   * @param clientInfo device info of client
   */
  public DummyServerClientBehaviour( AbstractAddress clientAddress, DeviceInfo clientInfo ) {
    this.clientInfo = (DummyDeviceInfo) clientInfo;
    Calendar cal = new GregorianCalendar();
    cal.add( Calendar.SECOND, 5 );
    this.waitUntil = cal.getTime();
  }
  
  @Override
  public Policy createPolicy() {
    if ( new Date().after( waitUntil ) ) {
      Calendar cal = new GregorianCalendar();
      cal.add( Calendar.SECOND, POLICY_VALIDITY_PERIOD );
      this.waitUntil = cal.getTime();
      level -= 1.0;
      return new DummyPolicy( level, TimeFrame.createTimeFrame( new Date(), 0, 0, POLICY_VALIDITY_PERIOD / 60 ) );
    }
    return null;
  }
  
  @Override
  public DeviceInfo getClientDeviceInfo() {
    return clientInfo;
  }

  @Override
  public void activatePolicy( Policy policy ) {
    this.policy = (DummyPolicy) policy;
    if (policy == null ) {
      logger.log( Level.INFO, "SERVER: policy activated null " +
          clientInfo.getValue() );
    } else {
      logger.log( Level.INFO, "SERVER: policy activated " + this.policy.getLevel() + " " +
          clientInfo.getValue() + " " + policy.getTimeFrame() );
    }
  }

  @Override
  public Policy getActivePolicy() {
    return policy;
  }

  @Override
  public void rejectPolicy( Policy policy ) {
    logger.log( Level.SEVERE, "never happens: policy was rejected" );
  }

  @Override
  public void setClientDeviceInfo(DeviceInfo deviceInfo) {
    this.clientInfo = (DummyDeviceInfo) deviceInfo;
  }

}
