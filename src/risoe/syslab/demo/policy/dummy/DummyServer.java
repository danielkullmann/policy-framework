/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;

/**
 * This application is a proof of concept for the policy protocol.
 * It uses the HTTP communication system and implements the server 
 * side of the policy protocol for the {@link DummyServerBehaviour} 
 */
public class DummyServer {

  /** Name of server agent */
  private String serverName;

  /** Standard constructor
   * 
   * @param serverName name of server agent 
   */
  public DummyServer( String serverName ) {
    super();
    this.serverName = serverName;
  }

  /**
   * starts up the application
   */
  public void run() {

    PolicyUtils.setup( "conf/policy-framework.properties" );
    CommunicationSystem commSystem = new HttpCommunicationSystem( "", serverName );
    PolicyUtils.setCommunicationSystem( commSystem  );

    AbstractAddress serverAddress = commSystem.createAddress( serverName );
    DummyServerBehaviour server = new DummyServerBehaviour();
    PolicyUtils.createServerAgent( serverAddress, server );
  }

  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    if ( args.length == 1 ) {
      new DummyServer( args[0] ).run();
    } else {
      System.err.println( "Usage: java " + DummyServer.class.getName() + " <server name>" );
    }
  }

}
