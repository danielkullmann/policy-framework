/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import java.net.InetAddress;
import java.net.UnknownHostException;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;

/**
 * This application is a proof of concept for the policy protocol.
 * It uses the HTTP communication system.
 * When it prints "== policy activated: " and some text for the policy, 
 * it means that the protocol has been successfully worked through.
 */
public class DummyApp implements Runnable {

  //private static final Logger logger = Logger.getLogger( DummyApp.class.getName() );

  /** Starts up the application */
  @Override
  public void run() {

    String hostname;
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    } catch ( UnknownHostException e ) {
      throw new AssertionError( "Can't determine local hostname" );
    }

    System.setProperty( "java.util.logging.config.file", "conf/logging-verbose.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );

    DummyServerBehaviour server = new DummyServerBehaviour();
    CommunicationSystem commSystem = new HttpCommunicationSystem( "", hostname );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    AbstractAddress serverAddress = commSystem.createAddress( "syslab-00" );
    AbstractAgent s = PolicyUtils.createServerAgent( serverAddress, server );
    AbstractAddress clientAddress = commSystem.createAddress( "syslab-01" );
    AbstractAgent c = PolicyUtils.createClientAgent( clientAddress, new DummyClientBehaviour( clientAddress ), serverAddress );
    
    try {
      Thread.sleep( 10000 );
    } catch (InterruptedException e) {
      // ignore
    }
    
    commSystem.shutdown();
    c.stopAgent();
    s.stopAgent();
    
  }


  /** Entry point for Java application; starts the class */
  public static void main( String[] args ) {
    new DummyApp().run();
  }

}
