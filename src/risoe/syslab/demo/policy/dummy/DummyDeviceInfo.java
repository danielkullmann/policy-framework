/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.dummy;

import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.ExpressionList;
import risoe.syslab.control.policy.message.parser.Keyword;
import risoe.syslab.control.policy.message.parser.StringConstant;
import risoe.syslab.control.policy.message.parser.Symbol;

/** Very simple DeviceInfo implementation for
 *  testing purposes.
 */
public final class DummyDeviceInfo implements DeviceInfo {

  /** message id for this kind of device info */
  public static final String INFO_ID = "dummy-device";
  
  /** payload of the device info */
  private String value;
  

  /** Standard constructor
   * 
   * @param value payload for this device info
   */
  public DummyDeviceInfo( String value ) {
    this.value = value;
  }

  
  public static DeviceInfo parse( Expression expression ) {
    if ( ! ( expression instanceof ExpressionList ) ) return null;
    
    ExpressionList list = (ExpressionList) expression;
    if ( ! list.getHead().equals( new Symbol( INFO_ID ) ) ) return null;

    list = list.getTail();
    if ( list == null ) return null;
    
    if ( ! list.getHead().equals( new Keyword( "value" ) ) ) return null;
    
    list = list.getTail();
    if ( list == null ) return null;
    if ( ! ( list.getHead() instanceof StringConstant ) ) return null;
    
    String value = ((StringConstant) list.getHead()).getValue();
    
    return new DummyDeviceInfo( value );
  }

  /** Getter for value field */
  public String getValue() {
    return value;
  }


  @Override
  public String toFipaContent() {
    return "(" + INFO_ID + " :value \"" + value + "\")";
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }


  @Override
  public boolean equals( Object obj ) {
    if ( this == obj ) {
      return true;
    }
    if ( obj == null ) {
      return false;
    }
    if ( getClass() != obj.getClass() ) {
      return false;
    }
    DummyDeviceInfo other = (DummyDeviceInfo) obj;
    if ( value == null ) {
      if ( other.value != null ) {
        return false;
      }
    } else if ( !value.equals( other.value ) ) {
      return false;
    }
    return true;
  }


  
}
