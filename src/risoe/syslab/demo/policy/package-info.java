/**
 * This package contains the implementations of the policy protocol.
 * To write a policy implementation, several calsses have to be created,
 * each an implementation of a certain interface:
 * <ul>
 * <li> {@link risoe.syslab.control.policy.core.DeviceInfo}
 * <li> {@link risoe.syslab.control.policy.core.Policy}
 * <li> {@link risoe.syslab.control.policy.core.ClientBehaviour}
 * <li> {@link risoe.syslab.control.policy.core.ServerClientBehaviour}
 * <li> {@link risoe.syslab.control.policy.core.PolicyImplementation}
 * </ul>
 *  
 */
package risoe.syslab.demo.policy;
