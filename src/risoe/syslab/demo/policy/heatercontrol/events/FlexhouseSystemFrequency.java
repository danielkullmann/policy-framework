package risoe.syslab.demo.policy.heatercontrol.events;

import java.rmi.RemoteException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.flexhouse.controller.ThermostaticController2;

/** Represents measurement of the system frequency.
 * This needs a class, because the rule base does 
 * not work with simple values.
 * 
 * Here, the frequency is just simulated. For real frequeny values,
 * refer to the "flexhouse-integration" brnach of this codebase.
 * 
 */
public class FlexhouseSystemFrequency implements SystemFrequency, Runnable {

  /** Logger used by this class */
  private static Logger logger = Logger.getLogger( FlexhouseSystemFrequency.class.getName() );

  /** current value of system frequency */
  private volatile double value;

  /** the ThermostaticController2 is the provider of the frequency source */
  private ThermostaticController2 frequencySource;
  
  /** Default constructor */
  public FlexhouseSystemFrequency( ThermostaticController2 frequencySource ) {
    super();
    this.frequencySource = frequencySource;
    new Thread( this ).start();
  }

  @Override
  public double getValue() {
    return value;
  }

  /** Sets a new value of the frequency */
  public synchronized void setValue(double value) {
    this.value = value;
    this.notifyAll();
  }

  @Override
  public void run() {
    Thread.currentThread().setName( "SystemFrequency " + Thread.currentThread().getId() );
    Random r = new Random();
    while ( ! Thread.interrupted() ) {
      synchronized (this) {
        try {
          if ( frequencySource == null ) {
            value = 49.8 + 0.2 * r.nextDouble();
          } else  {
            value = frequencySource.requestFrequencyValue();
          }
        } catch (RemoteException e) {
          e.printStackTrace();
        }

        logger.log( Level.FINER, "system frequency: " + value );
        this.notifyAll();
      }
      try {
        Thread.sleep( 1000 );
      } catch (InterruptedException e) {
        break;
      }
    }
  }

}
