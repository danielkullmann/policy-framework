package risoe.syslab.demo.policy.heatercontrol.events;

import risoe.syslab.control.policy.rules.EventSource;

public class EventSourceUtil {

  /** Method that is used for creating rule bases.
   * This method returns the constraint for the "when" part of a rule referencing this controller,
   * when dealing with {@link EventSource} objects.
   * 
   * It should always return true. 
   */
  public static String getActiveTest() {
    return "value != java.lang.Double.NaN";
  }
}
