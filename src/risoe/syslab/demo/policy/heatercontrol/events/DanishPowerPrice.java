package risoe.syslab.demo.policy.heatercontrol.events;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.comm.shared.RMITransportDetails;
import risoe.syslab.comm.typebased.rmi.GenericPriceRMIClient;

/** Represents a dynamic power price.
 * The power price is obtained by the {@link GenericPriceRMIClient} located
 * on the server given by the "demo.hc.price-server" System property.
 * The default is to use "localhost"
 * 
 */
public class DanishPowerPrice implements PowerPrice, Runnable {

  private static Logger logger = Logger.getLogger( DanishPowerPrice.class.getName() );

  /** current value of power price. */
  private double value;

  /** Default constructor. Starts the thread receiving prices. */
  public DanishPowerPrice() {
    super();
    new Thread( this ).start();
  }

  @Override
  public double getValue() {
    return value;
  }

  /** set a new value */
  public synchronized void setValue(double value) {
    this.value = value;
    this.notifyAll();
  }

  @Override
  public void run() {
    Thread.currentThread().setName( "PowerPrice " + Thread.currentThread().getId() );
    GenericPriceRMIClient priceClient = new GenericPriceRMIClient();
    while (true) {
      try {
        String hostName = System.getProperty("demo.hc.price-server", "localhost");
        RMITransportDetails rtd = new RMITransportDetails(hostName, 1099,
            "typebased_price", "pricehack1");
        priceClient.setTransportDetails(rtd);
      } catch (Exception e) {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException ex) {
          Thread.currentThread().interrupt();
          return;
        }
        continue;
      }
      break;
    }
    while ( ! Thread.interrupted() ) {
      synchronized (this) {
        try {
          value = priceClient.getCurrentPrice().value;
          logger.log( Level.FINEST, "power price: " + value );
        } catch (RemoteException e) {
          logger.log(  Level.SEVERE, "priceClient.getCurrentPrice()", e );
        }
        this.notifyAll();
      }
      try {
        Thread.sleep( 1000 );
      } catch (InterruptedException e) {
        break;
      }
    }
  }

  
}
