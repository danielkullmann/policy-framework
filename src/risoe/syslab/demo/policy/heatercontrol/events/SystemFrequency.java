package risoe.syslab.demo.policy.heatercontrol.events;

import risoe.syslab.control.policy.rules.EventSource;

/** This a a marker interface that all frequency event sources implement
 * 
 * @author daku
 */
public interface SystemFrequency extends EventSource {
  // empty on purpose
}
