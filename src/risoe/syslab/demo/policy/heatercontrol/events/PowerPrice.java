package risoe.syslab.demo.policy.heatercontrol.events;

import risoe.syslab.control.policy.rules.EventSource;

/** This a a marker interface that all power price event sources implement
 * 
 * @author daku
 */
public interface PowerPrice extends EventSource {
  // empty on purpose
}
