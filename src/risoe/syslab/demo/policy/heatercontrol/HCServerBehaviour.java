package risoe.syslab.demo.policy.heatercontrol;

import java.util.Collection;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.PolicyServer.ClientStatus;
import risoe.syslab.control.policy.message.AliveMessage;

public interface HCServerBehaviour {

  /** Get a list of all connected clients.
   * 
   * @return list of all connected clients (also those with {@link ClientStatus#MISSING} state) 
   */
  Collection<ClientData> getClients();
  
  /** Start renegotiation of policy with the given client.
   * 
   * This assumes that a new policy has been set for the client,
   * using {@link ClientData#setActivePolicy(risoe.syslab.control.policy.core.Policy)}
   * 
   * @param clientAddress address of client with which to renegotiate a new policy
   */
  void startRenegotiation(AbstractAddress clientAddress);

  /** Get the current state of the given client.
   * 
   * Of most interest is the {@link ClientStatus#MISSING} state,
   * which tells us that the client has not sent an {@link AliveMessage}
   * for a long time.
   *  
   * @param clientAddress
   * @return the current state
   */
  ClientStatus getClientStatus(AbstractAddress clientAddress);

}
