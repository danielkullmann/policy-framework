/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.heatercontrol;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.Policy;

/**
 * Class that encapsulates all info for a single client.
 * 
 * Only visible in this package.
 */
final class ClientData {

  /** device info of the connected client */
  private HCDeviceInfo deviceInfo;
  
  /** next policy for the connected client */
  private HCPolicy nextPolicy;

  /** currently active policy of the connected client */
  private Policy activePolicy;

  /** address of the connected client */
  private AbstractAddress clientAddress;


  /** Disabled default constructor */
  @SuppressWarnings( "unused" )
  private ClientData() {
    super();
    throw new IllegalAccessError();
  }

  /** Standard constructor
   * 
   * @param clientAddress address of client 
   */
  public ClientData( AbstractAddress clientAddress ) {
    super();
    this.clientAddress = clientAddress;
  }


  /** Simple getter for the active policy
   * @return the active policy
   */
  public Policy getActivePolicy() {
    return activePolicy;
  }

  /** Simple setter for the active policy
   * @param policy the active policy
   */
  public void setActivePolicy( Policy policy ) {
    this.activePolicy = policy;
  }

  /** Simple getter for the device info
   * @return the device info
   */
  public HCDeviceInfo getDeviceInfo() {
    return deviceInfo;
  }

  /** Simple setter for the device info
   * @param deviceInfo the device info
   */
  public void setDeviceInfo( HCDeviceInfo deviceInfo ) {
    this.deviceInfo = deviceInfo;
  }

  /** Simple getter for the next policy
   * @return the next policy
   */
  public Policy getNextPolicy() {
    return nextPolicy;
  }

  
  /** Simple setter for the next policy
   * @param nextPolicy the next policy
   */
  public void setNextPolicy( HCPolicy nextPolicy ) {
    this.nextPolicy = nextPolicy;
  }

  
  /** Simple getter for the client address
   * @return address of the client
   */
  public AbstractAddress getClientAddress() {
    return clientAddress;
  }


} // end of class Device
