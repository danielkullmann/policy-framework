package risoe.syslab.demo.policy.heatercontrol;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.control.FlexhouseHeaterControl;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;
import risoe.syslab.demo.policy.heatercontrol.events.DanishPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.events.FlexhouseSystemFrequency;

/** Java application that starts many heatercontroller clients at once,
 *  in simulation mode.
 * 
 * @author daku
 */
public class ManyClientsApp {

  /** logger used by this class */
  private static final Logger logger = Logger.getLogger( ManyClientsApp.class.getName() );

  /** List of event sources */
  private ArrayList<EventSource> eventSources = new ArrayList<EventSource>();

  /** {@link HttpCommunicationSystem} that is used */
  HttpCommunicationSystem commSystem = null;

  /** Standard constructor
   * 
   * @param num number of clients to create
   * @param host host name of server
   * @param port local port to listen on
   */
  public ManyClientsApp( int num, String host, int port ) {

    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );

    if ( port < 0 ) {
      commSystem = new HttpCommunicationSystem( "" );
    } else {
      commSystem = new HttpCommunicationSystem( "", ":" + port );
    }
    PolicyUtils.setCommunicationSystem( commSystem );
    
    eventSources.add( new FlexhouseSystemFrequency( null ) );
    eventSources.add( new DanishPowerPrice() );

    for (int i = 1; i <= num; i++) {
      createClient( i, host, port );
    }
  }

  
  /** Method to create a single client */
  private void createClient(int id, String host, int port) {
    //Random r = new Random();

    ServiceList services = new ServiceList( null );
    services.addService( HCServices.dppService );
    // Bit more than half of the clients support frequency control...
    /*if ( r.nextInt( 10 ) < 6 )*/ services.addService( HCServices.fctService );
    HCDeviceInfo deviceInfo = new HCDeviceInfo( id, services  );

    HeaterControl controller = new FlexhouseHeaterControl( null, "room" + id );
    
    AbstractAddress address = commSystem.createAddress( "client-" + id );
    logger.log( Level.FINE, address.toString() );
    AbstractAddress serverAddress = commSystem.createAddressFromFullName( host );
    PolicyUtils.createClientAgent( address, new HCClientBehaviour(address, deviceInfo, controller, eventSources ), serverAddress  );
  }

  /** Entry point for Java application; starts many clients */
  public static void main( String[] args ) {
    if ( args.length == 0 ) {
      System.out.println( "Usage: java ClientApp <numClients> <server-host> [<port>]" );
      System.exit( 1 );
    }
    try {
      int port = -1;
      if ( args.length >= 3 ) port = Integer.parseInt( args[2] );
      new ManyClientsApp( Integer.parseInt(args[0]), args[1], port );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
