/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.heatercontrol;

import java.util.HashMap;

import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.IntegerConstant;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.service.ServiceList;

/**
 * DeviceInfo class for the heatercontroller example implementation
 */
public final class HCDeviceInfo implements DeviceInfo {

  /** String that starts the FIPA content off this class */
  public static final String MSG_ID = "heater";

  /** key used in FIPA content */
  public static final String KEY_ID = ":id";
  
  /** List of services this heater supports */
  private ServiceList services;
  
  /** Unique ID of heater */
  private int id;
  
  /** Default constructor; should NEVER be called  */
  @SuppressWarnings( "unused" )
  private HCDeviceInfo() {
    throw new AssertionError();
  }
  
  
  /**
   * Standard constructor, giving values for all the fields.
   * 
   * @param id
   * @param soc
   * @param tsoc
   * @param capacity
   * @param nextUse
   */
  public HCDeviceInfo( int id, ServiceList services ) {
    super();
    this.id = id;
    if ( services == null ) throw new AssertionError();
    this.services = services;
  }


  /** Method to parse an object of this class fromString content
   * created by {@link #toFipaContent()}
   * @param expression Expression returned by {@link MessageParser#parse(String)}
   * @return the created {@link HCDeviceInfo}, or null if parsing failed
   */
  public static HCDeviceInfo parse( Expression expression ) {
    
    String pattern =  
      "(" + HCDeviceInfo.MSG_ID + " " + KEY_ID + " ?id  ?services )";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    int id = (int) ((IntegerConstant) match.get( "id" )).getValue();
    ServiceList services = ServiceList.parseServiceList( match.get("services") );
    
    return new HCDeviceInfo( id, services );
  }

  
  @Override
  public String toFipaContent() {
    return "(" + MSG_ID + " " + 
      KEY_ID       + " " + id   + " " + 
      services.toFipaContent()  + " " + 
      ")";
  }


  /** Simple getter for id of this heater
   * @return id of this object
   */
  public int getId() {
    return id;
  }


  /** Simple getter for the list of service supported by this client */
  public ServiceList getServices() {
    return services;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    result = prime * result + ((services == null) ? 0 : services.hashCode());
    return result;
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    HCDeviceInfo other = (HCDeviceInfo) obj;
    if (id != other.id)
      return false;
    if (services == null) {
      if (other.services != null)
        return false;
    } else if (!services.equals(other.services))
      return false;
    return true;
  }


}
