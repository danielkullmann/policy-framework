/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.heatercontrol;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.AbstractClientBehaviour;
import risoe.syslab.control.policy.core.ClientListener;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.IdlePolicy;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;

/**
 * Client side of heater controller example behaviour.
 * An instance of this class is created for every connected client.
 */
public final class HCClientBehaviour extends AbstractClientBehaviour {

  /** logger for this class */
  private static Logger logger = Logger.getLogger( HCClientBehaviour.class.getName() );

  /** AbstractAddress of this client */
  private AbstractAddress clientAddress;
  
  /** DeviceInfo of this client */
  private HCDeviceInfo deviceInfo;
  
  /** aqctive policy of this client */
  private Policy activePolicy;

  /** the executor of the rule base in a policy */
  private RuleBaseExecutor rbe;

  /** event sources for the policy */
  private ArrayList<EventSource> eventSources = new ArrayList<EventSource>();

  /** Actual controller of the heaters in a flexhouse room. */
  private HeaterControl controller;

  /** whether the rule base is executed automatically (false) or manually
   * (true).
   */
  private boolean manual = false;

  private boolean useRulebase;
  
  /** Standard constructor
   * @param clientAddress
   */
  public HCClientBehaviour( AbstractAddress clientAddress, HCDeviceInfo deviceInfo, HeaterControl controller, ArrayList<EventSource> eventSources ) {
    this.clientAddress = clientAddress;
    this.deviceInfo = deviceInfo;
    this.eventSources = eventSources;
    this.controller = controller;
    if ( clientAddress == null ) throw new AssertionError( "clientAddress may not be null" );
    if ( deviceInfo == null )    throw new AssertionError( "deviceInfo may not be null" );
    if ( eventSources == null )  throw new AssertionError( "eventSource may not be null" );
    if ( controller == null )    throw new AssertionError( "controller may not be null" );
  }


  @Override
  public boolean acceptPolicy( Policy policy ) {
    // This client accepts an EVCharging1Policy and an IdlePolicy
    // (though IdlePolicy is not used right now..)
    return policy.getClass().equals( HCPolicy.class ) ||
           policy instanceof IdlePolicy;
  }


  @Override
  public void activatePolicy( Policy policy ) {
    activePolicy = policy;

    // Tell interested parties..
    for ( ClientListener listener : clientListeners ) {
      listener.activePolicyChanged( clientAddress, policy );
    }
    
    HCPolicy hcPolicy = (HCPolicy) activePolicy;

    if ( rbe != null ) { rbe.interrupt(); rbe = null; }
    if (useRulebase) {
      RuleBaseExecutor nrbe = new RuleBaseExecutor( hcPolicy.getRulebase() , controller, eventSources  );
      if (manual) {
        nrbe.setup();
      } else {
        nrbe.start();
      }
      rbe = nrbe;
    }
    
    logger.log( Level.INFO, "== CLIENT " + clientAddress.getName() + ": rule base activated: \n" + hcPolicy.getRulebase() );
  }

  
  @Override
  public Policy getActivePolicy() {
    return activePolicy;
  }

  
  @Override
  public HCDeviceInfo provideDeviceInfo() {
    return deviceInfo;
  }

  
  @Override
  public boolean shouldUnregister() {
    // Is that always so? => 
    // Yes, unless the client can stop on its own
    return false;
  }


  @Override
  public void stop() {
    // does nothing that has to be stopped
  }


  /** Get the CommunicationSystem address of this client 
   * 
   * @return address of this client
   */
  public AbstractAddress getClientAddress() {
    return clientAddress;
  }


  /** Sets manual flag.
   * 
   * When manual is false, the rulebase is executed automatically.
   * If it is true, the rulebase has to be executed manually,
   * by using {@link RuleBaseExecutor#runOnce()}.
   * 
   * @param manual whether the rulebase is executed manually
   */
  public void setManual( boolean manual ) {    
    this.manual = manual;
  }


  public RuleBaseExecutor getRulebaseExecutor() {
    return rbe;
  }


  public void setUseRulebase( boolean useRulebase ) {
    this.useRulebase = useRulebase;
  }


  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return HCDeviceInfo.parse( expression );
  }


  @Override
  public Policy parsePolicy( Expression expression ) {
    return HCPolicy.parse( expression );
  }

}
