package risoe.syslab.demo.policy.heatercontrol;

import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;

/** Java application that starts a heater controller server */ 
public class ServerApp {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( ServerApp.class.getName() );

  public ServerApp(String[] addresses) {

    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );

    HCServerBehaviourImplementation server = new HCServerBehaviourImplementation();

    int port = HttpCommunicationSystem.DEFAULT_PORT;
    CommunicationSystem commSystem = new HttpCommunicationSystem( port, "", addresses );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    AbstractAddress serverAddress = commSystem.createAddress( "syslab-demo-server" );
    PolicyUtils.createServerAgent( serverAddress, server );
    logger.log( Level.INFO, "server created: " + serverAddress.getAddress().toString() );
}

  public static void main( String[] args ) {
    new ServerApp( args );
  }

}
