package risoe.syslab.demo.policy.heatercontrol;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.control.FlexhouseHeaterControl;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;
import risoe.syslab.demo.policy.heatercontrol.events.DanishPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.events.FlexhouseSystemFrequency;

/** Java application running a single client for the heatercontroller example 
 *  in simulation mode.
 */
public class ClientApp {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( ClientApp.class.getName() );
  
  /** list of event sources to be used */
  private ArrayList<EventSource> eventSources = new ArrayList<EventSource>();

  /** Standard constructor
   * 
   * @param id id of this client; each connected client must have a different number
   * @param host host of server to connect to
   * @param port local port to listen on
   */
  public ClientApp( int id, String host, int port ) {

    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );

    HttpCommunicationSystem commSystem = null;
    if ( port < 0 ) {
      commSystem = new HttpCommunicationSystem( "" );
    } else {
      commSystem = new HttpCommunicationSystem( "", ":" + port );
    }
    PolicyUtils.setCommunicationSystem( commSystem );
    
    // Simulated SystemFrequency
    eventSources.add( new FlexhouseSystemFrequency( null ) );
    eventSources.add( new DanishPowerPrice() );

//    Random r = new Random();

    ServiceList services = new ServiceList( null );
    services.addService( HCServices.dppService );
    // Bit more than half of the clients support frequency control...
    /*if ( r.nextInt( 10 ) < 6 )*/ services.addService( HCServices.fctService );
    HCDeviceInfo deviceInfo = new HCDeviceInfo( id, services  );

    HeaterControl controller = new FlexhouseHeaterControl( null, "room" + id );
    
    AbstractAddress address = commSystem.createAddress( "client-" + id );
    logger.log( Level.FINE, address.toString() );
    AbstractAddress serverAddress = commSystem.createAddressFromFullName( host );
    PolicyUtils.createClientAgent( address, new HCClientBehaviour(address, deviceInfo, controller, eventSources ), serverAddress  );
  }

  /** Java application entry point; starts the client */
  public static void main( String[] args ) {
    if ( args.length == 0 ) {
      System.err.println( "Usage: java ClientApp <local-id> <server-host> [<port>]" );
      System.exit( 1 );
    }
    try {
      int port = -1;
      if ( args.length >= 3 ) port = Integer.parseInt( args[2] );
      new ClientApp( Integer.parseInt(args[0]), args[1], port );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
