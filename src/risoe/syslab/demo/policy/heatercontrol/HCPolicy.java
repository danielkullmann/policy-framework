/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.heatercontrol;

import java.util.HashMap;

import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.core.TimeWindow;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.message.parser.StringConstant;
import risoe.syslab.demo.policy.heatercontrol.control.TimedDirectFreqHiLoThresholds;

/**
 * Policy used for this example implementation.
 * Ity contains:
 * <ul>
 * <li> a time frame (as all policies)
 * <li> the used rule base
 * <li> the thresholds for frequency and price control; this is solely for data logging reasons 
 * </ul>
 */
public final class HCPolicy implements Policy {

  /** message id for this policy class */
  private static final String MSG_ID = "heater-control-policy";

  /** time frame of policy */
  private TimeFrame timeFrame;
  
  /** Threshold for frequency controller. Only needed for data logging purposes */
  private Thresholds frequencyCtrl;

  /** Threshold for price-based controller. Only needed for data logging purposes */
  private Thresholds priceCtrl;

  /** Rule base */
  private String rulebase;

  /** {@link TimeWindow}, for using with {@link TimedDirectFreqHiLoThresholds}.
   * This is optional, because not everyone is using time windows.
   */
  private TimeWindow timeWindow;

  @SuppressWarnings( "unused" )
  private HCPolicy() {
    throw new IllegalAccessError("");
  }
  
  /** Standard constructor
   * 
   * @param timeFrame
   * @param ruleBase
   * @param frequencyCtrl
   * @param priceCtrl
   */
  public HCPolicy( TimeFrame timeFrame, String ruleBase, Thresholds frequencyCtrl, Thresholds priceCtrl, TimeWindow timeWindow ) {
    super();
    this.timeFrame = timeFrame;
    this.rulebase = ruleBase;
    this.frequencyCtrl = frequencyCtrl;
    this.priceCtrl = priceCtrl;
    this.timeWindow = timeWindow;
  }
  
  /** Parses the string representation of this policy, as created by {@link #toFipaContent()}.
   * @param expression Expression, as created by {@link MessageParser#parse(String)}
   * @return the parsed policy, or null if that failed
   */
  public static HCPolicy parse( Expression expression ) {
    
    String pattern =  "(" + MSG_ID + " " + 
    ":rule-base ?ruleBase " +
    ":freq-ctrl ?freq " +
    ":price-ctrl ?price " +
    ":time-window ?tw " +
    " " + TimeFrame.KEY_TIME_FRAME + " ?tf" +
    ")";
    HashMap<String, Expression> match = new MessageParser().match( pattern, expression );
    if ( match == null )
      return null;
    
    StringConstant ruleBase = (StringConstant) match.get( "ruleBase" );
    Thresholds frequencyCtrl = Thresholds.parse( match.get( "freq" ) );
    Thresholds priceCtrl = Thresholds.parse( match.get( "price" ) );
    TimeWindow tw = TimeWindow.parse( match.get( "tw" ) );
    
    TimeFrame timeFrame = TimeFrame.parse( match.get( "tf" ) );
    
    return new HCPolicy( timeFrame, ruleBase.getValue(), frequencyCtrl, priceCtrl, tw );
  }

  
  @Override
  public TimeFrame getTimeFrame() {
    return timeFrame;
  }

  
  @Override
  public String toFipaContent() {
    StringBuilder sb = new StringBuilder();
    sb.append( "(" + MSG_ID ); 
    sb.append( " :rule-base "  + new StringConstant( rulebase ) );
    sb.append( " :freq-ctrl "  + (frequencyCtrl == null ? "null" : frequencyCtrl.toFipaContent() ) );
    sb.append( " :price-ctrl " + (priceCtrl == null ? "null" : priceCtrl.toFipaContent() ) );
    sb.append( " :time-window " + (timeWindow == null ? "null" : timeWindow.toFipaContent() ) );
    sb.append( " " + TimeFrame.KEY_TIME_FRAME + " " + timeFrame.toFipaContent() );
    sb.append( ")" );
    return sb.toString();
  }


  /** Simple getter for frequency control threshold */
  public Thresholds getFrequencyCtrl() {
    return frequencyCtrl;
  }

  /** Simple getter for price threshold */
  public Thresholds getPriceCtrl() {
    return priceCtrl;
  }

  /** Simple getter for rule base */
  public String getRulebase() {
    return rulebase;
  }

  /** Simple getter for timeWindow field */
  public Object getTimeWindow() {
    return timeWindow;
  }


  /** This is used by the data logger to get info about the current policy
   * 
   * @return String
   */
  public String getInfo() {
    // fields should be separated by ":"; should not contain new lines or semicolons ";"..
    StringBuilder sb = new StringBuilder(":");
    if (frequencyCtrl == null ) {
      sb.append("nan:nan:nan");
    } else {
      sb.append( frequencyCtrl.getLow() + ":" + frequencyCtrl.getHigh() + ":" + frequencyCtrl.getHysteresis() );
    }
    sb.append(":");
    if ( priceCtrl == null ) {
      sb.append("nan:nan:nan");
    } else {
      sb.append( priceCtrl.getLow() + ":" + priceCtrl.getHigh() + ":" + priceCtrl.getHysteresis() );
    }
    sb.append(":");
    if (timeWindow == null ) {
      sb.append("nan:nan:nan");
    } else {
      sb.append( timeWindow.getFrame() + ":" + timeWindow.getStartTime() + ":" + timeWindow.getLength() );
    }
    sb.append(":");
    return sb.toString();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((frequencyCtrl == null) ? 0 : frequencyCtrl.hashCode());
    result = prime * result + ((priceCtrl == null) ? 0 : priceCtrl.hashCode());
    result = prime * result + ((rulebase == null) ? 0 : rulebase.hashCode());
    result = prime * result + ((timeFrame == null) ? 0 : timeFrame.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    HCPolicy other = (HCPolicy) obj;
    if (frequencyCtrl == null) {
      if (other.frequencyCtrl != null)
        return false;
    } else if (!frequencyCtrl.equals(other.frequencyCtrl))
      return false;
    if (priceCtrl == null) {
      if (other.priceCtrl != null)
        return false;
    } else if (!priceCtrl.equals(other.priceCtrl))
      return false;
    if (rulebase == null) {
      if (other.rulebase != null)
        return false;
    } else if (!rulebase.equals(other.rulebase))
      return false;
    if (timeFrame == null) {
      if (other.timeFrame != null)
        return false;
    } else if (!timeFrame.equals(other.timeFrame))
      return false;
    return true;
  }

}
