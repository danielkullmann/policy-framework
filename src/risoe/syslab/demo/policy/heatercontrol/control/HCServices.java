package risoe.syslab.demo.policy.heatercontrol.control;

import java.util.Properties;

import risoe.syslab.control.policy.service.Service;
import risoe.syslab.control.policy.service.ServiceNameImpl;

/**
 * Services that are known to this example:
 * 
 * @author daku
 *
 */
public class HCServices {

  /** name of the direct frequency control service. Is in {@link Service} format. */
  public static final String DIRECT_FREQUENCY_CONTROL_THRESHOLD = "frequency-control:direct-threshold:1.0";
  /** name of the timed direct frequency control service. Is in {@link Service} format. */
  public static final String TIMED_DIRECT_FREQUENCY_CONTROL_THRESHOLD = "frequency-control:timed-direct-threshold:1.0";
  /** name of the frequency control service. Is in {@link Service} format. */
  public static final String FREQUENCY_CONTROL_THRESHOLD = "frequency-control:threshold:1.0";
  /** name of the price-based service. Is in {@link Service} format. */
  public static final String DYNAMIC_PRICE = "dynamic-price-reaction:threshold:1.0";
  
  /** Service instance for price-based service */
  public static final Service dppService;

  /** Service instance for frequency control service */
  public static final Service fctService;
  
  /** direct frequency control service with thresholds */
  public static final Service dfctService;
  /** timed direct frequency control service with thresholds */
  public static final Service tdfctService;
  
  static {
    ServiceNameImpl dppServiceDescription = new ServiceNameImpl( HCServices.DYNAMIC_PRICE );
    Properties dppPropertiesDescription = new Properties();
    dppPropertiesDescription.put( "low", Double.class.getName() );
    dppPropertiesDescription.put( "high", Double.class.getName() );
    dppPropertiesDescription.put( "hysteresis", Double.class.getName() );
    dppService = new Service( dppServiceDescription, dppPropertiesDescription, new Properties() );
    
    ServiceNameImpl fctServiceDescription = new ServiceNameImpl( HCServices.FREQUENCY_CONTROL_THRESHOLD );
    Properties fctPropertiesDescription = new Properties();
    fctPropertiesDescription.put( "low", Double.class.getName() );
    fctPropertiesDescription.put( "high", Double.class.getName() );
    fctPropertiesDescription.put( "hysteresis", Double.class.getName() );
    fctService = new Service( fctServiceDescription, fctPropertiesDescription, new Properties() );

    ServiceNameImpl dfctServiceDescription = new ServiceNameImpl( HCServices.DIRECT_FREQUENCY_CONTROL_THRESHOLD );
    dfctService = new Service( dfctServiceDescription, fctPropertiesDescription, new Properties() );

    ServiceNameImpl tdfctServiceDescription = new ServiceNameImpl( HCServices.TIMED_DIRECT_FREQUENCY_CONTROL_THRESHOLD );
    tdfctService = new Service( tdfctServiceDescription, fctPropertiesDescription, new Properties() );
  }

  /** This is a class containing only static methods, so it should never be instantiated. */
  private HCServices() {
    throw new IllegalStateException("do not instantiate");
  }
  
}
