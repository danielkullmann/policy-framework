package risoe.syslab.demo.policy.heatercontrol.control;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.rules.Controller;

/** Controls the heaters of Flexhouse. 
 * There is one FlexhouseHeaterControl instance per room.
 * 
 * @author daku
 */
public class FlexhouseHeaterControl extends HeaterControl {

  /** logger for this class */
  private static Logger logger = Logger.getLogger( FlexhouseHeaterControl.class.getName() );
  
  /** Actual control of the heaters */
  private FlexhouseHeaterController controller;

  /** Controller that is used right now to control the heater.
   * In this example implementation, {@link DynamicPowerPrice}, {@link FreqHiLoThresholds}
   * {@link DirectFreqHiLoThresholds} and {@link TimedDirectFreqHiLoThresholds}
   * can be used.
   */
  private Controller current = null;

  /** temperature setpoint.
   * 
   *  TODO: this is not really used..
   */
  private double value = 20.0;

  /** name of the room to be controlled */
  private String roomName;

  /** Constructor
   * @param controller actual heater control to be used
   * @param roomName name of the room to be controlled
   */
  public FlexhouseHeaterControl( FlexhouseHeaterController controller, String roomName ) {
    super();
    this.controller = controller;
    this.roomName = roomName;
  }

  /**
   * Switch heater on (true) or off (false)
   * @param on whether to switch the heater on
   * @param message can be used for debugging purposes
   */
  @Override
  public void switchHeater( boolean on, String message) {
    try {
      logger.log( Level.FINER, "heater controller: switch heater " + (on ? "on" : "off") + " in " + roomName );
      if ( controller != null ) {
        // This disables thermostatic control
        controller.setSetPoint( roomName, Double.NaN );
        // This makes the heater switch on or off
        controller.switchHeatersInRoom( roomName, on );
      }
    } catch (RemoteException e) {
      String msg = "calling switchHeatersInRoom locally should not result in a RemoteException";
      throw new IllegalStateException( msg );
    }
  }

  /** Method used by the {@link Controller} to increase the thermostat temperature.
   * 
   * @param reason for debug purposes: reason the temperature is changed
   */
  @Override
  public void increaseTemperature(String string) {
    logger.log( Level.FINER, "heater controller: increase temp in " + roomName );
    // TODO: More than one step??
    value = 21.0;
    if ( controller != null ) controller.setSetPoint( roomName, value );
  }

  /** Method used by the {@link Controller} to decrease the thermostat temperature.
   * 
   * @param reason for debug purposes: reason the temperature is changed
   */
  @Override
  public void decreaseTemperature(String string) {
    logger.log( Level.FINER, "heater controller: decrease temp in " + roomName );
    // TODO: More than one step??
    value = 19.0;
    if ( controller != null ) controller.setSetPoint( roomName, value );
  }

  
  /** dummy method
   *  @return always true 
   */
  @Override
  public boolean active() {
    return true;
  }

  /** Use a different controller.
   * 
   * This controller is {@link Controller#start()}ed, and 
   * a previously running controller is stopped.
   * 
   * @param controller new controller to use
   */
  @Override
  public synchronized void use( Controller controller ) {
    if ( current != null && current != controller ) {
      try {
        current.stop();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    if ( current != controller ) {
      try {
        controller.start();
        current = controller;
        logger.log( Level.INFO, "using controller " + controller );
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
