package risoe.syslab.demo.policy.heatercontrol.control;

public class RoomState {
  
  private final static double DEFAULT_SETPOINT = 21.f;
  /** 1.0 means +/-1.0C deviation, that's why it's called *half* hysteresis */
  private final static double DEFAULT_HALF_HYSTERESIS = 1.0f;
  public final static int ACTION_UNKNOWN = -1;
  public final static int ACTION_ON = 1;
  public final static int ACTION_OFF = 0;

  private double roomTemperature;
  private double setPoint;
  private double halfHysteresis;
  private int lastAction;
  private String additionalInfo;
  private int directControl;

  public RoomState(double setp, double hyst) {
    roomTemperature = Double.NaN;
    if (setp == Double.NaN) {
      setPoint = DEFAULT_SETPOINT;
    } else {
      setPoint = setp;
    }
    if (hyst == Double.NaN) {
      halfHysteresis = DEFAULT_HALF_HYSTERESIS;
    } else {
      halfHysteresis = setp;
    }
    halfHysteresis = hyst;
    lastAction = ACTION_UNKNOWN;
  }

  public void setDirectControl(int directControl) {
    this.directControl = directControl;
  }

  public void setHalfHysteresis(double hysteresis) {
    this.halfHysteresis = hysteresis;
  }

  public void setSetpoint(double setPoint) {
    this.setPoint = setPoint;
  }

  public void setLastAction(int action) {
    lastAction = action;
  }

  public int getLastAction() {
    return lastAction;
  }

  public double getSetpoint() {
    return setPoint;
  }

  public double getHalfHysteresis() {
    return halfHysteresis;
  }

  public double getTemperature() {
    return roomTemperature;
  }

  public void updateTemperature(double temperature) {
    roomTemperature = temperature;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public int getDirectControl() {
    return directControl;
  }

}
