package risoe.syslab.demo.policy.heatercontrol.control;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.AbstractAgent;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.ClientListener;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.HCClientBehaviour;
import risoe.syslab.demo.policy.heatercontrol.HCDeviceInfo;
import risoe.syslab.demo.policy.heatercontrol.HCPolicy;
import risoe.syslab.demo.policy.heatercontrol.events.DanishPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.events.FlexhouseSystemFrequency;
import risoe.syslab.demo.policy.syslabdemo1.RMIHelper;
import risoe.syslab.flexhouse.common.DeviceAddress;
import risoe.syslab.flexhouse.controller.ThermostaticController2;
import risoe.syslab.flexhouse.hw.FileMessage;
import risoe.syslab.flexhouse.hw.HWConstants;
import risoe.syslab.flexhouse.hw.HouseDataRequest;

/**
 * Flexhouse controller for the heatercontrol policy example.
 * 
 * @author daku
 *
 */
public class FlexhouseHeaterController extends ThermostaticController2 implements ClientListener {

  private static final Logger logger = Logger.getLogger( FlexhouseHeaterController.class.getName() );

  public static final int NUM_HEATERS = 10;
  private static final String SOURCE_DATA_LOGFILE = "source-data.log";

  private ArrayList<EventSource> eventSources = new ArrayList<EventSource>();
  private HashMap<AbstractAddress, String> addressMap = new HashMap<AbstractAddress, String>();
  /** roomName -> PolicyData for this room */
  private HashMap<String,HCPolicy> policies = new HashMap<String, HCPolicy>();
  private EventSourcesLogger eventSourcesLogger;

  private Random r = new Random();

  public FlexhouseHeaterController() throws RemoteException {
    super(20.0f, 0.5);
  }

  private void setup() {
    
    try {
      System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
      System.setProperty( "log4j.configuration", "conf/log4j.properties" );
      PolicyUtils.setup( "conf/policy-framework.properties" );

      String host = System.getProperty( "demo.hc.policy-server" );
      int port = 41339;
      
      HttpCommunicationSystem commSystem = null;
      if ( port < 0 ) {
        commSystem = new HttpCommunicationSystem( "" );
      } else {
        commSystem = new HttpCommunicationSystem( "", ":" + port );
      }
      PolicyUtils.setCommunicationSystem( commSystem );
      
      FlexhouseSystemFrequency systemFrequency = new FlexhouseSystemFrequency( this );
      eventSources.add( systemFrequency );
      DanishPowerPrice powerPrice = new DanishPowerPrice();
      eventSources.add( powerPrice );
      
      eventSourcesLogger = new EventSourcesLogger( systemFrequency, powerPrice );
      eventSourcesLogger.start();
      
      new HeaterDataRMIServer();
      new HeaterControlRMIServer();
      
      AbstractAddress serverAddress = commSystem.createAddressFromFullName( host );

      int id = 1;
      for (String roomName : rooms.keySet() ) {
        createHeaterClient(this, roomName, id, eventSources, serverAddress, commSystem );
        id++;
      }
      
    } catch (NumberFormatException e) {
      throw new AssertionError( e );
    }
  }

  public AbstractAgent createHeaterClient( FlexhouseHeaterController controller, String roomName, int id, 
      ArrayList<EventSource> eventSources, AbstractAddress serverAddress, 
      HttpCommunicationSystem commSystem ) {

    ServiceList services = new ServiceList( null );
    services.addService( HCServices.dppService );
    // Bit more than half of the clients support frequency control...
    if ( r.nextInt( 10 ) < 6 ) services.addService( HCServices.fctService );
    HCDeviceInfo deviceInfo = new HCDeviceInfo( id, services  );

    HeaterControl heaterController = new FlexhouseHeaterControl( controller, roomName );
    
    AbstractAddress address = commSystem.createAddress( "client-" + id );
    logger.log(  Level.FINER, "created client: " + address );
    HCClientBehaviour client = new HCClientBehaviour(address, deviceInfo, heaterController, eventSources );
    client.registerClientListener( this );
    addressMap.put( address, roomName );
    return PolicyUtils.createClientAgent( address, client, serverAddress  );
  }

  
  @Override
  protected void writeLogLine(String s) {
    super.writeLogLine(s);
  }

  @Override
  public synchronized void processHouseArchitecture(FileMessage info) throws RemoteException {
    super.processHouseArchitecture(info);
    setup();
    
  }


  @Override
  public void deviceInfoChanged(AbstractAddress address, DeviceInfo deviceInfo) {
    // Ignore; not important..
  }

  @Override
  public void activePolicyChanged(AbstractAddress address, Policy policy) {
    String roomName = addressMap.get(address);
    RoomState rs = rooms.get( roomName );
    if ( policy == null ) {
      rs.setAdditionalInfo( rs.getDirectControl() + ":" );
      policies.put( roomName, null ); 
    } else {
      HCPolicy hcPolicy = ((HCPolicy)policy);
      rs.setAdditionalInfo( rs.getDirectControl() + ":" + hcPolicy.getInfo() );
      policies.put( roomName, hcPolicy ); 
    }
  }


  public class HeaterDataRMIServer implements HeaterDataAPI {

    public HeaterDataRMIServer() {
      super();
      try {
        String hostName = InetAddress.getLocalHost().getHostName();
        RMIHelper.register(this, 1099, HeaterDataAPI.SERVICE_NAME, hostName );
      } catch (UnknownHostException e) {
        throw new IllegalStateException("Could not determine my own host name");
      }
    }

    @Override
    public int getNumberOfRooms() throws RemoteException {
      return roomNames.size();
    }
    
    @Override
    public double getFrequency() throws RemoteException {
      return eventSourcesLogger.getFrequency();
    }
    
    @Override
    public double getPowerPrice() throws RemoteException {
      return eventSourcesLogger.getPowerPrice();
    }

    @Override
    public double getPowerConsumption() throws RemoteException {
      return eventSourcesLogger.getPowerConsumption();
    }

    @Override
    public RoomInformation[] getRoomData() throws RemoteException {
      RoomInformation[] result = new RoomInformation[ roomNames.size() ];
      int i = 0;
      for (String roomName : roomNames) {
        result[i] = new RoomInformation( policies.get( roomName ), rooms.get( roomName ) );
        i++;
      }
      return result;
    }

  }

  public class HeaterControlRMIServer implements HeaterControlAPI {

    public HeaterControlRMIServer() {
      super();
      try {
        String hostName = InetAddress.getLocalHost().getHostName();
        RMIHelper.register(this, 1099, HeaterControlAPI.SERVICE_NAME, hostName );
      } catch (UnknownHostException e) {
        throw new IllegalStateException("Could not determine my own host name");
      }
    }

    @Override
    public void switchHeater(String roomName, boolean on) throws RemoteException {
      FlexhouseHeaterController.this.switchHeatersInRoom(roomName, on);
    }

    @Override
    public void changeThermostatSetpoint(String roomName, double setpoint) throws RemoteException {
      FlexhouseHeaterController.this.setSetPoint(roomName, setpoint);
    }

  }

  
  private class EventSourcesLogger implements Runnable {

    private FlexhouseSystemFrequency systemFrequency;
    private DanishPowerPrice powerPrice;
    private PrintStream dataLogger;
    private double freq;
    private double price;
    private double power;

    public EventSourcesLogger(FlexhouseSystemFrequency systemFrequency, DanishPowerPrice powerPrice) {
      this.systemFrequency = systemFrequency;
      this.powerPrice = powerPrice;
      try {
        this.dataLogger = new PrintStream( new FileOutputStream( SOURCE_DATA_LOGFILE, true ) );
      } catch (FileNotFoundException e) {
        throw new AssertionError( SOURCE_DATA_LOGFILE + ": " + e.getMessage() );
      }
    }

    @Override
    public void run() {
      while ( ! Thread.interrupted() ) {
        synchronized(this) {
          freq = systemFrequency.getValue();
          price = powerPrice.getValue();
          power = Double.NaN;
          try {
            HouseDataRequest powerReq = new HouseDataRequest( 
                HWConstants.OUTSIDE_ROOMNAME, new DeviceAddress( "deif-power", 0 ), "state" );
            power = rmiControllerIServer.requestData( powerReq );
          } catch (RemoteException e) {
            // TODO: reconnect to rmiControllerIServer?
            e.printStackTrace();
          }
        }

        String s = String.format("%d;%.2f;%.2f;%.2f\n", System.currentTimeMillis(), freq, price, power );
        try {
          dataLogger.write( s.getBytes() );
        } catch (IOException e) {
          e.printStackTrace();
        }
        
        try {
          Thread.sleep( 5*1000 );
        } catch (InterruptedException e) {
          break;
        }
      }
    }

    public void start() {
      new Thread( this ).start();
    }

    public synchronized double getFrequency() {
      return freq;
    }

    public synchronized double getPowerPrice() {
      return price;
    }

    public synchronized double getPowerConsumption() {
      return power;
    }
    
  }

}
