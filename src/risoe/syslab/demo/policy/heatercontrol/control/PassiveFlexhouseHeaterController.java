package risoe.syslab.demo.policy.heatercontrol.control;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;

import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.demo.policy.heatercontrol.events.DanishPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.events.FlexhouseSystemFrequency;
import risoe.syslab.demo.policy.syslabdemo1.RMIHelper;
import risoe.syslab.flexhouse.common.DeviceAddress;
import risoe.syslab.flexhouse.controller.ThermostaticController2;
import risoe.syslab.flexhouse.hw.FileMessage;
import risoe.syslab.flexhouse.hw.HWConstants;
import risoe.syslab.flexhouse.hw.HouseDataRequest;

/**
 * Comparison flexhouse controller for the heatercontrol policy example.
 * This one does not use policy.based control, but just receives commands 
 * from a central controller.
 * 
 * @author daku
 *
 */
public class PassiveFlexhouseHeaterController extends ThermostaticController2 {

  public static final int NUM_HEATERS = 10;
  private static final String SOURCE_DATA_LOGFILE = "source-data.log";

  private EventSourcesLogger eventSourcesLogger;

  public PassiveFlexhouseHeaterController() throws RemoteException {
    super(20.0f, 0.5);
  }

  private void setup() {
    
    try {
      System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
      System.setProperty( "log4j.configuration", "conf/log4j.properties" );
      PolicyUtils.setup( "conf/policy-framework.properties" );

      FlexhouseSystemFrequency systemFrequency = new FlexhouseSystemFrequency( this );
      DanishPowerPrice powerPrice = new DanishPowerPrice();
      eventSourcesLogger = new EventSourcesLogger( systemFrequency, powerPrice );
      eventSourcesLogger.start();
      
      new HeaterDataRMIServer();
      new HeaterControlRMIServer();
      
    } catch (NumberFormatException e) {
      throw new AssertionError( e );
    }
  }

  @Override
  protected void writeLogLine(String s) {
    super.writeLogLine(s);
  }

  @Override
  public synchronized void processHouseArchitecture(FileMessage info) throws RemoteException {
    super.processHouseArchitecture(info);
    setup();
    
  }


  public interface HeaterPutInfoAPI {
    void putRoomInfo( RoomInformation[] roomInfo );
  };
  
  public class HeaterDataRMIServer implements HeaterDataAPI, HeaterPutInfoAPI {

    RoomInformation[] roomInfo;
    
    public HeaterDataRMIServer() {
      super();
      roomInfo = new RoomInformation[ roomNames.size() ];
      try {
        String hostName = InetAddress.getLocalHost().getHostName();
        RMIHelper.register(this, 1099, HeaterDataAPI.SERVICE_NAME, hostName );
      } catch (UnknownHostException e) {
        throw new IllegalStateException("Could not determine my own host name");
      }
    }

    @Override
    public int getNumberOfRooms() throws RemoteException {
      return roomNames.size();
    }
    
    @Override
    public double getFrequency() throws RemoteException {
      return eventSourcesLogger.getFrequency();
    }
    
    @Override
    public double getPowerPrice() throws RemoteException {
      return eventSourcesLogger.getPowerPrice();
    }

    @Override
    public double getPowerConsumption() throws RemoteException {
      return eventSourcesLogger.getPowerConsumption();
    }

    @Override
    public RoomInformation[] getRoomData() throws RemoteException {
      return roomInfo;
    }

    @Override
    public void putRoomInfo(RoomInformation[] roomInfo) {
      this.roomInfo = roomInfo.clone();
      // TODO Put temperature data into the array
      
    }

  }

  public class HeaterControlRMIServer implements HeaterControlAPI {

    public HeaterControlRMIServer() {
      super();
      try {
        String hostName = InetAddress.getLocalHost().getHostName();
        RMIHelper.register(this, 1099, HeaterControlAPI.SERVICE_NAME, hostName );
      } catch (UnknownHostException e) {
        throw new IllegalStateException("Could not determine my own host name");
      }
    }

    @Override
    public void switchHeater(String roomName, boolean on) throws RemoteException {
      PassiveFlexhouseHeaterController.this.switchHeatersInRoom(roomName, on);
    }

    @Override
    public void changeThermostatSetpoint(String roomName, double setpoint) throws RemoteException {
      PassiveFlexhouseHeaterController.this.setSetPoint(roomName, setpoint);
    }

  }

  
  private class EventSourcesLogger implements Runnable {

    private FlexhouseSystemFrequency systemFrequency;
    private DanishPowerPrice powerPrice;
    private PrintStream dataLogger;
    private double freq;
    private double price;
    private double power;

    public EventSourcesLogger(FlexhouseSystemFrequency systemFrequency, DanishPowerPrice powerPrice) {
      this.systemFrequency = systemFrequency;
      this.powerPrice = powerPrice;
      try {
        this.dataLogger = new PrintStream( new FileOutputStream( SOURCE_DATA_LOGFILE, true ) );
      } catch (FileNotFoundException e) {
        throw new AssertionError( SOURCE_DATA_LOGFILE + ": " + e.getMessage() );
      }
    }

    @Override
    public void run() {
      while ( ! Thread.interrupted() ) {
        synchronized(this) {
          freq = systemFrequency.getValue();
          price = powerPrice.getValue();
          power = Double.NaN;
          try {
            HouseDataRequest powerReq = new HouseDataRequest( 
                HWConstants.OUTSIDE_ROOMNAME, new DeviceAddress( "deif-power", 0 ), "state" );
            power = rmiControllerIServer.requestData( powerReq );
          } catch (RemoteException e) {
            e.printStackTrace();
          }
        }

        String s = String.format("%d;%.2f;%.2f;%.2f\n", System.currentTimeMillis(), freq, price, power );
        try {
          dataLogger.write( s.getBytes() );
        } catch (IOException e) {
          e.printStackTrace();
        }
        
        try {
          Thread.sleep( 5*1000 );
        } catch (InterruptedException e) {
          break;
        }
      }
    }

    public void start() {
      new Thread( this ).start();
    }

    public synchronized double getFrequency() {
      return freq;
    }

    public synchronized double getPowerPrice() {
      return price;
    }

    public synchronized double getPowerConsumption() {
      return power;
    }
    
  }

}
