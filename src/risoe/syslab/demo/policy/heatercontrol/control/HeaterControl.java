package risoe.syslab.demo.policy.heatercontrol.control;

import risoe.syslab.control.policy.rules.Controller;

/** Abstract parent of all classes that implement heater control, 
 * either per thermostat or per direct switching heaters on or off.
 * 
 * This class is used by all top-level controllers, like
 * {@link DynamicPowerPrice} or {@link FreqHiLoThresholds}.
 * 
 * @author daku
 */
public abstract class HeaterControl {

  public abstract void decreaseTemperature( String message );

  public abstract void increaseTemperature( String message );

  public abstract void switchHeater( boolean on, String message );

  public abstract boolean active();
  
  /** Use a different controller.
   * 
   * This controller is {@link Controller#start()}ed, and 
   * a previously running controller is stopped.
   * 
   * @param controller new controller to use
   */
  public abstract void use( Controller controller );

  /** Method that is used for creating rule bases. 
   * This method returns the constraint for the "when" part of a rule referencing this controller. 
   */
  public static String getActiveTest() {
    return "value != java.lang.Double.NaN";
  }

  /** Simple getter for value field */
  public double getValue() {
    // Does not do anything for now..
    return 1.0;
  }

}
