package risoe.syslab.demo.policy.heatercontrol.control;

import risoe.syslab.control.policy.core.StateChangeListener;
import risoe.syslab.control.policy.core.ThresholdRunner;
import risoe.syslab.control.policy.core.ThresholdRunner.State;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.rules.IdGenerator;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.demo.policy.heatercontrol.events.EventSourceUtil;
import risoe.syslab.demo.policy.heatercontrol.events.PowerPrice;

/** Implements a threshold-based dynamic power price controller. 
 * 
 * @author daku
 */
public class DynamicPowerPrice implements Controller, StateChangeListener<State> {

  Thresholds thresholds;
  ThresholdRunner runner;
  private HeaterControl heater;
  private volatile boolean started;
  

  /** Standard constructor */
  public DynamicPowerPrice(Thresholds thresholds, EventSource source, HeaterControl heater ) {
    super();
    this.thresholds = thresholds;
    this.heater = heater;
    this.started = false;
    this.runner = new ThresholdRunner(thresholds, source, this);
  }

  /** Returns the service provided by this controller */
  @Override
  public Service getProvidedService() {
    return HCServices.dppService;
  }

  @Override
  public boolean wantsToBeActivated() {
    boolean result = ! runner.getState().equals( ThresholdRunner.State.NORMAL );
    return result;
  }

  @Override
  public void start() {
    this.started = true;
    runner.start();
    // The state change has to be re-emitted; it was lost because this controller was not started yet.. 
    stateChanged( runner.getState() );
  }

  @Override
  public void stop() {
    this.started = false;
    runner.interrupt();
  }

  @Override
  public void stateChanged(State state) {
    if ( ! started ) {
      return;
    }
    switch (state ) {
    case NORMAL:
      break;
    case HIGH:
      heater.decreaseTemperature("high p");
      break;
    case LOW:
      heater.increaseTemperature("low p");
      break;
    default:
      throw new IllegalArgumentException("unsupported state: " + state);
    }
  }

  /** Method that is used for creating rule bases. 
   * This method returns the constraint for the "when" part of a rule referencing this controller. 
   */
  public static String getActiveTestString() {
    return "wantsToBeActivated == true";
  }

  /** Method that is used for creating rule bases. This method returns the code for the "setup" rule. */
  public static String[] getSetup( double low, double high ) {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String ctrlName = "ctrl" + gen.next();
    String sourceName = "source" + gen.next();
    return new String[] {
        ctrlName + " : " + HeaterControl.class.getName() + "( " + HeaterControl.getActiveTest() + ") and\n    " +
        sourceName + " : " + PowerPrice.class.getName() + "( " + EventSourceUtil.getActiveTest() + " )",
        "new " + DynamicPowerPrice.class.getName() + 
        "( new " + Thresholds.class.getName() + "(" + low + ", " + high + "), " + sourceName + ", " + ctrlName + ")"
    };
  }

}