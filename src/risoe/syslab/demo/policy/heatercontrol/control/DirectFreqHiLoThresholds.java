package risoe.syslab.demo.policy.heatercontrol.control;

import risoe.syslab.control.policy.core.StateChangeListener;
import risoe.syslab.control.policy.core.ThresholdRunner;
import risoe.syslab.control.policy.core.ThresholdRunner.State;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.rules.IdGenerator;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.demo.policy.heatercontrol.events.SystemFrequency;

/** Implementing a low/high threshold frequency controller
 * with direct control of heaters.
 * 
 *  @author daku
 */
public class DirectFreqHiLoThresholds implements Controller, StateChangeListener<State> {

  final private Thresholds thresholds;
  final private ThresholdRunner runner;
  final private HeaterControl heater;
  private volatile boolean started;
  

  public DirectFreqHiLoThresholds(Thresholds thresholds, EventSource source, HeaterControl heater) {
    super();
    this.thresholds = thresholds;
    this.heater = heater;
    this.started = false;
    runner = new ThresholdRunner(thresholds, source, this);
  }

  @Override
  public boolean wantsToBeActivated() {
    boolean result = ! runner.getState().equals( ThresholdRunner.State.NORMAL );
    System.out.println( "wtba " + result + " " + getClass().getName() );
    return result;
  }

  @Override
  public void start() {
    this.started = true;
    runner.start();
    // The state change has to be re-emitted; it was lost because this controller was not started yet.. 
    stateChanged( runner.getState() );
  }

  @Override
  public void stop() {
    this.started = false;
    runner.interrupt();
  }

  @Override
  public Service getProvidedService() {
    return HCServices.dfctService;
  }

  @Override
  public void stateChanged(State state) {
    if ( ! started ) {
      return;
    }
    switch (state ) {
    case NORMAL:
      break;
    case HIGH:
      heater.switchHeater( true, "high f");
      break;
    case LOW:
      heater.switchHeater( false, "low f");
      break;
    default:
      throw new IllegalArgumentException("unsupported state: " + state);
    }
  }

  public Thresholds getThresholds() {
    return thresholds;
  }

  public static String getActiveTestString() {
    return "wantsToBeActivated == true";
  }

  public static String[] getSetup( double low, double high ) {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String ctrlName = "ctrl" + gen.next();
    String sourceName = "source" + gen.next();
    return new String[] {
        ctrlName + " : " + HeaterControl.class.getName() + "() and\n    " +
        sourceName + " : " + SystemFrequency.class.getName() + "()",
        "new " + DirectFreqHiLoThresholds.class.getName() + 
        "( new " + Thresholds.class.getName() + "(" + low + ", " + high + "), " + sourceName + ", " + ctrlName + ")"
    };
  }
}
