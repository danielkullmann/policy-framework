package risoe.syslab.demo.policy.heatercontrol.control;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.demo.policy.heatercontrol.DirectControlServer;


/**
 * Slow Flexhouse controller for the heatercontrol policy example.
 * This controller only changes setpoints every 2 hours max.
 * This is to show that a system with similar features like ours,
 * but without local intelligence, would perform much worse.
 *
 * This class has been pretty much made obsolete by {@link DirectControlServer}.
 * 
 * @author daku
 */
public class SlowFlexhouseHeaterController extends FlexhouseHeaterController {

  private static final Logger logger = Logger.getLogger( SlowFlexhouseHeaterController.class.getName() );

  private HashMap<String, Long> lastChanges;

  public SlowFlexhouseHeaterController() throws RemoteException {
    super();
  }

  @Override
  public void setSetPoint(String roomName, double setPoint) {

    // Wait until house state has been read from FLEXHOUSE.xml 
    if ( ! house.contains( roomName ) ) {
      return;
    }
    
    boolean change = false;
    long now = System.currentTimeMillis();
    if ( lastChanges.containsKey( roomName ) ) {
      long last = lastChanges.get( roomName );
      // Allow setpoint change only every two hours
      if ( last + 2*3600*1000 < now ) {
        change = true;
        logger.log( Level.INFO, "do change!" );
      }
    } else {
      change = true;
    }
    
    if ( change ) {
      super.setSetPoint(roomName, setPoint);
      lastChanges.put( roomName, now );
    }
    
    
  }

  

}
