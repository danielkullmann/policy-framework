package risoe.syslab.demo.policy.heatercontrol.control;

import risoe.syslab.control.policy.core.StateChangeListener;
import risoe.syslab.control.policy.core.ThresholdRunner;
import risoe.syslab.control.policy.core.ThresholdRunner.State;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.rules.IdGenerator;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.demo.policy.heatercontrol.events.EventSourceUtil;
import risoe.syslab.demo.policy.heatercontrol.events.SystemFrequency;

/** Implementing a low/high threshold frequency controller,
 * with thermostat control of heaters.
 * 
 *  @author daku
 */
public class FreqHiLoThresholds implements Controller, StateChangeListener<State> {

  final private Thresholds thresholds;
  final private ThresholdRunner runner;
  final private HeaterControl heater;
  private volatile boolean started;
  

  /** Standard constructor */
  public FreqHiLoThresholds(Thresholds thresholds, EventSource source, HeaterControl heater) {
    super();
    this.thresholds = thresholds;
    this.heater = heater;
    this.started = false;
    runner = new ThresholdRunner(thresholds, source, this);
  }

  @Override
  public boolean wantsToBeActivated() {
    boolean result = ! runner.getState().equals( ThresholdRunner.State.NORMAL );
    return result;
  }

  @Override
  public void start() {
    this.started = true;
    runner.start();
    // The state change has to be re-emitted; it was lost because this controller was not started yet.. 
    stateChanged( runner.getState() );
  }

  @Override
  public void stop() {
    this.started = false;
    runner.interrupt();
  }

  /** Returns the service provided by this controller */
  @Override
  public Service getProvidedService() {
    return HCServices.fctService;
  }

  @Override
  public void stateChanged(State state) {
    if ( ! started ) {
      return;
    }
    switch (state ) {
    case NORMAL:
      break;
    case HIGH:
      heater.increaseTemperature("high f");
      break;
    case LOW:
      heater.decreaseTemperature("low f");
      break;
    default:
      throw new IllegalArgumentException("unsupported state: " + state);
    }
  }

  /** Simple getter for the thresholds fields */
  public Thresholds getThresholds() {
    return thresholds;
  }

  /** Method that is used for creating rule bases. 
   * This method returns the constraint for the "when" part of a rule referencing this controller. 
   */
  public static String getActiveTestString() {
    return "wantsToBeActivated == true";
  }

  /** Method that is used for creating rule bases. This method returns the code for the "setup" rule. */
  public static String[] getSetup( double low, double high ) {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String ctrlName = "ctrl" + gen.next();
    String sourceName = "source" + gen.next();
    return new String[] {
        ctrlName + " : " + HeaterControl.class.getName() + "( " + HeaterControl.getActiveTest() + ") and\n    " +
        sourceName + " : " + SystemFrequency.class.getName() + "( " + EventSourceUtil.getActiveTest() + " )",
        "new " + FreqHiLoThresholds.class.getName() + 
        "( new " + Thresholds.class.getName() + "(" + low + ", " + high + "), " + sourceName + ", " + ctrlName + ")"
    };
  }
}
