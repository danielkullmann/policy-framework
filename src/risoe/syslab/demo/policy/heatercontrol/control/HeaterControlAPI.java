package risoe.syslab.demo.policy.heatercontrol.control;

import java.rmi.Remote;
import java.rmi.RemoteException;

/** API declaration for the control of heaters in rooms.
 * 
 * It is implemented in {@link FlexhouseHeaterController}.
 *  
 * @author daku
 */
public interface HeaterControlAPI extends Remote {

  /** Name of the exported RMI service */
  static final String SERVICE_NAME = HeaterControlAPI.class.getName();
  
  /** Switches all heaters in a room on or off.
   * 
   * @param roomName room in which to switch heaters
   * @param on whether to switch the heaters on.
   * @throws RemoteException
   */
  void switchHeater(String roomName, boolean on) throws RemoteException;

  /** Set the thermostta setpoint of a single room
   * 
   * @param roomName room for which to change the setpoint
   * @param setpoint the new setpoint
   * @throws RemoteException
   */
  void changeThermostatSetpoint(String roomName, double setpoint) throws RemoteException;
  
}
