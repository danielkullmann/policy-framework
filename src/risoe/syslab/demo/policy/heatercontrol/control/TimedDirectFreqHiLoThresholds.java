package risoe.syslab.demo.policy.heatercontrol.control;

import risoe.syslab.control.policy.core.StateChangeListener;
import risoe.syslab.control.policy.core.ThresholdRunner;
import risoe.syslab.control.policy.core.ThresholdRunner.State;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.core.TimeWindow;
import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.rules.IdGenerator;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.demo.policy.heatercontrol.events.EventSourceUtil;
import risoe.syslab.demo.policy.heatercontrol.events.SystemFrequency;

/** Implementing a low/high threshold frequency controller
 * with timed direct control of heaters, i.e. the direct control
 * applies only to certain seconds per hour.
 * 
 *  @author daku
 */
public class TimedDirectFreqHiLoThresholds implements Controller, StateChangeListener<State> {

  final private Thresholds thresholds;
  final private ThresholdRunner runner;
  final private HeaterControl heater;
  final private TimeWindow timeWindow;
  private volatile boolean started;
  

  public TimedDirectFreqHiLoThresholds(Thresholds thresholds, TimeWindow timeWindow, EventSource source, HeaterControl heater) {
    super();
    this.thresholds = thresholds;
    this.timeWindow = timeWindow;
    this.heater = heater;
    this.started = false;
    runner = new ThresholdRunner(thresholds, source, this);
  }

  @Override
  public boolean wantsToBeActivated() {
    if ( ! timeWindow.isCurrent() ) return false;
    boolean result = ! runner.getState().equals( ThresholdRunner.State.NORMAL );
    return result;
  }

  @Override
  public void start() {
    this.started = true;
    runner.start();
    // The state change has to be re-emitted; it was lost because this controller was not started yet.. 
    stateChanged( runner.getState() );
  }

  @Override
  public void stop() {
    this.started = false;
    runner.interrupt();
  }

  @Override
  public Service getProvidedService() {
    return HCServices.dfctService;
  }

  @Override
  public void stateChanged(State state) {
    if ( ! started ) {
      return;
    }
    switch (state ) {
    case NORMAL:
      break;
    case HIGH:
      heater.switchHeater( true, "high f");
      break;
    case LOW:
      heater.switchHeater( false, "low f");
      break;
    default:
      throw new IllegalArgumentException("unsupported state: " + state);
    }
  }

  public Thresholds getThresholds() {
    return thresholds;
  }

  public static String getActiveTestString() {
    return "wantsToBeActivated == true";
  }

  public static String[] getSetup( double low, double high, TimeWindow timeWindow ) {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String ctrlName = "ctrl" + gen.next();
    String sourceName = "source" + gen.next();
    return new String[] {
        ctrlName + " : " + HeaterControl.class.getName() + "( " + HeaterControl.getActiveTest() + ") and\n    " +
        sourceName + " : " + SystemFrequency.class.getName() + "( " + EventSourceUtil.getActiveTest() + " )",
        "new " + TimedDirectFreqHiLoThresholds.class.getName() + 
        "( " + getThresholdConstructorCall(low, high) + ", " + 
        getTimeWindowConstructorCall(timeWindow) + ", " + 
        sourceName + ", " + ctrlName + ")"
    };
  }

  public static String getThresholdConstructorCall( double low, double high ) {
    return "new " + Thresholds.class.getName() + "(" + low + ", " + high + ")";
  }

  public static String getTimeWindowConstructorCall( TimeWindow tw ) {
    return "new " + TimeWindow.class.getName() + "( " +
      tw.getFrame() + ", " + tw.getStartTime() + ", " + tw.getLength() +
      " )";
  }
  
  public TimeWindow getTimeWindow() {
    return timeWindow;
  }
}
