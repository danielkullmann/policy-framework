package risoe.syslab.demo.policy.heatercontrol.control;

import java.rmi.Remote;
import java.rmi.RemoteException;

/** API declaration for the collection of data about the rooms
 * in Flexhouse, for the purpose of thermostat or direct control
 * 
 * It is implemented in {@link FlexhouseHeaterController}.
 *  
 * @author daku
 */

public interface HeaterDataAPI extends Remote {

  /** Name of the exported RMI service */
  static final String SERVICE_NAME = HeaterDataAPI.class.getName();
  
  /** Get the number of rooms informatrion is available for.
   * 
   * For Flexhouse, this is 8 (main hall plus seven rooms).
   * 
   * @return the number of rooms
   * 
   * @throws RemoteException
   */
  int getNumberOfRooms() throws RemoteException;

  /** Get information about every room
   * 
   * @return array of {@link RoomInformation} objects
   * @throws RemoteException
   */
  public abstract RoomInformation[] getRoomData() throws RemoteException;

  /** Get the current power price.
   * 
   * @return power price [-]
   * @throws RemoteException
   */
  public abstract double getPowerPrice() throws RemoteException;

  /** Get the current system frequency at the point of common oupling
   * 
   * @return frequency [Hz]
   * @throws RemoteException
   */
  public abstract double getFrequency() throws RemoteException;

  /** Get the current consumption of power of Flexhouse.
   * 
   * @return power consumption [W]
   * @throws RemoteException
   */
  public abstract double getPowerConsumption() throws RemoteException;

}
