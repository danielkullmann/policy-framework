package risoe.syslab.demo.policy.heatercontrol.control;

import java.io.Serializable;

import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.demo.policy.heatercontrol.HCPolicy;
import risoe.syslab.flexhouse.controller.ThermostaticController2.RoomState;

/** Container class for data interesting for visualisation */
public class RoomInformation implements Serializable {

  /** generated serial UID */
  private static final long serialVersionUID = 3629997417530713587L;
  
  private Thresholds freqCtrl;
  private Thresholds priceCtrl;
  private double temperature;
  private double setpoint;
  
  public RoomInformation( HCPolicy policy, RoomState roomState ) {
    if ( policy != null ) {
      freqCtrl = policy.getFrequencyCtrl();
      priceCtrl = policy.getPriceCtrl();
    } else {
      freqCtrl = new Thresholds(50, 50);
      priceCtrl = new Thresholds(60, 60);
    }
    if ( roomState != null ) {
      temperature = roomState.getTemperature();
      setpoint = roomState.getSetpoint();
    } else {
      temperature = 0.0;
      setpoint = 20.0;
    }
  }

  
  public RoomInformation(Thresholds freqCtrl, Thresholds priceCtrl, RoomState roomState) {
    this.freqCtrl = freqCtrl;
    this.priceCtrl = priceCtrl;
    if ( roomState != null ) {
      temperature = roomState.getTemperature();
      setpoint = roomState.getSetpoint();
    } else {
      temperature = 0.0;
      setpoint = 20.0;
    }
  }


  public Thresholds getFreqCtrl() {
    return freqCtrl;
  }

  public Thresholds getPriceCtrl() {
    return priceCtrl;
  }

  public double getTemperature() {
    return temperature;
  }

  public double getSetpoint() {
    return setpoint;
  }
  
  
}
