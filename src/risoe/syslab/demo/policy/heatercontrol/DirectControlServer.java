package risoe.syslab.demo.policy.heatercontrol;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.PolicyServer.ClientStatus;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.core.ThresholdRunner;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControlAPI;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterDataAPI;
import risoe.syslab.demo.policy.heatercontrol.control.RoomInformation;
import risoe.syslab.demo.policy.heatercontrol.control.PassiveFlexhouseHeaterController.HeaterPutInfoAPI;
import risoe.syslab.flexhouse.controller.ThermostaticController2.RoomState;

/** This is the anti-thesis controller for policy-based control,
 * because it does not use policies (at least not as intended; 
 * HCPlanner does generate policies, so we have to use them).
 * 
 * The controller controls the heaters of Flexhouse remotely,
 * by measuring frquency and power price here, and sending the
 * appropriate commands directly to the heaters.
 * 
 * @author daku
 *
 */
public class DirectControlServer {

  private static final Logger logger = Logger.getLogger( DirectControlServer.class.getName() );

  public static final String DATA_SERVER_URI = "//flexhouse:1099/" + HeaterDataAPI.SERVICE_NAME;
  public static final String CONTROL_SERVER_URI = "//flexhouse:1099/" + HeaterControlAPI.SERVICE_NAME;

  /** plz switch back to false when in production... */
  private static boolean DEBUG = true;
  static {
    if (DEBUG) {
      logger.log( Level.WARNING, "Running in DEBUG mode!" );
    }
  }
  
  private static final double LOW_THERMOSTAT_SETTING = 19.0;
  private static final double DEFAULT_THERMOSTAT_SETTING = 20.0;
  private static final double HIGH_THERMOSTAT_SETTING = 21.0;
  private HeaterDataAPI dataServer;
  private HeaterPutInfoAPI infoServer = null;
  private HeaterControlAPI controlServer;
  InternalServer policyServer;
  
  private int count = 0;
  
  /** Default constructor, runs the controller. 
   */
  public DirectControlServer() {

    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );

    connectToDataServer();
    connectToControlServer();

    policyServer = new InternalServer();
    HCPlanner planner = new HCPlanner( policyServer );
    new Thread( planner ).start();

    run();
  }

  /** Method that connects to the control server (HeaterControlAPI).
   */
  private void connectToControlServer() {
    if ( DEBUG ) {
      controlServer = new DebugController();
      return;
    }
    try {
      controlServer = (HeaterControlAPI) Naming.lookup( CONTROL_SERVER_URI );
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (RemoteException e) {
      e.printStackTrace();
    } catch (NotBoundException e) {
      e.printStackTrace();
    }
  }

  /** Method that connects to the data server (HeaterDataAPI).
   */
  private void connectToDataServer() {
    try {
      dataServer = (HeaterDataAPI) Naming.lookup( DATA_SERVER_URI );
      try {
        infoServer = (HeaterPutInfoAPI) dataServer;
      } catch (ClassCastException e) {
        logger.log( Level.WARNING, "no HeaterPutInfoAPI available", e );
      }
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (RemoteException e) {
      e.printStackTrace();
    } catch (NotBoundException e) {
      e.printStackTrace();
    }
  }

  /** Worker method of the controller.
   */
  private void run() {
    while ( true ) {
      
      if ( dataServer == null )
        connectToDataServer();
      
      if ( dataServer != null ) {
        try {
          // Determine power price
          double price = dataServer.getPowerPrice();
          // Determine frequency
          double frequency = dataServer.getFrequency();
          
          if ( count ++ % 100 == 0 ) {
            logger.log( Level.FINE, String.format( "%.2f  %.2f", price , frequency ) );
          }
          
          Collection<ClientData> clients = policyServer.getClients();
          RoomInformation[] roomInfo = new RoomInformation[ clients.size() ];
          int idx = -1;
          
          // Go through list of clients:
          for (ClientData client : clients) {
            idx++;
            HCPolicy policy = (HCPolicy) client.getActivePolicy();
            if ( policy == null ) {
              continue;
            }
            AbstractAddress address = client.getClientAddress();
            String roomName = address.getName();
            
            Thresholds ft = new Thresholds(50, 50);
            Thresholds pt = new Thresholds(60, 50);
  
            // - check thresholds for frequency and power price
            // - act accordingly
            ThresholdRunner fr = policyServer.getFrequencyThresholdRunner( address, policy );
            boolean handled = false;

            if ( fr != null ) {
              ft = fr.getThreshold();
              fr.injectValue( frequency );
              fr.switchState();
              
              switch ( fr.getState() ) {
              case NORMAL:
                // Gets handled in the price handling part
                break;
              case HIGH:
                handled = true;
                switchHeater( roomName, true );
                break;
              case LOW:
                handled = true;
                switchHeater( roomName, false );
                break;
              default:
                throw new IllegalStateException( "" + fr.getState() );
              }
            } else {
              HCDeviceInfo di = policyServer.getClient( address ).getDeviceInfo();
              if ( di.getServices().supports( HCServices.fctService ) ) {
                logger.log( Level.WARNING, "no frequency control for room " + roomName );
              }
            }
  
            ThresholdRunner pr = policyServer.getPriceThresholdRunner( address, policy );
            if ( pr != null ) {
              pt = pr.getThreshold();
            }
            
            double setpoint = Double.NaN;
            
            if ( pr != null ) {
              pr.injectValue( price );
              pr.switchState();
              
              //logger.log( Level.FINE, roomName + " " + price + " " + policy.getPriceCtrl() + " " + pr.getState() );

              switch ( pr.getState() ) {
              case NORMAL:
                setpoint = DEFAULT_THERMOSTAT_SETTING;
                break;
              case HIGH:
                setpoint = LOW_THERMOSTAT_SETTING;
                break;
              case LOW:
                setpoint = HIGH_THERMOSTAT_SETTING;
                break;
              default:
                throw new IllegalStateException( "" + pr.getState() );
              }
              if ( ! handled ) changeThermostat( roomName, setpoint );
            } else {
              logger.log( Level.WARNING, "no price control for room " + roomName );
            }
            
            roomInfo[idx] = new RoomInformation( ft, pt, new RoomState( setpoint, Double.NaN) );
          }
          
          infoServer.putRoomInfo(roomInfo);
            
        } catch (RemoteException e) {
          e.printStackTrace();
          dataServer = null;
        }
      }
      
      try {
        Thread.sleep( 500 );
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        break;
      }
    }
  }

  /** Switches all heaters in a romm on or off.
   * 
   * @param roomName room in which to sswitch all the heaters
   * @param on whether to switch the heaters on
   */
  private void switchHeater(String roomName, boolean on) {
    if ( controlServer == null ) {
      connectToControlServer();
    }
    if ( controlServer == null ) return;
    // Disable thermostat control
    changeThermostat(roomName, Double.NaN);
    if ( controlServer == null ) return;
    try {
      controlServer.switchHeater(roomName, on);
    } catch (RemoteException e) {
      controlServer = null;
    }
  }

  /** Set the thermostat setpoint of a room
   * 
   * @param roomName room for which to set the setpoint
   * @param setpoint new setpoint
   */
  private void changeThermostat(String roomName, double setpoint) {
    if ( controlServer == null ) {
      connectToControlServer();
    }
    if ( controlServer == null ) return;
    try {
      controlServer.changeThermostatSetpoint(roomName, setpoint);
    } catch (RemoteException e) {
      controlServer = null;
    }
  }

  
  /** Java application entry point 
   * @param args
   */
  public static void main(String[] args) {
    new DirectControlServer();
  }

  /** This is the implementation of {@link HCServerBehaviour}
   * for this direct control server. It is kind of a kludge, because the 
   * original system is not meant to be used in the direct-control way.
   * 
   * @author daku
   *
   */
  private static class InternalServer implements HCServerBehaviour {

    /** All rooms in Flexhouse */
    private static final String[] roomNames = {
      "main_hall",
      "room1",
      "room2",
      "room3",
      "room4",
      "room5",
      "room6",
      "room7",
    };
    
    /** Information about the rooms */
    ArrayList<ClientData> clients = new ArrayList<ClientData>();

    /** runners for the frequency control */
    private HashMap<AbstractAddress,ThresholdRunner> frequencyRunners = new HashMap<AbstractAddress, ThresholdRunner>();

    /** runners for the price control */
    private HashMap<AbstractAddress,ThresholdRunner> priceRunners = new HashMap<AbstractAddress, ThresholdRunner>();
    
    /** Default constructor */
    public InternalServer() {

      Random r = new Random();
      int id=0;
      
      for ( String roomName : roomNames ) {
        AbstractAddress clientAddress = new InternalAddress(roomName);
        ClientData clientData = new ClientData(clientAddress);
        ServiceList services = new ServiceList( null );
        services.addService( HCServices.dppService );
        // Bit more than half of the clients support frequency control...
        if ( r.nextInt( 10 ) < 6 ) services.addService( HCServices.fctService );
        HCDeviceInfo deviceInfo = new HCDeviceInfo(id++, services);
        clientData.setDeviceInfo( deviceInfo  );
        
        clients.add( clientData );
      }
    }

    public ThresholdRunner getFrequencyThresholdRunner( AbstractAddress address, HCPolicy policy ) {
      if ( policy == null || policy.getFrequencyCtrl() == null ) return null;
      ThresholdRunner r = frequencyRunners.get( address );
      if ( r == null || ! r.getThreshold().equals( policy.getFrequencyCtrl() ) ) {
        if ( r != null ) r.interrupt();
        r = new ThresholdRunner( policy.getFrequencyCtrl(),  null );
      }
      return r;
    }

    public ThresholdRunner getPriceThresholdRunner( AbstractAddress address, HCPolicy policy ) {
      if ( policy == null || policy.getPriceCtrl() == null ) return null;
      ThresholdRunner r = priceRunners.get( address );
      if ( r == null || ! r.getThreshold().equals( policy.getPriceCtrl() ) ) {
        if ( r != null ) r.interrupt();
        r = new ThresholdRunner( policy.getPriceCtrl(),  null );
      }
      return r;
    }

    @Override
    public Collection<ClientData> getClients() {
      return clients;
    }

    @Override
    public void startRenegotiation(AbstractAddress clientAddress) {
      // nothing to do, because policy does not have to be sent to the client
    }

    @Override
    public ClientStatus getClientStatus(AbstractAddress clientAddress) {
      ClientData client = getClient(clientAddress);
      if ( client.getActivePolicy() != null )
        return ClientStatus.POLICY_ACCEPTED;
      return ClientStatus.WANTS_POLICY;
    }

    /** Internal helper method to retrieve the {@link ClientData} of a 
     * client with the given address.
     * 
     * @param clientAddress address of the client we are interested in
     * @return The matching client data
     */
    private ClientData getClient(AbstractAddress clientAddress) {
      for (ClientData client : clients) {
        if ( client.getClientAddress().equals( clientAddress ) ) {
          return client;
        }
      }
      throw new IllegalArgumentException();
    }
    
  }
  
  /** Internal implementation of {@link AbstractAddress} for the
   * purpose of using it in the direct control server.
   * 
   * @author daku
   */
  private static class InternalAddress implements AbstractAddress {

    /** room name */
    private String name;

    /** Default constructor */
    public InternalAddress(String name) {
      super();
      this.name = name;
    }

    @Override
    public int compareTo(AbstractAddress o) {
      return name.compareTo( o.getName() );
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public Object getAddress() {
      return name;
    }

    @Override
    public String toString() {
      return name; 
    }
    
    
  }

  /** Implements {@link HeaterControlAPI} for debug purposes 
   * 
   * @author daku
   */
  private static class DebugController implements HeaterControlAPI {

    private static class Data {
      boolean direct = false;
      boolean on = false;
      double setpoint = Double.NaN;
    }
    
    Map<String,Data> data = new HashMap<String, DirectControlServer.DebugController.Data>();
    
    private Data getData( String roomName ) {
      if ( data.containsKey( roomName ) ) return data.get(roomName);
      Data d = new Data();
      data.put( roomName, d );
      return d;
    }
    
    @Override
    public void switchHeater(String roomName, boolean on) throws RemoteException {
      Data data = getData(roomName);
      if ( ! data.direct || data.on != on ) {
        logger.log( Level.INFO, "direct control: " + roomName + " " + on );
        data.direct = true;
        data.on = on;
      }
    }

    @Override
    public void changeThermostatSetpoint(String roomName, double setpoint) throws RemoteException {
      Data data = getData(roomName);
      if ( data.direct || data.setpoint != setpoint ) {
        logger.log( Level.INFO, "thermostat control: " + roomName + " " + setpoint );
        data.direct = false;
        data.setpoint = setpoint;
      }
    }

  }
}
