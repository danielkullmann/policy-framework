package risoe.syslab.demo.policy.heatercontrol.sim;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;

import risoe.syslab.demo.policy.heatercontrol.events.PowerPrice;
import simulator.services.forecast.ForecastServiceDispatcher.NoForecastException;
import simulator.services.forecast.PriceForecastDispatcher;
import simulator.services.forecast.PriceForecastPacket;

/** Power price source that gets the price from class
 * implementing {@link DispatcherPriceForecast}.
 *  
 * @author daku
 */
public class FlexpowerPowerPrice implements PowerPrice {

  private static final Logger logger = Logger.getLogger( FlexpowerPowerPrice.class.getName() );
  private PriceForecastDispatcher source;
  private DateTime timestamp = null;

  public FlexpowerPowerPrice( PriceForecastDispatcher source ) {
    this.source = source;
    if (source == null) throw new AssertionError("source is null");
  }

  @Override
  public double getValue() {
    try {
      // Only interested in one value, but PriceForecastDispatcher 
      // may return an empty list if length is too small.
      // Therefore, we ask for 1 hour worth of data:
      ArrayList<PriceForecastPacket> prices = source.get( timestamp, 3600*1000, 1 );
      if (prices.size()>0) {
        return prices.get( 0 ).price;
      }
      logger.log( Level.WARNING, "no price" );
    } catch ( NoForecastException e ) {
      logger.log( Level.SEVERE, "FlexpowerPowerPrice#getValue()", e );
    }
    return Double.NaN;
  }

  public void setTimestamp( DateTime timestamp ) {
    this.timestamp = timestamp;
  }

  public PriceForecastDispatcher getSource() {
    return source;
  }
  
  

}
