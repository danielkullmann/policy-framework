/**
 * This package contains classes that are only of interest 
 * for running the heatercontrol simulation, i.e. the 
 * policy-framework integrated into the flexpower-simulation 
 * codebase. 
 * 
 * TODO integrate the flexpower controllers into the policy-framework
 */
package risoe.syslab.demo.policy.heatercontrol.sim;
