package risoe.syslab.demo.policy.heatercontrol.sim;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import models.AbstractModel;
import models.linear.thermal.ThermalModel;

import org.drools.runtime.rule.FactHandle;
import org.joda.time.DateTime;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.HCClientBehaviour;
import risoe.syslab.demo.policy.heatercontrol.HCDeviceInfo;
import risoe.syslab.demo.policy.heatercontrol.control.FlexhouseHeaterControl;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;
import simulator.services.forecast.PriceForecastDispatcher;
import simulator.services.frequency.FrequencyDispatcher;
import controllers.ControllerRegistry;
import controllers.thermal.ThermalController;
import controllers.thermal.spaceheating.ThermalGradientDescent;

/**
 * This is the policy-based controller for the flexpower-simulation code base.
 * It extends {@link ThermalController}
 * 
 * It must be setup before using it:
 * {@link #priceSource} and {@link #frequencySource} must be set to something.
 * 
 * @author daku
 */
public class RuleBasedFlexpowerSimulationClient extends ThermalController {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( RuleBasedFlexpowerSimulationClient.class.getName() );

  public static boolean USE_HARDWARE = false;

  /**
   * This is the source of price forecasts. Has to be set before using this
   * class.
   */
  public static PriceForecastDispatcher priceSource;

  /**
   * This is the source of the system frequency. Has to be set before using this
   * class.
   */
  public static FrequencyDispatcher frequencySource;

  /**
   * clients are numbered starting from 1. This static field gives the id for
   * the next client to be instantiated.
   */
  private static int idSource = 1;

  static {
    ControllerRegistry.registerController( "POLICY", RuleBasedFlexpowerSimulationClient.class );
  }

  /** "Actual" heater controller, i.e. either a simulated heater controller, 
   * or one from flexhouse */
  private HeaterControl controller;

  /** The policy client behaviour. This knows the active policy, and therefore the rulebase */
  private HCClientBehaviour behaviour;

  /** One of the event sources (triggers): frequency measurement */ 
  private FlexpowerSystemFrequency frequency;

  /** One of the event sources (triggers): price measurement */ 
  private FlexpowerPowerPrice price;

  /** ID of the client */
  private int id;

  /** This object is also put into the rulebase, to provide some
   * ThermalController capability.
   */
  private FactHandle thisHandle = null;

  private ThermalController thermalController;

  /** Constructor */
  public RuleBasedFlexpowerSimulationClient( ThermalModel model, String clientId, double setpoint, double comfortZone ) {
    super( model, setpoint, comfortZone );
    
    // TODO This is hardcoded here, maybe change the other constructor to read more data from the CSV file?
    //      (See other constructor's todo)
    this.thermalController = new ThermalGradientDescent( model, setpoint, comfortZone, 18000 );

    // Get the next free id
    id = idSource++;

    Random r = new Random();

    ServiceList services = new ServiceList( null );
    services.addService( HCServices.dppService );
    // Bit more than half of the clients support frequency control,
    // but the first client always supports it
    if ( id == 1 || r.nextInt( 10 ) < 6 )
      services.addService( HCServices.fctService );
    HCDeviceInfo deviceInfo = new HCDeviceInfo( id, services );

    controller = new SimulatedHeaterControl( "client-" + id, clientId, USE_HARDWARE && id==1 );

    String host = System.getProperty( "demo.hc.policy-server" );

    // event sources: frequency and power price
    ArrayList<EventSource> eventSources = new ArrayList<EventSource>();
    frequency = new FlexpowerSystemFrequency( frequencySource );
    price = new FlexpowerPowerPrice( priceSource );
    eventSources.add( frequency );
    eventSources.add( price );

    HttpCommunicationSystem commSystem = (HttpCommunicationSystem) PolicyUtils.getCommunicationSystem();
    AbstractAddress address = commSystem.createAddress( "client-" + id );
    logger.log( Level.FINE, address.toString() );
    AbstractAddress serverAddress = commSystem.createAddressFromFullName( host );
    behaviour = new HCClientBehaviour( address, deviceInfo, controller, eventSources );
    behaviour.setManual( true );
    PolicyUtils.createClientAgent( address, behaviour, serverAddress );
  }

  public RuleBasedFlexpowerSimulationClient( AbstractModel model, String[] args, int column ) {
    this( (ThermalModel) model, args[1], Double.parseDouble( args[column + 1] ), Double.parseDouble( args[column + 2] ) );
    // TODO: Read underlying info for underlying controller from more columns (is there only one other controller?)
    // this( (ThermalModel) model, Double.parseDouble( args[column + 1] ), Double.parseDouble( args[column + 2] ), 
    //       ControllerRegistry.getContr( args, column+3, model ) );
  }

  /** just for debugging */
  private static boolean hasOutput = false;
  
  @Override
  public double getControl( DateTime timestamp ) {
    // TODO Temporary solution to let the code run in 1 second steps
    if ( id == 1 ) {
      try {
        Thread.sleep( 1000 );
      } catch ( InterruptedException e ) {
        Thread.currentThread().interrupt();
        return Double.NaN;
      }
    }
    thermalController.setFrequency( getFrequency() );
    thermalController.setMetForecast( getMetForecast() );
    thermalController.setPriceForecast( getPriceForecast() );
    
    frequency.setTimestamp( timestamp );
    price.setTimestamp( timestamp );
    if ( controller instanceof SimulatedHeaterControl ) {
      SimulatedHeaterControl shc = (SimulatedHeaterControl) controller;
      shc.setTimestamp( timestamp );
      RuleBaseExecutor rbe = behaviour.getRulebaseExecutor();
      if ( rbe != null ) {
        // A ThermalController has to be put into the rule environment
        if ( thisHandle == null || rbe.getKsession().getObject( thisHandle ) == null ) {
          thisHandle = rbe.getRegistrar().register( thermalController );
        }
        
        if ( ! hasOutput ) {
          hasOutput = true;
          PrintStream out = System.out;
          out.println( rbe.getRuleBase() );
          out.println("Registrar:");
          for ( FactHandle factHandle : rbe.getRegistrar().getFacts().keySet() ) {
            out.println( factHandle );
          }
          out.println("Session:");
          for ( FactHandle factHandle : rbe.getKsession().getFactHandles() ) {
            out.println( factHandle );
          }
          out.println( "" );
        }

        rbe.runOnce();
        double controlValue = shc.getControlValue();
        System.out.println( "control: " + controlValue );
        return shc.getControlValue();
      }
    } else if ( controller instanceof FlexhouseHeaterControl ) {
      // FlexhouseHeaterControl fhc = (FlexhouseHeaterControl) controller;
      throw new AssertionError( "not yet implemented" );
    } else {
      throw new IllegalArgumentException( "unknown controller type: " + controller.getClass().getName() );
    }
    return Double.NaN;
  }

  @Override
  public String toString() {
    String ret =  getClass().getName();
    ret += " setpoint: " + setPoint;
    ret += " comfort: " + comfortZone;
    return ret;
  }

}
