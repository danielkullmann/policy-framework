package risoe.syslab.demo.policy.heatercontrol.sim;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;

import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.rules.IdGenerator;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import simulator.Simulator;
import simulator.services.forecast.ForecastServiceDispatcher.NoForecastException;
import controllers.thermal.ThermalController;

/** Wraps a price-based {@link ThermalController} from project flexpower-simulation.
 * 
 *  @author daku
 */
public class PriceBasedService implements Controller, Runnable {

  private static final Logger logger = Logger.getLogger( PriceBasedService.class.getName() );
  
  final private FlexpowerPowerPrice eventSource;
  final private ThermalController controller;
  private DateTime timestamp;
  private Thread t;

  /** Standard constructor */
  public PriceBasedService(Thresholds thresholds, FlexpowerPowerPrice source, ThermalController controller) {
    super();
    this.eventSource = source;
    this.controller = controller;
    this.t = new Thread(this);
  }

  @Override
  public boolean wantsToBeActivated() {
    // TODO Strangely, this method seems to be called from the setup rule..
    boolean result = ! Double.isNaN( controller.getControl( timestamp ) );
    logger.log( Level.FINER, "wtba " + result + " " + getClass().getName() );
    return result;
  }

  @Override
  public void start() {
    this.t.start();
  }

  @Override
  public void stop() {
    this.t.interrupt();
  }

  /** Returns the service provided by this controller */
  @Override
  public Service getProvidedService() {
    return HCServices.dppService;
  }

  /** Method that is used for creating rule bases. 
   * This method returns the constraint for the "when" part of a rule referencing this controller. 
   */
  public static String getActiveTestString() {
    return "wantsToBeActivated == true";
  }

  /** Method that is used for creating rule bases. This method returns the code for the "setup" rule. */
  public static String[] getSetup( double low, double high ) {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String ctrlName = "ctrl" + gen.next();
    String sourceName = "source" + gen.next();
    String threshold = "new " + Thresholds.class.getName() + "(" + low + ", " + high + ")";
    return new String[] {
        ctrlName + " : " + ThermalController.class.getName() + "() and\n    " +
        sourceName + " : " + FlexpowerPowerPrice.class.getName() + "()",
        "new " + PriceBasedService.class.getName() + "( " + threshold + ", " + sourceName + ", " + ctrlName + ")"
    };
  }

  public DateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp( DateTime timestamp ) {
    this.timestamp = timestamp;
  }

  @Override
  public void run() {
    /* TODO Make the integration between Simulator and Policy-Framework nicer. */
    
    while ( ! Thread.interrupted() ) {
      try {
        controller.setPriceForecast( eventSource.getSource().get( timestamp, Simulator.HOURS_AHEAD * 3600 * 1000, Simulator.PRICE_PERIOD*1000 ) );
        Thread.sleep( 100 );
      } catch ( InterruptedException e ) {
        Thread.currentThread().interrupt();
        break;
      } catch ( NoForecastException e ) {
        throw new IllegalStateException( e );
      }
    }
  }

  
  public ThermalController getController() {
    return controller;
  }

}
