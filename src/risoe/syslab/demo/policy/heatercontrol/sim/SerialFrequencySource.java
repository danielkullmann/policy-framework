package risoe.syslab.demo.policy.heatercontrol.sim;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Date;

import org.joda.time.DateTime;

import risoe.syslab.demo.policy.evcharging1.CsvLogger;
import simulator.services.frequency.AbstractFrequencyDispatcher;

/** Class reading frequency from Olivers (old) frequency measurement
 * device.
 * 
 * The device is going to be replaced by a better one.
 * 
 * TODO
 * 
 * @author daku
 */
public class SerialFrequencySource extends AbstractFrequencyDispatcher implements Runnable {

  private static final String DEFAULT_PORT_NAME =  "/dev/ttyS0";
  /** timeout for opening the serial port [ms] */
  private static final int OPEN_TIMEOUT = 2000;
  
  private String portName;
  private InputStream inputStream;
  private double frequency = Double.NaN;
  private SerialPort serialPort;
  private Thread thread;

  public SerialFrequencySource() {
    this(DEFAULT_PORT_NAME);
  }

  public SerialFrequencySource( String portName ) {
    this.portName = portName;
    thread = new Thread(this);
    thread.setDaemon( true );
    thread.start();
  }

  private void init() {
    try {
      CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
      CommPort commPort = portIdentifier.open(this.getClass().getName(),OPEN_TIMEOUT);
      serialPort = (SerialPort) commPort;
      serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
      inputStream = serialPort.getInputStream();
      Runtime.getRuntime().addShutdownHook( new Thread( new Runnable() {
        @Override
        public void run() {
          if (thread != null) {
            thread.interrupt();
          }
          if (inputStream != null) {
            try {
              inputStream.close();
            } catch ( IOException e ) {
              e.printStackTrace();
            }
          }
          if (serialPort != null) {
            serialPort.close();
          }
        }
      } ) );
    } catch ( FileNotFoundException e ) {
      throw new IllegalArgumentException(e);
    } catch ( NoSuchPortException e ) {
      throw new IllegalArgumentException(e);
    } catch ( PortInUseException e ) {
      throw new IllegalArgumentException(e);
    } catch ( UnsupportedCommOperationException e ) {
      throw new IllegalArgumentException(e);
    } catch ( IOException e ) {
      throw new IllegalArgumentException(e);
    }
  }

  @Override
  public synchronized double getFrequency( DateTime timestamp ) {
    addItem( timestamp, frequency );
    return frequency;
  }

  @Override
  public void run() {
    Thread.currentThread().setName( this.getClass().getName() );
    init();
    try {
      BufferedReader br = new BufferedReader( new InputStreamReader( inputStream ) );
      try {
        // To read any incomplete line
        br.readLine();
      } catch ( IOException e ) {
        throw new IllegalStateException( e );
      }
      while ( !Thread.interrupted() ) {
        String line = null;
        try {
          do {
            line = br.readLine();
          } while (br.ready());
          double frequency = Double.parseDouble( line.trim() ) / 100.0;
          synchronized ( this ) {
            this.frequency = frequency;
          }
          Thread.yield();
        } catch ( NumberFormatException e ) {
          // ignore 
          System.err.println( "Could not parse double: " + line );
        } catch ( IOException e ) {
          throw new IllegalStateException( e );
        }
      }
    } finally {
      try {
        inputStream.close();
      } catch ( IOException e ) {
        e.printStackTrace();
      }
      serialPort.close();
    }
  }

  public static void main( String[] args ) {
    try {
      PrintStream out = System.out;
      SerialFrequencySource s = new SerialFrequencySource();
      double lastFrequency = Double.NaN;
      while ( true ) {
        double frequency = s.getFrequency( new DateTime() );
        if (! Double.isNaN( frequency ) && frequency != lastFrequency) {
          out.println( CsvLogger.df.format( new Date() ) + ";" + frequency );
        }
        lastFrequency = frequency;
        Thread.yield();
      }
    } catch ( Exception e ) {
      e.printStackTrace();
    }
  }

}
