package risoe.syslab.demo.policy.heatercontrol.sim;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;

import risoe.syslab.comm.shared.RMITransportDetails;
import risoe.syslab.comm.typebased.rmi.FlexHouseRMIClient;
import risoe.syslab.control.policy.core.ClientLogger;
import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;
import risoe.syslab.hw.pollserver.flexhouse.FlexHouseRoom;
import risoe.syslab.model.flexhouse.AbstractActuator;
import risoe.syslab.model.flexhouse.FlexHouseState;
import risoe.syslab.model.flexhouse.FlexhouseDeviceType;

/** Implements {@link HeaterControl} for use in simulations,
 * i.e. using the flexpower-simulation code.
 * 
 * @author daku
 */
public class SimulatedHeaterControl extends HeaterControl {

  private static final Logger logger = Logger.getLogger( SimulatedHeaterControl.class.getName() );
  
  private static ClientLogger clientLogger = ClientLogger.instance();

  /** Determines whether Flexhouse access is only simulated. */
  private static final boolean SIMULATE_HARDWARE = true;

  private String clientName;
  
  private String clientId;

  /** This is the currently active controller */
  private FlexpowerControllerWrapper controller = null;

  private boolean directControl = false;

  private boolean connectToHardware;
  
  private FlexHouseRMIClient flxh = null;

  private ArrayList<AbstractActuator> heaters = new ArrayList<AbstractActuator>();

  public SimulatedHeaterControl( String clientName, String clientId, boolean connectToHardware ) {
    this.clientName = clientName;
    this.clientId = clientId;
    this.connectToHardware = connectToHardware;
    if (connectToHardware) {
      connect();
      try {
        FlexHouseState state = flxh.getFlexhouseState();
        for ( FlexHouseRoom room : state.getRooms() ) {
          for ( AbstractActuator act : room.getActuators() ) {
            if (act.getType() == FlexhouseDeviceType.HeaterActuator) {
              heaters.add(act);
            }
          }
        }
      } catch ( RemoteException e ) {
        throw new IllegalStateException(e);
      }
    }
  }

  @Override
  public void decreaseTemperature( String message ) {
    directControl = false;
    clientLogger.logState( clientName, clientId, new String[] {"dec", clientId, ""+directControl, "-1", "false", message} );
  }

  @Override
  public void increaseTemperature( String message ) {
    directControl = false;
    clientLogger.logState( clientName, clientId, new String[] {"inc", ""+directControl, "+1", "false", message} );
  }

  @Override
  public void switchHeater( boolean on, String message ) {
    directControl = true;
    clientLogger.logState( clientName, clientId, new String[] {"swi", ""+directControl, "0", ""+on, message} );
  }

  @Override
  public boolean active() {
    return true;
  }

  @Override
  public void use( Controller controller ) {
    this.controller = (FlexpowerControllerWrapper) controller;
    String ctrlName = this.controller.getWrappedController().getClass().getName();
    clientLogger.logUse( clientName, clientId, new String[] {ctrlName} );
  }

  public double getControlValue() {
    if (controller != null) {
      double v = controller.getWrappedController().getControl( controller.getTimestamp() );
      clientLogger.logState( clientName, clientId, new String[] {"ctl", ""+v, controller.getWrappedController().getClass().getName()} );
      if (connectToHardware) {
        try {
          int i = 0;
          for ( AbstractActuator heater : heaters ) {
            logger.log( Level.INFO, "switch heater " + heater + " (" + i + ") " + (i < (int) (v+0.5) ? "1.0": "0.0") );
            if ( ! SIMULATE_HARDWARE ) {
              flxh.setActuatorState( heater, i < (int) v ? "1.0": "0.0" );
            }
            i+=1;
          }
        } catch ( RemoteException e ) {
          e.printStackTrace();
          System.out.println("Trying to connect..");
          connect();
        }
      }
      return v;
    }
    return Double.NaN;
  }

  private void connect() {
    flxh = new FlexHouseRMIClient();
    RMITransportDetails td = new RMITransportDetails("syslab-08", 1099, "typebased_Flexhouse", "house1" );
    flxh.setTransportDetails( td );
  }

  public boolean isDirectControl() {
    return directControl;
  }

  public void setTimestamp( DateTime timestamp ) {
    if (controller != null )
      controller.setTimestamp( timestamp );
  }
  
  public FlexpowerControllerWrapper getController() {
    return controller;
  }

}
