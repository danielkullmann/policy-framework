package risoe.syslab.demo.policy.heatercontrol.sim;

import org.joda.time.DateTime;

import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.service.Service;
import controllers.thermal.spaceheating.FrequencyController;

/** This class wraps a flexpower controller into a
 * policy-framework controller. This makes it possible
 * to define flexpower controllers in a policy, and then 
 * access those inside the policy-framework to access the
 * flexpower controller.  
 * @author daku
 *
 */
public class FlexpowerControllerWrapper implements Controller {

  private controllers.Controller controller;
  private Service service;
  private DateTime timestamp;
  
  public FlexpowerControllerWrapper( controllers.Controller controller, Service service ) {
    super();
    this.controller = controller;
    this.service = service;
  }

  @Override
  public Service getProvidedService() {
    return service;
  }

  @Override
  public boolean wantsToBeActivated() {
    if (controller instanceof FrequencyController) {
      FrequencyController fc = (FrequencyController) controller;
      return ! Double.isNaN( fc.getControl( timestamp ) );
    }
    return false;
  }

  @Override
  public void start() {
    // May be empty
  }

  @Override
  public void stop() {
    // May be empty
  }

  public controllers.Controller getWrappedController() {
    return controller;
  }

  public void setTimestamp( DateTime timestamp ) {
    this.timestamp = timestamp;
  }

  public DateTime getTimestamp() {
    return timestamp;
  }

  
  public Service getService() {
    return service;
  }

  
  public void setService( Service service ) {
    this.service = service;
  }

}
