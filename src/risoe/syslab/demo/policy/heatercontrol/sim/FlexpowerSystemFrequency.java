package risoe.syslab.demo.policy.heatercontrol.sim;

import org.joda.time.DateTime;

import risoe.syslab.demo.policy.heatercontrol.events.SystemFrequency;
import simulator.services.frequency.FrequencyDispatcher;

/** Frequency source that gets the frequency value from a
 * {@link FrequencyDispatcher} object.
 * 
 * TODO Add a logger for the frequency values
 *  
 * @author daku
 */
public class FlexpowerSystemFrequency implements SystemFrequency {

  private FrequencyDispatcher source;
  private DateTime timestamp = null;

  public FlexpowerSystemFrequency( FrequencyDispatcher source ) {
    this.source = source;
    if (source == null) throw new AssertionError("source is null");
  }

  @Override
  public double getValue() {
    if (timestamp == null) return Double.NaN;
    double result = source.getFrequency( timestamp );
    return result;
  }

  public void setTimestamp( DateTime timestamp ) {
    this.timestamp = timestamp;
  }

}
