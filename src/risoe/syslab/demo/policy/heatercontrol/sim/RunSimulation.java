package risoe.syslab.demo.policy.heatercontrol.sim;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.SimulatorManager;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.comm.jade.JadeCommunicationSystem;
import risoe.syslab.control.policy.core.ClientLogger;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.message.PolicyMessage;
import risoe.syslab.demo.policy.heatercontrol.HCPlanner;
import risoe.syslab.demo.policy.heatercontrol.HCServerBehaviourImplementation;
import simulator.services.forecast.PriceForecastCSV;
import simulator.services.forecast.PriceForecastDispatcher;
import simulator.services.frequency.FlexhouseSwitchboardFrequency;
import simulator.services.frequency.FrequencyDispatcher;
import simulator.services.frequency.SineFrequency;

/** Class running the flexpower simulation
 *
 * TODO refactor SimulationManager, so that it can be more easily configured from code
 * 
 * @author daku
 */
public class RunSimulation {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( RunSimulation.class.getName() );
  
  private static final boolean USE_HTTP = true;

  public static void main( String[] args ) {
    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );
    if ( new File( "conf/sim.properties" ).exists() )
      PolicyUtils.setup( "conf/sim.properties" );

    HCPlanner.GetExternalPrice = false;
    PolicyMessage.USE_SIGNATURES = false;
    
    // 5 minutes 
    PolicyServer.ALIVE_THRESHOLD = 5*60;
    
    CommunicationSystem commSystem = null;
    if ( USE_HTTP ) {
      int port = new Random().nextInt(10240) + 10240;
      commSystem = new HttpCommunicationSystem( port, "", null );
      PolicyUtils.setCommunicationSystem( commSystem );
    } else {
      commSystem = new JadeCommunicationSystem( "", "localhost" );
      PolicyUtils.setCommunicationSystem( commSystem );
    }

    try {
      // TODO Make it configurable which FlexpowerSimulationClient to use
      //Class.forName( RuleBasedFlexpowerSimulationClient.class.getName() );
      Class.forName( SimpleFlexpowerSimulationClient.class.getName() );
    } catch ( ClassNotFoundException e ) {
      throw new IllegalStateException( e );
    }

    AbstractAddress serverAddress = commSystem.createAddress( "syslab-demo-server" );
    System.setProperty( "demo.hc.policy-server", serverAddress.toString() );

    HCServerBehaviourImplementation server = new HCServerBehaviourImplementation();
    // Only plan once per hour
    server.getPlanner().setPlanningDelay( 60*60 );
    PolicyUtils.createServerAgent( serverAddress, server );
    logger.log( Level.INFO, "server created: " + serverAddress.getAddress().toString() );

    System.setProperty( "flexpower.simulation.base", "../flexpower-simulation" );
    SimulatorManager.defaultModelFile = "data/sim/models.csv";
    SimulatorManager.defaultPriceFile = "data/sim/price_forecast.csv";
    SimulatorManager.defaultWaterCFile = "data/sim/water_consumption_forecast.csv";
    FrequencyDispatcher frequencyService = new SineFrequency(); // FlexhouseSwitchboardFrequency();
    PriceForecastDispatcher priceService = new PriceForecastCSV(SimulatorManager.defaultPriceFile, true);
    priceService.update();
    SimulatorManager manager = new SimulatorManager();
    manager.parseArgs( args );
    manager.initSimulator();
    manager.getSimulator().setFrequencyService( frequencyService );
    manager.getSimulator().setPriceForecastDispatcher( priceService );
    manager.getSimulator().setRealTime( true );
    int numModels = manager.getSimulator().getNumberOfModels();
    HCPlanner.setExpectMinNumberOfClients( numModels );
    
    if (! manager.isGui() ) {
      boolean neededToWait = false;
      while (true) {
        if (SimpleFlexpowerSimulationClient.checkInstancesForPolicies( numModels  ))
          break;
        neededToWait = true;
        try {
          Thread.sleep( 10*1000 );
        } catch ( InterruptedException e ) {
          return;
        }
      }
      if (neededToWait) {
        logger.log( Level.WARNING, "Simulation starts now" );
      }
      try {
        // Setting up the special directory for log files
        String loggerDir = manager.initSimulation();
        ClientLogger.instance().relocateLogFiles( loggerDir );
      } catch ( IOException e ) {
        throw new IllegalStateException(e);
      }
      
    }
    manager.run();
    // stop server agent if the simulation is run in batch mode
    if (! manager.isGui() ) {
      logger.log(Level.INFO, "Closing down policy-framework");
      PolicyUtils.stopAllAgents();
      server.stop();
      commSystem.shutdown();
    }
  }

}
