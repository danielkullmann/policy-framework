package risoe.syslab.demo.policy.heatercontrol.sim;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;

import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.core.TimeWindow;
import risoe.syslab.control.policy.rules.Controller;
import risoe.syslab.control.policy.rules.IdGenerator;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;
import risoe.syslab.demo.policy.heatercontrol.control.TimedDirectFreqHiLoThresholds;
import risoe.syslab.demo.policy.heatercontrol.events.EventSourceUtil;
import risoe.syslab.demo.policy.heatercontrol.events.SystemFrequency;
import controllers.thermal.spaceheating.FrequencyController;

/** Implements a frequency controller that can be used with the project flexpower-simulation.
 * 
 * TODO Use this class
 * 
 *  @author daku
 */
public class FrequencyBasedService implements Controller, Runnable {

  private static final Logger logger = Logger.getLogger( FrequencyBasedService.class.getName() );
  
  final private FlexpowerSystemFrequency eventSource;
  final private FrequencyController controller;
  private DateTime timestamp;
  private Thread t;

  /** Standard constructor */
  public FrequencyBasedService(Thresholds thresholds, TimeWindow timeWindow, FlexpowerSystemFrequency source, FrequencyController controller) {
    super();
    this.eventSource = source;
    this.controller = controller;
    this.t = new Thread(this);
    if (thresholds == null) {
      throw new IllegalArgumentException( "thresholds == null!" );
    }
    // TODO thresholds.getHysteresis() is ignored for now..
    controller.setNominal((thresholds.getLow()+thresholds.getHigh())/2.0);
    controller.setDeadband((thresholds.getHigh()-thresholds.getLow())/2.0);
    // TODO: controller.setTimeWindow(timeWindow);
  }
  
  /** Standard constructor */
  public FrequencyBasedService(Thresholds thresholds, FlexpowerSystemFrequency source, FrequencyController controller) {
    this(thresholds, null, source, controller);
  }

  @Override
  public boolean wantsToBeActivated() {
    boolean result = ! Double.isNaN( controller.getControl( timestamp ) );
    logger.log( Level.FINER, "wtba " + result + " " + getClass().getName() );
    return result;
  }

  @Override
  public void start() {
    this.t.start();
  }

  @Override
  public void stop() {
    this.t.interrupt();
  }

  /** Returns the service provided by this controller */
  @Override
  public Service getProvidedService() {
    return HCServices.fctService;
  }

  /** Method that is used for creating rule bases. 
   * This method returns the constraint for the "when" part of a rule referencing this controller. 
   */
  public static String getActiveTestString() {
    return "wantsToBeActivated == true";
  }

  /** Method that is used for creating rule bases. This method returns the code for the "setup" rule. */
  public static String[] getSetup( double low, double high, TimeWindow timeWindow ) {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String ctrlName = "ctrl" + gen.next();
    String sourceName = "source" + gen.next();
    return new String[] {
        ctrlName + " : " + HeaterControl.class.getName() + "( " + HeaterControl.getActiveTest() + ") and\n    " +
        sourceName + " : " + SystemFrequency.class.getName() + "( " + EventSourceUtil.getActiveTest() + " )",
        "new " + TimedDirectFreqHiLoThresholds.class.getName() + 
        "( " + getThresholdConstructorCall(low, high) + ", " + 
        getTimeWindowConstructorCall(timeWindow) + ", " + 
        sourceName + ", " + ctrlName + ")"
    };
  }

  public static String getThresholdConstructorCall( double low, double high ) {
    return "new " + Thresholds.class.getName() + "(" + low + ", " + high + ")";
  }

  public static String getTimeWindowConstructorCall( TimeWindow tw ) {
    return "new " + TimeWindow.class.getName() + "( " +
      tw.getFrame() + ", " + tw.getStartTime() + ", " + tw.getLength() +
      " )";
  }

  public DateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp( DateTime timestamp ) {
    this.timestamp = timestamp;
  }

  @Override
  public void run() {
    /* TODO Make the integration between Simulator and Policy-Framework nicer. */
    
    while ( ! Thread.interrupted() ) {
      try {
        controller.setFrequency( eventSource.getValue() );
        Thread.sleep( 100 );
      } catch ( InterruptedException e ) {
        Thread.currentThread().interrupt();
        break;
      }
    }
  }

  
  public FrequencyController getController() {
    return controller;
  }

}
