package risoe.syslab.demo.policy.heatercontrol.sim;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import models.AbstractModel;
import models.linear.thermal.ThermalModel;

import org.joda.time.DateTime;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.CommunicationSystem;
import risoe.syslab.control.policy.core.ClientLogger;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.HCClientBehaviour;
import risoe.syslab.demo.policy.heatercontrol.HCDeviceInfo;
import risoe.syslab.demo.policy.heatercontrol.HCPolicy;
import risoe.syslab.demo.policy.heatercontrol.control.FlexhouseHeaterControl;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;
import simulator.services.forecast.PriceForecastDispatcher;
import simulator.services.forecast.PriceForecastPacket;
import simulator.services.frequency.FrequencyDispatcher;
import controllers.ControllerRegistry;
import controllers.thermal.ThermalController;
import controllers.thermal.spaceheating.FrequencyController;
import controllers.thermal.spaceheating.ThermalCETPaper;

/**
 * This is the policy-based controller for the flexpower-simulation code base.
 * It extends {@link ThermalController}
 * 
 * It must be setup before using it:
 * {@link #priceSource} and {@link #frequencySource} must be set to something.
 * 
 * @author daku
 */
public class SimpleFlexpowerSimulationClient extends ThermalController implements PriceForecastDispatcher, FrequencyDispatcher {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( SimpleFlexpowerSimulationClient.class.getName() );

  private static final ClientLogger clientLogger = ClientLogger.instance();

  public static boolean USE_HARDWARE = false;

  /**
   * clients are numbered starting from 1. This static field gives the id for
   * the next client to be instantiated.
   */
  private static int idSource = 1;

  static {
    ControllerRegistry.registerController( "POLICY", SimpleFlexpowerSimulationClient.class );
  }

  private static ArrayList<SimpleFlexpowerSimulationClient> instances = new ArrayList<SimpleFlexpowerSimulationClient>();
  
  /** "Actual" heater controller, i.e. either a simulated heater controller, 
   * or one from flexhouse */
  private HeaterControl controller;

  /** The policy client behaviour. This knows the active policy, and therefore the rulebase */
  private HCClientBehaviour behaviour;

  /** One of the event sources (triggers): frequency measurement */ 
  private FlexpowerSystemFrequency frequency;

  /** One of the event sources (triggers): price measurement */ 
  private FlexpowerPowerPrice price;

  /** ID of the client */
  private int id;

  private ThermalController thermalController;

  private FrequencyController frequencyController;

  private PriceBasedService pbc;

  private FrequencyBasedService fbc;

  private HCPolicy policy;

  /** This is the name that is used by the policy-framework */
  private String clientName;

  /** This is the id that is used by the flexpower-simulation */ 
  private String clientId;

  /** Constructor */
  public SimpleFlexpowerSimulationClient( ThermalModel model, String clientId, double setpoint, double comfortZone, int optimizationHorizon ) {
    super( model, setpoint, comfortZone );
    
    this.clientId = clientId;
    
    // TODO This is hardcoded here, maybe change the other constructor to read more data from the CSV file?
    //      (See other constructor's todo)
    //this.thermalController = new ThermalGradientDescent( model, setpoint, comfortZone, optimizationHorizon );
    this.thermalController = new ThermalCETPaper( model, setpoint, comfortZone );
    // Dummy 
    this.frequencyController = new FrequencyController( model, 50, 50 );

    // Get the next free id
    synchronized ( getClass() ) {
      id = idSource++;
    }
    this.clientName = "client-" + id;

    Random r = new Random();

    ServiceList services = new ServiceList( null );
    services.addService( HCServices.dppService );
    // Bit more than half of the clients support frequency control,
    // but the first client always supports it
    if ( id == 1 || r.nextInt( 10 ) < 6 )
      services.addService( HCServices.fctService );
    HCDeviceInfo deviceInfo = new HCDeviceInfo( id, services );

    controller = new SimulatedHeaterControl( clientName, clientId, USE_HARDWARE && id==1 );

    String host = System.getProperty( "demo.hc.policy-server" );

    // event sources: frequency and power price
    ArrayList<EventSource> eventSources = new ArrayList<EventSource>();
    frequency = new FlexpowerSystemFrequency( this );
    price = new FlexpowerPowerPrice( this );
    eventSources.add( frequency );
    eventSources.add( price );

    CommunicationSystem commSystem = PolicyUtils.getCommunicationSystem();
    AbstractAddress address = commSystem.createAddress( clientName );
    logger.log( Level.FINE, address.toString() );
    AbstractAddress serverAddress = commSystem.createAddressFromFullName( host );
    behaviour = new HCClientBehaviour( address, deviceInfo, controller, eventSources );
    behaviour.setUseRulebase( false );
    PolicyUtils.createClientAgent( address, behaviour, serverAddress );
    
    instances.add( this );
  }

  public SimpleFlexpowerSimulationClient( AbstractModel model, String[] args, int column ) {
    this( (ThermalModel) model, args[1], Double.parseDouble( args[column + 1] ), Double.parseDouble( args[column + 2] ), Integer.parseInt( args[column+3] ) );
    // TODO: Read underlying info for underlying controller from more columns (is there only one other controller?)
    // this( (ThermalModel) model, Double.parseDouble( args[column + 1] ), Double.parseDouble( args[column + 2] ), 
    //       ControllerRegistry.getContr( args, column+3, model ) );
  }

  @Override
  public double getControl( DateTime timestamp ) {

    thermalController.setFrequency( getFrequency() );
    thermalController.setMetForecast( getMetForecast() );
    thermalController.setPriceForecast( getPriceForecast() );
    
    frequencyController.setFrequency( getFrequency() );
    frequencyController.setMetForecast( getMetForecast() );
    frequencyController.setPriceForecast( getPriceForecast() );

    frequency.setTimestamp( timestamp );
    price.setTimestamp( timestamp );

    if ( controller instanceof SimulatedHeaterControl ) {
      SimulatedHeaterControl shc = (SimulatedHeaterControl) controller;
      HCPolicy policy = (HCPolicy) behaviour.getActivePolicy();
      if ( this.policy != policy ) {
        this.policy = policy;
        
        // Log data about policy; include clientId  
        String[] policyInfo = policy.getInfo().split( ":" );
        clientLogger.logPolicy( clientName, clientId, policyInfo );
        
        Thresholds pc = policy.getPriceCtrl();
        Thresholds fc = policy.getFrequencyCtrl();
        this.pbc = new PriceBasedService( pc, price, thermalController );
        if (fc == null) {
          this.fbc = null;
        } else {
          this.fbc = new FrequencyBasedService( fc, frequency, frequencyController );
        }
      }
      
      if (pbc != null) pbc.setTimestamp( timestamp );
      // TODO if (fbc != null) fbc.setTimestamp( timestamp );

      // TODO
      if (fbc != null && fbc.wantsToBeActivated()) {
        shc.use( new FlexpowerControllerWrapper( fbc.getController(), fbc.getProvidedService() ) );
      } else if (pbc != null && pbc.wantsToBeActivated()) {
        logger.log(Level.INFO, "pbc activated: " + this.id );
        shc.use( new FlexpowerControllerWrapper( pbc.getController(), pbc.getProvidedService() ) );
      }
      shc.setTimestamp( timestamp );
      double result = shc.getControlValue();
      return result;

    } else if ( controller instanceof FlexhouseHeaterControl ) {
      // FlexhouseHeaterControl fhc = (FlexhouseHeaterControl) controller;
      throw new AssertionError( "not yet implemented" );
    } else {
      throw new IllegalArgumentException( "unknown controller type: " + controller.getClass().getName() );
    }
  }
  
  /** Checks whether at least minClients instances of this class have a policy 
   * 
   * @param minClients minimum number of instances that should have a policy
   * @return
   */
  public static boolean checkInstancesForPolicies( int minClients) {
    if ( instances.size() < minClients ) return false;
    int numClients = 0;
    for ( SimpleFlexpowerSimulationClient instance : instances ) {
      if (instance.behaviour != null && instance.behaviour.getActivePolicy() != null) {
        numClients++;
      }
    }
    if (numClients < minClients) {
      logger.log( Level.WARNING, "Wait to start, " + (minClients-numClients) + " to go" );
    }
    return numClients >= minClients;
  }

  @Override
  public String toString() {
    String ret =  getClass().getName();
    ret += " setpoint: " + setPoint;
    ret += " comfort: " + comfortZone;
    return ret;
  }

  @Override
  public void update() {
    // TODO Auto-generated method stub
    
  }

  @Override
  public String[][] getData() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public double getFrequency( DateTime timestamp ) {
    return getFrequency();
  }

  private boolean dontCareOfTime;
  private int nextToSend;

  @Override
  public ArrayList<PriceForecastPacket> get( DateTime timestamp, int length, int resolution ) throws NoForecastException {
    // TODO: Copied from PriceForecastCSV#get(...) 
    ArrayList<PriceForecastPacket> prices = getPriceForecast();
    ArrayList<PriceForecastPacket> toSend = new ArrayList<PriceForecastPacket>();
    if (dontCareOfTime) {
      int priceToSend = (int) Math.floor(length/1000./60./5.);
      for (int i = 0 ; i < priceToSend ; i++) toSend.add(prices.get(i+nextToSend));
      nextToSend++;
    }
    else {
      throw new IllegalStateException("PriceForcastCSV.get() is not yet implemented for !dontCareOfTime");
    }
    return toSend;
  }
  

}
