package risoe.syslab.demo.policy.heatercontrol.test;

import java.util.Random;

import junit.framework.TestCase;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.HCDeviceInfo;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;

/** Test class for {@link HCDeviceInfo}
 * 
 * @author daku
 */
public class HCDeviceInfoTest extends TestCase {

  /** test parsing of {@link HCDeviceInfo} */
  public void testHCDeviceInfo() {
    Random r = new Random();
    int id = Math.abs( r.nextInt() );
    ServiceList services = new ServiceList( null );
    services.addService( HCServices.dppService );
    // Bit more than half of the clients support frequency control...
    if ( r.nextInt( 10 ) < 6 ) services.addService( HCServices.fctService );
    HCDeviceInfo deviceInfo = new HCDeviceInfo( id, services  );

    String str = deviceInfo.toFipaContent();
    
    HCDeviceInfo result = HCDeviceInfo.parse( new MessageParser().parse( str ) );
    assertNotNull( result );
    assertEquals( deviceInfo.getId(), result.getId() );
    assertEquals( deviceInfo.getServices(), result.getServices() );
  }
}
