package risoe.syslab.demo.policy.heatercontrol.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.drools.runtime.rule.FactHandle;
import org.joda.time.DateTime;

import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.demo.policy.heatercontrol.sim.FlexpowerPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.sim.FlexpowerSystemFrequency;
import risoe.syslab.demo.policy.heatercontrol.sim.SimulatedHeaterControl;
import simulator.services.forecast.PriceForecastCSV;
import simulator.services.forecast.PriceForecastDispatcher;
import simulator.services.frequency.ConstantFrequency;
import simulator.services.frequency.FrequencyDispatcher;

public class RuleDebugger {

  /**
   * @param args
   */
  public static void main( String[] args ) {
    
    FrequencyDispatcher frequencyService = new ConstantFrequency(51);
    PriceForecastDispatcher priceService = new PriceForecastCSV("data/sim/price_forecast.csv", true);
    priceService.update();
    
    // event sources: frequency and power price
    ArrayList<EventSource> eventSources = new ArrayList<EventSource>();
    FlexpowerSystemFrequency frequency = new FlexpowerSystemFrequency( frequencyService );
    FlexpowerPowerPrice price = new FlexpowerPowerPrice( priceService );
    eventSources.add(frequency);
    eventSources.add(price);
    
    DateTime timestamp = new DateTime();
    frequency.setTimestamp( timestamp );
    price.setTimestamp( timestamp );
    
    String ruleBase = "";
    try {
      BufferedReader br = new BufferedReader( new FileReader( new File(args[0] ) ) );
      String line;
      while (null != (line = br.readLine())) {
        ruleBase += line + "\n";
      }
    } catch ( FileNotFoundException e ) {
      throw new IllegalArgumentException(e);
    } catch ( IOException e ) {
      throw new IllegalArgumentException(e);
    }
    PrintStream out = System.out;
    
    out.println( ruleBase );
    SimulatedHeaterControl hc = new SimulatedHeaterControl( "house1", "1001", false );
    RuleBaseExecutor rbe = new RuleBaseExecutor( ruleBase , hc, eventSources );
    
    rbe.setup();
    
    rbe.runOnce();

    out.println("Registrar");
    HashMap<FactHandle, Object> rfacts = rbe.getRegistrar().getFacts();
    for ( FactHandle key : rfacts.keySet() ) {
      out.println( key + " " + rfacts.get( key ) );
    }

    out.println();
    out.println("Session");
    Collection<FactHandle> kfacts = rbe.getKsession().getFactHandles();
    for ( FactHandle fact : kfacts ) {
      out.println( fact );
    }
  }

}
