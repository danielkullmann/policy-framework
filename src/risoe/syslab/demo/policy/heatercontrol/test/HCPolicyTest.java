package risoe.syslab.demo.policy.heatercontrol.test;

import java.util.Date;

import junit.framework.TestCase;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.core.TimeWindow;
import risoe.syslab.control.policy.message.parser.Expression;
import risoe.syslab.control.policy.message.parser.MessageParser;
import risoe.syslab.demo.policy.heatercontrol.HCPolicy;

/** Test class for {@link HCPolicy}
 * 
 * @author daku
 */
public class HCPolicyTest extends TestCase {

  /** test parsing of {@link HCPolicy} */
  public void testPolicy() {
    TimeFrame timeFrame = TimeFrame.createTimeFrame(new Date(), 0, 1, 0);
    String ruleBase = "rulebase with \n and stuff \"\"";
    Thresholds frequencyCtrl = new Thresholds(1.0, 2.0);
    Thresholds priceCtrl = new Thresholds(10.0, 20.0);
    HCPolicy policy = new HCPolicy(timeFrame, ruleBase, frequencyCtrl, priceCtrl, null);
    
    String c = policy.toFipaContent();
    
    Expression expression = new MessageParser().parse( c );
    HCPolicy result = HCPolicy.parse(expression);
    
    assertNotNull( result );
    assertEquals(policy, result);
    
  }

  /** test parsing of {@link HCPolicy} */
  public void testPolicy2() {
    TimeFrame timeFrame = TimeFrame.createTimeFrame(new Date(), 0, 1, 0);
    String ruleBase = "rulebase with \n and stuff \"\"";
    Thresholds priceCtrl = new Thresholds(10.0, 20.0);
    HCPolicy policy = new HCPolicy(timeFrame, ruleBase, null, priceCtrl, null);
    
    String c = policy.toFipaContent();
    
    Expression expression = new MessageParser().parse( c );
    HCPolicy result = HCPolicy.parse(expression);
    
    assertNotNull( result );
    assertEquals(policy, result);
    
  }

  /** test parsing of {@link HCPolicy} */
  public void testPolicy3() {
    TimeFrame timeFrame = TimeFrame.createTimeFrame(new Date(), 0, 1, 0);
    String ruleBase = "rulebase with \n and stuff \"\"";
    Thresholds frequencyCtrl = new Thresholds(1.0, 2.0);
    HCPolicy policy = new HCPolicy(timeFrame, ruleBase, frequencyCtrl, null, null);
    
    String c = policy.toFipaContent();
    
    Expression expression = new MessageParser().parse( c );
    HCPolicy result = HCPolicy.parse(expression);
    
    assertNotNull( result );
    assertEquals(policy, result);
    
  }

  /** test parsing of {@link HCPolicy} */
  public void testPolicy4() {
    TimeFrame timeFrame = TimeFrame.createTimeFrame(new Date(), 0, 1, 0);
    TimeWindow tw = new TimeWindow(1500, 300, 600);
    String ruleBase = "rulebase with \n and stuff \"\"";
    Thresholds frequencyCtrl = new Thresholds(1.0, 2.0);
    HCPolicy policy = new HCPolicy(timeFrame, ruleBase, frequencyCtrl, null, tw);
    
    String c = policy.toFipaContent();
    
    Expression expression = new MessageParser().parse( c );
    HCPolicy result = HCPolicy.parse(expression);
    
    assertNotNull( result );
    assertEquals(policy, result);
    
  }
}
