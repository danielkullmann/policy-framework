package risoe.syslab.demo.policy.heatercontrol.test;

import java.util.ArrayList;

import junit.framework.TestCase;
import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.comm.http.HttpCommunicationSystem;
import risoe.syslab.control.policy.core.PolicyUtils;
import risoe.syslab.control.policy.rules.EventSource;
import risoe.syslab.control.policy.service.Service;
import risoe.syslab.control.policy.service.ServiceList;
import risoe.syslab.demo.policy.heatercontrol.HCClientBehaviour;
import risoe.syslab.demo.policy.heatercontrol.HCDeviceInfo;
import risoe.syslab.demo.policy.heatercontrol.HCPlanner;
import risoe.syslab.demo.policy.heatercontrol.HCPolicy;
import risoe.syslab.demo.policy.heatercontrol.HCServerBehaviourImplementation;
import risoe.syslab.demo.policy.heatercontrol.control.FlexhouseHeaterControl;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.HeaterControl;
import risoe.syslab.demo.policy.heatercontrol.events.DanishPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.events.FlexhouseSystemFrequency;
import test.util.MockedPriceRMIServer;

public class HCPlannerTest extends TestCase {

  private HttpCommunicationSystem commSystem;
  private MockedPriceRMIServer priceServer;
  private ArrayList<HCClientBehaviour> clients;
  private HCServerBehaviourImplementation server;

  @Override
  public void setUp() {
    commSystem = new HttpCommunicationSystem( "", "localhost:12345" );
    priceServer = new MockedPriceRMIServer();
  }
  
  public void testOne() {
    System.setProperty( "java.util.logging.config.file", "conf/logging.properties" );
    System.setProperty( "log4j.configuration", "conf/log4j.properties" );
    // TODO: Should this be used here?
    PolicyUtils.setup( "conf/policy-framework.properties" );
    
    System.setProperty( "demo.hc.price-server", "localhost" );
    System.setProperty( HCPlanner.POLICY_TYPE_PROPERTY, "multi-stepped.df");

    server = new HCServerBehaviourImplementation();
    server.getPlanner().setPlanningDelay( 2 );

    PolicyUtils.setCommunicationSystem( commSystem );
    
    AbstractAddress serverAddress = commSystem.createAddress( "syslab-demo-server" );
    PolicyUtils.createServerAgent( serverAddress, server );

    ArrayList<EventSource> eventSources = new ArrayList<EventSource>();
    eventSources.add( new FlexhouseSystemFrequency(null) );
    eventSources.add( new DanishPowerPrice());

    ServiceList services = new ServiceList( new ArrayList<Service>() );
    services.addService( HCServices.dppService );
    services.addService( HCServices.fctService );

    clients = new ArrayList<HCClientBehaviour>();

    for (int num = 1; num <= 10; num++) {
      AbstractAddress address = commSystem.createAddress( "client-" + num);
      HCDeviceInfo deviceInfo = new HCDeviceInfo(num, services);
      String roomName = "room" + num;
      HeaterControl controller = new FlexhouseHeaterControl(null, roomName);
      HCClientBehaviour client = new HCClientBehaviour(address, deviceInfo, controller, eventSources);
      PolicyUtils.createClientAgent( address, client, serverAddress);
      clients.add( client );
    }

    try {
      Thread.sleep( 10*1000 );
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    
    for (HCClientBehaviour client : clients) {
      HCPolicy p = (HCPolicy) client.getActivePolicy();
      assertNotNull( "" + client.getClientAddress(), p );
      if ( p.getFrequencyCtrl() != null ) {
        assertTrue( p.getFrequencyCtrl().getLow()  + " <= 49.95", p.getFrequencyCtrl().getLow()  <= 49.95 );
        assertTrue( p.getFrequencyCtrl().getLow()  + " >  49.5",  p.getFrequencyCtrl().getLow()  >  49.5  );
        assertTrue( p.getFrequencyCtrl().getHigh() + " >= 50.05", p.getFrequencyCtrl().getHigh() >= 50.05 );
        assertTrue( p.getFrequencyCtrl().getHigh() + " <  50.5",  p.getFrequencyCtrl().getHigh() <  50.5  );
      }
    }
    
  }
  
  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
    for (HCClientBehaviour client : clients) {
      client.stop();
    }
    if ( server != null ) server.stop();
    if ( commSystem != null ) commSystem.shutdown();
    if ( priceServer != null ) priceServer.stop();
    PolicyUtils.stopAllAgents();
  }
}
