package risoe.syslab.demo.policy.heatercontrol.test;

import java.util.HashMap;

import junit.framework.TestCase;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.rule.FactHandle;

import risoe.syslab.control.policy.rules.IdGenerator;
import risoe.syslab.control.policy.rules.Registrar;
import risoe.syslab.control.policy.rules.RuleBaseCreator;
import risoe.syslab.control.policy.rules.RuleBaseExecutor;
import risoe.syslab.demo.policy.heatercontrol.control.DynamicPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.control.FreqHiLoThresholds;
import risoe.syslab.demo.policy.heatercontrol.events.DanishPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.events.FlexhouseSystemFrequency;

/** Test class for {@link RuleBaseCreator}
 * 
 * @author daku
 */
public class RuleBaseCreatorTest extends TestCase {

  /** simple test of rule base creation */
  public void testCreator() {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String regName = "reg" + gen.next();
    String activationGroup = "actgroup" + gen.next();

    RuleBaseCreator rbc = createRuleBase(regName, activationGroup);
    String s = rbc.createRuleBase();
    
    System.err.println( s );
    
    assertNotNull( s );
    assertTrue( s.contains( regName ) );
    assertTrue( s.contains( activationGroup ) );
    assertTrue( s.contains( DynamicPowerPrice.class.getName() ) );
    assertTrue( s.contains( DynamicPowerPrice.getActiveTestString() ) );
    assertTrue( s.contains( FreqHiLoThresholds.class.getName() ) );
    assertTrue( s.contains( FreqHiLoThresholds.getActiveTestString() ) );
  }

  /** more complicated test of rule base creation */
  public void testRulebase() {
    IdGenerator gen = RuleBaseExecutor.idGenerator;
    String regName = "reg" + gen.next();
    String activationGroup = "actgroup" + gen.next();

    RuleBaseCreator rbc = createRuleBase(regName, activationGroup);
    
    String s = rbc.createRuleBase();
    
    System.err.println( s );
    
    assertNotNull( s );
    
    KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
    kbuilder.add( ResourceFactory.newByteArrayResource( s.getBytes() ), ResourceType.DRL );
    KnowledgeBuilderErrors errors = kbuilder.getErrors();
    if (errors.size() > 0) {
      for (KnowledgeBuilderError error : errors) {
        System.err.println(error);
      }
      throw new IllegalArgumentException("Could not parse knowledge.");
    }
    KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
    kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
    
    StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
    Registrar registrar = new Registrar( ksession );
    ksession.insert( registrar );

    ksession.insert( new FlexhouseSystemFrequency(null) );
    ksession.insert( new DanishPowerPrice() );
    
    for (int i = 0; i < 10; i++) {
      // update all facts (except registrar), so that all rules can fire
      HashMap<FactHandle, Object> facts = registrar.getFacts();
      for (FactHandle fact : facts.keySet() ) {
        ksession.update( fact, facts.get( fact ) );
      }
      
      // fire all rules...
      ksession.fireAllRules();
      
      try {
        Thread.sleep( 50 );
      } catch (InterruptedException e) {
        break;
      }
    }
    
    ksession.dispose();
    
  }

  /** creates rule base that is tested */
  private RuleBaseCreator createRuleBase(String regName, String activationGroup) {
    RuleBaseCreator rbc = new RuleBaseCreator("data/rulebase/hc-rulebase.template");
    rbc.setRegVarName( regName );
    rbc.setActivationGroup( activationGroup  );
    rbc.addRule( "data/rulebase/frequency-thresholds.template",
        FreqHiLoThresholds.class.getName(), FreqHiLoThresholds.getActiveTestString() );
    rbc.addRule( "data/rulebase/dynamic-power-price.template",
        DynamicPowerPrice.class.getName(), DynamicPowerPrice.getActiveTestString() );
    String[] setup = DynamicPowerPrice.getSetup( 17, 23 );
    rbc.addSetup( setup[0], setup[1] );
    setup = FreqHiLoThresholds.getSetup( 17, 23 );
    rbc.addSetup( setup[0], setup[1] );
    return rbc;
  }
}
