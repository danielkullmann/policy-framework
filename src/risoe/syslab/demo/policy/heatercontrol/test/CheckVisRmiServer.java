package risoe.syslab.demo.policy.heatercontrol.test;

import java.io.PrintStream;

import risoe.syslab.control.policy.vis.ControllerOutputMonitor;
import risoe.syslab.gui.wall.displets.flexhouse.Measurement;
import risoe.syslab.gui.wall.displets.flexhouse.TimeSeries;

public class CheckVisRmiServer {

  private int room = 3;
  
  private void run() {
    ControllerOutputMonitor mon = new ControllerOutputMonitor();
    while ( true ) {
      printTimeSeries( "lf", mon.getLowerFrequencyThreshold()[room] );
      printTimeSeries( "uf", mon.getUpperFrequencyThreshold()[room] );
      printTimeSeries( "lp", mon.getLowerPriceThreshold()[room] );
      printTimeSeries( "up", mon.getUpperPriceThreshold()[room] );

      try {
        Thread.sleep(3000);
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        break;
      }
    }
  }

  private void printTimeSeries( String msg, TimeSeries timeSeries) {
    PrintStream out = System.out;
    out.print( msg + ": " );
    for (Measurement meas : timeSeries.getAll()) {
      out.print( meas.getData() + " " );
    }
    out.println();
  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    new CheckVisRmiServer().run();
  }

}
