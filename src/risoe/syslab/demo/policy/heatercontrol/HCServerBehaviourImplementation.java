/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.heatercontrol;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyServer;
import risoe.syslab.control.policy.core.ServerBehaviour;
import risoe.syslab.control.policy.core.PolicyServer.ClientStatus;
import risoe.syslab.control.policy.message.parser.Expression;

/**
 * Policy implementation for the heater controller example.
 */
public final class HCServerBehaviourImplementation implements ServerBehaviour, HCServerBehaviour {

  /** map of connected clients */
  private HashMap<AbstractAddress, ClientData> clients = new HashMap<AbstractAddress, ClientData>();
  
  /** planner used to create policies for clients */
  private HCPlanner planner;
  
  /** Thread in which HCPlanner runs */
  private Thread plannerThread;
  
  /** policy server that does the negotiation and communication; */
  private PolicyServer policyServer;
  
  
  /** Default constructor. Starts the planner */
  public HCServerBehaviourImplementation() {
    super();
    planner = new HCPlanner( this );
    plannerThread = new Thread( planner );
    plannerThread.setDaemon( true );
    plannerThread.start();
  }


  @Override
  public HCServerClientBehaviour createServerBehaviour( AbstractAddress clientAddress, DeviceInfo deviceInfo ) {
    return new HCServerClientBehaviour( this, clientAddress, deviceInfo );
  }

  
  @Override
  public DeviceInfo parseDeviceInfo( Expression expression ) {
    return HCDeviceInfo.parse( expression );
  }

  
  @Override
  public Policy parsePolicy( Expression expression ) {
    return HCPolicy.parse( expression );
  }


  /**
   * Here, the server can register a client to be managed.
   * 
   * @param clientAddress AbstractAddress of the client to be managed
   * @param deviceInfo DeviceInfo of the client to be managed
   */
  public synchronized void register( AbstractAddress clientAddress, HCDeviceInfo deviceInfo ) {
    ClientData device = clients.get(clientAddress);
    if ( device == null ) {
      device = new ClientData( clientAddress );
      device.setDeviceInfo( deviceInfo );
      clients.put( clientAddress, device );
    } else {
      device.setDeviceInfo( deviceInfo );
    }
    synchronized (planner) {
      planner.notify();
    }
  }


  /** Activate a policy for the given client
   * @param clientAddress address of client that should get a new active policy
   * @param policy policy that should be activated
   */
  public void activatePolicy( AbstractAddress clientAddress, Policy policy ) {
    ClientData client = clients.get(clientAddress);
    if ( client == null ) {
      throw new AssertionError( "client unknown: " + clientAddress );
    } 
    client.setActivePolicy( policy );
    
    if ( policy != null && policy.equals( client.getNextPolicy() ) ) {
      client.setNextPolicy( null );
    }
    
  }


  /** Create a policy for the given client. 
   * It can be that there is no new policy created for a client. In this case, null is returned.
   * 
   * @param clientAddress address of client a policy should be created for
   * @return a new policy, or null if no new policy was created
   */
  public Policy createPolicy( AbstractAddress clientAddress ) {
    
    ClientData client = clients.get( clientAddress );
    if ( client == null ) {
      throw new AssertionError( "Client has not registered itself.." );
    }
    
    Date now = new Date();
    
    if ( client.getActivePolicy() != null ) {
      // check validity period of active policy
      if ( client.getActivePolicy().getTimeFrame().isCurrent( now ) )
        return client.getActivePolicy();
      // else..
      if ( client.getActivePolicy().getTimeFrame().isExpired( now )) 
        client.setActivePolicy( null );
    }
    
    if ( client.getNextPolicy() != null ) {
      // check validity period of next policy
      if ( client.getNextPolicy().getTimeFrame().isSoonCurrent( now, 5*60 ) )
        return client.getNextPolicy();
      // else..
      client.setNextPolicy( null );
    }
    
    // Instead of sending an IdlePolicy, I return null
    // here, and the PolicyServer waits until a policy
    // is available to the client
    return null;
  }


  /** Get the currently active policy for the client with the given address 
   * @param clientAddress address of client we are interested in
   * @return currently active policy of that client
   */
  public Policy getActivePolicy( AbstractAddress clientAddress ) {
    ClientData device = clients.get(clientAddress);
    if ( device == null ) {
      return null;
    } 
    
    return device.getActivePolicy();
  }


  /** Get the device info for the client with the given address 
   * @param clientAddress address of client we are interested in
   * @return device info of that client
   */
  public HCDeviceInfo getDeviceInfo( AbstractAddress clientAddress ) {
    ClientData device = clients.get(clientAddress);
    if ( device == null ) {
      return null;
    } 
    
    return device.getDeviceInfo();
    
  }


  /** Return the client with the given AbstractAddress
   * @param clientAddress the AbstractAddress of the wanted client
   */
  public ClientData getClient( AbstractAddress clientAddress ) {
    return clients.get( clientAddress );
  }


  /** Return the current state of the client list
   * @return the current client list
   */
  @Override
  public Collection<ClientData> getClients() {
    return clients.values();
  }


  /** Stops the server, i.e. it stops the planner */
  public void stop() {
    plannerThread.interrupt();
  }


  /** When the policy was rejected at the client..
   * @param clientAddress
   */
  public void rejectPolicy( AbstractAddress clientAddress, Policy policy) {
    ClientData client = getClient( clientAddress );
    if ( client.getNextPolicy().equals( policy ) ) {
      client.setNextPolicy( null );
    }
  }


  @Override
  public void setPolicyServer(PolicyServer policyServer) {
    this.policyServer = policyServer;
  }


  @Override
  public void startRenegotiation(AbstractAddress clientAddress) {
    policyServer.startRenegotiation(clientAddress);
  }

  @Override
  public ClientStatus getClientStatus(AbstractAddress clientAddress) {
    return policyServer.getClientStatus(clientAddress);
  }

  public HCPlanner getPlanner() {
    return planner;
  }

}
