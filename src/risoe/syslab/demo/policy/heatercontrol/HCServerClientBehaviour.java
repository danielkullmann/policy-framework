/**
 * @copyright 2009,2010 Risoe DTU, Denmark
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.demo.policy.heatercontrol;

import risoe.syslab.control.policy.comm.AbstractAddress;
import risoe.syslab.control.policy.core.DeviceInfo;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.ServerClientBehaviour;

/**
 * Server behaviour for a single client of the heatercontroller example
 */
public final class HCServerClientBehaviour implements ServerClientBehaviour {

  /** address of client */
  private AbstractAddress clientAddress;
  
  /** server implementation */
  private HCServerBehaviourImplementation impl;
  
  /** Standard constructor
   * @param impl  {@link HCServerBehaviourImplementation}
   * @param clientAddress address of client
   * @param deviceInfo device info of client
   */
  public HCServerClientBehaviour( HCServerBehaviourImplementation impl, AbstractAddress clientAddress, DeviceInfo deviceInfo ) {
    this.impl = impl;
    this.clientAddress = clientAddress;
    this.impl.register( clientAddress, (HCDeviceInfo) deviceInfo );
  }

  @Override
  public void activatePolicy( Policy policy ) {
    impl.activatePolicy( clientAddress, policy );
  }

  @Override
  public Policy createPolicy() {
    return impl.createPolicy( clientAddress );
  }

  @Override
  public Policy getActivePolicy() {
    return impl.getActivePolicy( clientAddress );
  }

  @Override
  public DeviceInfo getClientDeviceInfo() {
    return impl.getDeviceInfo( clientAddress );
  }

  
  @Override
  public void rejectPolicy( Policy policy ) {
    this.impl.rejectPolicy( clientAddress, policy );
  }


  @Override
  public void setClientDeviceInfo(DeviceInfo deviceInfo) {
    impl.getClient(clientAddress).setDeviceInfo((HCDeviceInfo) deviceInfo);
  }

}
