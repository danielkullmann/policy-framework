/**
 * This package contains an implementation of a really simple heater control policy.
 * The policy uses frequency control and reaction to a dynamic power price. 
 * 
 * TODO replace custom System properties (like which hc policy to use) with
 *      public static fields in the corresponding classes. (Does this make sense?)
 */
package risoe.syslab.demo.policy.heatercontrol;