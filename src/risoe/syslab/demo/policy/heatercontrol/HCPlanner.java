package risoe.syslab.demo.policy.heatercontrol;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import risoe.syslab.comm.shared.RMITransportDetails;
import risoe.syslab.comm.typebased.GenericPriceAccessAPI;
import risoe.syslab.comm.typebased.rmi.GenericPriceRMIClient;
import risoe.syslab.control.policy.core.Policy;
import risoe.syslab.control.policy.core.PolicyServer.ClientStatus;
import risoe.syslab.control.policy.core.Thresholds;
import risoe.syslab.control.policy.core.TimeFrame;
import risoe.syslab.control.policy.core.TimeWindow;
import risoe.syslab.control.policy.rules.RuleBaseCreator;
import risoe.syslab.demo.policy.heatercontrol.control.DirectFreqHiLoThresholds;
import risoe.syslab.demo.policy.heatercontrol.control.DynamicPowerPrice;
import risoe.syslab.demo.policy.heatercontrol.control.FreqHiLoThresholds;
import risoe.syslab.demo.policy.heatercontrol.control.HCServices;
import risoe.syslab.demo.policy.heatercontrol.control.TimedDirectFreqHiLoThresholds;
import risoe.syslab.demo.policy.heatercontrol.sim.FrequencyBasedService;
import risoe.syslab.demo.policy.heatercontrol.sim.PriceBasedService;
import risoe.syslab.demo.policy.heatercontrol.sim.RunSimulation;
import risoe.syslab.service.pricesignal.EnerginetWSPricePollServer;

/**
 * This is the planner part of the heatcontrol example. The planner is quite
 * simple right now: It just distributes frequency and price thresholds to all
 * the clients.
 * 
 * Usage: create an instance from the {@link HCServerBehaviourImplementation}, and then
 * start the planner using a Thread or something similar.
 * 
 * TODO: Make global thresholds for frequency and price configurable (i.e.
 * {@link #lowFrequency}, {@link #highFrequency}, ...)
 * 
 * @author daku
 */
public class HCPlanner implements Runnable {

  /** logger for this class */
  private static final Logger logger = Logger.getLogger( HCPlanner.class.getName() );

  /** frequency with which the step width is changed [ms]. 
   * This is used by 
   * {@link #makeMultiSteppedPolicies(List, boolean)},
   * {@link #makeMultiTimedDirectPolicies(List)}, 
   * {@link #makeSimulationPolicies(List)}. 
   */ 
  private static final long MULTI_POLICIES_FREQUENCY = 2 * 3600 * 1000L;

  /** System property for setting the policy for creating policies.
   * See {@link #run()} source code for the possible values of this. 
   */
  public static final String POLICY_TYPE_PROPERTY = "demo.hc.server.policy-policy";

  /** Defines default value for the frequency of planning [s] */
  private static final int DEFAULT_PLANNING_DELAY = 10 * 60;
  
  /** Default validity period of policies [minutes] */
  private static final int VALIDITY_PERIOD = 15;

  /** Defines frequency of planning [s] */
  private int planningDelay = DEFAULT_PLANNING_DELAY;

  /** server managing the connected clients */
  private HCServerBehaviour server;

  /** The normal frequency value */
  private final static double NORMAL_FREQUENCY = 50.0;
  /** Deadband for frequency control */
  private final static double FREQUENCY_DEADBAND = 0.05;
  /** Reaction width for frequency control */
  private final static double REACTION_WIDTH_1 = 0.05;
  private final static double REACTION_WIDTH_2 = 0.10;
  private final static double REACTION_WIDTH_3 = 0.20;

  /** Setting for the policy creation */
  private final static double lowFrequency = 49.9;

  /** Setting for the policy creation */
  private final static double highFrequency = 50.1;

  public static boolean GetExternalPrice = true;
  
  /** If > 0, gives the minimum number of clients the planner 
   * should wait for until it creates policies
   */
  private static int expectMinNumberOfClients = -1;

  /**
   * Setting for the policy creation. Prices from
   * {@link EnerginetWSPricePollServer} range from 40 to 80.
   */
  private double lowPrice = 55;

  /** Setting for the policy creation */
  private double highPrice = 65;

  /** Used in {@link #makeMultiSteppedPolicies(List, boolean)} */
  private long startMillis = System.currentTimeMillis();

  /** access to average price */
  GenericPriceAccessAPI priceApi = null;

  /** average price, read from {@link #priceApi} */
  double defaultPrice = 60.0;

  /**
   * Standard constructor
   * 
   * @param server
   *          policy server managing the clients
   */
  public HCPlanner( HCServerBehaviour server ) {
    this.server = server;
  }

  /** Runs the server */
  @Override
  public void run() {
    Thread.currentThread().setName( "HCPlanner" );
    try {
      // Just sleep 5 seconds at the beginning, to give clients 
      // time to setup themselves
      Thread.sleep( 5*1000 );
    } catch ( InterruptedException e ) {
      Thread.currentThread().interrupt();
      return;
    }
    while ( !Thread.interrupted() ) {

      if (GetExternalPrice) {
        if ( priceApi == null ) {
          try {
            GenericPriceRMIClient cli = new GenericPriceRMIClient();
            String hostName = System.getProperty( "demo.hc.price-server", "localhost" );
            RMITransportDetails rtd = new RMITransportDetails( hostName, 1099, "typebased_price", "energinetws" );
            cli.setTransportDetails( rtd );
            priceApi = cli;
          } catch ( Exception e ) {
            e.printStackTrace();
          }
        }
  
        if ( priceApi != null ) {
          try {
            defaultPrice = priceApi.getDailyPriceAverage().value;
            if ( ! Double.isNaN(defaultPrice) ) {
              highPrice = defaultPrice + 5;
              lowPrice = defaultPrice - 5;
            }
          } catch ( RemoteException e ) {
            logger.log( Level.WARNING, "no access to price server", e );
            priceApi = null;
          }
        }
      }
      
      // Get a list of the active clients
      ArrayList<ClientData> clients = new ArrayList<ClientData>();
      synchronized ( server ) {
        for ( ClientData client : server.getClients() ) {
          if ( server.getClientStatus( client.getClientAddress() ) != ClientStatus.MISSING ) {
            if ( client.getActivePolicy() == null || soonExpires( client.getActivePolicy() ) ) {
              clients.add( client );
            }
          }
        }
      }

      if ( clients.size() == 0 || expectMinNumberOfClients > 0 && server.getClients().size() < expectMinNumberOfClients ) {
        try {
          logger.log( Level.FINER, "waiting for clients: " + clients.size() + 
              " " + server.getClients().size() + " " + expectMinNumberOfClients );
          Thread.sleep( 1 * 1000 );
        } catch ( InterruptedException e ) {
          break;
        }
        continue;
      }

      // policy to create policies
      String policyPolicy = System.getProperty( POLICY_TYPE_PROPERTY, "stepped" );
      List<HCPolicy> policies;

      if ( policyPolicy.equalsIgnoreCase( "random" ) )
        policies = makeRandomPolicies( clients );
      else if ( policyPolicy.equalsIgnoreCase( "stepped" ) )
        policies = makeSteppedPolicies( clients, false );
      else if ( policyPolicy.equalsIgnoreCase( "stepped.df" ) )
        policies = makeSteppedPolicies( clients, true );
      else if ( policyPolicy.equalsIgnoreCase( "multi-stepped" ) )
        policies = makeMultiSteppedPolicies( clients, false );
      else if ( policyPolicy.equalsIgnoreCase( "multi-stepped.df" ) )
        policies = makeMultiSteppedPolicies( clients, true );
      else if ( policyPolicy.equalsIgnoreCase( "multi-timed-direct" ) )
        policies = makeMultiTimedDirectPolicies( clients );
      else if ( policyPolicy.equalsIgnoreCase( "simulation" ) )
        policies = makeSimulationPolicies( clients );
      else
        throw new IllegalArgumentException( "unknown policy to create policies: " + policyPolicy );

      setPolicies( clients, policies );

      try {
        Thread.sleep( planningDelay * 1000 );
      } catch ( InterruptedException e ) {
        break;
      }
    }
  }

  /**
   * Sets the policies for a list of clients.
   * 
   * The indices in the client list and the policies list correspond,
   * i.e. policies.get(n) is for clients.get(n).
   * 
   * @param clients list of clients
   * @param policies list of policies for these clients
   */
  private void setPolicies( List<ClientData> clients, List<HCPolicy> policies ) {
    if ( clients.size() != policies.size() )
      throw new IllegalArgumentException();

    for ( int i = 0; i < clients.size(); i++ ) {
      ClientData client = clients.get( i );
      client.setActivePolicy( policies.get( i ) );
      server.startRenegotiation( client.getClientAddress() );
    }

  }

  /**
   * Implements one of the strategies of the server. This one creates policies
   * with random thresholds.
   * 
   * @param clients clients for which to calculate policies
   * @return a list of policies for the given clients
   */
  public List<HCPolicy> makeRandomPolicies( List<ClientData> clients ) {

    Random r = new Random();

    ArrayList<HCPolicy> result = new ArrayList<HCPolicy>();

    final double deltaFrequency = (highFrequency - lowFrequency) / 2.0;
    final double deltaPrice = (highPrice - lowPrice) / 2.0;

    for ( ClientData client : clients ) {

      RuleBaseCreator rbc = new RuleBaseCreator( "data/rulebase/hc-rulebase.template" );
      rbc.setActivationGroup( "hc" );

      double lf = lowFrequency + r.nextDouble() * deltaFrequency;
      double hf = highFrequency - r.nextDouble() * deltaFrequency;
      if ( lf > hf ) {
        logger.log( Level.WARNING, "have to fix frequency thresholds " + deltaFrequency );
        double tmp = lf;
        lf = hf;
        hf = tmp;
      }
      double lp = lowPrice + r.nextDouble() * deltaPrice;
      double hp = highPrice - r.nextDouble() * deltaPrice;
      if ( lp > hp ) {
        logger.log( Level.WARNING, "have to fix price thresholds " + deltaPrice );
        double tmp = lp;
        lp = hp;
        hp = tmp;
      }
      String msg = client.getClientAddress() + ": ";

      Thresholds freq = null;
      Thresholds price = null;

      if ( client.getDeviceInfo().getServices().supports( HCServices.fctService ) ) {
        msg += "[f " + lf + " " + hf + "] ";
        String[] setup = FreqHiLoThresholds.getSetup( lf, hf );
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/frequency-thresholds.template", FreqHiLoThresholds.class.getName(), FreqHiLoThresholds.getActiveTestString() );
        freq = new Thresholds( lf, hf );
      }

      if ( client.getDeviceInfo().getServices().supports( HCServices.dfctService ) ) {
        msg += "[f " + lf + " " + hf + "] ";
        String[] setup = DirectFreqHiLoThresholds.getSetup( lf, hf );
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/frequency-thresholds.template", DirectFreqHiLoThresholds.class.getName(), DirectFreqHiLoThresholds.getActiveTestString() );
        freq = new Thresholds( lf, hf );
      }


      if ( client.getDeviceInfo().getServices().supports( HCServices.tdfctService ) ) {
        throw new IllegalArgumentException( HCServices.tdfctService.getServiceName().toString() + " is unsupported" );
      }


      if ( client.getDeviceInfo().getServices().supports( HCServices.dppService ) ) {
        msg += "[p " + lp + " " + hp + "]";
        String[] setup = DynamicPowerPrice.getSetup( lp, hp );
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/dynamic-power-price.template", DynamicPowerPrice.class.getName(), DynamicPowerPrice.getActiveTestString() );
        price = new Thresholds( lp, hp );
      }
      logger.log( Level.FINE, msg );

      String ruleBase = rbc.createRuleBase();
      //logger.log( Level.FINEST, ruleBase );
      Date from = thisQuarterHour();
      HCPolicy policy = new HCPolicy( TimeFrame.createTimeFrame( from, 0, 0, VALIDITY_PERIOD ), ruleBase, freq, price, null );
      result.add( policy );
    }
    if ( result.size() != clients.size() )
      throw new IllegalArgumentException();

    return result;
  }

  /**
   * Implements one of the strategies of the server. This one creates policies
   * with staggered thresholds.
   * 
   * @param clients clients for which to calculate policies
   * @param directHeaterControl
   *          whether to use a controller that controls the heaters directly; if
   *          false, it uses a thermostat-controller to control the heaters
   * @return a list of policies for the given clients
   */
  public List<HCPolicy> makeSteppedPolicies( List<ClientData> clients, boolean directHeaterControl ) {

    final double frequencySpan = (highFrequency - lowFrequency) / 2 / getNumFrequencyControlledClients( clients );
    final double priceSpan = (highPrice - lowPrice) / 2 / getNumPriceControlledClients( clients );

    ArrayList<HCPolicy> result = new ArrayList<HCPolicy>();

    int idxf = 0;
    int idxp = 0;
    for ( ClientData client : clients ) {
      RuleBaseCreator rbc = new RuleBaseCreator( "data/rulebase/hc-rulebase.template" );
      rbc.setActivationGroup( "hc" );

      double lf = lowFrequency + idxf * frequencySpan;
      double hf = highFrequency - idxf * frequencySpan;

      double lp = lowPrice + idxp * priceSpan;
      double hp = highPrice - idxp * priceSpan;

      Thresholds freq = null;
      Thresholds price = null;

      String msg = client.getClientAddress() + ":";

      if ( client.getDeviceInfo().getServices().supports( HCServices.fctService ) || 
           client.getDeviceInfo().getServices().supports( HCServices.dfctService ) ) {
        String[] setup = null;
        String className;
        String activeTest;
        if ( directHeaterControl ) {
          setup = DirectFreqHiLoThresholds.getSetup( lf, hf );
          className = DirectFreqHiLoThresholds.class.getName();
          activeTest = DirectFreqHiLoThresholds.getActiveTestString();
        } else {
          setup = FreqHiLoThresholds.getSetup( lf, hf );
          className = FreqHiLoThresholds.class.getName();
          activeTest = FreqHiLoThresholds.getActiveTestString();
        }
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/frequency-thresholds.template", className, activeTest );
        msg += " [f " + lf + " " + hf + "] ";
        freq = new Thresholds( lf, hf );
        idxf++;
      }

      if ( client.getDeviceInfo().getServices().supports( HCServices.dppService ) ) {
        String[] setup = DynamicPowerPrice.getSetup( lp, hp );
        rbc.addSetup( setup[0], setup[1] );
        String testString = DynamicPowerPrice.getActiveTestString();
        rbc.addRule( "data/rulebase/dynamic-power-price.template", DynamicPowerPrice.class.getName(), testString );
        msg += "[p " + lp + " " + hp + "]";
        price = new Thresholds( lp, hp );
        idxp++;
      }
      logger.log( Level.FINE, msg );

      String ruleBase = rbc.createRuleBase();
      //logger.log( Level.FINEST, ruleBase );
      Date from = thisQuarterHour();
      HCPolicy policy = new HCPolicy( TimeFrame.createTimeFrame( from, 0, 0, VALIDITY_PERIOD ), ruleBase, freq, price, null );
      result.add( policy );
    }
    if ( result.size() != clients.size() )
      throw new IllegalArgumentException();

    return result;
  }

  /**
   * Implements one of the strategies of the server. This one creates policies
   * with staggered thresholds, with three different step widths, changing the
   * step width every MULTI_POLICIES_FREQUENCY milliseconds.
   * 
   * @param clients clients for which to calculate policies
   * @param directHeaterControl
   *        whether to use a controller that controls the heaters directly; if
   *        false, it uses a thermostat-controller to control the heaters
   * @return a list of policies for the given clients
   */
  public List<HCPolicy> makeMultiSteppedPolicies( List<ClientData> clients, boolean directHeaterControl ) {

    double lowFrequency;
    double highFrequency;
    double frequencySpan;

    ArrayList<HCPolicy> result = new ArrayList<HCPolicy>();

    long nowMillis = System.currentTimeMillis();
    long d = (nowMillis - startMillis) / MULTI_POLICIES_FREQUENCY;

    int round = (int) (d % 3);
    int numFrequencyControlledClients = getNumFrequencyControlledClients( clients );
    switch ( round ) {
    case 0:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_1;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_1;
      frequencySpan = REACTION_WIDTH_1 / numFrequencyControlledClients;
      break;
    case 1:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_2;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_2;
      frequencySpan = REACTION_WIDTH_2 / numFrequencyControlledClients;
      break;
    case 2:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_3;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_3;
      frequencySpan = REACTION_WIDTH_3 / numFrequencyControlledClients;
      break;
    default:
      throw new AssertionError();
    }

    final double priceSpan = (highPrice - lowPrice) / 2 / getNumPriceControlledClients( clients );

    logger.log( Level.FINE, "round " + round + "; " + lowFrequency + " : " + highFrequency );

    int idxf = 0;
    int idxp = 0;
    for ( ClientData client : clients ) {

      RuleBaseCreator rbc = new RuleBaseCreator( "data/rulebase/hc-rulebase.template" );
      rbc.setActivationGroup( "hc" );

      double lf = lowFrequency + idxf * frequencySpan;
      double hf = highFrequency - idxf * frequencySpan;

      double lp = lowPrice + idxp * priceSpan;
      double hp = highPrice - idxp * priceSpan;

      Thresholds freq = null;
      Thresholds price = null;

      String msg = client.getClientAddress() + " " + idxf + " " + idxp + ":";

      if ( client.getDeviceInfo().getServices().supports( HCServices.fctService ) ) {
        String[] setup = FreqHiLoThresholds.getSetup( lf, hf );
        rbc.addSetup( setup[0], setup[1] );
        String className;
        String activeTest;
        if ( directHeaterControl ) {
          className = DirectFreqHiLoThresholds.class.getName();
          activeTest = DirectFreqHiLoThresholds.getActiveTestString();
        } else {
          className = FreqHiLoThresholds.class.getName();
          activeTest = FreqHiLoThresholds.getActiveTestString();
        }
        rbc.addRule( "data/rulebase/frequency-thresholds.template", className, activeTest );
        msg += " [f " + lf + " " + hf + "] ";
        freq = new Thresholds( lf, hf );
        idxf++;
      }

      if ( client.getDeviceInfo().getServices().supports( HCServices.dppService ) ) {
        String[] setup = DynamicPowerPrice.getSetup( lp, hp );
        rbc.addSetup( setup[0], setup[1] );
        String testString = DynamicPowerPrice.getActiveTestString();
        rbc.addRule( "data/rulebase/dynamic-power-price.template", DynamicPowerPrice.class.getName(), testString );
        msg += "[p " + lp + " " + hp + "]";
        price = new Thresholds( lp, hp );
        idxp++;
      }
      
      logger.log( Level.FINE, msg );

      String ruleBase = rbc.createRuleBase();
      //logger.log( Level.FINEST, ruleBase );
      Date from = thisQuarterHour();
      HCPolicy policy = new HCPolicy( TimeFrame.createTimeFrame( from, 0, 0, VALIDITY_PERIOD ), ruleBase, freq, price, null );
      result.add( policy );
    }
    if ( result.size() != clients.size() )
      throw new IllegalArgumentException();

    return result;
  }

  /**
   * Implements one of the strategies of the server. This one creates policies
   * with staggered timed thresholds, with three different step widths, changing the
   * step width every MULTI_POLICIES_FREQUENCY milliseconds. "timed" means that the
   * frequency control is not active all the time, only in a specified time window.
   * 
   * @param clients clients for which to calculate policies
   * @return a list of policies for the given clients
   */
  public List<HCPolicy> makeMultiTimedDirectPolicies( List<ClientData> clients ) {

    double lowFrequency;
    double highFrequency;
    double frequencySpan;

    ArrayList<HCPolicy> result = new ArrayList<HCPolicy>();

    long nowMillis = System.currentTimeMillis();
    long d = (nowMillis - startMillis) / MULTI_POLICIES_FREQUENCY;

    int numFrequencyControlledClients = getNumFrequencyControlledClients( clients );

    int round = (int) (d % 3);
    switch ( round ) {
    case 0:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_1;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_1 / 2;
      frequencySpan = REACTION_WIDTH_1 / 2;
      break;
    case 1:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_2;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_2 / 2;
      frequencySpan = REACTION_WIDTH_2 / 2;
      break;
    case 2:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_3;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_3 / 2;
      frequencySpan = REACTION_WIDTH_3 / 2;
      break;
    default:
      throw new AssertionError();
    }

    final double priceSpan = (highPrice - lowPrice) / 2 / getNumPriceControlledClients( clients );

    logger.log( Level.FINE, "round " + round + "; " + lowFrequency + " : " + highFrequency );

    int idxf = 0;
    int idxp = 0;
    for ( ClientData client : clients ) {

      RuleBaseCreator rbc = new RuleBaseCreator( "data/rulebase/hc-rulebase.template" );
      rbc.setActivationGroup( "hc" );

      double lf = lowFrequency + frequencySpan;
      double hf = highFrequency - frequencySpan;

      double lp = lowPrice + idxp * priceSpan;
      double hp = highPrice - idxp * priceSpan;

      Thresholds freq = null;
      Thresholds price = null;

      int windowFrame = 15 * 60;
      int timeSlice = windowFrame / numFrequencyControlledClients;

      String msg = client.getClientAddress() + " " + idxf + " " + idxp + ":";

      TimeWindow tw = null;
      if ( client.getDeviceInfo().getServices().supports( HCServices.fctService ) || 
           client.getDeviceInfo().getServices().supports( HCServices.dfctService ) || 
           client.getDeviceInfo().getServices().supports( HCServices.tdfctService ) ) {
        int startTime = timeSlice * idxf;
        tw = new TimeWindow( windowFrame, startTime, timeSlice );
        String[] setup = TimedDirectFreqHiLoThresholds.getSetup( lf, hf, tw );
        String className = TimedDirectFreqHiLoThresholds.class.getName();
        String activeTest = TimedDirectFreqHiLoThresholds.getActiveTestString();
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/frequency-thresholds.template", className, activeTest );
        msg += " [f " + lf + " " + hf + "] ";
        freq = new Thresholds( lf, hf );
        idxf++;
      }

      if ( client.getDeviceInfo().getServices().supports( HCServices.dppService ) ) {
        String[] setup = DynamicPowerPrice.getSetup( lp, hp );
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/dynamic-power-price.template", DynamicPowerPrice.class.getName(), DynamicPowerPrice.getActiveTestString() );
        msg += "[p " + lp + " " + hp + "]";
        price = new Thresholds( lp, hp );
        idxp++;
      }

      logger.log( Level.FINE, msg );

      String ruleBase = rbc.createRuleBase();
      //logger.log( Level.FINEST, ruleBase );
      Date from = thisQuarterHour();
      HCPolicy policy = new HCPolicy( TimeFrame.createTimeFrame( from, 0, 0, VALIDITY_PERIOD ), ruleBase, freq, price, tw );
      result.add( policy );
    }
    if ( result.size() != clients.size() )
      throw new IllegalArgumentException();

    return result;
  }

  /**
   * Implements one of the strategies of the server. This one creates policies
   * for the flexpower simulation (which {@link RunSimulation} runs).
   * 
   * @see RunSimulation
   * @param clients clients for which to calculate policies
   * @return a list of policies for the given clients
   */
  public List<HCPolicy> makeSimulationPolicies( List<ClientData> clients ) {

    double lowFrequency;
    double highFrequency;
    double frequencySpan;

    ArrayList<HCPolicy> result = new ArrayList<HCPolicy>();

    long nowMillis = System.currentTimeMillis();
    long d = (nowMillis - startMillis) / MULTI_POLICIES_FREQUENCY;

    int numFrequencyControlledClients = getNumFrequencyControlledClients( clients );

    int round = (int) (d % 3);
    switch ( round ) {
    case 0:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_1;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_1 / 2;
      frequencySpan = REACTION_WIDTH_1 / 2;
      break;
    case 1:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_2;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_2 / 2;
      frequencySpan = REACTION_WIDTH_2 / 2;
      break;
    case 2:
      lowFrequency = NORMAL_FREQUENCY - FREQUENCY_DEADBAND - REACTION_WIDTH_3;
      highFrequency = NORMAL_FREQUENCY + FREQUENCY_DEADBAND + REACTION_WIDTH_3 / 2;
      frequencySpan = REACTION_WIDTH_3 / 2;
      break;
    default:
      throw new AssertionError();
    }

    final double priceSpan = (highPrice - lowPrice) / 2 / getNumPriceControlledClients( clients );

    logger.log( Level.INFO, "round " + round + "; " + lowFrequency + " : " + highFrequency );

    int idxf = 0;
    int idxp = 0;
    for ( ClientData client : clients ) {

      RuleBaseCreator rbc = new RuleBaseCreator( "data/rulebase/hc-rulebase.template" );
      rbc.setActivationGroup( "hc" );

      double lf = lowFrequency + frequencySpan;
      double hf = highFrequency - frequencySpan;

      double lp = lowPrice + idxp * priceSpan;
      double hp = highPrice - idxp * priceSpan;

      Thresholds freq = null;
      Thresholds price = null;
      TimeWindow tw = null;

      int windowFrame = 15 * 60;
      // TODO This is stupid when many clients are used: the timeSlice should be something like 
      // 1 minute, and several houses should provide the service at the same time.
      int timeSlice = windowFrame / numFrequencyControlledClients;

      String msg = client.getClientAddress() + " " + idxf + " " + idxp + ":";

      if (client.getDeviceInfo().getServices().supports( HCServices.tdfctService ) ) {
        int startTime = timeSlice * idxf;
        tw = new TimeWindow( windowFrame, startTime, timeSlice );
        String[] setup = FrequencyBasedService.getSetup( lf, hf, tw );
        String className = FrequencyBasedService.class.getName();
        String activeTest = FrequencyBasedService.getActiveTestString();
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/frequency-thresholds.template", className, activeTest );
        msg += " [f " + lf + " " + hf + "] ";
        freq = new Thresholds( lf, hf );
        idxf++;
      }
      else if ( client.getDeviceInfo().getServices().supports( HCServices.fctService ) || 
                client.getDeviceInfo().getServices().supports( HCServices.dfctService ) ) {
       String[] setup = DirectFreqHiLoThresholds.getSetup( lf, hf );
       String className = DirectFreqHiLoThresholds.class.getName();
       String activeTest = DirectFreqHiLoThresholds.getActiveTestString();
       rbc.addSetup( setup[0], setup[1] );
       rbc.addRule( "data/rulebase/frequency-thresholds.template", className, activeTest );
       msg +=" [f " + lf + " " + hf + "] ";
       freq = new Thresholds( lf, hf );
       idxf++;
     }

      if ( client.getDeviceInfo().getServices().supports( HCServices.dppService ) ) {
        String[] setup = PriceBasedService.getSetup( lp, hp );
        rbc.addSetup( setup[0], setup[1] );
        rbc.addRule( "data/rulebase/dynamic-power-price.template", PriceBasedService.class.getName(), PriceBasedService.getActiveTestString() );
        msg += "[p " + lp + " " + hp + "]";
        price = new Thresholds( lp, hp );
        idxp++;
      }

      logger.log( Level.FINE, msg );
      
      String ruleBase = rbc.createRuleBase();
      //logger.log( Level.FINEST, ruleBase );
      Date from = thisQuarterHour();
      HCPolicy policy = new HCPolicy( TimeFrame.createTimeFrame( from, 0, 0, VALIDITY_PERIOD ), ruleBase, freq, price, tw );
      result.add( policy );
    }
    if ( result.size() != clients.size() )
      throw new IllegalArgumentException();

    return result;
  }

  /** Helper method, getting the number of clients that are price controlled */
  private int getNumPriceControlledClients( Collection<ClientData> clients ) {
    int num = 0;
    for ( ClientData client : clients ) {
      if ( server.getClientStatus( client.getClientAddress() ) == ClientStatus.MISSING )
        continue;
      if ( client.getDeviceInfo().getServices().supports( HCServices.dppService ) )
        num += 1;
    }
    return num;
  }

  /** Helper method, getting the number of clients that are frequency controlled */
  private int getNumFrequencyControlledClients( Collection<ClientData> clients ) {
    int num = 0;
    for ( ClientData client : clients ) {
      if ( server.getClientStatus( client.getClientAddress() ) == ClientStatus.MISSING )
        continue;
      if ( client.getDeviceInfo().getServices().supports( HCServices.fctService ) || 
           client.getDeviceInfo().getServices().supports( HCServices.dfctService ) ||
           client.getDeviceInfo().getServices().supports( HCServices.tdfctService ) )
        num += 1;
    }
    return num;
  }

  /**
   * Gets the date object that represents the start of the current quarter hour.
   * 
   * @return a date object, the next date from now that lies on a quarter hour
   *         boundary
   */
  private Date thisQuarterHour() {
    Calendar cal = new GregorianCalendar();
    cal.setTime( new Date() );
    cal.set( Calendar.SECOND, 0 );
    cal.set( Calendar.MILLISECOND, 0 );
    int minute = cal.get( Calendar.MINUTE );
    minute = minute - (minute % 15);
    cal.set( Calendar.MINUTE, minute );
    return cal.getTime();
  }

  /**
   * Checks whether the given policy expires soon..
   * 
   * @param policy
   *          the policy to be checked
   * @return returns true, iff the given policy expires soon, i.e. within the
   *         next 5 minutes
   */
  private boolean soonExpires( Policy activePolicy ) {
    Calendar cal = new GregorianCalendar();
    cal.setTime( new Date() );
    cal.add( Calendar.MINUTE, 5 );
    Date soon = cal.getTime();
    return soon.after( activePolicy.getTimeFrame().getUntil() );
  }

  /** Getter for the planningDelay field.
   * @return frequency of planning [s]
   */
  public int getPlanningDelay() {
    return planningDelay;
  }

  /** Setter for the planningDelay field.
   * @param planningDelay frequency of planning [s]
   */
  public void setPlanningDelay( int planningDelay ) {
    this.planningDelay = planningDelay;
  }

  
  public static int getExpectMinNumberOfClients() {
    return expectMinNumberOfClients;
  }

  
  public static void setExpectMinNumberOfClients( int expectMinNumberOfClients ) {
    HCPlanner.expectMinNumberOfClients = expectMinNumberOfClients;
  }

}
