/**
 * @author daniel.kullmann@risoe.dk
 */
package risoe.syslab.standards.cim;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * 
 */
public class ExtractBasicInformation {

  // TODO: This probably won't work on Windows, due to the usage of "/" 
  private static final String DIRECTORY = "data/cim/";
  private static final String XMI_FILE_NAME = DIRECTORY + "iec61970cim13v19_iec61968cim10v18_combined.xmi.gz";
  private static final String OUT_FILE_NAME = DIRECTORY + "cim-simple.txt";

  private HashMap<String, CimData> classData = new HashMap<String, CimData>();
  private HashMap<String, PackageData> packageData = new HashMap<String, PackageData>();
  private HashMap<String, String> childParent = new HashMap<String, String>();

  /**
   * @param args
   */
  public static void main( String[] args ) {

    new ExtractBasicInformation().run();

  }

  /**
   * 
   */
  private void run() {

    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      GZIPInputStream gis = new GZIPInputStream(new FileInputStream(XMI_FILE_NAME));
      // Fix to have the XML parser find the UML_EA.dtd file in data/cim
      String oldCurrentDir = System.getProperty("user.dir");
      System.setProperty( "user.dir", oldCurrentDir + "/" + DIRECTORY );
      Document doc = db.parse(gis);
      System.setProperty( "user.dir", oldCurrentDir );

      Element base = searchElement( doc.getDocumentElement(), "UML:Model" );
      parseModel1( base );
      parseModel2( base );
      postProcess();
      printIt();
    } catch ( ParserConfigurationException e ) {
      e.printStackTrace();
    } catch ( SAXException e ) {
      e.printStackTrace();
    } catch ( IOException e ) {
      e.printStackTrace();
    }

  }

  private static String formatDesc( String description, String prefix ) {
    if ( description == null )
      return "";
    description = description.replaceAll( "\n", " " );
    description = description.replaceAll( "  +", " " );
    String result = "";
    while ( description.length() > 82 ) {
      result = result + "\n" + prefix;
      int cut = Math.min( description.length() - 1, 82 );
      while ( cut > 0 ) {
        if ( description.charAt( cut ) == ' ' ) {
          cut++;
          break;
        }
        cut--;
      }
      if ( cut < 40 ) {
        // This happens when there are long parts of text with no spaces
        cut = Math.min( description.length(), 80 );
      }
      result = result + description.substring( 0, cut );
      description = description.substring( cut );
    }
    if ( !"".equals( description ) ) {
      result = result + "\n" + prefix + description;
    }
    return result;
  }

  /**
   * 
   */
  private void printIt() {
    try {
      FileOutputStream os = new FileOutputStream( OUT_FILE_NAME );
      PrintWriter out = new PrintWriter( os );
      List<String> sortedNames = new ArrayList<String>( classData.keySet() );
      Collections.sort( sortedNames );
      for ( String className : sortedNames ) {
        CimData data = classData.get( className );
        out.println( "== " + className );
        out.println( "  package: " + data.getPackageName() );
        out.println( "  description: " + formatDesc( data.getDescription(), "    " ) );
        String parents = data.getAllParents();
        if ( parents.length() > 0 ) out.println( "  parents: " + parents );
        String children = data.getAllChildren();
        if ( children.length() > 0 ) out.println( "  children: " + children );
        String attributes = data.getAttributes();
        if ( !"".equals( attributes ) ) {
          out.println( "  attributes: " );
          out.print( attributes );
        }

        String associations = data.getAssociations();
        if ( !"".equals( associations ) ) {
          out.println( "  associations: " );
          out.print( associations );
        }

        String backAssociations = data.getBackAssociations();
        if ( !"".equals( backAssociations ) ) {
          out.println( "  back associations: " );
          out.print( backAssociations );
        }
      }
      out.close();
    } catch ( FileNotFoundException e ) {
      e.printStackTrace();
    }
  }

  /**
   * 
   */
  private void postProcess() {
    for ( CimData data : classData.values() ) {
      String packages = "";
      String p = data.getPackageName();
      while ( p != null && !p.equals( "" ) && !p.equals( "IEC61970_IEC61968_combined" ) ) {
        if ( !packages.equals( "" ) )
          packages = packages + " : ";
        packages = packages + p;
        String parent = packageData.get( p ).getParentXmiId();
        if ( !packageData.containsKey( parent ) )
          break;
        p = packageData.get( parent ).getName();
      }
      data.setPackageName( packages );
    }

    HashMap<String, ArrayList<String>> childInfo = new HashMap<String, ArrayList<String>>();
    
    // create parent and child list
    for ( CimData data : classData.values() ) {
      String parent = data.getParentType();
      while ( parent != null && !parent.equals( "" ) ) {

        if ( ! childInfo.containsKey( parent ) ) {
          childInfo.put( parent, new ArrayList<String>() );
        }
        childInfo.get( parent ).add( data.getName() );

        data.addParent( parent );
        parent = childParent.get( parent );
      }
    }

    for ( CimData data : classData.values() ) {
      if ( childInfo.containsKey( data.getName() ) ) {
        data.allChildren = childInfo.get( data.getName() );
      }
      
    }

  }

  /**
   * @param elem
   * @return
   */
  private Element searchElement( Element elem, String tagName ) {
    Node child = elem.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element && child.getNodeName().equals( tagName ) ) {
        return (Element) child;
      }
      child = child.getNextSibling();
    }

    child = elem.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element ) {
        Element result = searchElement( (Element) child, tagName );
        if ( result != null ) {
          return (Element) child;
        }
      }
      child = child.getNextSibling();
    }

    return null;
  }

  /**
   */
  private void parseModel1( Element base ) {
    Node child = base.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element ) {
        if ( child.getNodeName().equals( "UML:Class" ) ) {
          handleClass( (Element) child );
        }
      }
      child = child.getNextSibling();
    }

    child = base.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element ) {
        parseModel1( (Element) child );
      }
      child = child.getNextSibling();
    }

  }

  /**
   */
  private void parseModel2( Element base ) {
    Node child = base.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element ) {
        if ( child.getNodeName().equals( "UML:Association" ) ) {
          handleAssoc( (Element) child );
        } else if ( child.getNodeName().equals( "UML:Generalization" ) ) {
          handleParent( (Element) child );
        } else if ( child.getNodeName().equals( "UML:Package" ) ) {
          handlePackage( (Element) child );
        }
      }
      child = child.getNextSibling();
    }

    child = base.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element ) {
        parseModel2( (Element) child );
      }
      child = child.getNextSibling();
    }

  }

  /**
   */
  private void handleParent( Element elem ) {
    String childType = getTaggedValue( elem, "ea_sourceName" );
    String parentType = getTaggedValue( elem, "ea_targetName" );
    classData.get( childType ).setParentType( parentType );
    childParent.put( childType, parentType );
  }

  /**
   */
  private void handleAssoc( Element elem ) {
    String startType = getTaggedValue( elem, "ea_sourceName" );
    String endType = getTaggedValue( elem, "ea_targetName" );
    String role1 = getTaggedValue( elem, "rt" );
    String role2 = getTaggedValue( elem, "lt" );
    String doc = getTaggedValue( elem, "documentation" );
    if ( role1.startsWith( "+" ) )
      role1 = role1.substring( 1 );
    if ( role2.startsWith( "+" ) )
      role2 = role2.substring( 1 );
    classData.get( startType ).addAssociation( role1, endType + " (" + getTaggedValue( elem, "rb" ) + ")", doc );
    classData.get( endType ).addBackAssociation( role2, startType + " (" + getTaggedValue( elem, "lb" ) + ")", doc );
  }

  /**
   */
  private void handlePackage( Element elem ) {
    String name = elem.getAttribute( "name" );
    String xmiId = elem.getAttribute( "xmi.id" );
    String parentXmiId = getTaggedValue( elem, "parent" );
    PackageData data = new PackageData( name, parentXmiId );
    packageData.put( name, data );
    packageData.put( xmiId, data );
  }

  /**
   * @param child
   */
  private void handleClass( Element elem ) {
    CimData data = new CimData( elem.getAttribute( "name" ) );
    data.setPackageName( getTaggedValue( elem, "package_name" ) );
    data.setDescription( getTaggedValue( elem, "documentation" ) );
    classData.put( data.getName(), data );
    handleAttributes( elem, data );
  }

  /**
   * @param elem
   * @param data
   */
  private void handleAttributes( Element elem, CimData data ) {
    Node child = elem.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element ) {
        Element cElem = (Element) child;
        if ( child.getNodeName().equals( "UML:Attribute" ) ) {
          String name = cElem.getAttribute( "name" );
          String type = getTaggedValue( cElem, "type" );
          String desc = getTaggedValue( cElem, "description" );
          data.addAttribute( name, type, desc );
        } else {
          handleAttributes( (Element) child, data );
        }
      }
      child = child.getNextSibling();
    }
  }

  private String getTaggedValue( Element elem, String tagName ) {
    Node child = elem.getFirstChild();
    while ( child != null ) {
      if ( child instanceof Element ) {
        if ( child.getNodeName().equals( "UML:TaggedValue" ) ) {
          if ( tagName.equals( ((Element) child).getAttribute( "tag" ) ) ) {
            return ((Element) child).getAttribute( "value" );
          }
        }

        String result = getTaggedValue( (Element) child, tagName );
        if ( result != null )
          return result;
      }

      child = child.getNextSibling();
    }
    return null;
  }

  private static class PackageData {
    private String name;
    private String parentXmiId;

    public PackageData( String name, String parentXmiId ) {
      super();
      this.name = name;
      this.parentXmiId = parentXmiId;
    }

    public String getName() {
      return name;
    }

    public String getParentXmiId() {
      return parentXmiId;
    }

  }

  private static class AttributeData {
    private String name = null;
    private String description = null;
    private String type = null;

    public AttributeData( String name, String type, String description ) {
      super();
      this.name = name;
      this.type = type;
      this.description = description == null ? "" : description.replaceAll( "\n", " " );
    }

    public String getInfo() {
      if ( type == null && ( description == null || "".equals( description ) ) ) {
        return "    " + name + "\n";
      } else if ( type == null ) {
        return "    " + name + formatDesc( description, "      " ) + "\n";
      }
      return "    " + name + ": " + type + formatDesc( description, "      " ) + "\n";
    }

  }

  private static class AssociationData {
    private String name;
    private String type;
    private String doc;

    /**
     * @param name
     * @param role
     * @param doc
     */
    public AssociationData( String name, String type, String doc ) {
      this.name = name;
      this.type = type;
      this.doc = doc;
    }

    public String getName() {
      return name;
    }

    public String getType() {
      return type;
    }

    public String getDoc() {
      return doc;
    }
  }

  private static class CimData {

    private String name = null;
    private String description = null;
    private String parentType = null;
    private String packageName = null;
    private ArrayList<String> allParents = new ArrayList<String>();
    private ArrayList<String> allChildren = new ArrayList<String>();
    // attributes: name -> type
    private HashMap<String, AttributeData> attributes = new HashMap<String, AttributeData>();
    // associations
    private ArrayList<AssociationData> associations = new ArrayList<AssociationData>();

    // back-associations
    private ArrayList<AssociationData> backAssociations = new ArrayList<AssociationData>();

    public CimData( String name ) {
      super();
      this.name = name;
    }

    public String getAttributes() {
      String result = "";
      for ( AttributeData attrData : attributes.values() ) {
        result = result + attrData.getInfo();
      }
      return result;
    }

    public String getAssociations() {
      String result = "";
      for ( AssociationData assoc : associations ) {
        result = result + "    " + assoc.getName() + ": " + assoc.getType();
        String doc = assoc.getDoc();
        if ( doc != null && !"".equals( doc ) ) {
          result = result + formatDesc( doc, "      " );
        }
        result = result + "\n";
      }
      return result;
    }

    public String getBackAssociations() {
      String result = "";
      for ( AssociationData assoc : backAssociations ) {
        result = result + "    " + assoc.getName() + ": " + assoc.getType();
        String doc = assoc.getDoc();
        if ( doc != null && !"".equals( doc ) ) {
          result = result + formatDesc( doc, "      " );
        }
        result = result + "\n";
      }
      return result;
    }

    public String getAllParents() {
      String result = "";
      for ( String parent : allParents ) {
        if ( result.length() > 0 )
          result = result + " ";
        result = result + parent;
      }
      return result;
    }

    public String getAllChildren() {
      // IdentifiedObject has too many children..
      if ( name.equals( "IdentifiedObject" ) ) return "...";
      String result = "";
      for ( String parent : allChildren ) {
        if ( result.length() > 0 )
          result = result + " ";
        result = result + parent;
      }
      return result;
    }

    public void addAttribute( String name, String type, String description ) {
      attributes.put( name, new AttributeData( name, type, description ) );
    }

    public void addAssociation( String name, String type, String doc ) {
      associations.add( new AssociationData( name, type, doc ) );
    }

    public void addBackAssociation( String name, String type, String doc ) {
      backAssociations.add( new AssociationData( name, type, doc ) );
    }

    public void setParentType( String parentType ) {
      this.parentType = parentType;
    }

    public String getName() {
      return name;
    }

    public String getParentType() {
      return parentType;
    }

    public void setPackageName( String packageName ) {
      this.packageName = packageName;
    }

    public String getPackageName() {
      return packageName;
    }

    public void addParent( String parent ) {
      allParents.add( parent );
    }

    public String getDescription() {
      return description;
    }

    public void setDescription( String description ) {
      this.description = description;
    }

  }

}
