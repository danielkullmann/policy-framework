set datafile separator ";"
set terminal png 
set output "testrun-upbw.png"
set title "Up Bandwidth"
set xlabel "Bandwidth [byte/ms]"
set ylabel "Time [ms]"
set key left bottom

plot "testrun-upbw.dat" using 3:5 title "time [ms]" with lines


