set datafile separator ";"
set terminal png 
set output "testrun-latency.png"
set title "Latency"
set xlabel "Latency [ms]"
set ylabel "Time [ms]"
set key left bottom

plot "testrun-latency.dat" using 2:5 title "time [ms]" with lines


