set datafile separator ";"
set terminal png 
set output "testrun-downbw.png"
set title "Down Bandwidth"
set xlabel "Bandwidth [byte/ms]"
set ylabel "Time [ms]"
set key left bottom

plot "testrun-downbw.dat" using 4:5 title "time [ms]" with lines


