/* time steps */
set Time;

/* connected clients */
set Clients;

/* maximal number of clients that may be charged at any time */
param max_num_clients default 2;

/* power price */
param power_price{t in Time};

/* time index, needed for observe_charge_end */
param time_index{t in Time};

/* how many time steps does the car need to be charged? */
param charge_needed{c in Clients};

/* last time step the car can be charged */
param charge_end{c in Clients}, integer;

/* charging of client c at time step t */
var charging{c in Clients, t in Time}, binary;

/* cost of charging vehicles */
minimize cost: sum{t in Time, c in Clients} power_price[t] * charging[c,t];

/* observe max_num_clients */
s.t. observe_max_num_clients{t in Time}: sum{c in Clients} charging[c,t] <= max_num_clients;

/* observe charge_needed */
s.t. observe_charge_needed{c in Clients}: sum{t in Time} charging[c,t] == charge_needed[c];

/* observe charge_end */
s.t. observe_charge_end{c in Clients, t in Time}: if time_index[t] > charge_end[c] then charging[c,t] == 0;

data;

set Time := q_2010_01_19_10_00 q_2010_01_19_10_15 q_2010_01_19_10_30 q_2010_01_19_10_45 q_2010_01_19_11_00;

set Clients := c_1 c_2;

param time_index :=
    q_2010_01_19_10_00 1 
    q_2010_01_19_10_15 2
    q_2010_01_19_10_30 3
    q_2010_01_19_10_45 4
    q_2010_01_19_11_00 5;

param power_price := 
    q_2010_01_19_10_00 10 
    q_2010_01_19_10_15 11
    q_2010_01_19_10_30 12
    q_2010_01_19_10_45 13
    q_2010_01_19_11_00 8;
    
param charge_needed := 
    c_1 3 
    c_2 2;
    
param charge_end :=
    c_1 3
    c_2 5;
    
end;
