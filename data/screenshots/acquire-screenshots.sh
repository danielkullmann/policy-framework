#!/bin/bash

scp flexhouse:/home/syslab/poli*.xwd . || exit 1

for i in *.xwd; do 
  convert "$i" "${i%%.xwd}.png" && (rm "$i"; ssh -l syslab flexhouse rm /home/syslab/$i )
done

