#!/bin/bash
# To be run on the flexhouse machine to make screenhsots of the visualisation

PREFIX=policy-vis-

if test -n "$1"; then
  PREFIX="$1"
fi

DISPLAY=:0 xwd -root > "$PREFIX"-$(date +"%Y-%m-%d-%H%M").xwd
