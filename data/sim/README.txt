This directory contains files for the simulation using the flexpower-simulation code base
(https://vea-e02.risoe.dk/hg/flexpower-simulation/)

models.csv: File containing the models configuration for the simulation run
price_forecast.csv: File containing the price forecast
