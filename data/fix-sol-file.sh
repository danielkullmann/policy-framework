#!/bin/sh

FILE="$1"
RFILE="${FILE%.txt}.2.txt"

if test "!" -f "$1"; then
  echo Usage: ...
  exit 1
fi

cat "$FILE" | while read a; do 
  if test -z "`echo $a | egrep \\]\\$`"; then
    echo $a; 
  else
    echo -n $a " "
  fi
done | tr -s " " | fgrep "charging[" | cut -d" " -f 2,3,4 | sort > $RFILE

