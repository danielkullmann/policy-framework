The Policy Framework
====================

This is part of the result of my PhD work. Unfortunately, this will 
not compile cleanly for people outside of our research group, due to 
dependencies on the internal projects SYSLAB ad PowerFlexhouse.


= Used libraries:

* Java (>= 1.6)
* SYSLAB (internal DTU project; the development branch)
** https://vea-e02.risoe.dk/hg/syslab/
* PowerFlexhouse (internal DTU project)
** https://vea-e02.risoe.dk/hg/flexhouse
* JADE (>= 3.7) (optional)
** jade-3.7.jar
* Commons collections in generics version, available from
** http://larvalabs.com/collections/
** http://sourceforge.net/projects/collections/files/
* GLPK (>= 4.39)
** http://www.gnu.org/s/glpk/
** for solving linear programming problem (ev charging example)
* junit (>= 4.5) 
** for unit tests
* drools (>= 5.1.1)
** http://www.jboss.org/drools/
** for rule bases in heatercontrol example
* apache http-core, http-client and httpmime (>= 4.0.1)
** http://hc.apache.org/
** used for http-based communication (HttpCommunicationSystem)
* apache commons-codec (>= 1.4)
** unused?
* apache commons-lang (>= 2.6)
** method join in class StringUtils is used for creating rule bases
** could be replaced easily with own code
* apache commons-logging (>= 1.1.1)
** used for logging
* log4j (>=1.2.15)
** used by JADE for logging
* jfreechart (>= 1.0.13)
** For graphs in visualisations
