#!/bin/bash

NAMES="syslab-00 syslab-ui0 syslab-ui1 syslab-ui2 syslab-ui3 syslab-aux syslab-01\
 syslab-02 syslab-03 syslab-04 syslab-05 syslab-06 syslab-07 syslab-08 syslab-09\
 syslab-10 syslab-11 syslab-12 syslab-13 syslab-14 syslab-15 syslab-16 syslab-17\
 syslab-18 syslab-19 syslab-20 claus syslab-ui4 flexhouse flexhouse-vis battery-vis\
 syslab-v01 syslab-v02 syslab-v03 syslab-v04 syslab-v05 syslab-w319 syslab-sw0 syslab-sw1\
 lp-012756 evc1"

NUM=1
while test $NUM -le 2000; do
  NAMES="$NAMES client-$NUM"
  NUM=$((NUM+1))
done

STORE=conf/keystore.jks
PUB_STORE=conf/keystore-pub.jks

STORE_PW="ek4j.t5,rmn.20ef.rdjf"
PUB_STORE_PW="sa3l.7kf4dj.klj"

KNOWN_NAMES=$(keytool -list -storetype jks -keystore "$STORE" -storepass "$STORE_PW" -v | grep "Alias name:" | cut -d" " -f 3)

for N in $NAMES; do 

  if echo $KNOWN_NAMES | egrep -q "\\b$N\\b"; then
    echo "$N already known; skip"
    continue
  fi

  KEY_PW=pw-$(echo $N | sed -e "s/syslab-/sys-/")-2010
  echo $N: $KEY_PW
  
  # create the private/public key pair
  keytool -genkeypair -storetype jks -keystore "$STORE" -keysize 1024 \
    -storepass "$STORE_PW" -keypass "$KEY_PW" \
    -alias "$N" -dname "cn=$N,ou=identity,o=syslab.risoe.dk"
    

  # clone the key for <hostname>.risoe.dk
  keytool -keyclone -storetype jks -keystore "$STORE" \
    -storepass "$STORE_PW" -keypass "$KEY_PW" -new "$KEY_PW" \
    -alias "$N" -dest "$N.risoe.dk"
  
  
  # copy the public key for <hostname> to the public keystore
  keytool -export -storetype jks -keystore "$STORE" \
    -storepass "$STORE_PW" -keypass "$KEY_PW" \
    -alias "$N" > "temp.cert"
    
  keytool -import -storetype jks -keystore "$PUB_STORE" \
    -storepass "$PUB_STORE_PW" -alias "$N" \
    -noprompt -file "temp.cert"
    
  rm -f temp.cert

  # copy the public key for <hostname>.risoe.dk to the public keystore
  keytool -export -storetype jks -keystore "$STORE" \
    -storepass "$STORE_PW" -keypass "$KEY_PW" \
    -alias "$N.risoe.dk" > "temp.cert"
    
  keytool -import -storetype jks -keystore "$PUB_STORE" \
    -storepass "$PUB_STORE_PW" -alias "$N.risoe.dk" \
    -noprompt -file "temp.cert"
  
  
  if test "$N" = "syslab-00"; then
    keytool -import -storetype jks -keystore "$PUB_STORE" \
      -storepass "$PUB_STORE_PW" -alias "10.0.17.186" \
      -noprompt -file "temp.cert"
  fi

  rm -f temp.cert

done

#keytool -list -storetype jks -keystore "$STORE" -storepass "$STORE_PW"

