#!/usr/bin/env python

import sys, re, string, subprocess
from datetime import datetime
import helpers

if len(sys.argv) < 2: 
  print "Usage"
  sys.exit(1)

# Example datetime string: 25-Apr-2012 18:31:36
DATE_FORMAT = "%d-%b-%Y %H:%M:%S"

# Sort "client-<num>" by <num>
offset = len("client-")
def compareFun(o1,o2):
  i1 = int(o1[offset:])
  i2 = int(o2[offset:])
  return cmp(int(i1), int(i2))

# Example lines:
#25-Apr-2012 18:31:36 risoe.syslab.control.policy.core.PolicyClient action
#FINE: client-1: client status: SEARCHING_SERVER
stateHeader = re.compile("^(.*) risoe.syslab.control.policy.core.PolicyClient action$")
stateLine   = re.compile("^FINE: (.*): client status: (.*)$")
lbaLine     = re.compile("^WARNING: lb settings: ([0-9.]*), ([0-9.]*), ([0-9.]*), ([0-9.]*), ([0-9.]*), ([0-9.]*)$")

stateMap = {
    'SEARCHING_SERVER': 1,
    'NEGOTIATE' : 2,
    'WAIT_FOR_POLICY' : 3,
    'ACTIVE': 4,
}

for fileName in sys.argv[1:]:

  # client-name: [(date,timestamp,state)]
  states = {}

  log = helpers.openFileForReading( fileName )

  nextLineIsState = False
  date = None
  client = None
  state = None
  startDate=None
  lbSettings = (0,0,0,0,0,0)
  for (no,line) in enumerate(log):
    m = lbaLine.match(line)
    if m != None:
      lbSettings = (m.group(1), m.group(2), m.group(3), m.group(4), m.group(5), m.group(6), )
    m = stateHeader.match(line)
    if m != None:
      if nextLineIsState:
        pass #print "WARNING: no state line found " + str(no) + ": " + line,
      nextLineIsState = True
      date = datetime.strptime(m.group(1),DATE_FORMAT)
      if startDate == None:
        startDate = date
    elif nextLineIsState:
      m = stateLine.match(line)
      if m != None:
        client = m.group(1)
        state = m.group(2)
        if not client in states:
          states[client] = []
        states[client].append((date,state,stateMap[state]))
        nextLineIsState = False
        date = None
        client = None
        state = None
      else: 
        pass #print "WARNING: state line expected " + str(no) + ": " + line,

  clients = states.keys()
  clients.sort(cmp=compareFun)

  baseName= fileName
  if baseName.endswith(".bz2"):
    baseName = baseName[0:-4]
  if baseName.endswith(".log"):
    baseName = baseName[0:-4]

  statesLogFileName = baseName + "-states.dat"

  statesLog = open(statesLogFileName, "w")

  for c in clients:
    data = states[c]
    for d in data:
      items = [
        d[0].strftime(DATE_FORMAT),
        str((d[0]-startDate).total_seconds()),
        d[1],
        str(d[2])
      ]

      line = ";".join(items) + "\n"
      statesLog.write(line)

    # finish gnuplot data block
    statesLog.write("\n")

  statesLog.close()


  plot = """
  set datafile separator ";"
  set terminal png crop
  set key off
  set xlabel "Time [s]"
  set ylabel "States [-]"
  set title "States of %s Clients (Latency: %s +/- %s ms, Up BW: %s +/- %s, Down BW: %s +-/%s)"
  set size 1.0,0.6
  set output "$BASE-states.png"
  plot "$BASE-states.dat" using 2:4:yticlabels(3) title "State" with lines
  set terminal svg
  set output "$BASE-states.svg"
  plot "$BASE-states.dat" using 2:4:yticlabels(3) title "State" with lines
  """ % ((len(clients),) + lbSettings)

  plot = string.Template(plot).substitute(BASE=baseName)

  statesPlotFileName = baseName+"-states.plot"
  statesPlot = open(statesPlotFileName, "w")
  statesPlot.write(plot)
  statesPlot.close()

  subprocess.call(["gnuplot", statesPlotFileName])

  statesSvgFileName = baseName+"-states.svg"
  statesPdfFileName = baseName+"-states.pdf"
  statesCroppedPdfFileName = baseName+"-states-crop.pdf"
  subprocess.call(["/usr/bin/inkscape", "--without-gui", "--export-pdf=" + statesPdfFileName, statesSvgFileName])
  subprocess.call(["/usr/bin/perl", "/usr/bin/pdfcrop", statesPdfFileName])
  subprocess.call(["/bin/mv", statesCroppedPdfFileName, statesPdfFileName])



