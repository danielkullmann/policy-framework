#!/bin/bash

for f in "$@"; do
  CMD=
  case "$f" in
    *.log.bz2) CMD=bzcat;;
    *) CMD=cat;;
  esac

  echo == $f
  $CMD $f | 
    tee >(grep "EVCharging1Planner runOnce"       | tail -n1 | cut -d" " -f 1-2 1>&2 ) |
          grep "EVCharging1Client activatePolicy" | tail -n1 | cut -d" " -f 1-2 

done

