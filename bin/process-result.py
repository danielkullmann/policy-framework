#!/usr/bin/env python

"""
Used to process the result.txt files produced by flexpower-simulation

TODOs:
- Use simulation datetime instead of real date time?
  Currently, current date time is used, and we have some larger steps
  because some simulation timesteps take a long time (several minutes)
  to complete, while the simulation timestep actually should be one second
- Treat non-aggregating columns, such as frequency, differently from 
  aggregating columns like power consumption
"""

import sys, os, bz2, math
from datetime import datetime as dtm
from datetime import timedelta as delta
import helpers


#### Constants

# Example datetime string: 12:38:02, 23/01/2012 
DATE_FORMAT = "%H:%M:%S, %d/%m/%Y"

# Example datetime string: Fri Mar 16 13:59:22 CET 2012
DATE_FORMAT2 = "%a %b %d %H:%M:%S %Z %Y"

#MET_FILE = "forecast.txt"


#### Global data

# List of header columns
headers = None

# Data is aggregated per data column and timestamp
# aggregatedData : HashMap<Date, HashMap<Column, Tuple(Count, Double)>>
aggregatedData = {}

#allDates = []

# Date of first entry in result.txt
firstDate = None

# Simulation datetime progresses one second at each time step
simDate = None

# This is the corresponding real datetime for a simDate; 
# if it changes, we know we have to increment simDate by one second
realDate = None

# Columns whose data is aggregated (i.e. summed up)
aggregatingColumns = [
  "power consumption", "model state", "error", "set point",
  "thermalresponse", "thermalcount", "freqresponse", "freqcount"
]


#### Helper functions

def parseDate(dateString):
  dt = dtm.strptime( dateString, DATE_FORMAT )
  return dt

def parseDate2(dateString):
  dt = dtm.strptime( dateString, DATE_FORMAT2 )
  return dt

def total_seconds(delta):
  return 86400*delta.days + delta.seconds + delta.microseconds/1000.0/1000.0

def registerData( id, date, column, value ):
  global aggregatedData, firstDate, simDate, realDate
  if firstDate == None:
    firstDate = date
    simDate = date
    realDate = date
  elif date != realDate:
    simDate += delta(0,1,0) # days, seconds, useconds
    realDate = date

  ts = total_seconds(simDate - firstDate)

  if column == "frequency" and value == 0.0:
    value = float('NaN')

  if date not in aggregatedData:
    aggregatedData[date] = { 'real date': date, 'date': simDate, 'ts': ts, 'count': 0 }
  cData = aggregatedData[date]
  if column not in cData:
    cData[column] = 0.0
  if not math.isnan(value):
    if column in aggregatingColumns:
      if math.isnan(cData[column]):
        cData[column] = value
      else:
        cData[column] += value
    elif cData[column] == 0.0:
      # Non-aggregating column
      cData[column] = value
  elif column not in aggregatingColumns:
      cData[column] = value

def sortByDateKey(d1, d2):
  return int((d1-d2).total_seconds())

def prepareRegisterMoreData():
  pass
  #global aggregatedData, allDates
  #allDates = [ d['real date'] for d in aggregatedData.values()]
  #allDates = sorted( allDates, cmp=sortByDateKey )

def registerMoreData( id, date, column, value):
  # I'll ignore every date entry that does not occur in allDates
  # This is because the simulation makes pauses, due to the models
  # taking too much time to simulate
  if date in aggregatedData:
    cData = aggregatedData[date]
    if column not in cData:
      cData[column] = 0.0
    if column+"-list" not in cData:
      cData[column+"-list"] = []
    # Don't count clients double
    if id in cData[column+"-list"]:
      print "double", id, date, column, value
      return
    cData[column+"-list"].append(id)
    if not math.isnan(value):
      if column in aggregatingColumns:
        if math.isnan(cData[column]):
          cData[column] = value
        else:
          cData[column] += value
      elif cData[column] == 0.0:
        # Non-aggregating column
        cData[column] = value
    elif column not in aggregatingColumns:
        cData[column] = value

  # Old code
  #idx = 0
  #while idx < len(allDates) and allDates[idx] <= lastDate:
  #  idx += 1
  #print lastDate, ":", date, ":", allDates[idx]
  ##while allDates[idx] <= date:
  ##  pass

def registerCount( date ):
    aggregatedData[date]['count'] += 1

def convert(data,columns,conversionMap):
  l = len(columns)
  result = []
  for idx in range(0,l):
    if idx >= len(data): break
    d = data[idx]
    c = columns[idx]
    if c in conversionMap.keys():
      m = conversionMap[c]
      if d in m.keys():
        result.append(m[d])
      else:
        raise Exception("unknown value: " + d + " in map: " + repr(conversionMap))
    else:
      result.append(d)
  return result


#### Read and aggregate data from result.txt(.bz2)?

# Open file, either bz2 or uncompressed
RESULT_FILE = "result.txt"
results = helpers.openFileForReading(RESULT_FILE)

ids = []

for l in results.readlines():
  if l.startswith("#"): continue
  l = l.rstrip()
  ls = l.split(";")
  if headers == None:
    headers = ls
    continue
  id = ls[0]
  if id not in ids: ids.append(id)
  dt = parseDate(ls[1])

  for (index,value) in enumerate(ls[2:]):
    colName = headers[2+index]
    registerData( id, dt, colName, float(value) )

  registerCount( dt )

  # Used for debugging:
  #if len(aggregatedData.values()) > 100: break

results.close()


#### Read and aggregate data from use.log(.bz2)?

controllerMap = {
  'controllers.thermal.spaceheating.ThermalGradientDescent' : 'thermalresponse', 
  'controllers.thermal.spaceheating.ThermalCETPaper' : 'thermalresponse', 
  'controllers.thermal.spaceheating.FrequencyController' : 'freqresponse',
}

conversionMap = {
  'controller' : controllerMap,
}

#uses = helpers.openFileForReading("use.log")


#### Read and aggregate data from state.log(.bz2)?

states = helpers.openFileForReading("state.log")
if states != None:
  first = True
  stateHeaders = ['date','timestamp','client','id','type','value','controller']
  prepareRegisterMoreData()
  for l in states.readlines():
    if l.startswith("#"): continue
    l = l.rstrip()
    ls = l.split(";")
    if first and ls[0] == 'date':
      continue
    first = False
    ls = convert(ls,stateHeaders,conversionMap)
    dt = parseDate2(ls[0])
    id = ls[2]
    value = float(ls[stateHeaders.index('value')])
    if ls[stateHeaders.index('type')] == 'ctl':
      ctrl = ls[stateHeaders.index('controller')]
      if ctrl == 'thermalresponse':
        registerMoreData( id, dt, 'thermalresponse', value )
        registerMoreData( id, dt, 'thermalcount', 1 )
      elif ctrl == 'freqresponse':
        registerMoreData( id, dt, 'freqresponse', value )
        registerMoreData( id, dt, 'freqcount', 1 )
      else:
        raise Exception("wrong data line: " + repr(data))


    # TODO continue 


#### Read and aggregate data from policy.log(.bz2)?

#policies = helpers.openFileForReading("policies.log")


#### Sort aggregated data by date and time

def getSortKey(item):
  return item['ts']

sortedItems = sorted(aggregatedData.values(), key=getSortKey)


#### Output aggregated data 

headers = headers[2:] + ['thermalresponse','thermalcount','freqresponse','freqcount']
OUT_FILE = "aggregated.txt"
fh = open(OUT_FILE, 'w')
fh.write( "real datetime;datetime;timestamp;count;" + ";".join(headers) + "\n")
for data in sortedItems:
  fh.write(str(data['real date']) + ";")
  fh.write(str(data['date']) + ";")
  fh.write(str(data['ts']) + ";")
  fh.write(str(data['count']) + ";")
  for h in headers:
    if h in data:
      fh.write( str(data[h]) + ";")
    else:
      fh.write( "nan;")

  fh.write("\n")
fh.close()




