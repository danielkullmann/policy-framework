#!/bin/sh

SYSLAB_BASE=../syslab/
FLEXHOUSE_BASE=../flexhouse/
CP="classes/"
CP="${CP}:lib/collections-generic-4.01.jar"
CP="${CP}:lib/commons-codec-1.4.jar"
CP="${CP}:lib/commons-lang-2.6.jar"
CP="${CP}:lib/commons-logging-1.1.1.jar"
CP="${CP}:lib/glpk-java-4.39.jar"
CP="${CP}:lib/httpclient-4.0.1.jar"
CP="${CP}:lib/httpcore-4.0.1.jar"
CP="${CP}:lib/httpmime-4.0.1.jar"
CP="${CP}:lib/drools/antlr-runtime-3.1.3.jar"
CP="${CP}:lib/drools/drools-api-5.1.1.jar"
CP="${CP}:lib/drools/drools-compiler-5.1.1.jar"
CP="${CP}:lib/drools/drools-core-5.1.1.jar"
CP="${CP}:lib/drools/ecj-3.5.1.jar"
CP="${CP}:lib/drools/mvel2-2.0.16.jar"
CP="${CP}:lib/drools/xstream-1.3.1.jar"
CP="${CP}:lib/log4j-1.2.15.jar"
CP="${CP}:$SYSLAB_BASE/bin/"
CP="${CP}:$SYSLAB_BASE/java/lib/batik-1.7.jar"
CP="${CP}:$SYSLAB_BASE/java/lib/commons-codec.jar"
CP="${CP}:$SYSLAB_BASE/java/lib/xml-apis-ext.jar"
CP="${CP}:$SYSLAB_BASE/java/lib/jdom.jar"
CP="${CP}:$SYSLAB_BASE/java/lib/jamod.jar"
CP="${CP}:$FLEXHOUSE_BASE/classes"
CP="${CP}:$FLEXHOUSE_BASE/lib/comm.jar"
CP="${CP}:$FLEXHOUSE_BASE/lib/postgresql-8.3-603.jdbc3.jar"

java -classpath "$CP" -Djava.library.path=lib:../flexhouse/lib/ risoe.syslab.platform.ApplicationServer
