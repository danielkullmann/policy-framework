#!/bin/bash

if test -z "$1"; then
  echo Usage: $(basename $0) evc1-xxxxxxxxxxx.csv
  exit 1
fi

./bin/runClass.sh risoe.syslab.demo.policy.evcharging1.CsvLogProcessor "$1"
cd $(dirname "$1")
gnuplot $(basename "${1%.csv}.plot")

