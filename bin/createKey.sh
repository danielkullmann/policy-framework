#!/bin/bash

if test -z "$1"; then
  echo "Usage: $0 name"
  exit 1
fi

NAME="$1"
STORE=conf/keystore.jks
PUB_STORE=conf/keystore-pub.jks

STORE_PW="ek4j.t5,rmn.20ef.rdjf"
PUB_STORE_PW="sa3l.7kf4dj.klj"

KEY_PW=pw-$(echo $NAME | sed -e "s/syslab-/sys-/")-2010
echo $NAME: $KEY_PW

# create the private/public key pair
keytool -genkeypair -storetype jks -keystore "$STORE" -keysize 1024 \
  -storepass "$STORE_PW" -keypass "$KEY_PW" \
  -alias "$NAME" -dname "cn=$NAME,ou=identity,o=syslab.risoe.dk"
  

# clone the key for <hostname>.risoe.dk
keytool -keyclone -storetype jks -keystore "$STORE" \
  -storepass "$STORE_PW" -keypass "$KEY_PW" -new "$KEY_PW" \
  -alias "$NAME" -dest "$NAME.risoe.dk"


# copy the public key for <hostname> to the public keystore
keytool -export -storetype jks -keystore "$STORE" \
  -storepass "$STORE_PW" -keypass "$KEY_PW" \
  -alias "$NAME" > "temp.cert"
  
keytool -import -storetype jks -keystore "$PUB_STORE" \
  -storepass "$PUB_STORE_PW" -alias "$NAME" \
  -noprompt -file "temp.cert"
  
rm -f temp.cert

# copy the public key for <hostname>.risoe.dk to the public keystore
keytool -export -storetype jks -keystore "$STORE" \
  -storepass "$STORE_PW" -keypass "$KEY_PW" \
  -alias "$NAME.risoe.dk" > "temp.cert"
  
keytool -import -storetype jks -keystore "$PUB_STORE" \
  -storepass "$PUB_STORE_PW" -alias "$NAME.risoe.dk" \
  -noprompt -file "temp.cert"


if test "$NAME" = "syslab-00"; then
  keytool -import -storetype jks -keystore "$PUB_STORE" \
    -storepass "$PUB_STORE_PW" -alias "10.0.17.186" \
    -noprompt -file "temp.cert"
fi

rm -f temp.cert

