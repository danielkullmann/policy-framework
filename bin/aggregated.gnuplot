set datafile separator ";"
set key left bottom
set xlabel "Time [hours]"

FILE="aggregated.txt"

set title "Model State"
set ylabel "Model State"
set terminal png
set output "model-state.png"
plot FILE using ($3/3600):($5/$4) title "Model State [°C]" with lines
set terminal svg
set output "model-state.svg"
plot FILE using ($3/3600):($5/$4) title "Model State [°C]" with lines

set title "Power Consumption"
set ylabel "Power [kW]"
set terminal png
set output "power-consumption.png"
plot FILE using ($3/3600):($6/1000) title "Power [kW]" with lines

set title "Energy Price"
set ylabel "Energy Price [-]"
set terminal png
set output "energy-price.png"
plot FILE using ($3/3600):7 title "Energy Price [-]" with lines

set title "Frequency"
set ylabel "Frequency [Hz]"
set terminal png
set output "frequency.png"
plot FILE using ($3/3600):8 title "Frequency [Hz]" with lines

set title "Setpoint"
set ylabel "Setpoint [°C]"
set terminal png
set output "set-point.png"
plot FILE using ($3/3600):9 title "Setpoint [°C]" with lines

set title "Error"
set ylabel "Error"
set terminal png
set output "error.png"
plot FILE using ($3/3600):10 title "Error" with lines

set title "Outside Temperature"
set ylabel "Outside Temperature [°C]"
set terminal png
set output "outside-temperature.png"
plot FILE using ($3/3600):11 title "Outside Temperature [°C]" with lines

set title "Solar Irradiation"
set ylabel "Solar Irradiation"
set terminal png
set output "solar-irradiation.png"
plot FILE using ($3/3600):12 title "Solar Irradiation" with lines

set title "States"
set ylabel "Power Consumption"
set terminal png
set output "states.png"
plot FILE using ($3/3600):13 title "thermal ctrl" with lines,\
     FILE using ($3/3600):15 title "frequency ctrl" with lines

set title "States (# of Units)"
set ylabel "Number of Units"
set terminal png
set output "states-count.png"
plot FILE using ($3/3600):14 title "thermal ctrl" with lines,\
     FILE using ($3/3600):16 title "frequency ctrl" with lines

set title "Frequency and Power Consumption"
set ylabel "Power [kW]"
set y2label "Frequency [Hz]"
set y2tics
set terminal png
set output "frequency+consumption.png"
plot FILE using ($3/3600):($6/1000) title "Power [kW]" with lines,\
     FILE using ($3/3600):($8) axis x1y2 title "Frequency [Hz]" with lines

set title "Energy Price and Power Consumption"
set ylabel "Power [kW]"
set y2label "Energy Price [-]"
set y2tics
set terminal png
set output "price+consumption.png"
plot FILE using ($3/3600):($6/1000) title "Power [kW]" with lines,\
     FILE using ($3/3600):($7) axis x1y2 title "Energy Price [-]" with lines

f1(x)=f1a*x+f1b 
fit f1(x) FILE using ($6/1000):8 via f1a,f1b

set title "Frequency vs. power consumption"
set xlabel "Power Consumption"
set ylabel "Frequency"
set noy2label
set noy2tics
set terminal png
set output "frequency-consumption.png"
plot FILE using ($6/1000):8 title "Frequency vs Consumption" with dots,\
     f1(x) title "Fitted linear curve"

f2(x)=f2a*x+f2b 
fit f2(x) FILE using ($6/1000):7 via f2a,f2b

set title "Energy Price vs. power consumption"
set xlabel "Power Consumption"
set ylabel "Energy Price"
set noy2label
set noy2tics
set terminal png
set output "price-consumption.png"
plot FILE using ($6/1000):7 title "Price vs Consumption" with dots,\
     f2(x) title "Fitted linear curve"



set terminal png
set output "frequency-over-consumption.png"
#set title "Frequency and Power Consumption"
unset title
set key off
set size 1.0, 0.7
set origin 0.0, 0.0
set lmargin 0
set multiplot layout 1,2 scale 1.0,0.5

set ylabel "Power [kW]"
set xlabel "Time [hours]"
set xtics
set ytics 250
set size 0.8,0.40
set origin 0.2,0.0
plot FILE using ($3/3600):($6/1000) title "Power [kW]" with lines

set ylabel "Frequency [Hz]"
unset xlabel
set format x ""
set ytics 0.1
set size 0.8,0.40
set origin 0.2,0.36
plot FILE using ($3/3600):($8) title "Frequency [Hz]" with lines

unset multiplot


set terminal png
set output "price-over-consumption.png"
#set title "Price and Power Consumption"
unset title
set key off
set lmargin 0
set multiplot layout 1,2 scale 1.0,0.5

set ylabel "Power [kW]"
set xlabel "Time [hours]"
set xtics
set ytics 250
set size 0.8,0.40
set origin 0.2,0.0
plot FILE using ($3/3600):($6/1000) title "Power [kW]" with lines

set ylabel "Price [EUR]"
unset xlabel
set format x ""
set ytics 10
set size 0.8,0.40
set origin 0.2,0.36
plot FILE using ($3/3600):($7) title "Price [EUR]" with lines

unset multiplot


set terminal png
set output "states-count-over.png"
#set title "States (# of Units)"
unset title
set key off
set lmargin 0
set offsets graph 0,0,0.1,0.1
set multiplot layout 1,2 scale 1.0,0.5

set ylabel "#Units (price)"
set xlabel "Time [hours]"
set xtics
set ytics 100
set size 0.8,0.40
set origin 0.2,0.0
plot FILE using ($3/3600):14 with lines

set ylabel "#Units (frequency)"
unset xlabel
set ytics 100
set format x ""
set size 0.8,0.40
set origin 0.2,0.36
plot FILE using ($3/3600):16 with lines

unset multiplot



set terminal svg
set output "frequency-over-consumption.svg"
#set title "Frequency and Power Consumption"
unset title
set key off
set lmargin 0
set multiplot layout 1,2 scale 1.0,0.5

set ylabel "Power [kW]"
set xlabel "Time [hours]"
set xtics
set ytics 1000
set size 0.8,0.40
set origin 0.2,0.0
plot FILE using ($3/3600):($6/1000) title "Power [kW]" with lines

set ylabel "Frequency [Hz]"
unset xlabel
set format x ""
set ytics 0.1
set size 0.8,0.40
set origin 0.2,0.36
plot FILE using ($3/3600):($8) title "Frequency [Hz]" with lines

unset multiplot


set terminal svg
set output "price-over-consumption.svg"
#set title "Price and Power Consumption"
unset title
set key off
set lmargin 0
set multiplot layout 1,2 scale 1.0,0.5

set ylabel "Power [kW]"
set xlabel "Time [hours]"
set xtics
set ytics 1000
set size 0.8,0.40
set origin 0.2,0.0
plot FILE using ($3/3600):($6/1000) title "Power [kW]" with lines

set ylabel "Price [EUR]"
unset xlabel
set format x ""
set ytics 10
set size 0.8,0.40
set origin 0.2,0.36
plot FILE using ($3/3600):($7) title "Price [EUR]" with lines

unset multiplot


set terminal svg
set output "states-count-over.svg"
#set title "States (# of Units)"
unset title
set key off
set ytics 100
set lmargin 0
set multiplot layout 1,2 scale 1.0,0.5

set ylabel "#Units (price)"
set xlabel "Time [hours]"
set xtics
set size 0.8,0.40
set origin 0.2,0.0
plot FILE using ($3/3600):14 with lines

set ylabel "#Units (frequency)"
unset xlabel
set format x ""
set size 0.8,0.40
set origin 0.2,0.36
plot FILE using ($3/3600):16 with lines

unset multiplot


