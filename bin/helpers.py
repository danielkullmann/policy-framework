
import os
import bz2, gzip

def openBZ2File(filename):
  return bz2.BZ2File(filename,'r')

def openGzipFile(filename):
  return gzip.GzipFile(filename,'r')

def openFile(filename):
  try: 
    return open(filename,'r')
  except IOError:
    return None

supportedExtensions = [
  ('.bz2', openBZ2File),
  ('.gz', openGzipFile),
  ('', openFile),
]

def openFileForReading(filename):
  """Opens a file for reading. 
  
  The file can be compressed using several
  compression libraries. Right now, gzip and bzip2 are supported (which have
  libraries in a standard python installation)
  """
  for (ext, fun) in supportedExtensions:
    if filename.endswith( ext ):
      return fun( filename )
    if os.path.exists( filename + ext ):
      return fun( filename + ext )

