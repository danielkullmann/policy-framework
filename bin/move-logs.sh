#!/bin/bash

DATE=$(date +"%Y-%m-%d")

mv -i thermostatic-controller.log thermostatic-controller-$DATE.log
mv -i source-data.log source-data-$DATE.log
bzip2 -9 thermostatic-controller-$DATE.log source-data-$DATE.log

