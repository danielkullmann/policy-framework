#!/bin/bash

./bin/runClass.sh risoe.syslab.demo.policy.evcharging1.app.EVC1PlannerSimulatorDelayApp "$@"
if test $? -eq 0; then
  CSV=$(grep "csv log is written into file" log.log | head -n1 | grep -Eo '"[^"]*"' | sed -e 's/"//g')
  if test -z "$CSV" || test ! -e "$CSV"; then
    CSV=$(ls -1 simulations/*.csv | tail -n1)
    echo "CSV file not found or wrong; using $CSV"
  fi
  LOG="${CSV%.csv}.log"
  mv -i log.log "$LOG"
  bzip2 -9 "$LOG"
  ./bin/runClass.sh risoe.syslab.demo.policy.evcharging1.CsvLogProcessor "$CSV"
fi

