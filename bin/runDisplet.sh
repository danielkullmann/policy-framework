#!/bin/sh

if test -z "$1"; then
  ./runClass.sh risoe.syslab.gui.wall.DispletTest
  echo "Known Displets:"
  ( cd src/; egrep -l -r "extends \w+Displet" . | sed -e "s!/!.!g" -e 's/\.java$//' -e "s/^\\.\\.//" -e "s/^/ /" | sort )
  echo ""
  exit 1
fi

exec ./runClass.sh risoe.syslab.gui.wall.DispletTest "$@"

