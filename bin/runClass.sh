#!/bin/sh
# 

set -e -u

CP_SEP=":"
PATH_SEP="/"
PATH_SEP_REGEX="/"
if uname -a | grep -q "Msys"; then
  CP_SEP=";"
  PATH_SEP="\\"
  PATH_SEP_REGEX="\\\\"
fi

SYSLAB_BASE=..${PATH_SEP}syslab
FLEXHOUSE_BASE=..${PATH_SEP}flexhouse
FLEXPOWER_BASE=..${PATH_SEP}flexpower-simulation
CP="classes"
for f in lib/*.jar lib/drools/*.jar; do CP="${CP}${CP_SEP}$( echo "$f" | sed -e "s#/#$PATH_SEP_REGEX#g")"; done
CP="${CP}${CP_SEP}$SYSLAB_BASE${PATH_SEP}bin"
CP="${CP}${CP_SEP}$SYSLAB_BASE${PATH_SEP}java${PATH_SEP}lib${PATH_SEP}batik-1.7.jar"
CP="${CP}${CP_SEP}$SYSLAB_BASE${PATH_SEP}java${PATH_SEP}lib${PATH_SEP}jdom.jar"
CP="${CP}${CP_SEP}$FLEXHOUSE_BASE${PATH_SEP}classes"
CP="${CP}${CP_SEP}$FLEXPOWER_BASE${PATH_SEP}bin"

JAVA_ARGS=""
while test -n "${1-}"; do
  case "$1" in
  -D*)
    JAVA_ARGS="${JAVA_ARGS-} $1"
    shift;;
  *) break;;
  esac
done

if test -z "${1-}" || test "${1-}" = "-l"; then
  ( cd src/; grep -l -r "public static void main" . | sed -e "s!/!.!g" -e 's/\.java$//' -e "s/^\\.\\.//" -e "s/^/ /" | sort )
  exit 1
fi

# When the class is given as a file name, that is converted into a class name by:
# * removing leading "java/src/", if present
# * removing trailing  ".java", if present
# * replacing any "/" chars with "."
CLASS=$(echo $1 | sed -e "s%^src/%%" -e "s%^bin/%%" -e "s%\\.java\$%%" -e "s%\\.class\$%%" -e "s%/%.%g")
shift

ARCH=x86
case "$(uname -a)" in
  *x86_64*) ARCH=x86_64;;
  *i686*) ARCH=x86_64;;
esac

export PATH="$PATH:./jni/$ARCH"
export LD_LIBRARY_PATH=".${PATH_SEP}jni${PATH_SEP}$ARCH"
java $JAVA_ARGS -classpath "$CP" -Djava.library.path=".${PATH_SEP}jni${PATH_SEP}$ARCH" "$CLASS" "$@"

